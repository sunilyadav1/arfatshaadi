-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2019 at 09:24 AM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matrimony_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminuser`
--

CREATE TABLE `adminuser` (
  `ADMIN_ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `ORIGINAL_PSW` varchar(50) NOT NULL,
  `CREATED_DATE` datetime NOT NULL,
  `UPDATED_DATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminuser`
--

INSERT INTO `adminuser` (`ADMIN_ID`, `NAME`, `EMAIL`, `PASSWORD`, `ORIGINAL_PSW`, `CREATED_DATE`, `UPDATED_DATE`) VALUES
(1, 'admin', 'admin@milanrishta.com', 'admin', 'admin', '2014-07-09 00:00:00', '2014-07-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `message`, `time`) VALUES
(1, 'hai', 1);

-- --------------------------------------------------------

--
-- Table structure for table `citylist`
--

CREATE TABLE `citylist` (
  `City_Id` int(11) NOT NULL,
  `Country_id` int(11) NOT NULL,
  `State_Id` int(11) NOT NULL,
  `City_Name` varchar(200) NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citylist`
--

INSERT INTO `citylist` (`City_Id`, `Country_id`, `State_Id`, `City_Name`, `Status`) VALUES
(1, 1, 28, 'Rangareddy', 1),
(2, 1, 28, 'East Godavari', 1),
(3, 1, 28, 'Guntur', 1),
(4, 1, 28, 'Krishna', 1),
(5, 1, 28, 'Visakhapatnam', 1),
(6, 1, 28, 'Chittoor', 1),
(7, 1, 28, 'Anantapur', 1),
(8, 1, 28, 'Kurnool', 1),
(9, 1, 28, 'Mahbubnagar', 1),
(10, 1, 28, 'Hyderabad', 1),
(11, 1, 28, 'West Godavari', 1),
(12, 1, 28, 'Karimnagar', 1),
(13, 1, 28, 'Warangal', 1),
(14, 1, 28, 'Nalgonda', 1),
(15, 1, 28, 'Prakasam', 1),
(16, 1, 28, 'Medak', 1),
(17, 1, 28, 'Sri Potti Sriramulu Nellore', 1),
(18, 1, 28, 'YSR', 1),
(19, 1, 28, 'Khammam', 1),
(20, 1, 28, 'Adilabad', 1),
(21, 1, 28, 'Srikakulam', 1),
(22, 1, 28, 'Nizamabad', 1),
(23, 1, 28, 'Vizianagaram', 1),
(24, 1, 29, 'Papumpare', 1),
(25, 1, 29, 'List', 1),
(26, 1, 29, 'Changlang', 1),
(27, 1, 29, 'Lohit', 1),
(28, 1, 29, 'West Siang', 1),
(29, 1, 29, 'Tirap', 1),
(30, 1, 29, 'East Siang', 1),
(31, 1, 29, 'Kurung Kumey', 1),
(32, 1, 29, 'West Kameng', 1),
(33, 1, 29, 'Upper Subansiri', 1),
(34, 1, 29, 'Lower Subansiri', 1),
(35, 1, 29, 'East Kameng', 1),
(36, 1, 29, 'Lower Dibang Valley', 1),
(37, 1, 29, 'Tawang', 1),
(38, 1, 29, 'Upper Siang', 1),
(39, 1, 29, 'Anjaw', 1),
(40, 1, 29, 'Dibang Valley', 1),
(41, 1, 30, 'Nagaon', 1),
(42, 1, 30, 'Dhubri', 1),
(43, 1, 30, 'Sonitpur', 1),
(44, 1, 30, 'Cachar', 1),
(45, 1, 30, 'Barpeta', 1),
(46, 1, 30, 'Kamrup', 1),
(47, 1, 30, 'Tinsukia', 1),
(48, 1, 30, 'Dibrugarh', 1),
(49, 1, 30, 'Kamrup Metropolitan', 1),
(50, 1, 30, 'Karimganj', 1),
(51, 1, 30, 'Sivasagar', 1),
(52, 1, 30, 'Jorhat', 1),
(53, 1, 30, 'Golaghat', 1),
(54, 1, 30, 'Lakhimpur', 1),
(55, 1, 30, 'Goalpara', 1),
(56, 1, 30, 'Morigaon', 1),
(57, 1, 30, 'Karbi Anglong', 1),
(58, 1, 30, 'Baksa', 1),
(59, 1, 30, 'Darrang', 1),
(60, 1, 30, 'Kokrajhar', 1),
(61, 1, 30, 'Udalguri', 1),
(62, 1, 30, 'Nalbari', 1),
(63, 1, 30, 'Bongaigaon', 1),
(64, 1, 30, 'Dhemaji', 1),
(65, 1, 30, 'Hailakandi', 1),
(66, 1, 30, 'Chirang', 1),
(67, 1, 30, 'Dima Hasao', 1),
(68, 1, 31, 'Patna', 1),
(69, 1, 31, 'Muzaffarpur', 1),
(70, 1, 31, 'Madhubani', 1),
(71, 1, 31, 'Gaya', 1),
(72, 1, 31, 'Samastipur', 1),
(73, 1, 31, 'Saran', 1),
(74, 1, 31, 'Darbhanga', 1),
(75, 1, 31, 'Pashchim Champaran', 1),
(76, 1, 31, 'Vaishali', 1),
(77, 1, 31, 'Sitamarhi', 1),
(78, 1, 31, 'Siwan', 1),
(79, 1, 31, 'Purnia', 1),
(81, 1, 31, 'Bhagalpur', 1),
(82, 1, 31, 'Begusarai', 1),
(83, 1, 31, 'Rohtas', 1),
(84, 1, 31, 'Nalanda', 1),
(85, 1, 31, 'Araria', 1),
(86, 1, 31, 'Bhojpur', 1),
(87, 1, 31, 'Gopalganj', 1),
(88, 1, 31, 'Aurangabad', 1),
(89, 1, 31, 'Supaul', 1),
(90, 1, 31, 'Nawada', 1),
(91, 1, 31, 'Banka', 1),
(92, 1, 31, 'Madhepura', 1),
(93, 1, 31, 'Saharsa', 1),
(94, 1, 31, 'Jamui', 1),
(95, 1, 31, 'Buxar', 1),
(96, 1, 31, 'Kishanganj', 1),
(97, 1, 31, 'Khagaria', 1),
(98, 1, 31, 'Kaimur', 1),
(99, 1, 31, 'Munger', 1),
(100, 1, 31, 'Jehanabad', 1),
(101, 1, 31, 'Lakhisarai', 1),
(102, 1, 31, 'Arwal', 1),
(103, 1, 31, 'Sheohar', 1),
(104, 1, 31, 'Sheikhpura', 1),
(105, 1, 32, 'Raipur', 1),
(106, 1, 32, 'Durg', 1),
(107, 1, 32, 'Bilaspur', 1),
(108, 1, 32, 'Surguja', 1),
(109, 1, 32, 'Janjgir Champa', 1),
(110, 1, 32, 'Rajnandgaon', 1),
(111, 1, 32, 'Raigarh', 1),
(112, 1, 32, 'Bastar', 1),
(113, 1, 32, 'Korba', 1),
(114, 1, 32, 'Mahasamund', 1),
(115, 1, 32, 'Jashpur', 1),
(116, 1, 32, 'Kabirdham', 1),
(117, 1, 32, 'Dhamtari', 1),
(118, 1, 32, 'Kanker', 1),
(119, 1, 32, 'Korea', 1),
(120, 1, 32, 'Dantewada', 1),
(121, 1, 32, 'Bijapur', 1),
(122, 1, 32, 'Narayanpur', 1),
(123, 1, 33, 'Margao', 1),
(124, 1, 33, 'Panaji', 1),
(125, 1, 33, 'Bardez', 1),
(126, 1, 33, 'Tiswadi', 1),
(127, 1, 33, 'Ponda', 1),
(128, 1, 33, 'Bicholim', 1),
(129, 1, 33, 'Pernem', 1),
(130, 1, 33, 'Satari', 1),
(131, 1, 33, 'Salcete', 1),
(132, 1, 33, 'Mormugao', 1),
(133, 1, 33, 'Quepem', 1),
(134, 1, 33, 'Sanguem', 1),
(135, 1, 33, 'Canacona', 1),
(136, 1, 34, 'Ahmadabad', 1),
(137, 1, 34, 'Surat', 1),
(138, 1, 34, 'Vadodara', 1),
(139, 1, 34, 'Rajkot', 1),
(140, 1, 34, 'Banaskantha', 1),
(141, 1, 34, 'Bhavnagar', 1),
(142, 1, 34, 'Junagadh', 1),
(143, 1, 34, 'Sabarkantha', 1),
(144, 1, 34, 'PanchMahal', 1),
(145, 1, 34, 'Kheda', 1),
(146, 1, 34, 'Jamnagar', 1),
(147, 1, 34, 'Dohad', 1),
(148, 1, 34, 'Anand', 1),
(149, 1, 34, 'Kachchh', 1),
(150, 1, 34, 'Mahesana', 1),
(151, 1, 34, 'Surendranagar', 1),
(152, 1, 34, 'Valsad', 1),
(153, 1, 34, 'Bharuch', 1),
(154, 1, 34, 'Amreli', 1),
(155, 1, 34, 'Gandhinagar', 1),
(156, 1, 34, 'Patan', 1),
(157, 1, 34, 'Navsari', 1),
(158, 1, 34, 'Tapi', 1),
(159, 1, 34, 'Narmada', 1),
(160, 1, 34, 'Porbandar', 1),
(161, 1, 34, 'The Dangs', 1),
(162, 1, 35, 'Faridabad', 1),
(163, 1, 35, 'Hisar', 1),
(164, 1, 35, 'Bhiwani', 1),
(165, 1, 35, 'Gurgaon', 1),
(166, 1, 35, 'Karnal', 1),
(167, 1, 35, 'Sonipat', 1),
(168, 1, 35, 'Jind', 1),
(169, 1, 35, 'Sirsa', 1),
(170, 1, 35, 'Yamunanagar', 1),
(171, 1, 35, 'Panipat', 1),
(172, 1, 35, 'Ambala', 1),
(173, 1, 35, 'Mewat', 1),
(174, 1, 35, 'Kaithal', 1),
(175, 1, 35, 'Rohtak', 1),
(176, 1, 35, 'Palwal', 1),
(177, 1, 35, 'Kurukshetra', 1),
(178, 1, 35, 'Jhajjar', 1),
(179, 1, 35, 'Fatehabad', 1),
(180, 1, 35, 'Mahendragarh', 1),
(181, 1, 35, 'Rewari', 1),
(182, 1, 35, 'Panchkula', 1),
(183, 1, 36, 'Kangra', 1),
(184, 1, 36, 'Mandi', 1),
(185, 1, 36, 'Shimla', 1),
(186, 1, 36, 'Solan', 1),
(187, 1, 36, 'Sirmaur', 1),
(188, 1, 36, 'Una', 1),
(189, 1, 36, 'Chamba', 1),
(190, 1, 36, 'Hamirpur', 1),
(191, 1, 36, 'Kullu', 1),
(192, 1, 36, 'Bilaspur', 1),
(193, 1, 36, 'Kinnaur', 1),
(194, 1, 36, 'Lahul and Spiti', 1),
(195, 1, 37, 'Jammu', 1),
(196, 1, 37, 'Srinagar', 1),
(197, 1, 37, 'Anantnag', 1),
(198, 1, 37, 'Baramula', 1),
(199, 1, 37, 'Kupwara', 1),
(200, 1, 37, 'Badgam', 1),
(201, 1, 37, 'Rajouri', 1),
(202, 1, 37, 'Kathua', 1),
(203, 1, 37, 'Pulwama', 1),
(204, 1, 37, 'Udhampur', 1),
(205, 1, 37, 'Punch', 1),
(206, 1, 37, 'Kulgam', 1),
(207, 1, 37, 'Doda', 1),
(208, 1, 37, 'Bandipora', 1),
(209, 1, 37, 'Samba', 1),
(210, 1, 37, 'Reasi', 1),
(211, 1, 37, 'Ganderbal', 1),
(212, 1, 37, 'Ramban', 1),
(213, 1, 37, 'Shupiyan', 1),
(214, 1, 37, 'Kishtwar', 1),
(215, 1, 37, 'Kargil', 1),
(216, 1, 37, 'Leh', 1),
(217, 1, 39, 'Bangalore', 1),
(218, 1, 39, 'Belgaum', 1),
(219, 1, 39, 'Mysore', 1),
(220, 1, 39, 'Tumkur', 1),
(221, 1, 39, 'Gulbarga', 1),
(222, 1, 39, 'Bellary', 1),
(223, 1, 39, 'Bijapur', 1),
(224, 1, 39, 'Dakshina Kannada', 1),
(225, 1, 39, 'Davanagere', 1),
(226, 1, 39, 'Raichur', 1),
(227, 1, 39, 'Bagalkot', 1),
(228, 1, 39, 'Dharwad', 1),
(229, 1, 39, 'Mandya', 1),
(230, 1, 39, 'Hassan', 1),
(231, 1, 39, 'Shimoga', 1),
(232, 1, 39, 'Bidar', 1),
(233, 1, 39, 'Chitradurga', 1),
(234, 1, 39, 'Haveri', 1),
(235, 1, 39, 'Kolar', 1),
(236, 1, 39, 'Uttara Kannada', 1),
(237, 1, 39, 'Koppal', 1),
(238, 1, 39, 'Chikkaballapura', 1),
(239, 1, 39, 'Udupi', 1),
(240, 1, 39, 'Yadgir', 1),
(241, 1, 39, 'Chikmagalur', 1),
(242, 1, 39, 'Ramanagara', 1),
(243, 1, 39, 'Gadag', 1),
(244, 1, 39, 'Chamarajanagar', 1),
(245, 1, 39, 'Bangalore Rural', 1),
(246, 1, 39, 'Kodagu', 1),
(247, 1, 40, 'Malappuram', 1),
(248, 1, 40, 'Thiruvananthapuram', 1),
(249, 1, 40, 'Ernakulam', 1),
(250, 1, 40, 'Thrissur', 1),
(251, 1, 40, 'Kozhikode', 1),
(252, 1, 40, 'Palakkad', 1),
(253, 1, 40, 'Kollam', 1),
(254, 1, 40, 'Kannur', 1),
(255, 1, 40, 'Alappuzha', 1),
(256, 1, 40, 'Kottayam', 1),
(257, 1, 40, 'Kasaragod', 1),
(258, 1, 40, 'Pathanamthitta', 1),
(259, 1, 40, 'Idukki', 1),
(260, 1, 40, 'Wayanad', 1),
(261, 1, 41, 'Indore', 1),
(262, 1, 41, 'Jabalpur', 1),
(263, 1, 41, 'Sagar', 1),
(264, 1, 41, 'Bhopal', 1),
(265, 1, 41, 'Rewa', 1),
(266, 1, 41, 'Satna', 1),
(267, 1, 41, 'Dhar', 1),
(268, 1, 41, 'Chhindwara', 1),
(269, 1, 41, 'Gwalior', 1),
(270, 1, 41, 'Ujjain', 1),
(271, 1, 41, 'Morena', 1),
(272, 1, 41, 'West Nimar', 1),
(273, 1, 41, 'Chhattarpur', 1),
(274, 1, 41, 'Shivpuri', 1),
(275, 1, 41, 'Bhind', 1),
(276, 1, 41, 'Balaghat', 1),
(277, 1, 41, 'Betul', 1),
(278, 1, 41, 'Dewas', 1),
(279, 1, 41, 'Rajgarh', 1),
(280, 1, 41, 'Shajapur', 1),
(281, 1, 41, 'Vidisha', 1),
(282, 1, 41, 'Ratlam', 1),
(283, 1, 41, 'Tikamgarh', 1),
(284, 1, 41, 'Barwani', 1),
(285, 1, 41, 'Seoni', 1),
(286, 1, 41, 'Mandsaur', 1),
(287, 1, 41, 'Raisen', 1),
(288, 1, 41, 'Sehore', 1),
(289, 1, 41, 'East Nimar', 1),
(290, 1, 41, 'Katni', 1),
(291, 1, 41, 'Damoh', 1),
(292, 1, 41, 'Guna', 1),
(293, 1, 41, 'Hoshangabad', 1),
(294, 1, 41, 'Singrauli', 1),
(295, 1, 41, 'Sidhi', 1),
(296, 1, 41, 'Narsimhapur', 1),
(297, 1, 41, 'Shahdol', 1),
(298, 1, 41, 'Mandla', 1),
(299, 1, 41, 'Jhabua', 1),
(300, 1, 41, 'Panna', 1),
(301, 1, 41, 'Ashoknagar', 1),
(302, 1, 41, 'Neemuch', 1),
(303, 1, 41, 'Datia', 1),
(304, 1, 41, 'Burhanpur', 1),
(305, 1, 41, 'Anuppur', 1),
(306, 1, 41, 'Alirajpur', 1),
(307, 1, 41, 'Dindori', 1),
(308, 1, 41, 'Sheopur', 1),
(309, 1, 41, 'Umaria', 1),
(310, 1, 41, 'Harda', 1),
(311, 1, 42, 'Thane', 1),
(312, 1, 42, 'Pune', 1),
(313, 1, 42, 'Mumbai Suburban', 1),
(314, 1, 42, 'Nashik', 1),
(315, 1, 42, 'Nagpur', 1),
(316, 1, 42, 'Ahmadnagar', 1),
(317, 1, 42, 'Solapur', 1),
(318, 1, 42, 'Jalgaon', 1),
(319, 1, 42, 'Kolhapur', 1),
(320, 1, 42, 'Aurangabad', 1),
(321, 1, 42, 'Nanded', 1),
(322, 1, 42, 'Mumbai City', 1),
(323, 1, 42, 'Satara', 1),
(324, 1, 42, 'Amravati', 1),
(325, 1, 42, 'Sangli', 1),
(326, 1, 42, 'Yavatmal', 1),
(327, 1, 42, 'Raigarh', 1),
(328, 1, 42, 'Buldana', 1),
(329, 1, 42, 'Bid', 1),
(330, 1, 42, 'Latur', 1),
(331, 1, 42, 'Chandrapur', 1),
(332, 1, 42, 'Dhule', 1),
(333, 1, 42, 'Jalna', 1),
(334, 1, 42, 'Parbhani', 1),
(335, 1, 42, 'Akola', 1),
(336, 1, 42, 'Osmanabad', 1),
(337, 1, 42, 'Nandurbar', 1),
(338, 1, 42, 'Ratnagiri', 1),
(339, 1, 42, 'Gondiya', 1),
(340, 1, 42, 'Wardha', 1),
(341, 1, 42, 'Bhandara', 1),
(342, 1, 42, 'Washim', 1),
(343, 1, 42, 'Hingoli', 1),
(344, 1, 42, 'Gadchiroli', 1),
(345, 1, 42, 'Sindhudurg', 1),
(346, 1, 43, 'Imphal West', 1),
(347, 1, 43, 'Imphal East', 1),
(348, 1, 43, 'Thoubal', 1),
(349, 1, 43, 'Churachandpur', 1),
(350, 1, 43, 'Bishnupur', 1),
(351, 1, 43, 'Senapati', 1),
(352, 1, 43, 'Ukhrul', 1),
(353, 1, 43, 'Chandel', 1),
(354, 1, 43, 'Tamenglong', 1),
(355, 1, 44, 'East Khasi Hills', 1),
(356, 1, 44, 'West Garo Hills', 1),
(357, 1, 44, 'Jaintia Hills', 1),
(358, 1, 44, 'West Khasi Hills', 1),
(359, 1, 44, 'East Garo Hills', 1),
(360, 1, 44, 'Ri Bhoi', 1),
(361, 1, 44, 'South Garo Hills', 1),
(362, 1, 45, 'Aizawl', 1),
(363, 1, 45, 'Lunglei', 1),
(364, 1, 45, 'Champhai', 1),
(365, 1, 45, 'Lawngtlai', 1),
(366, 1, 45, 'Mamit', 1),
(367, 1, 45, 'Kolasib', 1),
(368, 1, 45, 'Serchhip', 1),
(369, 1, 45, 'Saiha', 1),
(370, 1, 46, 'Dimapur', 1),
(371, 1, 46, 'Kohima', 1),
(372, 1, 46, 'Mon', 1),
(373, 1, 46, 'Tuensang', 1),
(374, 1, 46, 'Mokokchung', 1),
(375, 1, 46, 'Wokha', 1),
(376, 1, 46, 'Phek', 1),
(377, 1, 46, 'Zunheboto', 1),
(378, 1, 46, 'Peren', 1),
(379, 1, 46, 'Kiphire', 1),
(380, 1, 46, 'Longleng', 1),
(381, 1, 47, 'Ganjam', 1),
(382, 1, 47, 'Cuttack', 1),
(383, 1, 47, 'Mayurbhanj', 1),
(384, 1, 47, 'Baleshwar', 1),
(385, 1, 47, 'Khordha', 1),
(386, 1, 47, 'Sundargarh', 1),
(387, 1, 47, 'Jajapur', 1),
(388, 1, 47, 'Kendujhar', 1),
(389, 1, 47, 'Puri', 1),
(390, 1, 47, 'Balangir', 1),
(391, 1, 47, 'Kalahandi', 1),
(392, 1, 47, 'Bhadrak', 1),
(393, 1, 47, 'Bargarh', 1),
(394, 1, 47, 'Kendrapara', 1),
(395, 1, 47, 'Koraput', 1),
(396, 1, 47, 'Anugul', 1),
(397, 1, 47, 'Nabarangapur', 1),
(398, 1, 47, 'Dhenkanal', 1),
(399, 1, 47, 'Jagatsinghapur', 1),
(400, 1, 47, 'Sambalpur', 1),
(401, 1, 47, 'Rayagada', 1),
(402, 1, 47, 'Nayagarh', 1),
(403, 1, 47, 'Kandhamal', 1),
(404, 1, 47, 'Malkangiri', 1),
(405, 1, 47, 'Nuapada', 1),
(406, 1, 47, 'Subarnapur', 1),
(407, 1, 47, 'Jharsuguda', 1),
(408, 1, 47, 'Gajapati', 1),
(409, 1, 47, 'Baudh', 1),
(410, 1, 47, 'Debagarh', 1),
(411, 1, 47, 'Bhubaneshwar', 1),
(412, 1, 48, 'Ludhiana', 1),
(413, 1, 48, 'Amritsar', 1),
(414, 1, 48, 'Gurdaspur', 1),
(415, 1, 48, 'Jalandhar', 1),
(416, 1, 48, 'Firozpur', 1),
(417, 1, 48, 'Patiala', 1),
(418, 1, 48, 'Sangrur', 1),
(419, 1, 48, 'Hoshiarpur', 1),
(420, 1, 48, 'Bathinda', 1),
(421, 1, 48, 'Tarn Taran', 1),
(422, 1, 48, 'Moga', 1),
(423, 1, 48, 'Mohali', 1),
(424, 1, 48, 'Muktsar', 1),
(425, 1, 48, 'Kapurthala', 1),
(426, 1, 48, 'Mansa', 1),
(427, 1, 48, 'Rupnagar', 1),
(428, 1, 48, 'Faridkot', 1),
(429, 1, 48, 'Shahid Bhagat Singh Nagar', 1),
(430, 1, 48, 'Fatehgarh Sahib', 1),
(431, 1, 48, 'Barnala', 1),
(432, 1, 48, 'chandigad ', 1),
(433, 1, 49, 'Jaipur', 1),
(434, 1, 49, 'Jodhpur', 1),
(435, 1, 49, 'Alwar', 1),
(436, 1, 49, 'Nagaur', 1),
(437, 1, 49, 'Udaipur', 1),
(438, 1, 49, 'Sikar', 1),
(439, 1, 49, 'Barmer', 1),
(440, 1, 49, 'Ajmer', 1),
(441, 1, 49, 'Bharatpur', 1),
(442, 1, 49, 'Bhilwara', 1),
(443, 1, 49, 'Bikaner', 1),
(444, 1, 49, 'Jhunjhunun', 1),
(445, 1, 49, 'Churu', 1),
(446, 1, 49, 'Pali', 1),
(447, 1, 49, 'Ganganagar', 1),
(448, 1, 49, 'Kota', 1),
(449, 1, 49, 'Jalor', 1),
(450, 1, 49, 'Banswara', 1),
(451, 1, 49, 'Hanumangarh', 1),
(452, 1, 49, 'Dausa', 1),
(453, 1, 49, 'Chittaurgarh', 1),
(454, 1, 49, 'Karauli', 1),
(455, 1, 49, 'Tonk', 1),
(456, 1, 49, 'Jhalawar', 1),
(457, 1, 49, 'Dungarpur', 1),
(458, 1, 49, 'Sawai Madhopur', 1),
(459, 1, 49, 'Baran', 1),
(460, 1, 49, 'Dhaulpur', 1),
(461, 1, 49, 'Rajsamand', 1),
(462, 1, 49, 'Bundi', 1),
(463, 1, 49, 'Sirohi', 1),
(464, 1, 49, 'Pratapgarh', 1),
(465, 1, 49, 'Jaisalmer', 1),
(466, 1, 50, 'Gangtok', 1),
(467, 1, 50, 'East Sikkim', 1),
(468, 1, 50, 'Pakyong', 1),
(469, 1, 50, 'East Sikkim', 1),
(470, 1, 50, 'Rongli', 1),
(471, 1, 50, 'East Sikkim', 1),
(472, 1, 50, 'Namchi', 1),
(473, 1, 50, 'South Sikkim', 1),
(474, 1, 50, 'Ravong', 1),
(475, 1, 50, 'South Sikkim', 1),
(476, 1, 50, 'Gyalshing', 1),
(477, 1, 50, 'West Sikkim', 1),
(478, 1, 50, 'Soreng', 1),
(479, 1, 50, 'Mangan', 1),
(480, 1, 50, 'North Sikkim', 1),
(481, 1, 50, 'Chungthang', 1),
(482, 1, 50, 'North Sikkim', 1),
(483, 1, 51, 'Chennai', 1),
(484, 1, 51, 'Kancheepuram', 1),
(485, 1, 51, 'Vellore', 1),
(486, 1, 51, 'Thiruvallur', 1),
(487, 1, 51, 'Salem', 1),
(488, 1, 51, 'Viluppuram', 1),
(489, 1, 51, 'Coimbatore', 1),
(490, 1, 51, 'Tirunelveli', 1),
(491, 1, 51, 'Madurai', 1),
(492, 1, 51, 'Tiruchirappalli', 1),
(493, 1, 51, 'Cuddalore', 1),
(494, 1, 51, 'Tiruppur', 1),
(495, 1, 51, 'Tiruvannamalai', 1),
(496, 1, 51, 'Thanjavur', 1),
(497, 1, 51, 'Erode', 1),
(498, 1, 51, 'Dindigul', 1),
(499, 1, 51, 'Virudhunagar', 1),
(500, 1, 51, 'Krishnagiri', 1),
(501, 1, 51, 'Kanniyakumari', 1),
(502, 1, 51, 'Thoothukkudi', 1),
(503, 1, 51, 'Namakkal', 1),
(504, 1, 51, 'Pudukkottai', 1),
(505, 1, 51, 'Nagapattinam', 1),
(506, 1, 51, 'Dharmapuri', 1),
(507, 1, 51, 'Ramanathapuram', 1),
(508, 1, 51, 'Sivaganga', 1),
(509, 1, 51, 'Thiruvarur', 1),
(510, 1, 51, 'Theni', 1),
(511, 1, 51, 'Karur', 1),
(512, 1, 51, 'Ariyalur', 1),
(513, 1, 51, 'The Nilgiris', 1),
(514, 1, 51, 'Perambalur', 1),
(515, 1, 52, 'Bishalgarh', 1),
(516, 1, 52, 'Mohanpur', 1),
(517, 1, 52, 'Jirania', 1),
(518, 1, 52, 'Melaghar', 1),
(519, 1, 52, 'Dukli', 1),
(520, 1, 52, 'Teliamura', 1),
(521, 1, 52, 'Khowai', 1),
(522, 1, 52, 'Kathalia', 1),
(523, 1, 52, 'Boxanagar', 1),
(524, 1, 52, 'Kalyanpur', 1),
(525, 1, 52, 'Jampuijala', 1),
(526, 1, 52, 'Mandai', 1),
(527, 1, 52, 'Tulashikhar', 1),
(528, 1, 52, 'Hezamara', 1),
(529, 1, 52, 'Padmabil', 1),
(530, 1, 52, 'Mungiakumi', 1),
(531, 1, 52, 'Matarbari', 1),
(532, 1, 52, 'Bokafa', 1),
(533, 1, 52, 'Rajnagar', 1),
(534, 1, 52, 'Satchand', 1),
(535, 1, 52, 'Kakraban', 1),
(536, 1, 52, 'South Tripura', 1),
(537, 1, 52, 'Amarpur', 1),
(538, 1, 52, 'Hrishyamukh', 1),
(539, 1, 52, 'Rupaichhari', 1),
(540, 1, 52, 'Karbuk', 1),
(541, 1, 52, 'Killa', 1),
(542, 1, 52, 'Ompi', 1),
(543, 1, 52, 'Kadamtala', 1),
(544, 1, 52, 'Panisagar', 1),
(545, 1, 52, 'Gournagar', 1),
(546, 1, 52, 'Dasda', 1),
(547, 1, 52, 'Kumarghat', 1),
(548, 1, 52, 'North Tripura', 1),
(549, 1, 52, 'Pencharthal', 1),
(550, 1, 52, 'Damchhara', 1),
(551, 1, 52, 'Jampuii hills', 1),
(552, 1, 52, 'Salema', 1),
(553, 1, 52, 'Manu', 1),
(554, 1, 52, 'Dumburnagar', 1),
(555, 1, 52, 'Ambassa', 1),
(556, 1, 52, 'Chhamanu', 1),
(557, 1, 52, 'Dhalai', 1),
(558, 1, 53, 'Haridwar', 1),
(559, 1, 53, 'Dehradun', 1),
(560, 1, 53, 'Udham Singh Nagar', 1),
(561, 1, 53, 'Nainital', 1),
(562, 1, 53, 'Pauri Garhwal', 1),
(563, 1, 53, 'Almora', 1),
(564, 1, 53, 'Tehri Garhwal', 1),
(565, 1, 53, 'Pithoragarh', 1),
(566, 1, 53, 'Chamoli', 1),
(567, 1, 53, 'Uttarkashi', 1),
(568, 1, 53, 'Bageshwar', 1),
(569, 1, 53, 'Champawat', 1),
(570, 1, 53, 'Rudraprayag', 1),
(571, 1, 54, 'Allahabad', 1),
(572, 1, 54, 'Moradabad', 1),
(573, 1, 54, 'Ghaziabad', 1),
(574, 1, 54, 'Azamgarh', 1),
(575, 1, 54, 'Lucknow', 1),
(576, 1, 54, 'Kanpur Nagar', 1),
(577, 1, 54, 'Jaunpur', 1),
(578, 1, 54, 'Sitapur', 1),
(579, 1, 54, 'Bareilly', 1),
(580, 1, 54, 'Gorakhpur', 1),
(581, 1, 54, 'Agra', 1),
(582, 1, 54, 'Muzaffarnagar', 1),
(583, 1, 54, 'Hardoi', 1),
(584, 1, 54, 'Kheri', 1),
(585, 1, 54, 'Sultanpur', 1),
(586, 1, 54, 'Bijnor', 1),
(587, 1, 54, 'Budaun', 1),
(588, 1, 54, 'Varanasi', 1),
(589, 1, 54, 'Aligarh', 1),
(590, 1, 54, 'Ghazipur', 1),
(591, 1, 54, 'Kushinagar', 1),
(592, 1, 54, 'Bulandshahar', 1),
(593, 1, 54, 'Bahraich', 1),
(594, 1, 54, 'Saharanpur', 1),
(595, 1, 54, 'Meerut', 1),
(596, 1, 54, 'Gonda', 1),
(597, 1, 54, 'Rae Bareli', 1),
(598, 1, 54, 'Barabanki', 1),
(599, 1, 54, 'Ballia', 1),
(600, 1, 54, 'Pratapgarh', 1),
(601, 1, 54, 'Unnao', 1),
(602, 1, 54, 'Deoria', 1),
(603, 1, 54, 'Shahjahanpur', 1),
(604, 1, 54, 'Maharajganj', 1),
(605, 1, 54, 'Fatehpur', 1),
(606, 1, 54, 'Siddharth Nagar', 1),
(607, 1, 54, 'Mathura', 1),
(608, 1, 54, 'Firozabad', 1),
(609, 1, 54, 'Mirzapur', 1),
(610, 1, 54, 'Faizabad', 1),
(611, 1, 54, 'Basti', 1),
(612, 1, 54, 'Ambedkar Nagar', 1),
(613, 1, 54, 'Rampur', 1),
(614, 1, 54, 'Mau', 1),
(615, 1, 54, 'Balrampur', 1),
(616, 1, 54, 'Pilibhit', 1),
(617, 1, 54, 'Jhansi', 1),
(618, 1, 54, 'Chandauli', 1),
(619, 1, 54, 'Farrukhabad', 1),
(620, 1, 54, 'Mainpuri', 1),
(621, 1, 54, 'Sonbhadra', 1),
(622, 1, 54, 'Jyotiba Phule Nagar', 1),
(623, 1, 54, 'Banda', 1),
(624, 1, 54, 'Ramabai Nagar', 1),
(625, 1, 54, 'Etah', 1),
(626, 1, 54, 'Sant Kabir Nagar', 1),
(627, 1, 54, 'Jalaun', 1),
(628, 1, 54, 'Kannauj', 1),
(629, 1, 54, 'Gautam Buddha Nagar', 1),
(630, 1, 54, 'Kaushambi', 1),
(631, 1, 54, 'Etawah', 1),
(632, 1, 54, 'Sant Ravidas Nagar', 1),
(633, 1, 54, 'Mahamaya Nagar', 1),
(634, 1, 54, 'Kanshiram Nagar', 1),
(635, 1, 54, 'Auraiya', 1),
(636, 1, 54, 'Baghpat', 1),
(637, 1, 54, 'Lalitpur', 1),
(638, 1, 54, 'Shrawasti', 1),
(639, 1, 54, 'Hamirpur', 1),
(640, 1, 54, 'Chitrakoot', 1),
(641, 1, 54, 'Mahoba', 1),
(642, 1, 55, 'North Twenty Four Parganas', 1),
(643, 1, 55, 'South Twenty Four Parganas', 1),
(644, 1, 55, 'Barddhaman', 1),
(645, 1, 55, 'Murshidabad', 1),
(646, 1, 55, 'Paschim Medinipur', 1),
(647, 1, 55, 'Hugli', 1),
(648, 1, 55, 'Nadia', 1),
(649, 1, 55, 'Purba Medinipur', 1),
(650, 1, 55, 'Haora', 1),
(651, 1, 55, 'Kolkata', 1),
(652, 1, 55, 'Maldah', 1),
(653, 1, 55, 'Jalpaiguri', 1),
(654, 1, 55, 'Bankura', 1),
(655, 1, 55, 'Birbhum', 1),
(656, 1, 55, 'Uttar Dinajpur', 1),
(657, 1, 55, 'Puruliya', 1),
(658, 1, 55, 'Koch Bihar', 1),
(659, 1, 55, 'Darjiling', 1),
(660, 1, 55, 'Dakshin Dinajpur', 1),
(661, 1, 28, 'Kadapa', 1),
(662, 8, 56, 'Bagh', 1),
(663, 8, 56, 'Bhimber', 1),
(664, 8, 56, 'Greenpatch', 1),
(665, 8, 56, 'Koli', 1),
(666, 8, 56, 'Mirpure', 1),
(667, 8, 56, 'Muzuaffarabad', 1),
(668, 8, 56, 'Plandri', 1),
(669, 8, 56, 'Rawalakot', 1),
(670, 8, 57, 'Chaman', 1),
(671, 8, 57, 'Dera Murad Jamali', 1),
(672, 8, 57, 'Dera Allah Yar', 1),
(673, 8, 57, 'Gwadar', 1),
(674, 8, 57, 'Hub', 1),
(675, 8, 57, 'Khuzdar', 1),
(676, 8, 57, 'Kharan', 1),
(677, 8, 57, 'Kalat', 1),
(678, 8, 57, 'Loralai', 1),
(679, 8, 57, 'Mastung', 1),
(680, 8, 57, 'Nushki', 1),
(681, 8, 57, 'Pasni', 1),
(682, 8, 57, 'Quetta', 1),
(683, 8, 57, 'Sibi', 1),
(684, 8, 57, 'Turbat', 1),
(685, 8, 57, 'Usta Mohammad', 1),
(686, 8, 57, 'Zhob', 1),
(687, 8, 65, 'Islamabad', 1),
(688, 8, 63, 'Parachinar', 1),
(689, 8, 63, 'Bajour', 1),
(690, 1, 69, 'New delhi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Clientdetails`
--

CREATE TABLE `Clientdetails` (
  `Client_Id` bigint(33) NOT NULL,
  `Client_Name` varchar(300) NOT NULL,
  `Client_Address` text NOT NULL,
  `Client_Phone` varchar(11) NOT NULL,
  `Created_date` date NOT NULL,
  `Created_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Clientdetails`
--

INSERT INTO `Clientdetails` (`Client_Id`, `Client_Name`, `Client_Address`, `Client_Phone`, `Created_date`, `Created_time`) VALUES
(1, 'Tabreez', 'Bangalore', '253424677', '2015-06-11', '00:00:00'),
(2, 'jayanand', 'Mysore', '253424677', '2015-06-11', '00:00:00'),
(3, 'anand', 'Bangalore', '46565757', '2015-06-01', '00:00:00'),
(4, 'vijetha', 'bangalore', '899979768', '2015-06-09', '00:00:00'),
(5, 'test', 'test', '7687689789', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `communitylist`
--

CREATE TABLE `communitylist` (
  `Community_Id` int(11) NOT NULL,
  `Religion_Id` int(11) NOT NULL,
  `Community_Name` varchar(200) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communitylist`
--

INSERT INTO `communitylist` (`Community_Id`, `Religion_Id`, `Community_Name`, `Status`) VALUES
(1, 2, 'Bengali', 1),
(2, 2, 'Dawoodi Bohra', 1),
(3, 2, 'Khoja', 1),
(4, 2, 'Mapila', 1),
(5, 2, 'Memon', 1),
(6, 2, 'Rajput', 1),
(7, 2, 'Rowther', 1),
(8, 2, 'Shafi', 1),
(9, 2, 'Shia - All', 1),
(10, 2, 'Shia', 1),
(11, 2, 'Shia Bohra', 1),
(12, 2, 'Shia Imami Ismaili', 1),
(13, 2, 'Shia Ithna ashariyyah', 1),
(14, 2, 'Shia Zaidi', 1),
(15, 2, 'Sunni - All', 1),
(16, 2, 'Sunni', 1),
(17, 2, 'Sunni Ehle-Hadith', 1),
(18, 2, 'Sunni Hanafi', 1),
(19, 2, 'Sunni Hunbali', 1),
(20, 2, 'Sunni Maliki', 1),
(21, 2, 'Sunni Shafi', 1),
(22, 2, 'Reverted Muslim', 1),
(23, 3, 'Anglo Indian', 1),
(24, 3, 'Basel Mission', 1),
(25, 3, 'Born Again', 1),
(26, 3, 'Bretheren', 1),
(27, 3, 'Cannonite', 1),
(28, 3, 'Catholic', 1),
(29, 3, 'Catholic Knanya', 1),
(30, 3, 'Catholic Malankara', 1),
(31, 3, 'Chaldean Syrian', 1),
(32, 3, 'Cheramar', 1),
(33, 3, 'Christian Nadar', 1),
(34, 3, 'Church of North India (CNI)', 1),
(35, 3, 'Church of South India (CSI)', 1),
(36, 3, 'CMS', 1),
(37, 3, 'Convert', 1),
(38, 3, 'Evangelical', 1),
(39, 3, 'Indian Orthodox', 1),
(40, 3, 'Intercaste', 1),
(41, 3, 'IPC', 1),
(42, 3, 'Jacobite', 1),
(43, 3, 'Jacobite Knanya', 1),
(44, 3, 'Knanaya', 1),
(45, 3, 'Knanaya Catholic', 1),
(46, 3, 'Knanaya Jacobite', 1),
(47, 3, 'Knanaya Pentecostal', 1),
(48, 3, 'Knanya', 1),
(49, 3, 'Latin Catholic', 1),
(50, 3, 'Malankara', 1),
(51, 3, 'Malankara Catholic', 1),
(52, 3, 'Manglorean', 1),
(53, 3, 'Marthoma', 1),
(54, 3, 'Methodist', 1),
(55, 3, 'Mormon', 1),
(56, 3, 'Nadar', 1),
(57, 3, 'Orthodox', 1),
(58, 3, 'Pentecost', 1),
(59, 3, 'Presbyterian', 1),
(60, 3, 'Protestant', 1),
(61, 3, 'RCSC', 1),
(62, 3, 'Roman Catholic', 1),
(63, 3, 'Salvation Army', 1),
(64, 3, 'Scheduled Caste', 1),
(65, 3, 'Scheduled Tribe', 1),
(66, 3, 'Seventh day Adventist', 1),
(67, 3, 'Syrian', 1),
(68, 3, 'Syrian Catholic', 1),
(69, 3, 'Syrian Orthodox', 1),
(70, 3, 'Syro Malabar', 1),
(71, 3, 'Converted Christian', 1),
(72, 7, 'Arora', 1),
(73, 7, 'Clean Shaven', 1),
(74, 7, 'Gursikh', 1),
(75, 7, 'Jat', 1),
(76, 7, 'Kamboj', 1),
(77, 7, 'Kesadhari', 1),
(78, 7, 'Khatri', 1),
(79, 7, 'Kshatriya', 1),
(80, 7, 'Labana', 1),
(81, 7, 'Mazhbi/Majabi', 1),
(82, 7, 'Rajput', 1),
(83, 7, 'Ramdasia', 1),
(84, 7, 'Ramgharia', 1),
(85, 7, 'Ravidasia', 1),
(86, 7, 'Saini', 1),
(87, 8, 'Parsi', 1),
(88, 8, 'Zoroastrians.', 1),
(89, 9, 'Agrawal/ Agarwal', 1),
(90, 9, 'Arasu', 1),
(91, 9, 'Asathi Vaishya', 1),
(92, 9, 'Ayodhyavasi', 1),
(93, 9, 'Bagherwal', 1),
(94, 9, 'Bakarwal', 1),
(95, 9, 'Bannore', 1),
(96, 9, 'Baraiya/Varaiya', 1),
(97, 9, 'Bhabra', 1),
(98, 9, 'Bhavsar', 1),
(99, 9, 'Bhojak', 1),
(100, 9, 'Bogar', 1),
(101, 9, 'Chaturth', 1),
(102, 9, 'Chippiga', 1),
(103, 9, 'Chitoda', 1),
(104, 9, 'Dhakad', 1),
(105, 9, 'Dharmpal', 1),
(106, 9, 'Gangerwal', 1),
(107, 9, 'Golalare', 1),
(108, 9, 'Golapurv', 1),
(109, 9, 'Golsinghare', 1),
(110, 9, 'Indra', 1),
(111, 9, 'Jain Brahman', 1),
(112, 9, 'Jain Bunt', 1),
(113, 9, 'Jain Gouda', 1),
(114, 9, 'Jain Kalar', 1),
(115, 9, 'Jain Koshti', 1),
(116, 9, 'Jaiswal', 1),
(117, 9, 'Jangada Porwal', 1),
(118, 9, 'Jat', 1),
(119, 9, 'Harda', 1),
(120, 9, 'Humad/Humbad', 1),
(121, 9, 'Kuchchhi Oswal', 1),
(122, 9, 'Kamboj', 1),
(123, 9, 'Kandoi', 1),
(124, 9, 'Kasar', 1),
(125, 9, 'Khandelwal', 1),
(126, 9, 'Kshatriya Ghanchi', 1),
(127, 9, 'Kshatriya Parmar', 1),
(128, 9, 'Laad', 1),
(129, 9, 'Lamechuval', 1),
(130, 9, 'Mevada', 1),
(131, 9, 'Nainar', 1),
(132, 9, 'Nagda', 1),
(133, 9, 'Narsinhpura', 1),
(134, 9, 'Nema', 1),
(135, 9, 'Nevi', 1),
(136, 9, 'Oswal', 1),
(137, 9, 'Padmavati Purwal', 1),
(138, 9, 'Palliwal', 1),
(139, 9, 'Pancham', 1),
(140, 9, 'Parwar', 1),
(141, 9, 'Patidar', 1),
(142, 9, 'Porwal', 1),
(143, 9, 'Saitwal', 1),
(144, 9, 'Sadaru', 1),
(145, 9, 'Sarak', 1),
(146, 9, 'Sevak', 1),
(147, 9, 'Shrimali', 1),
(148, 9, 'Samaiya', 1),
(149, 9, 'Upadhye', 1),
(150, 9, 'Veerval', 1),
(151, 9, 'Punjabi arora', 1),
(152, 9, 'khatri', 1),
(153, 9, 'Jain Gujjars', 1),
(154, 9, 'Pancham', 1),
(155, 9, 'kuttchi dasa oswal', 1),
(156, 9, 'jain Namadar', 1),
(157, 9, '?kutchi dasa oswal', 1),
(158, 9, 'bania\'a jain', 1),
(159, 9, 'Digambara', 1),
(160, 9, 'Shwetember ', 1),
(161, 9, 'Vania', 1),
(162, 11, 'Budhist ', 1),
(163, 10, 'Bene Israels', 1),
(164, 10, 'Cochini Jews', 1),
(165, 10, 'Baghdadi Jews?', 1),
(166, 1, '96K Kokanastha', 1),
(167, 1, 'Adi Andhra', 1),
(168, 1, 'Adi Dravida', 1),
(169, 1, 'Agarwal', 1),
(170, 1, 'Kshatriya - All', 1),
(171, 1, 'Agnikula Kshatriya', 1),
(172, 1, 'Agri', 1),
(173, 1, 'Ahom', 1),
(174, 1, 'Ambalavasi', 1),
(175, 1, 'Arekatica', 1),
(176, 1, 'Arora', 1),
(177, 1, 'Arunthathiyar', 1),
(178, 1, 'Aryasamaj', 1),
(179, 1, 'Arya Vysya', 1),
(180, 1, 'Ayyaraka', 1),
(181, 1, 'Baghel/Pal/Gaderiya', 1),
(182, 1, 'Bahi', 1),
(183, 1, 'Baidya', 1),
(184, 1, 'Baishnab', 1),
(185, 1, 'Baishya', 1),
(186, 1, 'Balija', 1),
(187, 1, 'Naidu - All', 1),
(188, 1, 'Balija - Naid', 1),
(189, 1, 'Banik', 1),
(190, 1, 'Baniya', 1),
(191, 1, 'Bari', 1),
(192, 1, 'Barujibi', 1),
(193, 1, 'Bengali', 1),
(194, 1, 'Besta', 1),
(195, 1, 'Bhandari', 1),
(196, 1, 'Bhatia', 1),
(197, 1, 'Bhatraju', 1),
(198, 1, 'Bhavsar', 1),
(199, 1, 'Bhovi', 1),
(200, 1, 'Billava', 1),
(201, 1, 'Boya/Nayak/Naik', 1),
(202, 1, 'Boyer', 1),
(203, 1, 'Brahmbatt', 1),
(204, 1, 'Brahmin - All', 1),
(205, 1, 'Brahmin', 1),
(206, 1, 'Brahmin - Anavil', 1),
(207, 1, 'Brahmin - Audichya', 1),
(208, 1, 'Brahmin - Bengali', 1),
(209, 1, 'Brahmin - Bhatt', 1),
(210, 1, 'Brahmin - Bhumihar', 1),
(211, 1, 'Brahmin - Brahmbhatt', 1),
(212, 1, 'Brahmin - Danua', 1),
(213, 1, 'Brahmin - Davadnya', 1),
(214, 1, 'Brahmin - Deshastha', 1),
(215, 1, 'Brahmin - Dhiman', 1),
(216, 1, 'Brahmin - Dravida', 1),
(217, 1, 'Brahmin - Garhwali', 1),
(218, 1, 'Brahmin - Goswami', 1),
(219, 1, 'Brahmin - Gour', 1),
(220, 1, 'Brahmin - Gowd Saraswat', 1),
(221, 1, 'Brahmin - Gurukkal', 1),
(222, 1, 'Brahmin - Halua', 1),
(223, 1, 'Brahmin - Havyaka', 1),
(224, 1, 'Brahmin - Himachali', 1),
(225, 1, 'Brahmin - Hoysala', 1),
(226, 1, 'Brahmin - Iyengar', 1),
(227, 1, 'Brahmin - Iyer', 1),
(228, 1, 'Brahmin - Jhadua', 1),
(229, 1, 'Brahmin - Jhijhotiya', 1),
(230, 1, 'Brahmin - Kannada Madhva', 1),
(231, 1, 'Brahmin - Kanyakubja', 1),
(232, 1, 'Brahmin - Karhade', 1),
(233, 1, 'Brahmin - Kashmiri Pandit', 1),
(234, 1, 'Brahmin - Kokanastha', 1),
(235, 1, 'Brahmin - Kota', 1),
(236, 1, 'Brahmin - Kulin', 1),
(237, 1, 'Brahmin - Kumaoni', 1),
(238, 1, 'Brahmin - Madhwa', 1),
(239, 1, 'Brahmin - Maharashtrian', 1),
(240, 1, 'Brahmin - Maithili', 1),
(241, 1, 'Brahmin - Modh', 1),
(242, 1, 'Brahmin - Mohyal', 1),
(243, 1, 'Brahmin - Nagar', 1),
(244, 1, 'Brahmin - Namboodiri', 1),
(245, 1, 'Brahmin - Niyogi', 1),
(246, 1, 'Brahmin - Niyogi Nandavariki', 1),
(247, 1, 'Brahmin - Other', 1),
(248, 1, 'Brahmin - Paliwal', 1),
(249, 1, 'Brahmin - Panda', 1),
(250, 1, 'Brahmin - Pareek', 1),
(251, 1, 'Brahmin - Punjabi', 1),
(252, 1, 'Brahmin - Pushkarna', 1),
(253, 1, 'Brahmin - Rarhi', 1),
(254, 1, 'Brahmin - Rudraj', 1),
(255, 1, 'Brahmin - Sakaldwipi', 1),
(256, 1, 'Brahmin - Sanadya', 1),
(257, 1, 'Brahmin - Sanketi', 1),
(258, 1, 'Brahmin - Saraswat', 1),
(259, 1, 'Brahmin - Saryuparin', 1),
(260, 1, 'Brahmin - Shivhalli', 1),
(261, 1, 'Brahmin - Shrimali', 1),
(262, 1, 'Brahmin - Smartha', 1),
(263, 1, 'Brahmin - Sri Vaishnava', 1),
(264, 1, 'Brahmin - Stanika', 1),
(265, 1, 'Brahmin - Tamil', 1),
(266, 1, 'Brahmin - Telugu', 1),
(267, 1, 'Brahmin - Tyagi', 1),
(268, 1, 'Brahmin - Vaidiki', 1),
(269, 1, 'Brahmin - Vaikhanasa', 1),
(270, 1, 'Brahmin - Velanadu', 1),
(271, 1, 'Brahmin - Viswabrahmin', 1),
(272, 1, 'Brahmin - Vyas', 1),
(273, 1, 'Brahmo', 1),
(274, 1, 'Buddar', 1),
(275, 1, 'Chambhar', 1),
(276, 1, 'Chandravanshi Kahar', 1),
(277, 1, 'Chasa', 1),
(278, 1, 'Chattada Sri Vaishnava', 1),
(279, 1, 'Chaudary', 1),
(280, 1, 'Chaurasia', 1),
(281, 1, 'Chekkala - Nair', 1),
(282, 1, 'Cheramar', 1),
(283, 1, 'Chettiar', 1),
(284, 1, 'Chhetri', 1),
(285, 1, 'Chippolu/Mera', 1),
(286, 1, 'CKP', 1),
(287, 1, 'Coorgi', 1),
(288, 1, 'Devadiga', 1),
(289, 1, 'Devanga', 1),
(290, 1, 'Devendra Kula Vellalar', 1),
(291, 1, 'Dhangar', 1),
(292, 1, 'Dheevara', 1),
(293, 1, 'Dhiman', 1),
(294, 1, 'Dhoba', 1),
(295, 1, 'Dhobi', 1),
(296, 1, 'Digambar', 1),
(297, 1, 'Dommala', 1),
(298, 1, 'Ediga', 1),
(299, 1, 'Ezhava', 1),
(300, 1, 'Ezhavathi', 1),
(301, 1, 'Ezhuthachan', 1),
(302, 1, 'Gabit', 1),
(303, 1, 'Ganakar', 1),
(304, 1, 'Gandla', 1),
(305, 1, 'Ganiga', 1),
(306, 1, 'Garhwali', 1),
(307, 1, 'Gavara', 1),
(308, 1, 'Ghumar', 1),
(309, 1, 'Goala', 1),
(310, 1, 'Goan', 1),
(311, 1, 'Goswami', 1),
(312, 1, 'Goud', 1),
(313, 1, 'Gounder', 1),
(314, 1, 'Gowda', 1),
(315, 1, 'Gramani', 1),
(316, 1, 'Gudia', 1),
(317, 1, 'Gujarati', 1),
(318, 1, 'Gujjar', 1),
(319, 1, 'Gupta', 1),
(320, 1, 'Guptan', 1),
(321, 1, 'Gurjar', 1),
(322, 1, 'Hegde', 1),
(323, 1, 'Hugar (Jeer)', 1),
(324, 1, 'Jangam', 1),
(325, 1, 'Kalinga', 1),
(326, 1, 'Kalinga Vysya', 1),
(327, 1, 'Kannada Mogaveera', 1),
(328, 1, 'Karuneegar', 1),
(329, 1, 'Kongu Vellala?Gounder', 1),
(330, 1, 'Konkani', 1),
(331, 1, 'Kori', 1),
(332, 1, 'Koshti', 1),
(333, 1, 'Kshatriya', 1),
(334, 1, 'Kshatriya - Bhavasar', 1),
(335, 1, 'Kshatriya/Raju/Varma', 1),
(336, 1, 'Kudumbi', 1),
(337, 1, 'Kulal', 1),
(338, 1, 'Kulalar', 1),
(339, 1, 'Kulita', 1),
(340, 1, 'Kumawat', 1),
(341, 1, 'Kumbara', 1),
(342, 1, 'Kumbhakar/Kumbhar', 1),
(343, 1, 'Kumhar', 1),
(344, 1, 'Kummari', 1),
(345, 1, 'Kunbi', 1),
(346, 1, 'Kurava', 1),
(347, 1, 'Kuravan', 1),
(348, 1, 'Kurmi', 1),
(349, 1, 'Kuruba', 1),
(350, 1, 'Kuruhina Shetty', 1),
(351, 1, 'Kurumbar', 1),
(352, 1, 'Kurup', 1),
(353, 1, 'Kushwaha', 1),
(354, 1, 'Kutchi', 1),
(355, 1, 'Lambadi/Banjara', 1),
(356, 1, 'Lambani', 1),
(357, 1, 'Leva Patil', 1),
(358, 1, 'Lingayath', 1),
(359, 1, 'Lohana', 1),
(360, 1, 'Lohar', 1),
(361, 1, 'Lubana', 1),
(362, 1, 'Mala', 1),
(363, 1, 'Malayalee Variar', 1),
(364, 1, 'Mali', 1),
(365, 1, 'Mangalorean', 1),
(366, 1, 'Mapila', 1),
(367, 1, 'Maratha - All', 1),
(368, 1, 'Mudaliar - All', 1),
(369, 1, 'Mudaliar', 1),
(370, 1, 'Mudaliar - Arcot', 1),
(371, 1, 'Mudaliar - Saiva', 1),
(372, 1, 'Mudaliar - Senguntha', 1),
(373, 1, 'Mudiraj', 1),
(374, 1, 'Mukkulathor', 1),
(375, 1, 'Mukulathur', 1),
(376, 1, 'Munnuru Kapu', 1),
(377, 1, 'Muthuraja', 1),
(378, 1, 'Naagavamsam', 1),
(379, 1, 'Nadar', 1),
(380, 1, 'Nagaralu', 1),
(381, 1, 'Nai', 1),
(382, 1, 'Naicken', 1),
(383, 1, 'Naicker', 1),
(384, 1, 'Naidu', 1),
(385, 1, 'Naik', 1),
(386, 1, 'Nair - All', 1),
(387, 1, 'Nair', 1),
(388, 1, 'Nair - Vaniya', 1),
(389, 1, 'Nair - Velethadathu', 1),
(390, 1, 'Nair - Vilakkithala', 1),
(391, 1, 'Namasudra', 1),
(392, 1, 'Nambiar', 1),
(393, 1, 'Nambisan', 1),
(394, 1, 'Namosudra', 1),
(395, 1, 'Napit', 1),
(396, 1, 'Nayak', 1),
(397, 1, 'Neeli', 1),
(398, 1, 'Nepali', 1),
(399, 1, 'Nhavi', 1),
(400, 1, 'OBC - Barber/Naayee', 1),
(401, 1, 'Oswal', 1),
(402, 1, 'Otari', 1),
(403, 1, 'Other', 1),
(404, 1, 'Padmasali', 1),
(405, 1, 'Panchal', 1),
(406, 1, 'Panicker', 1),
(407, 1, 'Paravan', 1),
(408, 1, 'Parit', 1),
(409, 1, 'Parkava Kulam', 1),
(410, 1, 'Partraj', 1),
(411, 1, 'Patel - All', 1),
(412, 1, 'Patel', 1),
(413, 1, 'Patel - Desai', 1),
(414, 1, 'Patel - Dodia', 1),
(415, 1, 'Patel - Kadva', 1),
(416, 1, 'Patel - Leva', 1),
(417, 1, 'Patnaick', 1),
(418, 1, 'Patra', 1),
(419, 1, 'Perika', 1),
(420, 1, 'Pillai', 1),
(421, 1, 'Pisharody', 1),
(422, 1, 'Poduval', 1),
(423, 1, 'Poosala', 1),
(424, 1, 'Prajapati', 1),
(425, 1, 'Pulaya', 1),
(426, 1, 'Punjabi', 1),
(427, 1, 'Rajaka', 1),
(428, 1, 'Rajaka/Chakali/Dhobi', 1),
(429, 1, 'Rajbhar', 1),
(430, 1, 'Rajput - All', 1),
(431, 1, 'Rajput', 1),
(432, 1, 'Rajput - Garhwali', 1),
(433, 1, 'Rajput - Kumaoni', 1),
(434, 1, 'Rajput - Lodhi', 1),
(435, 1, 'Ramdasia', 1),
(436, 1, 'Ramgharia', 1),
(437, 1, 'Ravidasia', 1),
(438, 1, 'Rawat', 1),
(439, 1, 'Reddiar', 1),
(440, 1, 'Reddy', 1),
(441, 1, 'Relli', 1),
(442, 1, 'Sadgop', 1),
(443, 1, 'Sagara - Uppara', 1),
(444, 1, 'Saha', 1),
(445, 1, 'Sahu', 1),
(446, 1, 'Saini', 1),
(447, 1, 'Saiva Vellala', 1),
(448, 1, 'Saliya', 1),
(449, 1, 'Sambava', 1),
(450, 1, 'Savji', 1),
(451, 1, 'Scheduled Caste', 1),
(452, 1, 'Scheduled Tribe', 1),
(453, 1, 'Senai Thalaivar', 1),
(454, 1, 'Sepahia', 1),
(455, 1, 'Setti Balija', 1),
(456, 1, 'Shah', 1),
(457, 1, 'Shimpi', 1),
(458, 1, 'Sindhi - All', 1),
(459, 1, 'Sindhi', 1),
(460, 1, 'Sindhi-Amil', 1),
(461, 1, 'Sindhi-Baibhand', 1),
(462, 1, 'Sindhi - Bhanusali', 1),
(463, 1, 'Sindhi - Bhatia', 1),
(464, 1, 'Sindhi - Chhapru', 1),
(465, 1, 'Sindhi - Dadu', 1),
(466, 1, 'Sindhi - Hyderabadi', 1),
(467, 1, 'Sindhi - Larai', 1),
(468, 1, 'Sindhi-Larkana', 1),
(469, 1, 'Sindhi - Lohana', 1),
(470, 1, 'Sindhi - Rohiri', 1),
(471, 1, 'Sindhi-Sahiti', 1),
(472, 1, 'Sindhi-Sakkhar', 1),
(473, 1, 'Sindhi - Sehwani', 1),
(474, 1, 'Sindhi-Shikarpuri', 1),
(475, 1, 'Sindhi - Thatai', 1),
(476, 1, 'Somvanshi', 1),
(477, 1, 'Sonar', 1),
(478, 1, 'Sowrashtra', 1),
(479, 1, 'Sozhiya Vellalar', 1),
(480, 1, 'Srisayana', 1),
(481, 1, 'Sri Vaishnava', 1),
(482, 1, 'SSK', 1),
(483, 1, 'Subarna Banik', 1),
(484, 1, 'Sugali (Naika)', 1),
(485, 1, 'Sundhi', 1),
(486, 1, 'Surya Balija', 1),
(487, 1, 'Sutar', 1),
(488, 1, 'Swarnakar', 1),
(489, 1, 'Tamboli', 1),
(490, 1, 'Tamil Yadava', 1),
(491, 1, 'Tanti', 1),
(492, 1, 'Tantuway', 1),
(493, 1, 'Telaga', 1),
(494, 1, 'Teli', 1),
(495, 1, 'Telugu', 1),
(496, 1, 'Thachar', 1),
(497, 1, 'Thakkar', 1),
(498, 1, 'Thakur', 1),
(499, 1, 'Thandan', 1),
(500, 1, 'Thevar', 1),
(501, 1, 'Thigala', 1),
(502, 1, 'Thiyya', 1),
(503, 1, 'Togata', 1),
(504, 1, 'Turupu Kapu', 1),
(505, 1, 'Udayar', 1),
(506, 1, 'Urs', 1),
(507, 1, 'Vada Balija', 1),
(508, 1, 'Vadagalai', 1),
(509, 1, 'Vaddera', 1),
(510, 1, 'Vaduka', 1),
(511, 1, 'Vaish - All', 1),
(512, 1, 'Vaish', 1),
(513, 1, 'Vaish - Dhaneshawat', 1),
(514, 1, 'Vaishnav - All', 1),
(515, 1, 'Vaishnav', 1),
(516, 1, 'Vaishnav - Bhatia', 1),
(517, 1, 'Vaishnav - Vania', 1),
(518, 1, 'Vaishya', 1),
(519, 1, 'Vaishya', 1),
(520, 1, 'Vallala', 1),
(521, 1, 'Valluvan', 1),
(522, 1, 'Valmiki', 1),
(523, 1, 'Vanjara', 1),
(524, 1, 'Vankar', 1),
(525, 1, 'Vannan', 1),
(526, 1, 'Vannar', 1),
(527, 1, 'Vanniyakullak Kshatriya', 1),
(528, 1, 'Vanniyar', 1),
(529, 1, 'Variar', 1),
(530, 1, 'Varshney', 1),
(531, 1, 'Veera Saivam', 1),
(532, 1, 'Veerashaiva', 1),
(533, 1, 'Velaan', 1),
(534, 1, 'Velama', 1),
(535, 1, 'Velar', 1),
(536, 1, 'Vellalar', 1),
(537, 1, 'Veluthedathu - Nair', 1),
(538, 1, 'Vishwakarma', 1),
(539, 1, 'Viswabrahmin', 1),
(540, 1, 'Vokaliga', 1),
(541, 1, 'Vokkaliga', 1),
(542, 1, 'Vysya', 1),
(543, 1, 'Waada Balija', 1),
(544, 1, 'Yadav', 1),
(545, 1, 'Yellapu', 1),
(546, 2, 'Muslim', 1),
(547, 1, 'Khatri', 1),
(548, 0, '0', 1),
(549, 0, '0', 1),
(550, 2, 'sunni Ehle Hadith', 1),
(551, 2, 'sunni Ehle Hadith', 1),
(552, 15, 'testCommunity', 1),
(553, 1, 'avc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countrylist`
--

CREATE TABLE `countrylist` (
  `Country_Id` int(11) NOT NULL,
  `Country_Name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countrylist`
--

INSERT INTO `countrylist` (`Country_Id`, `Country_Name`, `status`) VALUES
(1, ' India(+91)', 1),
(2, 'USA(+1)', 1),
(3, 'United Kingdom(+44)', 1),
(4, 'United Arab Emirates ( +971)', 1),
(5, 'Canada (+1)', 1),
(6, 'Australia(+61)', 1),
(7, 'New Zealand(+64)', 1),
(8, 'Pakistan(+92)', 1),
(9, 'Saudi Arabia(+966)', 1),
(10, 'Kuwait', 1),
(11, 'South Africa(+27)', 1),
(12, 'Afghanistan ( +93)', 1),
(13, 'Austria (+43)', 1),
(14, 'Bahrain ( +973)', 1),
(15, 'Bangladesh (+880)', 1),
(16, 'Belgium (  +32	)', 1),
(17, 'Botswana ( +267 )', 1),
(18, 'Brunei', 1),
(19, 'Chile ( +56 )', 1),
(20, 'China ( +86 )', 1),
(21, 'Cyprus ( +357 )', 1),
(22, 'Denmark ( +45 )', 1),
(23, 'Dominican Republic', 1),
(24, 'Fiji Islands', 1),
(25, 'Finland', 1),
(26, 'France', 1),
(27, 'Germany', 1),
(28, 'Greece', 1),
(29, 'Guyana', 1),
(30, 'Hong Kong SAR', 1),
(31, 'Hungary', 1),
(32, 'Indonesia', 1),
(33, 'Iran', 1),
(34, 'Ireland', 1),
(35, 'Israel', 1),
(36, 'Italy', 1),
(37, 'Jamaica', 1),
(38, 'Japan', 1),
(39, 'Kenya', 1),
(40, 'Malaysia', 1),
(41, 'Maldives', 1),
(42, 'Mauritius', 1),
(43, 'Mexico', 1),
(44, 'Nepal', 1),
(45, 'Netherlands', 1),
(46, 'Netherlands Antilles', 1),
(47, 'Norway', 1),
(48, 'Oman', 1),
(49, 'Philippines', 1),
(50, 'Poland', 1),
(51, 'Qatar', 1),
(52, 'Russia', 1),
(53, 'Singapore', 1),
(54, 'Spain', 1),
(55, 'Sri Lanka', 1),
(56, 'Sweden', 1),
(57, 'Switzerland', 1),
(58, 'Tanzania', 1),
(59, 'Thailand', 1),
(60, 'Trinidad and Tobago', 1);

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE `degree` (
  `Degree_Id` int(11) NOT NULL,
  `Degree_Name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `degree`
--

INSERT INTO `degree` (`Degree_Id`, `Degree_Name`, `status`) VALUES
(2, 'Master', 1),
(3, 'Bachelor ', 1),
(4, 'Doctorate', 1),
(5, 'Diploma', 1),
(6, 'Undergraduate', 1),
(7, 'Associate Degree', 1),
(8, 'Honours Degree', 1),
(9, 'Trade School', 1),
(10, 'High School', 1),
(11, 'Less Then High School', 1),
(12, '2nd Puc ', 1),
(14, 'abc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emailsalertssms`
--

CREATE TABLE `emailsalertssms` (
  `id` bigint(33) NOT NULL,
  `User_Id` bigint(33) NOT NULL,
  `DialyRecommendationsMail` int(11) NOT NULL,
  `PremiumMatchMail` int(11) NOT NULL,
  `RecentVisitorsMail` int(11) NOT NULL,
  `MembersShortlistedMail` int(11) NOT NULL,
  `ViewedProfilesMail` int(11) NOT NULL,
  `SimilarProfilesMail` int(11) NOT NULL,
  `ContactAlert` int(11) NOT NULL,
  `EmailRecievedAlert` int(11) NOT NULL,
  `SMSAlert` int(11) NOT NULL,
  `ProfileBlaster` int(11) NOT NULL,
  `MilanRishtaSpecials` int(11) NOT NULL,
  `MilanRishtaInSite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailsalertssms`
--

INSERT INTO `emailsalertssms` (`id`, `User_Id`, `DialyRecommendationsMail`, `PremiumMatchMail`, `RecentVisitorsMail`, `MembersShortlistedMail`, `ViewedProfilesMail`, `SimilarProfilesMail`, `ContactAlert`, `EmailRecievedAlert`, `SMSAlert`, `ProfileBlaster`, `MilanRishtaSpecials`, `MilanRishtaInSite`) VALUES
(1, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 74, 1, 4, 1, 4, 2, 5, 4, 6, 4, 7, 4, 9),
(3, 389, 1, 1, 1, 2, 2, 5, 0, 6, 0, 7, 0, 0),
(4, 430, 1, 1, 1, 2, 2, 5, 6, 6, 6, 7, 8, 9);

-- --------------------------------------------------------

--
-- Table structure for table `emailsalertssmstype`
--

CREATE TABLE `emailsalertssmstype` (
  `Id` int(11) NOT NULL,
  `Type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailsalertssmstype`
--

INSERT INTO `emailsalertssmstype` (`Id`, `Type`) VALUES
(1, 'Daily'),
(2, 'Weekly'),
(3, 'Tri-Weekly'),
(4, 'Unsubscribe'),
(5, 'Bi-Weekly'),
(6, 'Instant'),
(7, 'Subscribe'),
(8, 'Occasionally'),
(9, 'Monthly');

-- --------------------------------------------------------

--
-- Table structure for table `familyinfo`
--

CREATE TABLE `familyinfo` (
  `User_id` bigint(33) NOT NULL,
  `father_status` varchar(100) NOT NULL,
  `mother_status` varchar(100) NOT NULL,
  `num_bro` int(4) NOT NULL DEFAULT '0',
  `num_sis` int(4) NOT NULL DEFAULT '0',
  `num_bro_married` int(4) NOT NULL DEFAULT '0',
  `num_sis_married` int(4) NOT NULL DEFAULT '0',
  `native` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `family_value` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `familyinfo`
--

INSERT INTO `familyinfo` (`User_id`, `father_status`, `mother_status`, `num_bro`, `num_sis`, `num_bro_married`, `num_sis_married`, `native`, `level`, `family_value`) VALUES
(98, 'Business', 'Employed', 2, 3, 4, 1, '0', 'Upper Middle Class', 'Traditional'),
(100, 'Employed', 'Employed', 2, 1, 1, 0, '0', 'Upper Middle Class', 'Traditional'),
(115, 'Employed', 'Employed', 1, 0, 0, 0, '0', 'Upper Middle Class', 'Traditional'),
(132, 'Not Employed', 'Employed', 1, 2, 2, 0, '0', 'Upper Middle Class', 'Traditional'),
(152, 'Employed', 'Employed', 0, 1, 0, 1, '86876', 'Upper Middle Class', 'Moderate'),
(164, '', 'Homemaker', 1, 0, 1, 0, 'Bangalore', 'Middle Clas', 'Traditional'),
(171, 'Business', 'Homemaker', 0, 1, 0, 1, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(172, 'Retired', 'Homemaker', 0, 2, 0, 1, 'Bangalore', 'Middle Clas', 'Traditional'),
(194, 'Professional', 'Employed', 2, 2, 2, 2, 'bangalore ', 'Upper Middle Class', 'Traditional'),
(195, 'Employed', 'Homemaker', 1, 0, 0, 0, '', 'Middle Clas', 'Traditional'),
(197, 'Business', 'Homemaker', 1, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(200, 'Business', 'Homemaker', 1, 0, 0, 0, 'Gulbarga', 'Upper Middle Class', 'Traditional'),
(201, 'Employed', 'Homemaker', 1, 0, 0, 0, 'UttarPradesh', 'Upper Middle Class', 'Traditional'),
(202, 'Business', 'Homemaker', 1, 0, 1, 0, 'gulbarga ', 'Upper Middle Class', 'Traditional'),
(203, 'Business', 'Homemaker', 1, 1, 0, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(204, 'Business', 'Homemaker', 1, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(210, 'Business', 'Homemaker', 1, 0, 0, 0, 'bangalore', 'Upper Middle Class', 'Traditional'),
(216, 'Retired', 'Homemaker', 0, 1, 0, 1, 'Bangalore/Karataka', 'Middle Clas', 'Moderate'),
(217, 'Retired', 'Homemaker', 1, 0, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(220, 'Employed', 'Homemaker', 4, 3, 1, 3, 'Bangalore', 'Middle Clas', 'Moderate'),
(222, 'Passed Away', 'Homemaker', 2, 1, 0, 1, 'Thyamagondlu, Bangalore Rural', 'Middle Clas', 'Moderate'),
(225, 'Business', 'Homemaker', 4, 1, 0, 1, 'bangalore', 'Middle Clas', 'Traditional'),
(226, 'Business', 'Homemaker', 0, 2, 0, 2, 'Banglore', 'Middle Clas', 'Traditional'),
(227, 'Business', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(228, 'Retired', 'Homemaker', 0, 5, 0, 0, 'Hubli City ', 'Upper Middle Class', 'Moderate'),
(236, 'Business', 'Homemaker', 2, 2, 0, 2, 'Bangalore', 'Middle Clas', 'Moderate'),
(237, 'Business', 'Homemaker', 0, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(238, 'Employed', 'Employed', 0, 2, 0, 2, 'kerala', 'Middle Clas', 'Traditional'),
(243, 'Employed', 'Homemaker', 1, 0, 0, 0, 'bangalore ', 'Upper Middle Class', 'Traditional'),
(244, 'Employed', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(245, 'Employed', 'Homemaker', 1, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(246, 'Business', 'Homemaker', 3, 0, 1, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(249, 'Employed', 'Homemaker', 1, 0, 0, 0, 'gulbarga', 'Upper Middle Class', 'Traditional'),
(250, 'Business', 'Employed', 1, 0, 0, 0, 'varanasi', 'Upper Middle Class', 'Liberal'),
(252, 'Employed', 'Homemaker', 0, 0, 0, 0, 'gulbarga ', 'Middle Clas', 'Traditional'),
(254, 'Employed', 'Homemaker', 1, 0, 1, 0, 'Tumkur', 'Upper Middle Class', 'Moderate'),
(255, 'Passed Away', 'Homemaker', 1, 1, 1, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(257, 'Retired', 'Homemaker', 0, 1, 0, 1, '', 'Upper Middle Class', 'Moderate'),
(259, 'Passed Away', 'Homemaker', 0, 1, 0, 1, 'Hubli', 'Upper Middle Class', 'Traditional'),
(260, 'Retired', 'Homemaker', 2, 1, 0, 0, 'Andra Pradesh', 'Middle Clas', 'Moderate'),
(262, 'Passed Away', 'Homemaker', 1, 1, 1, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(264, 'Retired', 'Homemaker', 5, 1, 2, 1, 'Bangalore', 'Middle Clas', 'Traditional'),
(265, 'Employed', 'Homemaker', 2, 3, 2, 2, 'Bangalore', 'Middle Clas', 'Moderate'),
(266, 'Retired', 'Homemaker', 1, 1, 0, 1, 'Rama kuppam  Chittoor DT Andhra pradesh', 'Upper Middle Class', 'Traditional'),
(291, 'Employed', 'Homemaker', 1, 0, 0, 0, 'Bangalore ', 'Middle Clas', 'Traditional'),
(292, 'Employed', 'Homemaker', 1, 0, 0, 0, 'davangere', 'Upper Middle Class', 'Moderate'),
(303, 'Retired', 'Homemaker', 2, 0, 0, 0, 'Tirunelveli ', 'Middle Clas', 'Moderate'),
(304, 'Employed', 'Homemaker', 1, 2, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(305, 'Employed', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(308, 'Employed', 'Homemaker', 0, 0, 1, 0, 'Gulbarga', 'Upper Middle Class', 'Traditional'),
(309, 'Employed', 'Homemaker', 0, 0, 0, 0, 'shimoga', 'Upper Middle Class', 'Moderate'),
(310, 'Retired', 'Homemaker', 1, 1, 0, 1, 'KOLAR GOLD FIELDS', 'Middle Clas', 'Liberal'),
(311, 'Passed Away', 'Passed Away', 3, 0, 2, 0, 'bangalore', 'Middle Clas', 'Traditional'),
(312, 'Retired', 'Homemaker', 0, 2, 0, 2, 'Punjab', 'Upper Middle Class', 'Moderate'),
(314, 'Employed', 'Homemaker', 1, 1, 1, 1, 'bangalore', 'Upper Middle Class', 'Liberal'),
(315, 'Passed Away', 'Homemaker', 0, 1, 0, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(316, 'Employed', 'Homemaker', 0, 1, 0, 1, 'Karnataka', 'Middle Clas', 'Moderate'),
(317, 'Employed', 'Homemaker', 1, 1, 1, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(318, 'Employed', 'Homemaker', 2, 1, 1, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(319, 'Employed', 'Homemaker', 1, 1, 1, 1, 'Bangalore', 'Middle Clas', 'Moderate'),
(320, 'Business', 'Homemaker', 2, 4, 2, 4, 'Rajestan Jaipure', 'Upper Middle Class', 'Moderate'),
(321, 'Passed Away', 'Business', 0, 1, 0, 0, 'bangalore', 'Upper Middle Class', 'Moderate'),
(322, 'Business', 'Homemaker', 0, 3, 0, 2, 'Hubli', 'Middle Clas', 'Traditional'),
(325, 'Employed', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(327, 'Employed', 'Homemaker', 3, 3, 3, 2, 'Bangalore ', 'Lower Middle Class', 'Traditional'),
(28, 'Employed', 'Homemaker', 1, 1, 0, 1, 'bangalore', 'Upper Middle Class', ''),
(328, 'Employed', 'Homemaker', 0, 0, 0, 0, 'na', 'Upper Middle Class', 'Traditional'),
(329, 'Passed Away', 'Not Employed', 0, 0, 0, 0, 'Bangalore', 'Middle Clas', 'Traditional'),
(180, 'Employed', 'Homemaker', 2, 2, 2, 2, 'Bangalore', 'Middle Clas', 'Traditional'),
(334, 'Employed', 'Employed', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(335, 'Employed', 'Homemaker', 1, 1, 0, 0, 'Vijaywada Ap', 'Middle Clas', 'Moderate'),
(78, '', '', 0, 0, 0, 0, '', '', ''),
(336, 'Passed Away', 'Employed', 1, 0, 1, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(337, 'Employed', 'Homemaker', 0, 1, 0, 1, 'Bangalore', 'Middle Clas', 'Traditional'),
(338, 'Retired', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(341, 'Employed', 'Employed', 0, 1, 0, 1, 'Bangalore', 'Middle Clas', 'Liberal'),
(342, 'Retired', 'Homemaker', 1, 1, 0, 1, 'kolar gold field / bangalore ', 'Middle Clas', 'Liberal'),
(343, 'Business', 'Homemaker', 1, 1, 0, 1, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(344, 'Employed', 'Homemaker', 1, 1, 0, 1, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(347, 'Employed', 'Employed', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(349, 'Business', 'Homemaker', 1, 3, 0, 0, '', 'Upper Middle Class', 'Traditional'),
(350, 'Employed', 'Homemaker', 1, 0, 1, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(353, 'Passed Away', 'Homemaker', 0, 0, 0, 0, 'bangalore', 'Middle Clas', 'Moderate'),
(355, 'Employed', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(356, 'Retired', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(357, 'Business', 'Homemaker', 1, 1, 1, 1, 'Bangalore', 'Middle Clas', 'Traditional'),
(358, 'Business', 'Homemaker', 1, 0, 0, 0, 'Gulbarga', 'Upper Middle Class', 'Moderate'),
(359, 'Retired', 'Homemaker', 1, 0, 0, 0, 'Gulbarga', 'Upper Middle Class', 'Traditional'),
(361, 'Business', 'Homemaker', 1, 0, 0, 0, 'karnataka', 'Middle Clas', 'Liberal'),
(362, 'Employed', 'Homemaker', 1, 0, 1, 0, 'bangalore', 'Middle Clas', 'Traditional'),
(363, 'Passed Away', 'Homemaker', 2, 3, 2, 2, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(364, 'Retired', 'Homemaker', 0, 3, 0, 2, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(365, 'Business', 'Business', 0, 0, 0, 0, 'Chennai', 'Upper Middle Class', 'Traditional'),
(366, 'Retired', 'Homemaker', 2, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(367, 'Retired', 'Homemaker', 1, 1, 1, 1, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(368, 'Retired', 'Homemaker', 1, 1, 0, 0, 'bangalore', 'Middle Clas', 'Traditional'),
(369, 'Employed', 'Homemaker', 2, 1, 0, 0, '', 'Middle Clas', 'Traditional'),
(370, 'Employed', 'Homemaker', 0, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(371, 'Business', 'Homemaker', 0, 5, 0, 4, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(372, 'Business', 'Homemaker', 1, 5, 0, 4, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(373, 'Professional', 'Homemaker', 1, 0, 1, 0, 'Tumkuru ', 'Middle Clas', 'Moderate'),
(374, 'Professional', 'Homemaker', 1, 1, 0, 1, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(375, 'Business', 'Homemaker', 1, 0, 1, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(376, 'Business', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Moderate'),
(377, 'Business', 'Homemaker', 0, 2, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(378, 'Employed', 'Homemaker', 0, 2, 0, 0, 'Bangalore', 'Middle Clas', 'Moderate'),
(379, 'Passed Away', 'Homemaker', 0, 3, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(380, 'Retired', 'Homemaker', 0, 4, 0, 2, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(381, 'Retired', 'Homemaker', 0, 2, 0, 2, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(382, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(383, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(384, 'Employed', 'Homemaker', 1, 0, 0, 0, 'hyderabad', 'Upper Middle Class', 'Traditional'),
(385, 'Business', 'Homemaker', 2, 3, 1, 3, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(386, 'Business', 'Homemaker', 0, 0, 0, 0, 'Maddur', 'Upper Middle Class', 'Moderate'),
(387, 'Business', 'Homemaker', 2, 3, 0, 2, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(388, 'Business', 'Employed', 1, 1, 0, 0, '', 'Upper Middle Class', 'Moderate'),
(389, 'Business', 'Business', 1, 1, 0, 0, '', 'Upper Middle Class', 'Moderate'),
(390, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(391, 'Business', 'Homemaker', 1, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(392, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(393, 'Business', 'Homemaker', 0, 0, 0, 0, 'Dharward', 'Upper Middle Class', 'Traditional'),
(394, 'Business', 'Homemaker', 2, 0, 1, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(395, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(396, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(397, 'Business', 'Homemaker', 0, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(400, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(401, 'Retired', 'Homemaker', 1, 0, 1, 0, 'Rajasthan', 'Upper Middle Class', 'Moderate'),
(402, 'Business', 'Homemaker', 1, 0, 1, 0, 'Madurai ', 'Upper Middle Class', 'Traditional'),
(403, 'Business', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(405, 'Employed', 'Homemaker', 0, 0, 0, 0, 'chennai', 'Upper Middle Class', 'Traditional'),
(406, 'Business', 'Homemaker', 1, 1, 0, 0, 'Bangalore', '', ''),
(407, 'Business', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(407, 'Business', 'Homemaker', 1, 1, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(409, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(410, 'Business', 'Homemaker', 1, 1, 1, 1, 'delhi', 'Upper Middle Class', 'Traditional'),
(411, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(412, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(413, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(415, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(418, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(419, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(420, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(421, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(422, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(423, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(424, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(425, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(427, 'Employed', 'Homemaker', 0, 2, 0, 0, 'K.G.F.', 'Middle Clas', 'Traditional'),
(429, 'Retired', 'Homemaker', 1, 1, 0, 0, 'Bangalore ', 'Middle Clas', 'Traditional'),
(430, 'Employed', 'Employed', 2, 1, 1, 0, 'bangalore', '', 'Traditional'),
(431, 'Employed', 'Retired', 1, 2, 1, 0, '', 'Middle Clas', 'Traditional'),
(432, 'Business', 'Homemaker', 1, 2, 1, 0, '', 'Upper Middle Class', 'Traditional'),
(433, 'Employed', 'Homemaker', 2, 1, 0, 0, 'bangalore', 'Middle Clas', 'Moderate'),
(440, 'Retired', 'Homemaker', 1, 1, 1, 1, '', 'Upper Middle Class', 'Traditional'),
(441, 'Business', 'Homemaker', 1, 2, 1, 0, '', 'Upper Middle Class', 'Traditional'),
(442, 'Business', 'Homemaker', 1, 1, 1, 1, '', 'Upper Middle Class', 'Traditional'),
(447, 'Retired', 'Homemaker', 2, 0, 1, 0, 'kolkata', 'Upper Middle Class', 'Traditional'),
(448, 'Retired', 'Homemaker', 1, 0, 1, 0, 'bangalore', 'Upper Middle Class', 'Traditional'),
(449, 'Business', 'Homemaker', 0, 0, 0, 0, 'agra', 'Middle Class', 'Moderate'),
(450, 'Employed', 'Homemaker', 2, 1, 2, 0, 'gokul', 'Middle Class', 'Moderate'),
(451, 'Employed', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(452, 'Business', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(453, 'Business', 'Homemaker', 1, 0, 0, 0, 'agra', 'Upper Middle Class', 'Liberal'),
(454, 'Professional', 'Homemaker', 0, 0, 0, 0, 'Bangalore', 'Upper Middle Class', 'Traditional'),
(456, 'Employed', 'Homemaker', 2, 0, 1, 0, 'gokul', 'Middle Clas', 'Moderate'),
(455, 'Business', 'Homemaker', 1, 1, 1, 0, 'kolkata', 'Middle Clas', 'Moderate'),
(457, 'Employed', 'Employed', 1, 0, 0, 0, 'agra', 'Middle Clas', 'Traditional'),
(459, 'Business', 'Employed', 0, 1, 0, 0, 'punjab', 'Upper Middle Class', 'Liberal'),
(458, 'Business', 'Homemaker', 1, 0, 0, 0, '', 'Upper Middle Class', 'Traditional'),
(460, 'Employed', 'Homemaker', 0, 0, 0, 0, 'manipur', 'Middle Clas', 'Moderate'),
(461, 'Retired', 'Homemaker', 1, 0, 0, 0, '', 'Middle Clas', 'Traditional'),
(462, 'Employed', 'Employed', 2, 1, 0, 0, 'bihar', 'Middle Clas', 'Traditional'),
(463, 'Retired', 'Retired', 0, 2, 0, 0, '', 'Upper Middle Class', 'Traditional'),
(464, 'Business', 'Employed', 1, 0, 0, 0, '', 'Upper Middle Class', 'Moderate'),
(465, 'Employed', 'Homemaker', 1, 0, 0, 0, 'agra', 'Middle Clas', 'Moderate'),
(0, 'Business', '', 0, 0, 0, 0, '', '', ''),
(468, 'Employed', 'Homemaker', 1, 1, 0, 0, 'gujarat', 'Middle Clas', 'Traditional');

-- --------------------------------------------------------

--
-- Table structure for table `HeightList`
--

CREATE TABLE `HeightList` (
  `Height_Id` int(11) NOT NULL,
  `Height` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `HeightList`
--

INSERT INTO `HeightList` (`Height_Id`, `Height`) VALUES
(53, '4\' 05\'\' - 134cm'),
(54, '4\' 06\'\' - 137cm'),
(55, '4\' 07\'\' - 139cm'),
(56, '4\' 08\'\' - 142cm'),
(57, '4\' 09\'\' - 144cm'),
(58, '4\' 10\'\' - 147cm'),
(59, '4\' 11\'\' - 149cm'),
(60, '5\' 00\'\' - 152cm'),
(61, '5\' 01\'\' - 154cm'),
(62, '5\' 02\'\' - 157cm'),
(63, '5\' 03\'\' - 160cm'),
(64, '5\' 04\'\' - 162cm'),
(65, '5\' 05\'\' - 165cm'),
(66, '5\' 06\'\' - 167cm'),
(67, '5\' 07\'\' - 170cm'),
(68, '5\' 08\'\' - 172cm'),
(69, '5\' 09\'\' - 175cm'),
(70, '5\' 10\'\' - 177cm'),
(71, '5\' 11\'\' - 180cm'),
(72, '6\' 00\'\'- 182cm'),
(73, '6\' 01\'\' - 185cm'),
(74, '6\' 02\'\' - 187cm'),
(75, '6\' 03\'\' - 190cm'),
(76, '6\' 04\'\' - 193cm'),
(77, '6\' 05\'\' - 195cm'),
(78, '6\' 06\'\' - 198cm'),
(79, '6\' 07\'\' - 200cm'),
(80, '6\' 08\'\' - 203cm'),
(81, '6\' 09\'\' - 205cm'),
(82, '6\' 10\'\' - 208cm'),
(83, '6\' 11\'\' - 210cm'),
(84, '7\' 00\'- 213cm');

-- --------------------------------------------------------

--
-- Table structure for table `languagelist`
--

CREATE TABLE `languagelist` (
  `Language_Id` int(11) NOT NULL,
  `Language_Name` varchar(100) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languagelist`
--

INSERT INTO `languagelist` (`Language_Id`, `Language_Name`, `Status`) VALUES
(1, 'Assamese', 1),
(2, 'Bengali', 1),
(3, 'English', 1),
(4, 'Gujarati', 1),
(5, 'Hindi', 1),
(6, 'Kannada', 1),
(7, 'Konkani', 1),
(8, 'Malayalam', 1),
(9, 'Marathi', 1),
(10, 'Marwari', 1),
(11, 'Odia', 1),
(12, 'Punjabi', 1),
(13, 'Sindhi', 1),
(14, 'Tamil', 1),
(15, 'Telugu', 1),
(16, 'Urdu', 1),
(17, 'Aka', 1),
(18, 'Arabic', 1),
(19, 'Awadhi', 1),
(20, 'Bhojpuri', 1),
(21, 'Chattisgarhi', 1),
(22, 'Coorgi', 1),
(23, 'Dogri', 1),
(24, 'French', 1),
(25, 'Garhwali', 1),
(26, 'Garo', 1),
(27, 'Haryanavi', 1),
(28, 'Himachali/Pahari', 1),
(29, 'Hindko', 1),
(30, 'Kashmiri', 1),
(31, 'Khasi', 1),
(32, 'Kumaoni', 1),
(33, 'Kutchi', 1),
(34, 'Magahi', 1),
(35, 'Maithili', 1),
(36, 'Malay', 1),
(37, 'Manipuri', 1),
(38, 'Miji', 1),
(39, 'Mizo', 1),
(40, 'Nepali', 1),
(41, 'Pashto', 1),
(42, 'Persian', 1),
(43, 'Rajasthani', 1),
(44, 'Russian', 1),
(45, 'Sanskrit', 1),
(46, 'Sinhala', 1),
(47, 'Spanish', 1),
(48, 'Tulu', 1),
(49, 'TestLanguage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mailbox`
--

CREATE TABLE `mailbox` (
  `Mail_Id` bigint(250) NOT NULL,
  `User_IdTo` int(11) NOT NULL,
  `User_IdFrom` int(11) NOT NULL,
  `MailType_Id` int(11) NOT NULL,
  `Subject` text NOT NULL,
  `Message` text NOT NULL,
  `MailRead` tinyint(2) NOT NULL,
  `Deleted_fromuser` tinyint(2) NOT NULL,
  `Deleted_Touser` int(2) NOT NULL,
  `Date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailbox`
--

INSERT INTO `mailbox` (`Mail_Id`, `User_IdTo`, `User_IdFrom`, `MailType_Id`, `Subject`, `Message`, `MailRead`, `Deleted_fromuser`, `Deleted_Touser`, `Date`) VALUES
(7, 20, 28, 1, '', 'hi hello hru', 0, 0, 0, '0000-00-00 00:00:00'),
(8, 24, 20, 1, '', 'i am fine. hru ?', 0, 0, 0, '0000-00-00 00:00:00'),
(9, 24, 20, 1, '', 'hi hru', 0, 0, 0, '0000-00-00 00:00:00'),
(10, 24, 28, 1, '', 'sundayb', 0, 0, 0, '0000-00-00 00:00:00'),
(11, 28, 20, 1, '', 'sdjsgdjsgf', 1, 0, 0, '0000-00-00 00:00:00'),
(13, 100, 28, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9886656363 or through email: lvijetha90@gmail.com. Warm Regards, vijetha', 0, 0, 0, '2015-04-12 19:33:19'),
(14, 51, 80, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9741784637 or through email: pavanpatil196@gmail.com. Warm Regards, pavan', 0, 0, 0, '2015-04-13 12:44:08'),
(15, 64, 77, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9035269409 or through email: riyazwahabi@yahoo.com. Warm Regards, Fathima', 0, 0, 0, '2015-04-13 13:59:50'),
(16, 77, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-04-13 15:48:08'),
(17, 142, 157, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9845322786 or through email: rehman.t@gmail.com. Warm Regards, Rehman', 0, 0, 0, '2015-04-16 14:22:28'),
(18, 51, 158, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9740064516 or through email: shivraj.vn@gmail.com. Warm Regards, shivraj', 0, 0, 0, '2015-04-16 14:44:25'),
(19, 145, 28, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9886656363 or through email: lvijetha90@gmail.com. Warm Regards, vijetha', 0, 0, 0, '2015-04-17 23:22:21'),
(20, 77, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-04-20 13:25:19'),
(21, 164, 168, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez23@gmail.com. Warm Regards, Abdul ', 0, 0, 0, '2015-04-22 20:01:04'),
(22, 51, 80, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9741784637 or through email: pavanpatil196@gmail.com. Warm Regards, pavan', 0, 0, 0, '2015-04-23 09:47:02'),
(23, 50, 196, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9845123456 or through email: avj2352@gmail.com. Warm Regards, Pramod', 0, 0, 0, '2015-04-25 23:04:32'),
(24, 77, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-05-04 12:33:50'),
(25, 77, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-05-05 18:35:11'),
(26, 82, 241, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9880055341 or through email: pavankpatil043@gmail.com. Warm Regards, pavan', 0, 0, 0, '2015-05-11 10:38:17'),
(27, 244, 246, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9036878230 or through email: Syedwasim.222@gmail.com. Warm Regards, Syed ', 0, 0, 0, '2015-05-12 12:37:18'),
(28, 86, 256, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9066372270 or through email: tanhajrashid@gmail.com. Warm Regards, Rashid', 0, 0, 0, '2015-05-15 20:26:03'),
(29, 258, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-05-17 15:07:08'),
(30, 258, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-05-17 15:07:08'),
(31, 51, 180, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9741784637 or through email: pavanpatil196@gmail.com. Warm Regards, pavan', 0, 0, 0, '2015-06-12 16:38:49'),
(32, 51, 295, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9845341889 or through email: Althaf.ma92@gmail.com. Warm Regards, Reshma', 0, 0, 0, '2015-06-13 18:32:37'),
(33, 51, 180, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9741784637 or through email: pavanpatil196@gmail.com. Warm Regards, pavan', 0, 0, 0, '2015-06-19 13:02:29'),
(34, 74, 74, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: knkda or through email: pavan@bigperl.com. Warm Regards, pavan', 0, 0, 0, '2015-07-09 13:00:28'),
(35, 51, 74, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: knkda or through email: pavan@bigperl.com. Warm Regards, pavan', 0, 0, 0, '2015-07-09 13:12:55'),
(36, 51, 74, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: knkda or through email: pavan@bigperl.com. Warm Regards, pavan', 0, 0, 0, '2015-07-10 10:24:52'),
(37, 314, 20, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9880055341 or through email: raja@gmail.com. Warm Regards, raja', 0, 0, 0, '2015-08-13 14:37:33'),
(38, 363, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-08-13 18:39:28'),
(39, 370, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-08-19 19:18:10'),
(40, 203, 370, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8861016389 or through email: drspeed57@yahoo.com. Warm Regards, Loria', 0, 0, 0, '2015-08-19 19:41:38'),
(41, 344, 245, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9035564351 or through email: Zainahmed217@gmail.com. Warm Regards, Asra ', 0, 0, 0, '2015-08-21 12:06:44'),
(42, 375, 68, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-08-21 13:53:26'),
(43, 363, 68, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-08-21 16:13:15'),
(44, 363, 68, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9663627372 or through email: mtabrez18@yahoo.in. Warm Regards, Mohammed ', 0, 0, 0, '2015-08-21 16:13:23'),
(45, 363, 374, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9902593840 or through email: shakirg62@gmail.com. Warm Regards, Najma', 0, 0, 0, '2015-08-21 17:20:36'),
(46, 363, 374, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9902593840 or through email: shakirg62@gmail.com. Warm Regards, Najma', 0, 0, 0, '2015-08-21 17:20:43'),
(47, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 01:21:03'),
(48, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 01:21:16'),
(49, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 01:23:39'),
(50, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 01:25:13'),
(51, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 22:17:19'),
(52, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 22:18:32'),
(53, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-22 22:19:02'),
(54, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-23 21:16:33'),
(55, 370, 389, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9019950902 or through email: shajahansapfico@gmail.com. Warm Regards, shajahan', 0, 0, 0, '2015-08-25 00:53:10'),
(56, 164, 430, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9988667755 or through email: test@gmail.com. Warm Regards, test1', 0, 0, 0, '2019-07-04 10:34:11'),
(57, 164, 430, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9988667755 or through email: test@gmail.com. Warm Regards, test1', 0, 0, 0, '2019-07-04 10:34:31'),
(58, 430, 440, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9988663325 or through email: test937@gmail.com. Warm Regards, test987', 0, 0, 0, '2019-07-05 06:38:59'),
(59, 432, 448, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9988546321 or through email: testm@gmail.com. Warm Regards, testm', 0, 0, 0, '2019-07-10 05:56:08'),
(60, 449, 450, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: abcd@gmail.com. Warm Regards, raghu', 0, 0, 0, '2019-07-10 06:04:27'),
(61, 449, 450, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: abcd@gmail.com. Warm Regards, raghu', 0, 0, 0, '2019-07-10 06:11:00'),
(62, 449, 450, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: abcd@gmail.com. Warm Regards, raghu', 0, 0, 0, '2019-07-10 06:36:20'),
(63, 450, 449, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 08876556788 or through email: tests@gmail.com. Warm Regards, testabc', 1, 1, 0, '2019-07-10 06:52:50'),
(64, 449, 450, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: abcd@gmail.com. Warm Regards, raghu', 1, 1, 0, '2019-07-10 06:53:17'),
(65, 450, 449, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 08876556788 or through email: tests@gmail.com. Warm Regards, testabc', 0, 1, 0, '2019-07-10 07:09:32'),
(66, 450, 449, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 08876556788 or through email: tests@gmail.com. Warm Regards, testabc', 1, 0, 0, '2019-07-10 07:11:27'),
(67, 449, 450, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: abcd@gmail.com. Warm Regards, raghu', 1, 0, 0, '2019-07-10 07:12:14'),
(68, 189, 450, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: abcd@gmail.com. Warm Regards, raghu', 0, 0, 0, '2019-07-10 07:20:58'),
(69, 447, 448, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9988546321 or through email: testm@gmail.com. Warm Regards, testm', 0, 0, 0, '2019-07-12 09:21:34'),
(70, 452, 451, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9988667755 or through email: adamwhite@gmail.com. Warm Regards, Adam', 0, 0, 0, '2019-07-17 08:41:18'),
(71, 451, 459, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: sakshi@gmail.com. Warm Regards, Sakshi', 0, 0, 0, '2019-07-18 05:40:39'),
(72, 459, 465, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 08876556788 or through email: ronak@gmail.com. Warm Regards, Ronak', 0, 0, 0, '2019-07-18 06:02:47'),
(73, 465, 459, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: sakshi@gmail.com. Warm Regards, Sakshi', 1, 0, 0, '2019-07-18 06:03:47'),
(74, 465, 459, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 8907654689 or through email: sakshi@gmail.com. Warm Regards, Sakshi', 1, 0, 0, '2019-07-18 07:20:18'),
(75, 459, 465, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 08876556788 or through email: ronak@gmail.com. Warm Regards, Ronak', 1, 0, 0, '2019-07-18 07:23:11'),
(76, 468, 457, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 7899999999 or through email: rahul@gmail.com. Warm Regards, rahul', 0, 0, 0, '2019-07-19 06:43:04'),
(77, 468, 457, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 7899999999 or through email: rahul@gmail.com. Warm Regards, rahul', 1, 0, 0, '2019-07-19 06:43:12'),
(78, 457, 468, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9878747844 or through email: mishti@gmail.com. Warm Regards, Mishti', 1, 0, 0, '2019-07-19 06:46:40'),
(79, 468, 456, 2, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 89827878821 or through email: abhinav@gmail.com. Warm Regards, Abhinav', 0, 0, 0, '2019-07-19 06:52:21'),
(80, 468, 456, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 89827878821 or through email: abhinav@gmail.com. Warm Regards, Abhinav', 1, 0, 0, '2019-07-19 06:52:50'),
(81, 456, 468, 1, '', 'Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: 9878747844 or through email: mishti@gmail.com. Warm Regards, Mishti', 0, 0, 0, '2019-07-19 06:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `mailsubtypes`
--

CREATE TABLE `mailsubtypes` (
  `MailSubType_Id` int(11) NOT NULL,
  `MailType_Id` int(11) NOT NULL,
  `MailSubType_Name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailsubtypes`
--

INSERT INTO `mailsubtypes` (`MailSubType_Id`, `MailType_Id`, `MailSubType_Name`) VALUES
(1, 1, 'Emails'),
(2, 1, 'Invitations'),
(4, 1, 'requests'),
(5, 2, 'Emails'),
(6, 2, 'Invitations'),
(7, 2, 'Requests'),
(8, 3, 'Invitations'),
(9, 3, 'Requests'),
(10, 3, 'Emails');

-- --------------------------------------------------------

--
-- Table structure for table `mailtypes`
--

CREATE TABLE `mailtypes` (
  `MailType_Id` int(11) NOT NULL,
  `MailType_Name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailtypes`
--

INSERT INTO `mailtypes` (`MailType_Id`, `MailType_Name`) VALUES
(1, 'EMAILS'),
(2, 'INVITATIONS'),
(3, 'REQUESTS');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `Id` bigint(22) NOT NULL,
  `NotificationTo` bigint(33) NOT NULL,
  `NotificationFrom` bigint(33) NOT NULL,
  `Notification_Id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`Id`, `NotificationTo`, `NotificationFrom`, `Notification_Id`, `date`) VALUES
(1, 28, 100, 7, '2015-04-12'),
(2, 28, 25, 6, '2015-04-01'),
(3, 51, 80, 8, '2015-04-13'),
(4, 51, 80, 7, '2015-04-13'),
(5, 51, 80, 4, '2015-04-13'),
(6, 64, 77, 4, '2015-04-13'),
(7, 77, 68, 4, '2015-04-13'),
(8, 51, 80, 8, '2015-04-14'),
(9, 51, 80, 7, '2015-04-14'),
(10, 105, 104, 7, '2015-04-15'),
(11, 142, 157, 4, '2015-04-16'),
(12, 51, 158, 4, '2015-04-16'),
(13, 145, 28, 4, '2015-04-17'),
(14, 77, 68, 4, '2015-04-20'),
(15, 51, 74, 7, '2015-04-20'),
(16, 51, 62, 7, '2015-04-22'),
(17, 164, 168, 4, '2015-04-22'),
(18, 51, 80, 4, '2015-04-23'),
(19, 50, 196, 4, '2015-04-25'),
(20, 40, 28, 8, '2015-05-03'),
(21, 40, 28, 8, '2015-05-03'),
(22, 40, 28, 8, '2015-05-03'),
(23, 77, 68, 4, '2015-05-04'),
(24, 77, 68, 4, '2015-05-05'),
(25, 40, 28, 8, '2015-05-05'),
(26, 82, 241, 4, '2015-05-11'),
(27, 244, 246, 4, '2015-05-12'),
(28, 86, 256, 4, '2015-05-15'),
(29, 258, 68, 4, '2015-05-17'),
(30, 258, 68, 4, '2015-05-17'),
(31, 186, 258, 8, '2015-05-17'),
(32, 170, 258, 8, '2015-05-17'),
(33, 270, 74, 7, '2015-05-29'),
(34, 51, 180, 4, '2015-06-12'),
(35, 51, 295, 4, '2015-06-13'),
(36, 51, 180, 4, '2015-06-19'),
(37, 74, 74, 4, '2015-07-09'),
(38, 51, 74, 4, '2015-07-09'),
(39, 51, 74, 4, '2015-07-10'),
(40, 51, 74, 7, '2015-07-14'),
(41, 195, 343, 8, '2015-08-01'),
(42, 314, 20, 4, '2015-08-13'),
(43, 363, 68, 4, '2015-08-13'),
(44, 203, 363, 7, '2015-08-13'),
(45, 370, 68, 4, '2015-08-19'),
(46, 203, 370, 4, '2015-08-19'),
(47, 344, 245, 4, '2015-08-21'),
(48, 375, 68, 4, '2015-08-21'),
(49, 363, 68, 4, '2015-08-21'),
(50, 363, 68, 4, '2015-08-21'),
(51, 363, 374, 4, '2015-08-21'),
(52, 363, 374, 4, '2015-08-21'),
(53, 370, 389, 4, '2015-08-22'),
(54, 370, 389, 4, '2015-08-22'),
(55, 370, 389, 4, '2015-08-22'),
(56, 370, 389, 4, '2015-08-22'),
(57, 370, 389, 4, '2015-08-22'),
(58, 370, 389, 4, '2015-08-22'),
(59, 370, 389, 4, '2015-08-22'),
(60, 370, 389, 4, '2015-08-23'),
(61, 370, 389, 4, '2015-08-25'),
(62, 195, 68, 4, '2015-08-27'),
(63, 370, 389, 4, '2015-08-28'),
(64, 164, 430, 4, '2019-07-04'),
(65, 164, 430, 4, '2019-07-04'),
(66, 171, 430, 7, '2019-07-04'),
(67, 197, 430, 7, '2019-07-04'),
(68, 430, 440, 4, '2019-07-05'),
(69, 432, 448, 4, '2019-07-10'),
(70, 442, 448, 7, '2019-07-10'),
(71, 449, 450, 4, '2019-07-10'),
(72, 449, 450, 4, '2019-07-10'),
(73, 449, 450, 4, '2019-07-10'),
(74, 450, 449, 4, '2019-07-10'),
(75, 449, 450, 4, '2019-07-10'),
(76, 450, 449, 4, '2019-07-10'),
(77, 450, 449, 4, '2019-07-10'),
(78, 449, 450, 4, '2019-07-10'),
(79, 189, 450, 4, '2019-07-10'),
(80, 432, 448, 7, '2019-07-10'),
(81, 447, 448, 4, '2019-07-12'),
(82, 440, 448, 7, '2019-07-12'),
(83, 171, 430, 8, '2019-07-12'),
(84, 432, 448, 8, '2019-07-12'),
(85, 432, 448, 8, '2019-07-12'),
(86, 452, 451, 4, '2019-07-17'),
(87, 451, 459, 4, '2019-07-18'),
(88, 459, 465, 4, '2019-07-18'),
(89, 465, 459, 4, '2019-07-18'),
(90, 465, 459, 4, '2019-07-18'),
(91, 459, 465, 4, '2019-07-18'),
(92, 468, 457, 4, '2019-07-19'),
(93, 468, 457, 4, '2019-07-19'),
(94, 457, 468, 4, '2019-07-19'),
(95, 468, 456, 4, '2019-07-19'),
(96, 468, 456, 4, '2019-07-19'),
(97, 456, 468, 4, '2019-07-19');

-- --------------------------------------------------------

--
-- Table structure for table `notificationtype`
--

CREATE TABLE `notificationtype` (
  `Notification_id` bigint(33) NOT NULL,
  `NotificationType` varchar(200) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notificationtype`
--

INSERT INTO `notificationtype` (`Notification_id`, `NotificationType`, `Description`) VALUES
(1, 'ProfilePic', ''),
(2, 'FamiltyInfo', ''),
(3, 'EducationCareer', ''),
(4, 'SendMail', 'Sent email'),
(5, 'SendRequest', ''),
(6, 'SendInvitation', 'sent an invitation'),
(7, 'MayBe', 'Added to maybe list'),
(8, 'Ignore', 'Ignored profile');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `Page_id` int(11) NOT NULL,
  `Page_Heading` varchar(300) NOT NULL,
  `Page_text` text NOT NULL,
  `Status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`Page_id`, `Page_Heading`, `Page_text`, `Status`) VALUES
(1, 'About Us', '<p>Indian tradition places a great emphasis on marriage and match-making. MilanRishta was set up as a viable channel for prospective brides and grooms to meet up online and get to know each other better before tying the knot in holy matrimony. For those who look for better choices in life partners, or do not have the time or resources to take their marriage plans ahead, MilanRishta comes across as a trustworthy companion in your search for the right life partner. The matrimonial site is an ambitious venture of JS BRIGHT LINK ENTERPRISES. The company has proven competencies in various niches such as CCTV surveillance systems and Air Conditioning systems.</p>\n\n<p>JS Bright Link strives to provide superior quality servicing at extremely nominal price points. Our clients benefit from our partnership because of our commitment to total integrity and innovative value-add.Our endeavor to make a meaningful difference in people&rsquo;s lives have seen us branch out into the matrimonial services space with MilanRishta.</p>\n\n<p>Spend your life with the partner you want As part of our efforts to ensure all-round excellence in servicing, we have managed to build a formidable matrimony portfolio spanning across religions, castes, communities or socio- economic profiles. With MilanRishta, you can gain invaluable search matches based on various preferences. You can narrow down your search by as detailed factors as eating habits, future work preference or location choice.</p>\n\n<p>This helps ensure that you tick all the boxes when selecting the best match to spend your life with. The huge range of categories helps make certain that both you and your family are happy with your selection. The membership to the site allows you to know a prospect well before you even meet him/her for the first time. Having basic background information also helps you screen out your choices better and thus gain full control over your preferred lie partner.</p>\n\n<h2><strong>Why MilanRishta?&nbsp;</strong></h2>\n\n<p>With its well categorized listing and amazing ease of use, MilanRishta has rapidly emerged as an online portal providing exceptional quality matchmaking services targeted exclusively for you. We have a proven record of touching lives for the better for two families, through our interactive site.Our servicing excellence ensures that we are available.</p>\n', 1),
(2, 'FAQ', 'We strive to provide a unique matchmaking experience to our site visitors, hence request \n\nyou to read terms of use carefully. Users continuing to use our services will be deemed to \n\nhave read, understood and expressly agreed to all points under the terms of use\n\n1. You must be above 18 years to be eligible to sign up for our services\n\n\n2. The exchange of profile and information doesn’t amount to acceptance of marriage. \n\nMilanRishta is not responsible for any content or images posted on site and any \n\naction arising out of communication between two or more profiles. \n\n3. MilanRishta reserves the right to delete obscene/ pornographic/ abusive/ \n\ndefamatory or harassing. Further, we have the right to take strict legal action against \n\nperpetrators. \n\n4. MilanRishta reserves the right to take action against those who engage in \n\ncommercial sales/ activity or transmit spam, junk, chain letters or other forms of \n\nunsolicited mass mailing.\n\n5. You will not hold MilanRishta responsible for any incorrect/false or inaccurate \n\ninformation or any delay, defect, transmission issues or technical malfunction. \n\n6. In the event of termination of services, there will be no refund of any kind. \n\nMilanRishta can terminate your account after communicating with your on \n\nregistered email address.\n\n7. You cannot post, distribute, or reproduce in any way any copyrighted material, \n\ntrademarks, or other proprietary information without obtaining the prior written \n\nconsent of the owner of such proprietary rights.\n\n8. You agree to indemnify and hold MilanRishta fully harmless from any kind of loss, \n\nclaim, liability, or demand either for self or on behalf of third-parties. This indemnity \n\nextends to all partners, affiliates, agents, officers or employees of MilanRishta\n\n9. MilanRishta is not responsible for any non-acceptance of any kind of proposal to \n\nanother profile. We will not be under any circumstances held responsible for any \n\ndelays or rejection in search for marriage partner.\n\n10.  MilanRishta is just a personal matrimonial platform and is not responsible for any \n\nstep beyond facilitating the meeting of two matching profiles.\n\n11. If at any point of time, MilanRishta comes to know that any information posted by \n\nyou in incomplete, falsified or incorrect, we reserve the right to suspend the account \n\nimmediately without prior notification. \n\n12. MilanRishta gives you limited license to access and use the content of the site. You \n\ncannot download any content (except by cookies or caching) or modify any portion \n\nexcept with written approval from MilanRishta.\n\n13. The content, elements or portion of the site may not be sold, resold, duplicated, \n\ncopied or reproduced in any way\n\n14. MilanRishta cannot confirm that its servers, networks and hardware will be always \n\nfunctioning or software will never have viruses. In case of such unfortunate \n\nscenarios of downtime of virus attack, MilanRishta cannot be held responsible in \n\nany manner  \n\n15. MilanRishta advises its members to behave with maturity and respect when dealing \n\nwith other profiles over interest, chat or telephone. They need to exercise all due \n\ndiligence before agreeing to meet or chat with another profile. MilanRishta will not \n\nbe responsible for any loss or harm arising due to non-adherence to due diligence.', 1),
(3, 'Terms and Conditions ', '<p>We strive to provide a unique matchmaking experience to our site visitors, hence request you to read terms of use carefully. Users continuing to use our services will be deemed to have read, understood and expressly agreed to all points under the terms of use.</p>\n\n<p><strong>1</strong>. You must be above 18 years to be eligible to sign up for our services.</p>\n\n<p><strong>2</strong>. The exchange of profile and information doesn&rsquo;t amount to acceptance of marriage. MilanRishta is not responsible for any content or images posted on site and any action arising out of communication between two or more profiles.</p>\n\n<p><strong>3</strong>. MilanRishta reserves the right to delete obscene/ pornographic/ abusive/ defamatory or harassing. Further, we have the right to take strict legal action against perpetrators.</p>\n\n<p><strong>4</strong>. MilanRishta reserves the right to take action against those who engage in commercial sales/ activity or transmit spam, junk, chain letters or other forms of unsolicited mass mailing.</p>\n\n<p><strong>5</strong>. You will not hold MilanRishta responsible for any incorrect/false or inaccurate information or any delay, defect, transmission issues or technical malfunction.</p>\n\n<p><strong>6</strong>. In the event of termination of services, there will be no refund of any kind. MilanRishta can terminate your account after communicating with your on registered email address.</p>\n\n<p><strong>7</strong>. You cannot post, distribute, or reproduce in any way any copyrighted material, trademarks, or other proprietary information without obtaining the prior written consent of the owner of such proprietary rights.</p>\n\n<p>8. You agree to indemnify and hold MilanRishta fully harmless from any kind of loss, claim, liability, or demand either for self or on behalf of third-parties. This indemnity extends to all partners, affiliates, agents, officers or employees of MilanRishta</p>\n\n<p><strong>9. </strong>MilanRishta is not responsible for any non-acceptance of any kind of proposal to another profile. We will not be under any circumstances held responsible for any delays or rejection in search for marriage partner.</p>\n\n<p><strong>10</strong>. MilanRishta is just a personal matrimonial platform and is not responsible for any step beyond facilitating the meeting of two matching profiles.</p>\n\n<p><strong>11. </strong>If at any point of time, MilanRishta comes to know that any information posted by you in incomplete, falsified or incorrect, we reserve the right to suspend the account immediately without prior notification.</p>\n\n<p><strong>12.</strong> MilanRishta gives you limited license to access and use the content of the site. You cannot download any content (except by cookies or caching) or modify any portion except with written approval from MilanRishta.</p>\n\n<p><strong>13.</strong> The content, elements or portion of the site may not be sold, resold, duplicated, copied or reproduced in any way</p>\n\n<p><strong>14. </strong>MilanRishta cannot confirm that its servers, networks and hardware will be always functioning or software will never have viruses. In case of such unfortunate scenarios of downtime of virus attack, MilanRishta cannot be held responsible in any manner</p>\n\n<p><strong>15. </strong>MilanRishta advises its members to behave with maturity and respect when dealing with other profiles over interest, chat or telephone. They need to exercise all due diligence before agreeing to meet or chat with another profile. MilanRishta will not be responsible for any loss or harm arising due to non-adherence to due diligence.</p>\n', 1),
(4, 'Blog', '<h1><strong>test blog</strong></h1>\n', 1),
(5, 'test', '<p>hello</p>\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `partnerprofile`
--

CREATE TABLE `partnerprofile` (
  `PartnerProfile_Id` int(11) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `AgeFrom` int(11) NOT NULL,
  `AgeTo` int(11) NOT NULL,
  `HeightFrom` varchar(12) NOT NULL,
  `HeightTo` varchar(100) NOT NULL,
  `MaritalStatus` varchar(100) NOT NULL DEFAULT 'Never married',
  `Religion` varchar(110) NOT NULL,
  `MotherTongue` varchar(200) NOT NULL,
  `Community` varchar(110) NOT NULL,
  `ProfileCreatedby` varchar(100) NOT NULL DEFAULT 'self',
  `CountryLivingIn` varchar(100) NOT NULL,
  `LivingState` varchar(200) NOT NULL,
  `LivingCity` varchar(200) NOT NULL,
  `ResidencyStatus` varchar(100) NOT NULL,
  `CountryGrewUp` varchar(110) NOT NULL,
  `Education` varchar(110) NOT NULL,
  `Education_Field` varchar(100) NOT NULL,
  `WorkingWith` varchar(110) NOT NULL,
  `ProfessionArea` varchar(110) NOT NULL,
  `AnualIncome` varchar(100) NOT NULL,
  `Diet` varchar(100) NOT NULL,
  `Dosham` varchar(100) NOT NULL,
  `Smoke` varchar(100) NOT NULL DEFAULT '2',
  `Drink` varchar(100) NOT NULL DEFAULT '2',
  `BodyType` varchar(100) NOT NULL,
  `SkinTone` varchar(100) NOT NULL,
  `Disability` varchar(100) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partnerprofile`
--

INSERT INTO `partnerprofile` (`PartnerProfile_Id`, `User_Id`, `AgeFrom`, `AgeTo`, `HeightFrom`, `HeightTo`, `MaritalStatus`, `Religion`, `MotherTongue`, `Community`, `ProfileCreatedby`, `CountryLivingIn`, `LivingState`, `LivingCity`, `ResidencyStatus`, `CountryGrewUp`, `Education`, `Education_Field`, `WorkingWith`, `ProfessionArea`, `AnualIncome`, `Diet`, `Dosham`, `Smoke`, `Drink`, `BodyType`, `SkinTone`, `Disability`) VALUES
(252, 451, 24, 26, '65', '69', 'Never married', '3', '3', '28', 'self', '1', '39', '217', '', '1', '2', '28', '2', '13', 'INR 10 Lakh to 15 Lakh', '', '2', '0', '0', '', '', '0'),
(253, 452, 25, 30, '68', '79', 'Never married', '3', '3', '28', 'self', '1', '39', '', '', '1', '2', '28', '', '13', '', '', '2', '0', '0', '', '', '0'),
(256, 456, 20, 23, '59', '64', 'Never married', '1', '5', '171', 'self', '1', '30', '45', '', '1', '3', '28', '2', '14', 'INR 2 Lakh to 4 Lakh', 'Non veg', '1,2', '2', '2', 'average', '', '2'),
(257, 455, 21, 28, '62', '68', 'Never married', '1', '2,3,5', '', 'self', '1', '', '', '', '1', '', '', '', '', '', '', '2', '0', '0', '', '', '0'),
(258, 457, 20, 25, '58', '65', 'Never married', '2', '16', '15', 'self', '1', '31', '69', '', '1', '3', '28', '2', '8', 'INR 2 Lakh to 4 Lakh', 'Non veg', '1,2', '2', '2', 'average', '', '2'),
(259, 458, 27, 35, '68', '80', 'Never married', '', '', '', 'self', '', '', '', '', '', '', '', '', '', '', '', '2', '0', '0', '', '', '0'),
(260, 459, 22, 23, '64', '65', 'Never married', '1', '12', '195', 'self', '1', '32', '108', '', '1', '2', '24', '', '8', 'INR 2 Lakh to 4 Lakh', 'veg', '1', '2', '2', 'average', '', '2'),
(261, 460, 22, 29, '63', '67', 'Never married', '1', '5', '170', 'self', '1', '33', '127', '', '1', '5', '26', '2', '10', 'INR 2 Lakh to 4 Lakh', 'Non veg', '2', '2', '2', 'average', '', '2'),
(262, 461, 22, 26, '', '', 'Never married', '', '', '', 'self', '', '', '', '', '', '', '', '', '', '', '', '2', '0', '0', '', '', '0'),
(263, 462, 20, 27, '60', '65', 'Never married', '2', '16', '17', 'self', '1', '36', '186', '', '1', '3', '28', '2', '10', 'INR 2 Lakh to 4 Lakh', 'Non veg', '2', '2', '2', 'average', '', '2'),
(264, 463, 25, 30, '', '', 'Never married,Divorced,Awaiting_Divorced', '', '', '', 'self', '', '', '', '', '', '', '', '', '', '', '', '2', '0', '0', '', '', '0'),
(265, 464, 22, 28, '62', '74', 'Never married', '', '', '', 'self', '', '', '', '', '', '', '', '', '', '', '', '2', '0', '0', '', '', '0'),
(266, 465, 24, 25, '62', '63', 'Never married', '1', '12', '195', 'self', '1', '48', '412', '', '1', '2', '30', '2', '11', 'INR 4 Lakh to 7 Lakh', 'veg', '1,2', '2', '2', 'average', '', '2'),
(267, 468, 23, 27, '63', '66', 'Never married', '1', '4', '170', 'self', '1', '34', '136', '', '1', '3', '28', '2', '12', 'INR 2 Lakh to 4 Lakh', 'veg', '2', '2', '2', 'average', '', '2');

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo`
--

CREATE TABLE `paymentinfo` (
  `Payment_Id` bigint(22) NOT NULL,
  `firstname` varchar(330) NOT NULL,
  `lastname` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `country` int(11) NOT NULL,
  `state` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `Service_Id` int(11) NOT NULL,
  `User_Id` bigint(33) NOT NULL,
  `service_provider` varchar(200) NOT NULL,
  `furl` varchar(400) NOT NULL,
  `surl` varchar(400) NOT NULL,
  `amount` int(11) NOT NULL,
  `txnid` varchar(300) NOT NULL,
  `hash` varchar(300) NOT NULL,
  `key` varchar(300) NOT NULL,
  `mihpayid` varchar(100) NOT NULL,
  `mode` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `PG_TYPE` varchar(100) NOT NULL,
  `bank_ref_num` varchar(100) NOT NULL,
  `bankcode` varchar(100) NOT NULL,
  `error` varchar(100) NOT NULL,
  `error_Message` text NOT NULL,
  `payuMoneyId` varchar(100) NOT NULL,
  `net_amount_debit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paymentinfo`
--

INSERT INTO `paymentinfo` (`Payment_Id`, `firstname`, `lastname`, `email`, `phone`, `country`, `state`, `city`, `zipcode`, `Service_Id`, `User_Id`, `service_provider`, `furl`, `surl`, `amount`, `txnid`, `hash`, `key`, `mihpayid`, `mode`, `status`, `PG_TYPE`, `bank_ref_num`, `bankcode`, `error`, `error_Message`, `payuMoneyId`, `net_amount_debit`) VALUES
(4, 'vijetha', 'l', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 100, '7a5df6356e8292ff0c80', '', 'JBZaLc', '403993715512440896', 'CC', 'success', 'HDFCPG', '2504029092251721', 'CC', 'E000', 'No Error', '1110237205', 100),
(5, 'vijetha', 'patil', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 2200, '4444adbfa080d96d5e08', '', 'JBZaLc', '403993715512447656', 'CC', 'success', 'HDFCPG', '6959311011651731', 'CC', 'E000', 'No Error', '1110238439', 2200),
(6, 'pavan', 'patil', 'pavanpatil196@gmail.com', '', 0, '', '', 0, 4, 180, 'payu_paisa', '', '', 1300, '4a20c8ebc30e65a63baf', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(7, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 2200, '36026aedd4fdaaee0c42', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(8, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 2, 68, 'payu_paisa', '', '', 3400, '594abadc0b8e9ece5d97', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(9, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 450, '6d7886b724b06aefa384', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(10, 'Sudip ', 'kumar', 'vimal.joseph05@gmail.com', '', 0, '', '', 0, 1, 238, 'payu_paisa', '', '', 450, '657e20cc6c95a34d292f', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(11, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 450, 'bca8c816dc5d67b9a6af', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(12, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 450, '62c6db4f87dc6d92c399', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(13, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 450, '745f76f20e758be0fea6', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(14, 'vijetha', '', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 450, '00225e4e8a5cfa434c3e', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(15, 'vijetha', '', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 450, '00225e4e8a5cfa434c3e', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(16, 'vijetha', '', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 450, '78f8c78fdeccd5f17967', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(17, 'vijetha', '', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 450, '0509bc6fc77e9f929aa3', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(18, 'vijetha', '', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 450, 'c56078e6be6c1bc7838c', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(19, 'vijetha', '', 'lvijetha90@gmail.com', '', 0, '', '', 0, 1, 28, 'payu_paisa', '', '', 450, '7b8208da8269aba564f5', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(20, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 450, 'ccefeb6b2893919758cd', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(21, 'Sudip ', 'kumar', 'vimal.joseph05@gmail.com', '', 0, '', '', 0, 1, 238, 'payu_paisa', '', '', 450, 'f17627e9a4425ac94465', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(22, 'Priya', 'Gowda', 'premaviva@gmail.com', '', 0, '', '', 0, 1, 334, 'PayTm', '', '', 450, 'e04abc6dc58f9ebbc5ac', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(23, 'Mohammed ', 'Tabrez', 'mtabrez18@yahoo.in', '', 0, '', '', 0, 1, 68, 'payu_paisa', '', '', 450, 'dc8b80658424181f34e8', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(24, 'Chandresh', 'Bala', 'bala_s_murthy@yahoo.com', '', 0, '', '', 0, 1, 335, 'payu_paisa', '', '', 450, '7d8f73be7e6691d87b3c', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0),
(25, 'Chandresh', 'Bala', 'bala_s_murthy@yahoo.com', '', 0, '', '', 0, 1, 335, 'payu_paisa', '', '', 450, '5db07e73b854d4943215', '', 'JBZaLc', '', '', '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profilepictures`
--

CREATE TABLE `profilepictures` (
  `Id` bigint(33) NOT NULL,
  `User_Id` bigint(33) NOT NULL,
  `ProfilePic` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepictures`
--

INSERT INTO `profilepictures` (`Id`, `User_Id`, `ProfilePic`) VALUES
(1, 180, '1929061041pavan.jpg'),
(2, 180, '780363082images.jpg'),
(3, 168, '2127336709DSC04155.JPG'),
(4, 68, '730776870251172_396335050447222_1004965265_n.jpg'),
(5, 74, '102093004133eae6f (1).png'),
(6, 341, '1285817105image.jpg'),
(7, 341, '621898981image.jpg'),
(8, 20, '2057573705images.jpg'),
(9, 20, '409545382BigPerl-Solutions-pvt.-ltd.-Hiring-Freshers978.jpg'),
(10, 20, '1487734861New Doc_1.jpg'),
(11, 359, '377120693_MG_4232(A).jpg'),
(12, 359, '84585172510620333_653065241468993_7631860195742133649_o.jpg'),
(16, 20, '1510154594webstore.png'),
(17, 68, '115252161320140601_142123-1-1.jpg'),
(18, 370, '673767327IMG-20140927-WA0003.jpg'),
(19, 401, '956710636BeautyPlus_20150406170546_save.jpg'),
(20, 401, '914744719BeautyPlus_20150406163116_save.jpg'),
(21, 401, '61709040BeautyPlus_20150406163203_save.jpg'),
(22, 401, '2069929597BeautyPlus_20150404191156_save.jpg'),
(23, 28, '923193750Koala.jpg'),
(24, 28, '2093154464Penguins.jpg'),
(28, 430, '321234711images (1).jpg'),
(30, 447, '1575174759adult-beautiful-bouquet-566454.jpg'),
(31, 447, '1556573904Portland-South-Indian-Wedding-001.jpg'),
(32, 447, '989825305hand-holding-1404623_1920.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `recentvisitors`
--

CREATE TABLE `recentvisitors` (
  `Id` bigint(33) NOT NULL,
  `User_Id` bigint(33) NOT NULL,
  `Visitor_Id` bigint(33) NOT NULL,
  `Visit_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recentvisitors`
--

INSERT INTO `recentvisitors` (`Id`, `User_Id`, `Visitor_Id`, `Visit_date`) VALUES
(1, 40, 28, '2015-04-10'),
(2, 28, 40, '2015-04-01'),
(3, 28, 20, '0000-00-00'),
(4, 51, 137, '2015-04-12'),
(5, 87, 146, '2015-04-12'),
(6, 108, 80, '2015-04-13'),
(7, 77, 68, '2015-04-13'),
(8, 64, 77, '2015-04-13'),
(9, 70, 104, '2015-04-13'),
(10, 138, 147, '2015-04-14'),
(11, 147, 62, '2015-04-16'),
(12, 92, 154, '2015-04-16'),
(13, 85, 155, '2015-04-16'),
(14, 116, 156, '2015-04-16'),
(15, 142, 157, '2015-04-16'),
(16, 51, 158, '2015-04-16'),
(17, 85, 159, '2015-04-16'),
(18, 51, 74, '2015-04-20'),
(19, 165, 168, '2015-04-21'),
(20, 167, 166, '2015-04-22'),
(21, 168, 164, '2015-04-22'),
(22, 143, 185, '2015-04-23'),
(23, 154, 161, '2015-04-24'),
(24, 71, 73, '2015-04-25'),
(25, 51, 196, '2015-04-25'),
(26, 189, 75, '2015-04-27'),
(27, 51, 180, '2015-04-27'),
(28, 168, 203, '2015-04-27'),
(29, 203, 217, '2015-05-01'),
(30, 164, 220, '2015-05-01'),
(31, 77, 229, '2015-05-06'),
(32, 227, 234, '2015-05-07'),
(33, 82, 241, '2015-05-11'),
(34, 244, 246, '2015-05-12'),
(35, 246, 237, '2015-05-12'),
(36, 71, 248, '2015-05-13'),
(37, 187, 251, '2015-05-14'),
(38, 246, 254, '2015-05-15'),
(39, 77, 256, '2015-05-15'),
(40, 68, 258, '2015-05-17'),
(41, 216, 260, '2015-05-18'),
(42, 75, 189, '2015-05-20'),
(43, 164, 265, '2015-05-20'),
(44, 195, 266, '2015-05-21'),
(45, 0, 51, '2015-05-29'),
(46, 95, 294, '2015-06-13'),
(47, 68, 295, '2015-06-13'),
(48, 20, 314, '2015-07-13'),
(49, 29, 331, '2015-07-16'),
(50, 51, 333, '2015-07-19'),
(51, 195, 343, '2015-08-01'),
(52, 195, 357, '2015-08-08'),
(53, 350, 361, '2015-08-13'),
(54, 216, 362, '2015-08-13'),
(55, 68, 363, '2015-08-13'),
(56, 325, 365, '2015-08-13'),
(57, 203, 364, '2015-08-13'),
(58, 203, 370, '2015-08-19'),
(59, 363, 371, '2015-08-20'),
(60, 344, 374, '2015-08-20'),
(61, 344, 375, '2015-08-20'),
(62, 371, 376, '2015-08-20'),
(63, 344, 245, '2015-08-21'),
(64, 260, 386, '2015-08-21'),
(65, 223, 388, '2015-08-22'),
(66, 370, 389, '2015-08-22'),
(67, 28, 398, '2015-08-22'),
(68, 311, 402, '2015-08-23'),
(69, 254, 406, '2015-08-24'),
(70, 203, 427, '2015-08-28'),
(71, 164, 430, '2019-07-04'),
(72, 430, 440, '2019-07-05'),
(73, 362, 441, '2019-07-05'),
(74, 432, 448, '2019-07-10'),
(75, 449, 450, '2019-07-10'),
(76, 450, 449, '2019-07-10'),
(77, 452, 451, '2019-07-12'),
(78, 448, 447, '2019-07-12'),
(79, 460, 457, '2019-07-17'),
(80, 452, 456, '2019-07-18'),
(81, 464, 459, '2019-07-18'),
(82, 459, 465, '2019-07-18'),
(83, 457, 468, '2019-07-19');

-- --------------------------------------------------------

--
-- Table structure for table `religionlist`
--

CREATE TABLE `religionlist` (
  `Religion_Id` int(11) NOT NULL,
  `Religion_Name` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religionlist`
--

INSERT INTO `religionlist` (`Religion_Id`, `Religion_Name`, `status`) VALUES
(1, 'Hindu', 1),
(2, 'Islam', 1),
(3, 'Christian', 1),
(6, 'Nadar', 1),
(7, 'Sikh', 1),
(8, 'Parsi', 1),
(9, 'Jain', 1),
(10, 'Jewish', 1),
(11, 'Buddist', 1),
(12, 'Others', 1),
(13, 'No Religion', 1),
(14, 'Spirutual', 1),
(15, 'testreligiona', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rolematrix`
--

CREATE TABLE `rolematrix` (
  `Role_Id` bigint(20) NOT NULL,
  `User_Id` bigint(20) NOT NULL,
  `Role` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolematrix`
--

INSERT INTO `rolematrix` (`Role_Id`, `User_Id`, `Role`) VALUES
(0, 4, ''),
(0, 5, ''),
(0, 6, ''),
(0, 7, ''),
(0, 8, ''),
(0, 9, ''),
(0, 10, ''),
(0, 11, ''),
(0, 29, ''),
(0, 30, ''),
(0, 31, ''),
(0, 32, ''),
(0, 33, ''),
(0, 34, ''),
(0, 35, ''),
(0, 36, ''),
(0, 37, ''),
(0, 38, ''),
(0, 39, ''),
(0, 40, ''),
(0, 41, ''),
(0, 42, ''),
(0, 43, ''),
(0, 44, ''),
(0, 45, ''),
(0, 46, ''),
(0, 47, ''),
(0, 48, ''),
(0, 49, '');

-- --------------------------------------------------------

--
-- Table structure for table `servicetypes`
--

CREATE TABLE `servicetypes` (
  `Service_Id` int(11) NOT NULL,
  `Service_name` varchar(200) NOT NULL,
  `Service_Cost` int(11) NOT NULL,
  `Service_Discount` int(11) NOT NULL,
  `Service_PayAmount` int(11) NOT NULL,
  `Service_Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicetypes`
--

INSERT INTO `servicetypes` (`Service_Id`, `Service_name`, `Service_Cost`, `Service_Discount`, `Service_PayAmount`, `Service_Description`) VALUES
(1, 'Premium Plus 1 Week Membership', 450, 0, 450, 'Connect directly with Matches, View detailed Profile information, Be featured under Spotlight, Standout with Bold Listing'),
(2, 'Premium Plus 1 Month Membership', 1500, 0, 1500, 'Connect directly with Matches, View detailed Profile information, Be featured under Spotlight, Standout with Bold Listing'),
(3, 'Premium Plus 3 Months Membership', 2500, 0, 2500, 'Connect directly with Matches, View detailed Profile information, Be featured under Spotlight, Standout with Bold Listing'),
(4, 'Premium Plus 6 Months Membership', 3800, 0, 3800, 'Connect directly with Matches, View detailed Profile information'),
(5, 'Premium Plus 12 Months Membership', 5200, 0, 5200, 'Connect directly with Matches, View detailed Profile information'),
(6, 'Personalized Match Making 3 Months membership', 6000, 0, 6000, 'Connect directly with Matches, View detailed Profile information\nHandpicked Matches  \nSending profiles by Mails  \nQuicker Responses from Prospects  \nPersonal Introductions & Meetings  '),
(7, 'Personalized Match Making 6 Months membership', 10000, 0, 10000, 'Handpicked Matches  \r\nSending profiles by Mails  \r\nQuicker Responses from Prospects  \r\nPersonal Introductions & Meetings  '),
(8, 'Personalized Match Making 12 Months membership', 20000, 0, 20000, 'Handpicked Matches  \r\nSending profiles by Mails  \r\nQuicker Responses from Prospects  \r\nPersonal Introductions & Meetings  ');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `Id` bigint(33) NOT NULL,
  `User_Id` bigint(33) NOT NULL,
  `NameSetting` int(11) NOT NULL DEFAULT '3',
  `PhoneSetting` int(11) NOT NULL DEFAULT '11',
  `PhotoSetting` int(11) NOT NULL DEFAULT '10',
  `HoroscopeSetting` int(11) NOT NULL DEFAULT '12',
  `ProfilePrivacy` int(11) NOT NULL DEFAULT '11'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`Id`, `User_Id`, `NameSetting`, `PhoneSetting`, `PhotoSetting`, `HoroscopeSetting`, `ProfilePrivacy`) VALUES
(1, 20, 2, 5, 10, 11, 10),
(2, 24, 3, 11, 10, 12, 11),
(3, 25, 3, 11, 10, 12, 11),
(4, 28, 3, 11, 10, 10, 10),
(5, 29, 3, 11, 10, 12, 11),
(6, 40, 3, 11, 10, 12, 11),
(7, 43, 3, 11, 10, 12, 11),
(8, 44, 3, 11, 10, 12, 11),
(9, 48, 3, 11, 10, 12, 11),
(10, 50, 3, 11, 10, 12, 11),
(11, 51, 3, 11, 10, 12, 11),
(12, 52, 3, 11, 10, 12, 11),
(13, 61, 3, 11, 10, 12, 11),
(14, 62, 3, 11, 10, 12, 11),
(15, 65, 3, 11, 10, 12, 11),
(16, 66, 3, 11, 10, 12, 11),
(17, 67, 3, 11, 10, 12, 11),
(18, 68, 3, 5, 10, 11, 11),
(19, 69, 3, 11, 10, 12, 11),
(20, 70, 3, 11, 10, 12, 11),
(21, 71, 3, 11, 10, 12, 11),
(22, 72, 3, 11, 10, 12, 11),
(23, 73, 3, 11, 10, 12, 11),
(24, 74, 3, 11, 10, 12, 11),
(25, 75, 3, 11, 10, 12, 11),
(26, 76, 3, 11, 10, 12, 11),
(27, 77, 3, 11, 10, 12, 11),
(28, 78, 3, 11, 10, 12, 11),
(29, 79, 3, 11, 10, 12, 11),
(30, 82, 3, 11, 10, 12, 11),
(31, 84, 3, 11, 10, 12, 11),
(32, 85, 3, 11, 10, 12, 11),
(33, 86, 3, 11, 10, 12, 11),
(34, 87, 3, 11, 10, 12, 11),
(35, 88, 3, 11, 10, 12, 11),
(36, 89, 3, 11, 10, 12, 11),
(37, 90, 3, 11, 10, 12, 11),
(38, 91, 3, 11, 10, 12, 11),
(39, 92, 3, 11, 10, 12, 11),
(40, 94, 3, 11, 10, 12, 11),
(41, 95, 3, 11, 10, 12, 11),
(42, 96, 3, 11, 10, 12, 11),
(43, 97, 3, 11, 10, 12, 11),
(44, 98, 3, 11, 10, 12, 11),
(45, 99, 3, 11, 10, 12, 11),
(46, 100, 3, 11, 10, 12, 11),
(47, 101, 3, 11, 10, 12, 11),
(48, 102, 3, 11, 10, 12, 11),
(49, 106, 3, 11, 10, 12, 11),
(50, 114, 3, 11, 10, 12, 11),
(51, 116, 3, 11, 10, 12, 11),
(52, 117, 3, 11, 10, 12, 11),
(53, 118, 3, 11, 10, 12, 11),
(54, 119, 3, 11, 10, 12, 11),
(55, 125, 3, 11, 10, 12, 11),
(56, 127, 3, 11, 10, 12, 11),
(57, 128, 3, 11, 10, 12, 11),
(58, 129, 3, 11, 10, 12, 11),
(59, 130, 3, 11, 10, 12, 11),
(60, 133, 3, 11, 10, 12, 11),
(61, 134, 3, 11, 10, 12, 11),
(62, 135, 3, 11, 10, 12, 11),
(63, 136, 3, 11, 10, 12, 11),
(64, 138, 3, 11, 10, 12, 11),
(65, 139, 3, 11, 10, 12, 11),
(66, 140, 3, 11, 10, 12, 11),
(67, 141, 3, 11, 10, 12, 11),
(68, 142, 3, 11, 10, 12, 11),
(69, 143, 3, 11, 10, 12, 11),
(70, 144, 3, 11, 10, 12, 11),
(71, 145, 3, 11, 10, 12, 11),
(72, 146, 3, 11, 10, 12, 11),
(73, 147, 3, 11, 10, 12, 11),
(74, 148, 3, 11, 10, 12, 11),
(75, 149, 3, 11, 10, 12, 11),
(76, 150, 3, 11, 10, 12, 11),
(77, 151, 3, 11, 10, 12, 11),
(78, 153, 3, 11, 10, 12, 11),
(79, 154, 3, 11, 10, 12, 11),
(80, 155, 3, 11, 10, 12, 11),
(81, 156, 3, 11, 10, 12, 11),
(82, 157, 3, 11, 10, 12, 11),
(83, 158, 3, 11, 10, 12, 11),
(84, 159, 3, 11, 10, 12, 11),
(85, 160, 3, 11, 10, 12, 11),
(86, 161, 3, 11, 10, 12, 11),
(87, 162, 3, 11, 10, 12, 11),
(88, 163, 3, 11, 10, 12, 11),
(89, 164, 3, 11, 10, 12, 11),
(90, 165, 3, 11, 10, 12, 11),
(91, 166, 3, 11, 10, 12, 11),
(92, 167, 3, 11, 10, 12, 11),
(93, 168, 3, 11, 10, 12, 11),
(94, 169, 3, 11, 10, 12, 11),
(95, 170, 3, 11, 10, 12, 11),
(96, 171, 3, 11, 10, 12, 11),
(97, 172, 3, 11, 10, 12, 11),
(98, 173, 3, 11, 10, 12, 11),
(99, 175, 3, 11, 10, 12, 11),
(100, 176, 3, 11, 10, 12, 11),
(101, 177, 3, 11, 10, 12, 11),
(102, 178, 3, 11, 10, 12, 11),
(103, 179, 3, 11, 10, 12, 11),
(104, 180, 3, 11, 10, 0, 0),
(105, 181, 3, 11, 10, 12, 11),
(106, 182, 3, 11, 10, 12, 11),
(107, 183, 3, 11, 10, 12, 11),
(108, 184, 3, 11, 10, 12, 11),
(109, 185, 3, 11, 10, 12, 11),
(110, 186, 3, 11, 10, 12, 11),
(111, 187, 3, 11, 10, 12, 11),
(112, 188, 3, 11, 10, 12, 11),
(113, 189, 3, 11, 10, 12, 11),
(114, 190, 3, 11, 10, 12, 11),
(115, 191, 3, 11, 10, 12, 11),
(116, 193, 3, 11, 10, 12, 11),
(117, 194, 3, 11, 10, 12, 11),
(118, 195, 3, 11, 10, 12, 11),
(119, 196, 3, 11, 10, 12, 11),
(120, 197, 3, 11, 10, 12, 11),
(121, 198, 3, 11, 10, 12, 11),
(122, 199, 3, 11, 10, 12, 11),
(123, 200, 3, 11, 10, 12, 11),
(124, 201, 3, 11, 10, 12, 11),
(125, 202, 3, 11, 10, 12, 11),
(126, 203, 3, 11, 10, 12, 11),
(127, 204, 3, 11, 10, 12, 11),
(128, 205, 3, 11, 10, 12, 11),
(129, 206, 3, 11, 10, 12, 11),
(130, 207, 3, 11, 10, 12, 11),
(131, 208, 3, 11, 10, 12, 11),
(132, 209, 3, 11, 10, 12, 11),
(133, 210, 3, 11, 10, 12, 11),
(134, 211, 3, 11, 10, 12, 11),
(135, 212, 3, 11, 10, 12, 11),
(136, 213, 3, 11, 10, 12, 11),
(137, 214, 3, 11, 10, 12, 11),
(138, 215, 3, 11, 10, 12, 11),
(139, 216, 3, 11, 10, 12, 11),
(140, 217, 3, 11, 10, 12, 11),
(141, 218, 3, 11, 10, 12, 11),
(142, 219, 3, 11, 10, 12, 11),
(143, 220, 3, 11, 10, 12, 11),
(144, 221, 3, 11, 10, 12, 11),
(145, 222, 3, 11, 10, 12, 11),
(146, 223, 3, 11, 10, 12, 11),
(147, 224, 3, 11, 10, 12, 11),
(148, 225, 3, 11, 10, 12, 11),
(149, 226, 3, 11, 10, 12, 11),
(150, 227, 3, 11, 10, 12, 11),
(151, 228, 3, 11, 10, 12, 11),
(152, 229, 3, 11, 10, 12, 11),
(153, 230, 3, 11, 10, 12, 11),
(154, 231, 3, 11, 10, 12, 11),
(155, 232, 3, 11, 10, 12, 11),
(156, 233, 3, 11, 10, 12, 11),
(157, 234, 3, 11, 10, 12, 11),
(158, 235, 3, 11, 10, 12, 11),
(159, 236, 3, 11, 10, 12, 11),
(160, 237, 3, 11, 10, 12, 11),
(161, 238, 3, 11, 10, 12, 11),
(162, 240, 3, 11, 10, 12, 11),
(163, 241, 3, 11, 10, 12, 11),
(164, 242, 3, 11, 10, 12, 11),
(165, 243, 3, 11, 10, 12, 11),
(166, 244, 3, 11, 10, 12, 11),
(167, 245, 3, 11, 10, 12, 11),
(168, 246, 3, 11, 10, 12, 11),
(169, 247, 3, 11, 10, 12, 11),
(170, 248, 3, 11, 10, 12, 11),
(171, 249, 3, 11, 10, 12, 11),
(172, 250, 3, 11, 10, 12, 11),
(173, 251, 3, 11, 10, 12, 11),
(174, 252, 3, 11, 10, 12, 11),
(175, 253, 3, 11, 10, 12, 11),
(176, 254, 3, 11, 10, 12, 11),
(177, 255, 3, 11, 10, 12, 11),
(178, 256, 3, 11, 10, 12, 11),
(179, 257, 3, 11, 10, 12, 11),
(180, 258, 3, 11, 10, 12, 11),
(181, 259, 3, 11, 10, 12, 11),
(182, 260, 3, 11, 10, 12, 11),
(183, 261, 3, 11, 10, 12, 11),
(184, 262, 3, 11, 10, 12, 11),
(185, 263, 3, 11, 10, 12, 11),
(186, 264, 3, 11, 10, 12, 11),
(187, 265, 3, 11, 10, 12, 11),
(188, 266, 3, 11, 10, 12, 11),
(189, 275, 3, 11, 10, 12, 11),
(190, 276, 3, 11, 10, 12, 11),
(191, 277, 3, 11, 10, 12, 11),
(192, 278, 3, 11, 10, 12, 11),
(193, 279, 3, 11, 10, 12, 11),
(194, 280, 3, 11, 10, 12, 11),
(195, 281, 3, 11, 10, 12, 11),
(196, 282, 3, 11, 10, 12, 11),
(197, 283, 3, 11, 10, 12, 11),
(198, 284, 3, 11, 10, 12, 11),
(199, 285, 3, 11, 10, 12, 11),
(200, 286, 3, 11, 10, 12, 11),
(201, 287, 3, 11, 10, 12, 11),
(202, 288, 3, 11, 10, 12, 11),
(203, 289, 3, 11, 10, 12, 11),
(204, 290, 3, 11, 10, 12, 11),
(205, 291, 3, 11, 10, 12, 11),
(206, 293, 3, 11, 10, 12, 11),
(207, 292, 3, 11, 10, 12, 11),
(208, 294, 3, 11, 10, 12, 11),
(209, 295, 3, 11, 10, 12, 11),
(210, 296, 3, 11, 10, 12, 11),
(211, 297, 3, 11, 10, 12, 11),
(212, 298, 3, 11, 10, 12, 11),
(213, 299, 3, 11, 10, 12, 11),
(214, 300, 3, 11, 10, 12, 11),
(215, 301, 3, 11, 10, 12, 11),
(216, 302, 3, 11, 10, 12, 11),
(217, 303, 3, 11, 10, 12, 11),
(218, 304, 3, 11, 10, 12, 11),
(219, 305, 3, 11, 10, 12, 11),
(220, 306, 3, 11, 10, 12, 11),
(221, 307, 3, 11, 10, 12, 11),
(222, 308, 3, 11, 10, 12, 11),
(223, 309, 3, 11, 10, 12, 11),
(224, 310, 3, 11, 10, 12, 11),
(225, 311, 3, 11, 10, 12, 11),
(226, 312, 3, 11, 10, 12, 11),
(227, 313, 3, 11, 10, 12, 11),
(228, 314, 3, 11, 10, 12, 11),
(229, 315, 3, 11, 10, 12, 11),
(230, 316, 1, 11, 10, 11, 11),
(231, 317, 3, 11, 10, 12, 11),
(232, 318, 3, 11, 10, 12, 11),
(233, 319, 3, 11, 10, 12, 11),
(234, 320, 3, 11, 10, 12, 11),
(235, 321, 3, 11, 10, 12, 11),
(236, 322, 3, 11, 10, 12, 11),
(237, 325, 3, 11, 10, 12, 11),
(238, 326, 3, 11, 10, 12, 11),
(239, 327, 3, 11, 10, 12, 11),
(240, 328, 4, 11, 10, 0, 0),
(241, 329, 3, 11, 10, 12, 11),
(242, 330, 3, 11, 10, 12, 11),
(243, 331, 3, 11, 10, 12, 11),
(244, 332, 3, 11, 10, 12, 11),
(245, 333, 3, 11, 10, 12, 11),
(246, 334, 3, 11, 10, 12, 11),
(247, 335, 3, 11, 10, 12, 11),
(248, 336, 3, 11, 10, 12, 11),
(249, 336, 3, 11, 10, 12, 11),
(250, 337, 3, 11, 10, 12, 11),
(251, 338, 3, 11, 10, 12, 11),
(252, 339, 3, 11, 10, 12, 11),
(253, 340, 3, 11, 10, 12, 11),
(254, 341, 3, 11, 10, 12, 11),
(255, 342, 3, 11, 10, 12, 11),
(256, 343, 3, 11, 10, 12, 11),
(257, 344, 3, 11, 10, 12, 11),
(258, 345, 3, 11, 10, 12, 11),
(259, 347, 3, 11, 10, 12, 11),
(260, 348, 3, 11, 10, 12, 11),
(261, 349, 3, 11, 10, 12, 11),
(262, 350, 3, 11, 10, 12, 11),
(263, 351, 3, 11, 10, 12, 11),
(264, 352, 3, 11, 10, 12, 11),
(265, 353, 3, 11, 10, 12, 11),
(266, 354, 3, 11, 10, 12, 11),
(267, 355, 3, 11, 10, 12, 11),
(268, 356, 3, 11, 10, 12, 11),
(269, 357, 3, 11, 10, 12, 11),
(270, 358, 3, 11, 10, 12, 11),
(271, 359, 3, 11, 10, 12, 11),
(272, 361, 3, 11, 10, 12, 11),
(273, 362, 3, 11, 10, 10, 10),
(274, 363, 3, 11, 10, 12, 11),
(275, 364, 3, 11, 10, 12, 11),
(276, 365, 3, 11, 10, 12, 11),
(277, 366, 3, 11, 10, 12, 11),
(278, 367, 3, 11, 10, 12, 11),
(279, 368, 3, 11, 10, 12, 11),
(280, 369, 3, 11, 10, 12, 11),
(281, 370, 3, 11, 10, 12, 11),
(282, 371, 3, 11, 10, 12, 11),
(283, 372, 3, 11, 10, 12, 11),
(284, 373, 3, 11, 10, 12, 11),
(285, 374, 3, 11, 10, 12, 11),
(286, 375, 3, 11, 10, 12, 11),
(287, 376, 3, 11, 10, 12, 11),
(288, 377, 3, 11, 10, 12, 11),
(289, 377, 3, 11, 10, 12, 11),
(290, 378, 3, 11, 10, 12, 11),
(291, 379, 3, 11, 10, 12, 11),
(292, 380, 3, 11, 10, 12, 11),
(293, 381, 3, 11, 10, 12, 11),
(294, 382, 3, 11, 10, 12, 11),
(295, 383, 3, 11, 10, 12, 11),
(296, 384, 3, 11, 10, 12, 11),
(297, 385, 3, 11, 10, 12, 11),
(298, 386, 3, 11, 10, 12, 11),
(299, 387, 3, 11, 10, 12, 11),
(300, 388, 3, 11, 10, 12, 11),
(301, 389, 2, 11, 10, 11, 11),
(302, 390, 3, 11, 10, 12, 11),
(303, 391, 3, 11, 10, 12, 11),
(304, 392, 3, 11, 10, 12, 11),
(305, 393, 3, 11, 10, 12, 11),
(306, 394, 3, 11, 10, 12, 11),
(307, 395, 3, 11, 10, 12, 11),
(308, 396, 3, 11, 10, 12, 11),
(309, 397, 3, 11, 10, 12, 11),
(310, 398, 3, 11, 10, 12, 11),
(311, 399, 3, 11, 10, 12, 11),
(312, 400, 3, 11, 10, 12, 11),
(313, 401, 3, 11, 10, 12, 11),
(314, 402, 3, 11, 10, 10, 11),
(315, 403, 3, 11, 10, 12, 11),
(316, 404, 3, 11, 10, 12, 11),
(317, 405, 3, 11, 10, 12, 11),
(318, 406, 3, 11, 10, 12, 11),
(319, 407, 3, 11, 10, 12, 11),
(320, 408, 3, 11, 10, 12, 11),
(321, 409, 3, 11, 10, 12, 11),
(322, 410, 3, 11, 10, 12, 11),
(323, 411, 3, 11, 10, 12, 11),
(324, 412, 3, 11, 10, 12, 11),
(325, 413, 3, 11, 10, 12, 11),
(326, 414, 3, 11, 10, 12, 11),
(327, 415, 3, 11, 10, 12, 11),
(328, 416, 3, 11, 10, 12, 11),
(329, 417, 3, 11, 10, 12, 11),
(330, 418, 3, 11, 10, 12, 11),
(331, 419, 3, 11, 10, 12, 11),
(332, 420, 3, 11, 10, 12, 11),
(333, 421, 3, 11, 10, 12, 11),
(334, 422, 3, 11, 10, 12, 11),
(335, 423, 3, 11, 10, 12, 11),
(336, 424, 3, 11, 10, 12, 11),
(337, 425, 3, 11, 10, 12, 11),
(338, 426, 3, 11, 10, 12, 11),
(339, 427, 3, 11, 10, 12, 11),
(340, 428, 3, 11, 10, 12, 11),
(341, 429, 3, 11, 10, 12, 11),
(342, 430, 3, 11, 10, 10, 11),
(343, 431, 3, 11, 10, 12, 11),
(344, 432, 3, 11, 10, 12, 11),
(345, 433, 3, 11, 10, 12, 11),
(346, 440, 3, 11, 10, 12, 11),
(347, 441, 3, 11, 10, 12, 11),
(348, 442, 3, 11, 10, 12, 11),
(349, 447, 3, 11, 10, 12, 11),
(350, 448, 3, 11, 10, 12, 11),
(351, 449, 3, 11, 10, 12, 11),
(352, 450, 3, 11, 10, 12, 11),
(353, 451, 3, 11, 10, 12, 11),
(354, 452, 3, 11, 10, 12, 11),
(355, 453, 3, 11, 10, 12, 11),
(356, 454, 3, 11, 10, 12, 11),
(357, 455, 3, 11, 10, 12, 11),
(358, 456, 3, 11, 10, 12, 11),
(359, 457, 3, 11, 10, 12, 11),
(360, 458, 3, 11, 10, 12, 11),
(361, 459, 3, 11, 10, 12, 11),
(362, 460, 3, 11, 10, 12, 11),
(363, 461, 3, 11, 10, 12, 11),
(364, 462, 3, 11, 10, 12, 11),
(365, 463, 3, 11, 10, 12, 11),
(366, 464, 3, 11, 10, 12, 11),
(367, 465, 3, 11, 10, 12, 11),
(368, 468, 3, 11, 10, 12, 11);

-- --------------------------------------------------------

--
-- Table structure for table `settingtype`
--

CREATE TABLE `settingtype` (
  `Setting_Id` int(11) NOT NULL,
  `SettingType` varchar(400) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settingtype`
--

INSERT INTO `settingtype` (`Setting_Id`, `SettingType`, `Description`) VALUES
(1, 'PartialName', 'Hide my last name'),
(2, 'PartialNameInverse', 'Hide my first name'),
(3, 'FullName', 'Displays my full name'),
(4, 'ProfileID', 'Hide my full name(Displays only Profile ID)'),
(5, 'VisibleToAll', 'Visible to all Premium Members'),
(8, 'PasswordProtected', 'Password protected'),
(10, 'VisibleToAll', 'Visible to all Members'),
(11, 'HideFromAll', 'Hide from all'),
(12, 'VisibleToSentAccepted', 'Visible only on Invitation Sent/Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `sliderimages`
--

CREATE TABLE `sliderimages` (
  `slider_id` int(11) NOT NULL,
  `slider_image` varchar(300) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliderimages`
--

INSERT INTO `sliderimages` (`slider_id`, `slider_image`, `status`) VALUES
(21, 'Indian-Wedding-1.jpg', 1),
(27, 'Indian_wedding_Delhi.jpg', 1),
(28, 'Florida-Best-indian-wedding-photographer-4.jpg', 1),
(35, 'hand-holding-1404623_1920.jpg', 1),
(45, 'adult-beautiful-bouquet-566454 (2).jpg', 1),
(48, 'affair-anniversary-beach-1024993.jpg', 1),
(49, 'attractive-bangladesh-beautiful-2106463.jpg', 1),
(50, 'beach-beach-wedding-chairs-169207.jpg', 1),
(52, 'adult-architecture-beautiful-949223 (1) - Copy.jpg', 1),
(53, 'ballroom-candles-candlesticks-265947 - Copy.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE `specialization` (
  `Id` int(11) NOT NULL,
  `Degree_Id` int(11) NOT NULL,
  `SName` varchar(100) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialization`
--

INSERT INTO `specialization` (`Id`, `Degree_Id`, `SName`, `Status`) VALUES
(22, 0, 'Advertising/Marketing', 1),
(23, 0, 'Administrative Services', 1),
(24, 0, 'Arhitecture', 1),
(25, 0, 'Armed Forces', 1),
(26, 0, 'Arts', 1),
(27, 0, 'Commerce', 1),
(28, 0, 'Computer/IT', 1),
(29, 0, 'Education', 1),
(30, 0, 'Engineering/Technology', 1),
(31, 0, 'Fashion', 1),
(32, 0, 'Finance ', 1),
(33, 0, 'Fine Arts', 1),
(34, 0, 'Homes Science', 1),
(35, 0, 'Law', 1),
(36, 0, 'Management', 1),
(37, 0, 'Medicines', 1),
(38, 0, 'Nursing / Health Science', 1),
(39, 0, 'Office Adminstrative ', 1),
(40, 0, 'Science ', 1),
(41, 0, 'Shipping', 1),
(42, 0, 'Travel / Tourism', 1);

-- --------------------------------------------------------

--
-- Table structure for table `statelist`
--

CREATE TABLE `statelist` (
  `State_Id` int(11) NOT NULL,
  `Country_Id` int(11) NOT NULL,
  `State_Name` varchar(200) NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statelist`
--

INSERT INTO `statelist` (`State_Id`, `Country_Id`, `State_Name`, `Status`) VALUES
(28, 1, 'Andra Pradesh', 1),
(29, 1, 'Arunachal Pradesh', 1),
(30, 1, 'Assam', 1),
(31, 1, 'Bihar', 1),
(32, 1, 'Chhattisgarh', 1),
(33, 1, 'Goa', 1),
(34, 1, 'Gujarat', 1),
(35, 1, 'Haryana', 1),
(36, 1, 'Himachal Pradesh', 1),
(37, 1, 'Jammu and Kashmir', 1),
(38, 1, 'Jharkhand', 1),
(39, 1, 'Karnataka', 1),
(40, 1, 'Kerala', 1),
(41, 1, 'Madya Pradesh', 1),
(42, 1, 'Maharashtra', 1),
(43, 1, 'Manipur', 1),
(44, 1, 'Meghalaya', 1),
(45, 1, 'Mizoram', 1),
(46, 1, 'Nagaland', 1),
(47, 1, 'Orissa', 1),
(48, 1, 'Punjab', 1),
(49, 1, 'Rajasthan', 1),
(50, 1, 'Sikkim', 1),
(51, 1, 'Tamil Nadu', 1),
(52, 1, 'Tripura', 1),
(53, 1, 'Uttaranchal', 1),
(54, 1, 'Uttar Pradesh', 1),
(55, 1, 'West Bengal', 1),
(56, 8, 'Azad Kashmir ', 1),
(57, 8, 'Baluchistan ', 1),
(58, 8, 'Fed. Tribal Areas ', 1),
(59, 8, 'North-West Frontier ', 1),
(60, 8, 'Northern Areas ', 1),
(61, 8, 'Punjab ', 1),
(62, 8, 'Sind', 1),
(63, 8, 'Tribal Areas ', 1),
(64, 8, 'Others', 1),
(65, 8, 'Islamabad', 1),
(67, 61, 'teststate', 1),
(68, 2, 'california', 1),
(69, 1, 'Delhi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcommunitylist`
--

CREATE TABLE `subcommunitylist` (
  `SubCommunityList_Id` int(11) NOT NULL,
  `Religion_Id` int(11) NOT NULL,
  `Community_Id` int(11) NOT NULL,
  `SubCommunity_Name` varchar(200) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcommunitylist`
--

INSERT INTO `subcommunitylist` (`SubCommunityList_Id`, `Religion_Id`, `Community_Id`, `SubCommunity_Name`, `Status`) VALUES
(1, 0, 0, 'adi ', 1),
(3, 0, 0, 'oc', 0),
(4, 15, 552, 'testsubcommunity', 1),
(5, 1, 553, 'abc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `msg_id` bigint(234) NOT NULL,
  `thread_id` bigint(234) NOT NULL,
  `sender_id` bigint(234) NOT NULL,
  `message` text NOT NULL,
  `created_date` datetime NOT NULL,
  `flag_seen` varchar(20) NOT NULL,
  `seen_on` datetime NOT NULL,
  `delete_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`msg_id`, `thread_id`, `sender_id`, `message`, `created_date`, `flag_seen`, `seen_on`, `delete_status`) VALUES
(2737154477, 1, 68, 'hi hru', '2015-08-23 19:57:34', '', '0000-00-00 00:00:00', 0),
(2837601613, 1, 68, 'hello', '2015-08-23 20:04:39', '', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message_thread`
--

CREATE TABLE `tbl_message_thread` (
  `thread_id` bigint(255) NOT NULL,
  `sender_id` bigint(255) NOT NULL,
  `receiver_id` bigint(255) NOT NULL,
  `created_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message_thread`
--

INSERT INTO `tbl_message_thread` (`thread_id`, `sender_id`, `receiver_id`, `created_time`) VALUES
(1, 68, 217, '2015-08-23 19:57:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_network`
--

CREATE TABLE `tbl_network` (
  `id` bigint(20) NOT NULL,
  `accepter_id` bigint(20) NOT NULL,
  `requester_id` bigint(20) NOT NULL,
  `status_info` int(11) DEFAULT '1',
  `creation_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_network`
--

INSERT INTO `tbl_network` (`id`, `accepter_id`, `requester_id`, `status_info`, `creation_time`) VALUES
(60, 20, 28, 1, '2015-03-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_online`
--

CREATE TABLE `tbl_users_online` (
  `id` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users_online`
--

INSERT INTO `tbl_users_online` (`id`, `userid`) VALUES
(4, 104),
(15, 104),
(16, 104),
(17, 104),
(23, 150),
(32, 145),
(33, 145),
(101, 114),
(102, 114),
(103, 172),
(104, 172),
(105, 172),
(106, 172),
(107, 172),
(109, 102),
(118, 114),
(160, 222),
(161, 137),
(163, 136),
(173, 172),
(351, 250),
(353, 248),
(377, 248),
(378, 189),
(465, 62),
(489, 292),
(569, 303),
(599, 309),
(602, 62),
(614, 172),
(615, 172),
(651, 328),
(654, 176),
(657, 314),
(670, 74),
(671, 190),
(672, 190),
(673, 190),
(674, 62),
(675, 194),
(700, 351),
(701, 309),
(702, 62),
(708, 360),
(710, 360),
(717, 389),
(718, 389),
(723, 389),
(725, 401),
(726, 389),
(727, 401),
(728, 389),
(734, 404),
(735, 68),
(736, 68),
(749, 433),
(750, 433),
(758, 197),
(761, 435),
(762, 435),
(763, 435),
(793, 447),
(795, 447),
(797, 430),
(798, 430),
(799, 430),
(800, 451),
(801, 430),
(809, 451),
(811, 449),
(812, 430),
(813, 449),
(815, 451),
(817, 449),
(824, 465),
(829, 468),
(831, 456),
(832, 459);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_registration`
--

CREATE TABLE `tbl_user_registration` (
  `id` bigint(255) NOT NULL,
  `user_id` bigint(255) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT '0000-00-00',
  `gender` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `profile_pic_id` bigint(255) DEFAULT '0',
  `mobile_no` bigint(255) DEFAULT NULL,
  `mobi` varchar(255) DEFAULT NULL,
  `alternate_email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `current_college` varchar(255) DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `college_location` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `prev_college` varchar(255) DEFAULT NULL,
  `about_me` varchar(255) DEFAULT NULL,
  `hobbies` varchar(255) DEFAULT NULL,
  `dream` varchar(255) DEFAULT NULL,
  `teaching_at` varchar(255) DEFAULT NULL,
  `primary_sub` varchar(255) DEFAULT NULL,
  `studied_from` varchar(255) DEFAULT NULL,
  `prev_taught_at` varchar(255) DEFAULT NULL,
  `landline_no` int(10) DEFAULT NULL,
  `principal_mob_no` varchar(255) DEFAULT NULL,
  `principal_email` varchar(255) DEFAULT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `admin_email` varchar(100) DEFAULT NULL,
  `course_provided` varchar(100) DEFAULT NULL,
  `ugc_univ-aicte` varchar(4) DEFAULT NULL,
  `university` varchar(100) DEFAULT NULL,
  `trust_name` varchar(100) DEFAULT NULL,
  `trustees` varchar(200) DEFAULT NULL,
  `no_of_staff` int(5) DEFAULT NULL,
  `library` varchar(4) DEFAULT NULL,
  `event_name` varchar(100) DEFAULT NULL,
  `vision` varchar(300) DEFAULT NULL,
  `activation_status` int(1) DEFAULT '0',
  `activation_link` text,
  `user_type` int(1) DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `profiletype` enum('0','university_page','college_page','education','politics','general_awareness','healthcare','arts','technology','computer','local_business','community','others') DEFAULT '0',
  `user_block` enum('0','1') DEFAULT '0',
  `mytwitter_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userprofile`
--

CREATE TABLE `userprofile` (
  `User_Id` int(11) NOT NULL,
  `Email` varchar(300) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `ProfileFor` varchar(100) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `LastName` varchar(200) NOT NULL,
  `Gender` tinyint(2) NOT NULL,
  `DOB` date NOT NULL,
  `Religion` int(11) NOT NULL,
  `MotherTongue` int(11) NOT NULL,
  `LivingIn` int(11) NOT NULL,
  `LivingState` int(11) NOT NULL DEFAULT '0',
  `LivingCity` int(11) NOT NULL DEFAULT '0',
  `MobileNo` varchar(50) NOT NULL,
  `Country` varchar(100) NOT NULL DEFAULT '0',
  `State` int(11) NOT NULL DEFAULT '0',
  `City` int(11) NOT NULL DEFAULT '0',
  `PinNo` int(11) NOT NULL DEFAULT '0',
  `MaritalStatus` varchar(100) DEFAULT NULL,
  `Weight` varchar(100) DEFAULT NULL,
  `Height` varchar(100) DEFAULT NULL,
  `HIV` int(2) NOT NULL DEFAULT '0',
  `SkinTone` varchar(100) DEFAULT NULL,
  `BodyType` varchar(100) DEFAULT NULL,
  `Diet` varchar(100) DEFAULT NULL,
  `Smoke` varchar(100) NOT NULL DEFAULT '2',
  `Drink` varchar(100) NOT NULL DEFAULT '2',
  `EducationLevel` int(11) DEFAULT NULL,
  `EducationField` int(11) DEFAULT NULL,
  `WorkingWith` varchar(200) DEFAULT NULL,
  `WorkingAs` varchar(200) DEFAULT NULL,
  `AnualIncome` varchar(200) DEFAULT NULL,
  `Community` int(11) DEFAULT NULL,
  `SubCommunity` int(11) DEFAULT NULL,
  `PartnerCommunity` tinyint(2) DEFAULT NULL,
  `Disability` tinyint(2) NOT NULL DEFAULT '2',
  `About` text,
  `ProfilePic` varchar(250) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `fb_login` int(2) NOT NULL DEFAULT '0',
  `TimeStamp` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userprofile`
--

INSERT INTO `userprofile` (`User_Id`, `Email`, `Password`, `ProfileFor`, `Name`, `LastName`, `Gender`, `DOB`, `Religion`, `MotherTongue`, `LivingIn`, `LivingState`, `LivingCity`, `MobileNo`, `Country`, `State`, `City`, `PinNo`, `MaritalStatus`, `Weight`, `Height`, `HIV`, `SkinTone`, `BodyType`, `Diet`, `Smoke`, `Drink`, `EducationLevel`, `EducationField`, `WorkingWith`, `WorkingAs`, `AnualIncome`, `Community`, `SubCommunity`, `PartnerCommunity`, `Disability`, `About`, `ProfilePic`, `DateCreated`, `DateUpdated`, `fb_login`, `TimeStamp`) VALUES
(451, 'adamwhite@gmail.com', 'adamwhite', 'Self', 'Adam', 'White', 1, '1992-03-11', 3, 3, 1, 39, 217, '9988667755', '1', 39, 217, 560060, 'Never married', '75', '74', 2, '0', 'Average', '0', '2', '2', 2, 28, '3', '13', 'INR 20 Lakh to 30 Lakh', 62, 0, NULL, 2, 'hi', '451.png', '0000-00-00 00:00:00', '2019-07-17 12:00:34', 0, '1563364834'),
(452, 'anafernandes@gmail.com', 'anafernandes', 'Self', 'Ana', 'Fernandes', 2, '1994-06-23', 3, 3, 1, 39, 217, '9988563555', '1', 0, 0, 560060, 'Never married', '56', '68', 2, 'wheatish', 'Average', '0', '2', '2', 2, 28, '3', '13', 'INR 10 Lakh to 15 Lakh', 28, NULL, NULL, 2, 'Hey iam looking for groom to my sister who is well settled and honest and decent family kindly contact us', '452.png', '0000-00-00 00:00:00', '2019-07-12 10:26:29', 0, '1562927189'),
(455, 'sujitroy@gmail.com', 'sujitroy', 'Self', 'Sujit', 'Roy', 1, '1989-06-14', 1, 2, 1, 0, 0, '9988663325', '1', 55, 651, 700024, 'Never married', '74', '68', 2, 'wheatish', 'Average', 'Non veg', '2', '2', 3, 28, '3', '12', 'INR 7 Lakh to 10 Lakh', 208, NULL, NULL, 2, 'hi am Name Visible on Login \ni am open minded fun loving simple guy. Pls contact directly ', '455.png', '2019-07-17 11:10:54', '2019-07-17 11:31:02', 0, '1563363040'),
(456, 'abhinav@gmail.com', 'abhinav', 'Self', 'Abhinav', '', 1, '1990-06-06', 1, 5, 1, 35, 167, '89827878821', '1', 34, 144, 578890, 'Never married', '89', '63', 2, 'wheatish', 'Average', 'Non veg', '2', '2', 4, 37, '2', '351', 'INR 4 Lakh to 7 Lakh', 171, NULL, NULL, 2, 'hello', '456.png', '2019-07-17 11:13:44', '2019-07-19 11:52:10', 0, '1563537130'),
(457, 'rahul@gmail.com', 'rahul', 'Self', 'rahul', '', 1, '1989-03-23', 2, 16, 1, 41, 261, '7899999999', '1', 41, 261, 786676, 'Never married', '54', '60', 2, 'fair', 'Average', 'Occasionally Non-Veg', '2', '2', 5, 28, '3', '16', 'INR 1 Lakh to 2 Lakh', 1, NULL, NULL, 2, 'hi', '457.png', '2019-07-17 11:21:05', '2019-07-19 06:48:43', 0, '1563518923'),
(458, 'Amarpreetsingh@gmail.com', ' Amarpreetsingh', 'Self', ' Amarpreet', 'singh', 2, '1990-07-19', 7, 12, 1, 48, 412, '9988663325', '1', 0, 0, 0, 'Never married', '55', '68', 2, 'fair', 'Average', 'Occasionally Non-Veg', '2', '2', 3, 35, '2', 'Doesn\'t Matter', 'INR 10 Lakh to 15 Lakh', 0, NULL, NULL, 2, 'i describe myself Crazy, laughing panda, talkative, responsible, Loves to enjoy each and every moment of life. Also takes intiative to make other laugh.\n', '458.png', '2019-07-17 11:26:08', '2019-07-17 11:30:03', 0, NULL),
(459, 'sakshi@gmail.com', 'sakshi', 'Self', 'Sakshi', '', 2, '1994-04-21', 1, 12, 1, 48, 412, '8907654689', '1', 48, 412, 789689, 'Never married', '45', '63', 2, 'fair', 'Average', 'veg', '2', '2', 2, 30, '2', '11', 'INR 4 Lakh to 7 Lakh', 195, NULL, NULL, 2, 'hey', '459.png', '2019-07-17 11:26:49', '2019-07-31 11:24:29', 0, '1564572269'),
(460, 'smita@gmail.com', 'smita', 'Self', 'Smita', '', 2, '1990-11-13', 1, 5, 1, 43, 346, '89827878821', '1', 43, 346, 987654, 'Never married', '34', '60', 2, 'fair', 'Average', 'veg', '2', '2', 2, 25, '3', '21', 'INR 2 Lakh to 4 Lakh', 170, NULL, NULL, 2, 'hello', '460.png', '2019-07-17 11:33:28', '2019-07-17 11:43:39', 0, '1563363819'),
(461, 'jatinmodi@gmail.com', 'jatinmodi', 'Self', 'Jatin', 'Modi', 1, '1992-06-24', 1, 4, 1, 0, 0, '9985663325', '1', 34, 136, 0, 'Never married', '71', '70', 2, 'fair', 'Average', '0', '2', '2', 2, 28, '3', 'Doesn\'t Matter', 'INR 15 Lakh to 20 Lakh', 0, NULL, NULL, 2, 'Hi my name is Jatin. I have completed my Bachelors in Commerce. Working in Pvt. Co. As a Account Assistant. & also doing computer & software selling business with my friend in my spare time. If you like my profile please leave a message. \n', '461.png', '2019-07-17 11:33:56', '2019-07-17 11:38:19', 0, NULL),
(462, 'saniya@gmail.com', 'saniya', 'Self', 'Saniya', '', 2, '1988-05-15', 2, 16, 1, 36, 183, '7899999999', '1', 36, 183, 675567, 'Never married', '43', '62', 2, 'very fair', 'Average', 'Non veg', '2', '2', 3, 24, '4', '6', 'INR 2 Lakh to 4 Lakh', 17, NULL, NULL, 2, 'hey', '462.png', '2019-07-17 11:39:04', '2019-07-17 11:43:17', 0, '1563363797'),
(463, 'arjunrajkumar@gmail.com', 'arjunrajkumar', 'Son', 'arjunraj', 'kumar', 1, '1988-07-21', 1, 6, 1, 39, 217, '9985663325', '1', 0, 0, 0, 'Divorced', '71', '70', 2, 'black', 'Average', '0', '2', '2', 2, 28, '3', 'Doesn\'t Matter', 'INR 7 Lakh to 10 Lakh', 0, NULL, NULL, 2, ' I have completed my Bachelors in Commerce. Working in Pvt. Co. As a Account Assistant. & also doing computer & software selling business with my friend in my spare time. If you like my profile please leave a message. ', '463.png', '2019-07-17 11:42:28', '2019-07-17 11:56:36', 0, '1563364596'),
(464, 'jamesstokes@gmail.com', 'jamesstokes', 'Self', 'James', 'stokes', 1, '1993-08-24', 3, 3, 1, 39, 217, '9988663325', '2', 68, 0, 0, 'Never married', '74', '73', 2, 'very fair', 'Average', 'Non veg', '1', '1', 2, 28, '3', 'Doesn\'t Matter', 'INR 10 Lakh to 15 Lakh', 28, NULL, NULL, 2, 'Hi i am James, I was living in London ,UK from last 8 years recently in 2016 I have move to India. In my family father and mother looks after business, My sister is Ele. engineer and currently she is settled in South Africa with her husband. My brother is dentist and currently doing his Masters from Benaras Hindu university .Regarding my profession, I was handling London company process as operation head and was earning 12 lakhs per annum, but that process move to Baroda, gujrat. As I dont want to move to Baroda I have accepted lower income job as 7-8 lakhs per annum. As family and friends is important to me, I like to live with them and when they are with you, you can do everything in life. And after tax I m earning almost same amount. So looking for right opportunity to prove myself in better position, or start my own business. As in life you have to keep progressing to achieve your goal. So Looking for my best friend who can enjoy this precious life me and be with me forever.In my personal life I would like to visit new places travel on my day off , explore new places every week, and like to visit restaurant and go to movie at least once a week if cant go out. So looking for same person who like to go out and enjoy precious life. Its getting so long so gonna stop here. Rest can be discuss when we meet. Thank you for your time and best of luck for your search .', '464.png', '2019-07-17 12:00:45', '2019-07-17 12:45:55', 0, '1563367555'),
(465, 'ronak@gmail.com', 'ronak', 'Self', 'Ronak', '', 1, '1996-06-11', 1, 12, 1, 41, 261, '08876556788', '1', 41, 261, 678979, 'Never married', '78', '65', 2, 'fair', 'Average', 'Occasionally Non-Veg', '2', '2', 2, 24, '3', '8', 'INR 2 Lakh to 4 Lakh', 195, NULL, NULL, 2, 'hi i am ronak', '465.png', '2019-07-18 05:47:58', '2019-07-18 10:22:45', 0, '1563445365'),
(466, '', '', '', 'test2', '', 2, '0000-00-00', 0, 0, 0, 0, 0, '', '0', 0, 0, 0, '', '', '', 0, NULL, NULL, NULL, '2', '2', 0, 0, '', 'Doesn\'t Matter', '', 1, NULL, NULL, 2, '\n													', NULL, '2019-07-18 06:27:32', '0000-00-00 00:00:00', 0, NULL),
(467, '', '', '', 'test3', '', 2, '0000-00-00', 0, 0, 0, 0, 0, '', '0', 0, 0, 0, '', '', '', 0, NULL, NULL, NULL, '2', '2', 0, 0, '', 'Doesn\'t Matter', '', 1, NULL, NULL, 2, '\n													', NULL, '2019-07-18 06:32:38', '0000-00-00 00:00:00', 0, NULL),
(468, 'mishti@gmail.com', 'mishti', 'Self', 'Mishti', '', 2, '1994-06-14', 1, 4, 1, 34, 136, '9878747844', '1', 34, 136, 786789, 'Never married', '45', '66', 2, 'fair', 'Average', 'veg', '2', '2', 3, 30, '3', '16', 'INR 2 Lakh to 4 Lakh', 317, NULL, NULL, 2, 'hey', '468.png', '2019-07-19 05:22:38', '2019-07-19 07:27:13', 0, '1563521233');

-- --------------------------------------------------------

--
-- Table structure for table `workingas`
--

CREATE TABLE `workingas` (
  `Work_Id` int(11) NOT NULL,
  `Work_Name` varchar(100) NOT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workingas`
--

INSERT INTO `workingas` (`Work_Id`, `Work_Name`, `Status`) VALUES
(6, 'Banking Professional ', 1),
(7, 'Charted Accountant ', 1),
(8, 'Company Secretary', 1),
(9, 'Finance Professional', 1),
(10, 'Investment Preofessional', 1),
(11, 'Accounting Professional', 1),
(12, 'Admin Professional', 1),
(13, 'Human Resource Professional', 1),
(14, 'Advertising Professional', 1),
(15, 'Entertainment Professional', 1),
(16, 'Event Manager', 1),
(17, 'Media Professional', 1),
(18, 'Public Relation Professional', 1),
(19, 'Agricultural Professional', 1),
(20, 'Air Hostess / Flight Attendent', 1),
(21, 'Software Engineer', 1),
(22, 'Assistant Manager', 1),
(23, 'Accountants', 1),
(24, 'Accountants and Auditors', 1),
(25, 'Actor', 1),
(28, 'Acute Care Nurses', 1),
(31, 'Administrative Law Judges, Adjudicators, and Hearing Officers', 1),
(32, '?Administrative Services Managers', 0),
(33, '?Adult Literacy, Remedial Education, and GED Teachers and Instructors', 1),
(34, '?Advanced Practice Psychiatric Nurses?', 1),
(35, '?Advertising and Promotions Managers', 1),
(36, '?Advertising Sales Agents?', 1),
(37, '?Aerospace Engineering and Operations Technicians', 1),
(38, '?Aerospace Engineers', 1),
(39, '?Agents and Business Managers of Artists, Performers, and Athletes', 1),
(40, '?Agricultural and Food Science Technicians', 1),
(41, '?Agricultural Crop Farm Managers', 1),
(42, '?Agricultural Engineers', 1),
(43, '?Agricultural Equipment Operators', 1),
(44, '?Agricultural Inspectors', 1),
(45, '?Agricultural Sciences Teachers, Postsecondary', 1),
(46, '?Agricultural Technicians', 1),
(48, '?Air Crew Members', 1),
(49, '?Air Crew Officers', 1),
(50, '?Air Traffic Controllers', 1),
(51, '?Aircraft Body and Bonded Structure Repairers', 1),
(52, '?Aircraft Cargo Handling Supervisors', 1),
(53, '?Aircraft Engine Specialists', 1),
(54, '?Aircraft Launch and Recovery Officers', 1),
(55, '?Aircraft Launch and Recovery Specialists', 1),
(56, '?Aircraft Mechanics and Service Technicians', 1),
(57, '?Aircraft Rigging Assemblers', 1),
(58, '?Aircraft Structure Assemblers, Precision', 1),
(59, '?Aircraft Structure, Surfaces, Rigging, and Systems Assemblers', 1),
(60, '?Aircraft Systems Assemblers, Precision', 1),
(61, '?Airfield Operations Specialists', 1),
(62, '?Airframe-and-Power-Plant Mechanics', 1),
(63, '?Airline Pilots, Copilots, and Flight Engineers', 1),
(64, '?Allergists and Immunologists?', 1),
(65, '?Ambulance Drivers and Attendants, Except Emergency Medical Technicians', 1),
(66, '?Amusement and Recreation Attendants', 1),
(67, '?Anesthesiologist Assistants?', 1),
(68, '?Anesthesiologists', 1),
(69, '?Animal Breeders', 1),
(70, '?Animal Control Workers', 1),
(73, '?Anthropologists', 1),
(74, '?Anthropologists and Archeologists', 1),
(75, '?Anthropology and Archeology Teachers, Postsecondary', 1),
(76, '?Appraisers and Assessors of Real Estate', 1),
(77, '?Appraisers, Real Estate', 1),
(78, '?Aquacultural Managers?', 1),
(79, '?Arbitrators, Mediators, and Conciliators', 1),
(80, '?Archeologists', 1),
(81, '?Architects, Except Landscape and Naval', 1),
(83, '?Architectural Drafters', 1),
(84, '?Architecture Teachers, Postsecondary', 1),
(85, '?Archivists', 1),
(86, '?Area, Ethnic, and Cultural Studies Teachers, Postsecondary', 1),
(87, '?Armored Assault Vehicle Crew Members', 1),
(88, '?Armored Assault Vehicle Officers', 1),
(89, '?Art Directors', 1),
(90, '?Art Therapists?', 1),
(91, '?Art, Drama, and Music Teachers, Postsecondary', 1),
(92, '?Artillery and Missile Crew Members', 1),
(93, '?Artillery and Missile Officers', 1),
(94, '?Artists and Related Workers, All Other', 1),
(96, '?Assessors', 1),
(97, '?Astronomers', 1),
(98, '?Athletes and Sports Competitors', 1),
(99, '?Athletic Trainers', 1),
(100, '?Atmospheric and Space Scientists', 1),
(101, '?Atmospheric, Earth, Marine, and Space Sciences Teachers, Postsecondary', 1),
(102, '?Audio and Video Equipment Technicians', 1),
(103, '?Audiologists', 1),
(104, '?Audiologists?', 1),
(105, '?Audio-Visual Collections Specialists', 1),
(106, '?Auditors', 1),
(107, '?Automatic Teller Machine Servicers', 1),
(108, '?Automotive Body and Related Repairers', 1),
(109, '?Automotive Engineering Technicians?', 1),
(110, '?Automotive Engineers?', 1),
(111, '?Automotive Glass Installers and Repairers', 1),
(112, '?Automotive Master Mechanics', 1),
(113, '?Automotive Service Technicians and Mechanics', 1),
(114, '?Automotive Specialty Technicians', 1),
(115, '?Auxiliary Equipment Operators, Power', 1),
(116, '?Aviation Inspectors', 1),
(117, '?Avionics Technicians', 1),
(118, '?Baggage Porters and Bellhops', 1),
(119, '?Bailiffs', 1),
(120, '?Bakers', 1),
(121, '?Bakers, Bread and Pastry', 1),
(122, '?Bakers, Manufacturing', 1),
(123, '?Barbers', 1),
(124, '?Baristas?', 1),
(126, '?Battery Repairers', 1),
(127, '?Bench Workers, Jewelry', 1),
(128, '?Bicycle Repairers', 1),
(129, '?Bill and Account Collectors', 1),
(130, '?Billing and Posting Clerks and Machine Operators', 1),
(131, '?Billing, Cost, and Rate Clerks', 1),
(132, '?Billing, Posting, and Calculating Machine Operators', 1),
(133, '?Bindery Machine Operators and Tenders', 1),
(134, '?Bindery Machine Setters and Set-Up Operators', 1),
(135, '?Bindery Workers', 1),
(136, '?Biochemical Engineers?', 1),
(137, '?Biochemists', 1),
(138, '?Biochemists and Biophysicists', 1),
(139, '?Biofuels Processing Technicians?', 1),
(140, '?Biofuels Production Managers?', 1),
(141, '?Biofuels/Biodiesel Technology and Product Development Managers?', 1),
(142, '?Bioinformatics Scientists?', 1),
(143, '?Bioinformatics Technicians?', 1),
(144, '?Biological Science Teachers, Postsecondary', 1),
(145, '?Biological Scientists, All Other', 1),
(146, '?Biological Technicians', 1),
(147, '?Biologists', 1),
(148, '?Biomass Plant Technicians?', 1),
(149, '?Biomass Power Plant Managers?', 1),
(150, '?Biomedical Engineers', 1),
(151, '?Biophysicists', 1),
(152, '?Biostatisticians?', 1),
(153, '?Boat Builders and Shipwrights', 1),
(154, '?Boiler Operators and Tenders, Low Pressure', 1),
(155, '?Boilermakers', 1),
(156, '?Bookbinders', 1),
(157, '?Bookkeeping, Accounting, and Auditing Clerks', 1),
(158, '?Brattice Builders', 1),
(159, '?Brazers', 1),
(160, '?Brickmasons and Blockmasons', 1),
(161, '?Bridge and Lock Tenders', 1),
(162, '?Broadcast News Analysts', 1),
(163, '?Broadcast Technicians', 1),
(164, '?Brokerage Clerks', 1),
(165, '?Brownfield Redevelopment Specialists and Site Managers?', 1),
(166, '?Budget Analysts', 1),
(167, '?Buffing and Polishing Set-Up Operators', 1),
(168, '?Building Cleaning Workers, All Other', 1),
(169, '?Bus and Truck Mechanics and Diesel Engine Specialists', 1),
(172, '?Business Continuity Planners?', 1),
(173, '?Business Intelligence Analysts?', 1),
(174, '?Business Operations Specialists, All Other', 1),
(175, '?Business Teachers, Postsecondary', 1),
(176, '?Butchers and Meat Cutters', 1),
(177, '?Cabinetmakers and Bench Carpenters', 1),
(178, '?Calibration and Instrumentation Technicians', 1),
(179, '?Camera and Photographic Equipment Repairers', 1),
(180, '?Camera Operators', 1),
(181, '?Camera Operators, Television, Video, and Motion Picture', 1),
(182, '?Captains, Mates, and Pilots of Water Vessels', 1),
(183, '?Caption Writers', 1),
(184, '?Cardiovascular Technologists and Technicians', 1),
(185, '?Cargo and Freight Agents', 1),
(186, '?Carpenter Assemblers and Repairers', 1),
(187, '?Carpenters', 1),
(188, '?Carpet Installers', 1),
(189, '?Cartographers and Photogrammetrists', 1),
(190, '?Cartoonists', 1),
(191, '?Cashiers', 1),
(192, '?Casting Machine Set-Up Operators', 1),
(193, '?Ceiling Tile Installers', 1),
(194, '?Cement Masons and Concrete Finishers', 1),
(195, '?Cementing and Gluing Machine Operators and Tenders', 1),
(196, '?Central Office and PBX Installers and Repairers', 1),
(197, '?Central Office Operators', 1),
(198, '?Chefs and Head Cooks', 1),
(199, '?Chemical Engineers', 1),
(200, '?Chemical Equipment Controllers and Operators', 1),
(201, '?Chemical Equipment Operators and Tenders', 1),
(202, '?Chemical Equipment Tenders', 1),
(203, '?Chemical Plant and System Operators', 1),
(204, '?Chemical Technicians', 1),
(205, '?Chemistry Teachers, Postsecondary', 1),
(206, '?Chemists', 1),
(207, '?Chief Executives', 1),
(208, '?Chief Sustainability Officers?', 1),
(209, '?Child Care Workers', 1),
(210, '?Child Support, Missing Persons, and Unemployment Insurance Fraud Investigators', 1),
(211, '?Child, Family, and School Social Workers', 1),
(212, '?Chiropractors', 1),
(213, '?Choreographers', 1),
(214, '?City Planning Aides', 1),
(215, '?Civil Drafters', 1),
(216, '?Civil Engineering Technicians', 1),
(217, '?Civil Engineers', 1),
(218, '?Claims Adjusters, Examiners, and Investigators', 1),
(219, '?Claims Examiners, Property and Casualty Insurance', 1),
(220, '?Claims Takers, Unemployment Benefits', 1),
(221, '?Cleaners of Vehicles and Equipment', 1),
(222, '?Cleaning, Washing, and Metal Pickling Equipment Operators and Tenders', 1),
(223, '?Clergy', 1),
(224, '?Climate Change Analysts?', 1),
(225, '?Clinical Data Managers?', 1),
(226, '?Clinical Nurse Specialists?', 1),
(227, '?Clinical Psychologists', 1),
(228, '?Clinical Research Coordinators?', 1),
(229, '?Clinical, Counseling, and School Psychologists', 1),
(230, '?Coaches and Scouts', 1),
(231, '?Coating, Painting, and Spraying Machine Operators and Tenders', 1),
(232, '?Coating, Painting, and Spraying Machine Setters and Set-Up Operators', 1),
(233, '?Coating, Painting, and Spraying Machine Setters, Operators, and Tenders', 1),
(234, '?Coil Winders, Tapers, and Finishers', 1),
(235, '?Coin, Vending, and Amusement Machine Servicers and Repairers', 1),
(236, '?Combination Machine Tool Operators and Tenders, Metal and Plastic', 1),
(237, '?Combination Machine Tool Setters and Set-Up Operators, Metal and Plastic', 1),
(238, '?Combined Food Preparation and Serving Workers, Including Fast Food', 1),
(239, '?Command and Control Center Officers', 1),
(240, '?Command and Control Center Specialists', 1),
(241, '?Commercial and Industrial Designers', 1),
(242, '?Commercial Divers', 1),
(243, '?Commercial Pilots', 1),
(244, '?Communication Equipment Mechanics, Installers, and Repairers', 1),
(245, '?Communications Equipment Operators, All Other', 1),
(246, '?Communications Teachers, Postsecondary', 1),
(247, '?Community and Social Service Specialists, All Other', 1),
(248, '?Community Health Workers?', 1),
(249, '?Compensation and Benefits Managers?', 1),
(250, '?Compensation and Benefits Managers', 1),
(251, '?Compensation, Benefits, and Job Analysis Specialists?', 1),
(252, '?Compensation, Benefits, and Job Analysis Specialists', 1),
(253, '?Compliance Managers?', 1),
(254, '?Compliance Officers, Except Agriculture, Construction, Health and Safety, and Transportation', 1),
(255, '?Composers', 1),
(257, '?Computer and Information Scientists, Research', 1),
(258, '?Computer and Information Systems Managers', 1),
(259, '?Computer Hardware Engineers', 1),
(260, '?Computer Network Architects?', 1),
(261, '?Computer Network Support Specialists?', 1),
(262, '?Computer Operators', 1),
(263, '?Computer Programmers', 1),
(264, '?Computer Programmers?', 1),
(265, '?Computer Science Teachers, Postsecondary', 1),
(266, '?Computer Security Specialists', 1),
(267, '?Computer Software Engineers, Applications', 1),
(268, '?Computer Software Engineers, Systems Software', 1),
(269, '?Computer Specialists, All Other', 1),
(270, '?Computer Support Specialists', 1),
(271, '?Computer Systems Analysts', 1),
(272, '?Computer Systems Analysts?', 1),
(273, '?Computer Systems Engineers/Architects?', 1),
(274, '?Computer User Support Specialists?', 1),
(275, '?Computer, Automated Teller, and Office Machine Repairers', 1),
(276, '?Computer-Controlled Machine Tool Operators, Metal and Plastic', 1),
(277, '?Concierges', 1),
(278, '?Conservation Scientists', 1),
(279, '?Construction and Building Inspectors', 1),
(280, '?Construction and Related Workers, All Other', 1),
(281, '?Construction Carpenters', 1),
(282, '?Construction Drillers', 1),
(283, '?Construction Laborers', 1),
(284, '?Construction Managers', 1),
(285, '?Continuous Mining Machine Operators', 1),
(286, '?Control and Valve Installers and Repairers, Except Mechanical Door', 1),
(287, '?Conveyor Operators and Tenders', 1),
(288, '?Cooks, All Other', 1),
(289, '?Cooks, Fast Food', 1),
(290, '?Cooks, Institution and Cafeteria', 1),
(291, '?Cooks, Private Household', 1),
(292, '?Cooks, Restaurant', 1),
(293, '?Cooks, Short Order', 1),
(294, '?Cooling and Freezing Equipment Operators and Tenders', 1),
(295, '?Copy Writers', 1),
(296, '?Coroners', 1),
(297, '?Correctional Officers and Jailers', 1),
(298, '?Correspondence Clerks', 1),
(300, '?Costume Attendants', 1),
(301, '?Counseling Psychologists', 1),
(302, '?Counselors, All Other', 1),
(303, '?Counter and Rental Clerks', 1),
(304, '?Counter Attendants, Cafeteria, Food Concession, and Coffee Shop', 1),
(305, '?Couriers and Messengers', 1),
(306, '?Court Clerks', 1),
(307, '?Court Reporters', 1),
(308, '?Court, Municipal, and License Clerks', 1),
(309, '?Craft Artists', 1),
(310, '?Crane and Tower Operators', 1),
(311, '?Creative Writers', 1),
(312, '?Credit Analysts', 1),
(313, '?Credit Authorizers', 1),
(314, '?Credit Authorizers, Checkers, and Clerks', 1),
(315, '?Credit Checkers', 1),
(316, '?Criminal Investigators and Special Agents', 1),
(317, '?Criminal Justice and Law Enforcement Teachers, Postsecondary', 1),
(318, '?Critical Care Nurses?', 1),
(319, '?Crossing Guards', 1),
(320, '?Crushing, Grinding, and Polishing Machine Setters, Operators, and Tenders', 1),
(321, '?Curators', 1),
(322, '?Custom Tailors', 1),
(323, '?Customer Service Representatives', 1),
(324, '?Customer Service Representatives, Utilities', 1),
(325, '?Customs Brokers?', 1),
(326, '?Cutters and Trimmers, Hand', 1),
(327, '?Cutting and Slicing Machine Operators and Tenders', 1),
(328, '?Cutting and Slicing Machine Setters, Operators, and Tenders', 1),
(329, '?Cutting, Punching, and Press Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(330, '?Cytogenetic Technologists?', 1),
(331, '?Cytotechnologists?', 1),
(332, '?Dancers', 1),
(333, '?Data Entry Keyers', 1),
(334, '?Data Processing Equipment Repairers', 1),
(335, '?Data Warehousing Specialists?', 1),
(336, '?Database Administrators?', 1),
(337, '?Database Administrators', 1),
(338, '?Database Architects?', 1),
(339, '?Demonstrators and Product Promoters', 1),
(340, '?Dental Assistants', 1),
(341, '?Dental Hygienists', 1),
(342, '?Dental Laboratory Technicians', 1),
(343, '?Dentists, All Other Specialists', 1),
(344, '?Dentists, General', 1),
(345, '?Dermatologists?', 1),
(346, '?Derrick Operators, Oil and Gas', 1),
(347, '?Design Printing Machine Setters and Set-Up Operators', 1),
(348, '?Designers, All Other', 1),
(349, '?Desktop Publishers', 1),
(350, '?Detectives and Criminal Investigators', 1),
(351, '?Diagnostic Medical Sonographers', 1),
(352, '?Dietetic Technicians', 1),
(353, '?Dietitians and Nutritionists', 1),
(354, '?Dining Room and Cafeteria Attendants and Bartender Helpers', 1),
(355, '?Directors- Stage, Motion Pictures, Television, and Radio', 1),
(356, '?Directors, Religious Activities and Education', 1),
(357, '?Directory Assistance Operators', 1),
(358, '?Dishwashers', 1),
(359, '?Dispatchers, Except Police, Fire, and Ambulance', 1),
(360, '?Distance Learning Coordinators?', 1),
(361, '?Document Management Specialists?', 1),
(362, '?Door-To-Door Sales Workers, News and Street Vendors, and Related Workers', 1),
(363, '?Dot Etchers', 1),
(364, '?Drafters, All Other', 1),
(365, '?Dragline Operators', 1),
(366, '?Dredge Operators', 1),
(368, '?Driver-Sales Workers', 1),
(369, '?Drywall and Ceiling Tile Installers', 1),
(370, '?Drywall Installers', 1),
(371, '?Duplicating Machine Operators', 1),
(372, '?Earth Drillers, Except Oil and Gas', 1),
(373, '?Economics Teachers, Postsecondary', 1),
(374, '?Economists', 1),
(375, '?Editors', 1),
(376, '?Education Administrators, All Other', 1),
(377, '?Education Administrators, Elementary and Secondary School', 1),
(378, '?Education Administrators, Postsecondary', 1),
(379, '?Education Administrators, Preschool and Child Care Center--Program', 1),
(380, '?Education Teachers, Postsecondary', 1),
(381, '?Education, Training, and Library Workers, All Other', 1),
(382, '?Educational Psychologists', 1),
(383, '?Educational, Vocational, and School Counselors', 1),
(384, '?Electric Home Appliance and Power Tool Repairers', 1),
(385, '?Electric Meter Installers and Repairers', 1),
(386, '?Electric Motor and Switch Assemblers and Repairers', 1),
(388, '?Electrical and Electronic Engineering Technicians', 1),
(389, '?Electrical and Electronic Equipment Assemblers', 1),
(390, '?Electrical and Electronic Inspectors and Testers', 1),
(391, '?Electrical and Electronics Drafters', 1),
(392, '?Electrical and Electronics Installers and Repairers, Transportation Equipment', 1),
(393, '?Electrical and Electronics Repairers, Commercial and Industrial Equipment', 1),
(394, '?Electrical and Electronics Repairers, Powerhouse, Substation, and Relay', 1),
(395, '?Electrical Drafters', 1),
(396, '?Electrical Engineering Technicians', 1),
(397, '?Electrical Engineering Technologists?', 1),
(398, '?Electrical Engineers', 1),
(399, '?Electrical Parts Reconditioners', 1),
(400, '?Electrical Power-Line Installers and Repairers', 1),
(401, '?Electricians', 1),
(402, '?Electrolytic Plating and Coating Machine Operators and Tenders, Metal and Plastic', 1),
(403, '?Electrolytic Plating and Coating Machine Setters and Set-Up Operators, Metal and Plastic', 1),
(404, '?Electromechanical Engineering Technologists?', 1),
(405, '?Electromechanical Equipment Assemblers', 1),
(406, '?Electro-Mechanical Technicians', 1),
(407, '?Electronic Drafters', 1),
(408, '?Electronic Equipment Installers and Repairers, Motor Vehicles', 1),
(409, '?Electronic Home Entertainment Equipment Installers and Repairers', 1),
(410, '?Electronic Masking System Operators', 1),
(411, '?Electronics Engineering Technicians', 1),
(412, '?Electronics Engineering Technologists?', 1),
(413, '?Electronics Engineers, Except Computer', 1),
(414, '?Electrotypers and Stereotypers', 1),
(415, '?Elementary School Teachers, Except Special Education', 1),
(416, '?Elevator Installers and Repairers', 1),
(417, '?Eligibility Interviewers, Government Programs', 1),
(418, '?Embalmers', 1),
(419, '?Embossing Machine Set-Up Operators', 1),
(420, '?Emergency Management Directors?', 1),
(421, '?Emergency Management Specialists', 1),
(422, '?Emergency Medical Technicians and Paramedics', 1),
(423, '?Employment Interviewers, Private or Public Employment Service', 1),
(424, '?Employment, Recruitment, and Placement Specialists', 1),
(425, '?Endoscopy Technicians?', 1),
(426, '?Energy Auditors?', 1),
(427, '?Energy Brokers?', 1),
(428, '?Energy Engineers?', 1),
(429, '?Engine and Other Machine Assemblers', 1),
(430, '?Engineering Managers', 1),
(431, '?Engineering Teachers, Postsecondary', 1),
(432, '?Engineering Technicians, Except Drafters, All Other', 1),
(433, '?Engineers, All Other', 1),
(434, '?English Language and Literature Teachers, Postsecondary', 1),
(436, '?Engravers, Hand', 1),
(437, '?Engravers--Carvers', 1),
(438, '?Entertainers and Performers, Sports and Related Workers, All Other', 1),
(439, '?Entertainment Attendants and Related Workers, All Other', 1),
(440, '?Environmental Compliance Inspectors', 1),
(441, '?Environmental Economists?', 1),
(442, '?Environmental Engineering Technicians', 1),
(443, '?Environmental Engineers', 1),
(444, '?Environmental Restoration Planners?', 1),
(445, '?Environmental Science and Protection Technicians, Including Health', 1),
(446, '?Environmental Science Teachers, Postsecondary', 1),
(447, '?Environmental Scientists and Specialists, Including Health', 1),
(448, '?Epidemiologists', 1),
(449, '?Equal Opportunity Representatives and Officers', 1),
(450, '?Etchers', 1),
(451, '?Etchers and Engravers', 1),
(452, '?Etchers, Hand', 1),
(453, '?Excavating and Loading Machine and Dragline Operators', 1),
(454, '?Excavating and Loading Machine Operators', 1),
(455, '?Executive Secretaries and Administrative Assistants', 1),
(456, '?Exercise Physiologists?', 1),
(458, '?Explosives Workers, Ordnance Handling Experts, and Blasters', 1),
(459, '?Extraction Workers, All Other', 1),
(460, '?Extruding and Drawing Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(461, '?Extruding and Forming Machine Operators and Tenders, Synthetic or Glass Fibers', 1),
(462, '?Extruding and Forming Machine Setters, Operators, and Tenders, Synthetic and Glass Fibers', 1),
(463, '?Extruding, Forming, Pressing, and Compacting Machine Operators and Tenders', 1),
(464, '?Extruding, Forming, Pressing, and Compacting Machine Setters and Set-Up Operators', 1),
(465, '?Extruding, Forming, Pressing, and Compacting Machine Setters, Operators, and Tenders', 1),
(466, '?Fabric and Apparel Patternmakers', 1),
(467, '?Fabric Menders, Except Garment', 1),
(468, '?Fallers', 1),
(469, '?Family and General Practitioners', 1),
(470, '?Farm and Home Management Advisors', 1),
(471, '?Farm and Ranch Managers?', 1),
(472, '?Farm Equipment Mechanics', 1),
(473, '?Farm Labor Contractors', 1),
(474, '?Farm Labor Contractors?', 1),
(475, '?Farm, Ranch, and Other Agricultural Managers', 1),
(476, '?Farmers and Ranchers', 1),
(477, '?Farmworkers and Laborers, Crop, Nursery, and Greenhouse', 1),
(478, '?Farmworkers, Farm and Ranch Animals', 1),
(480, '?Fence Erectors', 1),
(481, '?Fiber Product Cutting Machine Setters and Set-Up Operators', 1),
(482, '?Fiberglass Laminators and Fabricators', 1),
(483, '?File Clerks', 1),
(484, '?Film and Video Editors', 1),
(485, '?Film Laboratory Technicians', 1),
(486, '?Financial Analysts', 1),
(487, '?Financial Examiners', 1),
(488, '?Financial Managers', 1),
(489, '?Financial Managers, Branch or Department', 1),
(490, '?Financial Quantitative Analysts?', 1),
(491, '?Financial Specialists, All Other', 1),
(492, '?Fine Artists, Including Painters, Sculptors, and Illustrators', 1),
(493, '?Fire Fighters', 1),
(494, '?Fire Inspectors', 1),
(495, '?Fire Inspectors and Investigators', 1),
(496, '?Fire Investigators', 1),
(497, '?Fire-Prevention and Protection Engineers', 1),
(498, '?First-Line Supervisors and Manager-Supervisors - Agricultural Crop Workers', 1),
(499, '?First-Line Supervisors and Manager-Supervisors - Animal Care Workers, Except Livestock', 1),
(500, '?First-Line Supervisors and Manager-Supervisors - Animal Husbandry Workers', 1),
(501, '?First-Line Supervisors and Manager-Supervisors - Fishery Workers', 1),
(502, '?First-Line Supervisors and Manager-Supervisors - Horticultural Workers', 1),
(503, '?First-Line Supervisors and Manager-Supervisors - Landscaping Workers', 1),
(504, '?First-Line Supervisors and Manager-Supervisors - Logging Workers', 1),
(505, '?First-Line Supervisors and Manager-Supervisors- Construction Trades Workers', 1),
(506, '?First-Line Supervisors and Manager-Supervisors- Extractive Workers', 1),
(507, '?First-Line Supervisors of Agricultural Crop and Horticultural Workers?', 1),
(508, '?First-Line Supervisors of Animal Husbandry and Animal Care Workers?', 1),
(509, '?First-Line Supervisors, Administrative Support', 1),
(510, '?First-Line Supervisors, Customer Service', 1),
(511, '?First-Line Supervisors-Managers of Air Crew Members', 1),
(513, '?First-Line Supervisors-Managers of Construction Trades and Extraction Workers', 1),
(514, '?First-Line Supervisors-Managers of Correctional Officers', 1),
(515, '?First-Line Supervisors-Managers of Farming, Fishing, and Forestry Workers', 1),
(516, '?First-Line Supervisors-Managers of Fire Fighting and Prevention Workers', 1),
(517, '?First-Line Supervisors-Managers of Food Preparation and Serving Workers', 1),
(518, '?First-Line Supervisors-Managers of Helpers, Laborers, and Material Movers, Hand', 1),
(519, '?First-Line Supervisors-Managers of Housekeeping and Janitorial Workers', 1),
(520, '?First-Line Supervisors-Managers of Landscaping, Lawn Service, and Groundskeeping Workers', 1),
(521, '?First-Line Supervisors-Managers of Mechanics, Installers, and Repairers', 1),
(522, '?First-Line Supervisors-Managers of Non-Retail Sales Workers', 1),
(523, '?First-Line Supervisors-Managers of Office and Administrative Support Workers', 1),
(524, '?First-Line Supervisors-Managers of Personal Service Workers', 1),
(525, '?First-Line Supervisors-Managers of Police and Detectives', 1),
(526, '?First-Line Supervisors-Managers of Production and Operating Workers', 1),
(527, '?First-Line Supervisors-Managers of Retail Sales Workers', 1),
(528, '?First-Line Supervisors-Managers of Transportation and Material-Moving Machine and Vehicle Operators', 1),
(529, '?First-Line Supervisors-Managers of Weapons Specialists--Crew Members', 1),
(530, '?First-Line Supervisors-Managers, Protective Service Workers, All Other', 1),
(531, '?Fish and Game Wardens', 1),
(532, '?Fish Hatchery Managers', 1),
(533, '?Fishers and Related Fishing Workers', 1),
(534, '?Fitness and Wellness Coordinators?', 1),
(535, '?Fitness Trainers and Aerobics Instructors', 1),
(536, '?Fitters, Structural Metal- Precision', 1),
(537, '?Flight Attendants', 1),
(538, '?Flight Attendants?', 1),
(539, '?Floor Layers, Except Carpet, Wood, and Hard Tiles', 1),
(540, '?Floor Sanders and Finishers', 1),
(541, '?Floral Designers', 1),
(542, '?Food and Tobacco Roasting, Baking, and Drying Machine Operators and Tenders', 1),
(543, '?Food Batchmakers', 1),
(544, '?Food Cooking Machine Operators and Tenders', 1),
(545, '?Food Preparation and Serving Related Workers, All Other', 1),
(546, '?Food Preparation Workers', 1),
(547, '?Food Science Technicians', 1),
(548, '?Food Scientists and Technologists', 1),
(549, '?Food Servers, Nonrestaurant', 1),
(550, '?Food Service Managers', 1),
(551, '?Foreign Language and Literature Teachers, Postsecondary', 1),
(552, '?Forensic Science Technicians', 1),
(553, '?Forest and Conservation Technicians', 1),
(554, '?Forest and Conservation Workers', 1),
(555, '?Forest Fire Fighters', 1),
(556, '?Forest Fire Fighting and Prevention Supervisors', 1),
(557, '?Forest Fire Inspectors and Prevention Specialists', 1),
(558, '?Foresters', 1),
(559, '?Forestry and Conservation Science Teachers, Postsecondary', 1),
(560, '?Forging Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(561, '?Foundry Mold and Coremakers', 1),
(562, '?Frame Wirers, Central Office', 1),
(563, '?Fraud Examiners, Investigators and Analysts?', 1),
(564, '?Freight and Cargo Inspectors?', 1),
(565, '?Freight Forwarders?', 1),
(566, '?Freight Inspectors', 1),
(567, '?Freight, Stock, and Material Movers, Hand', 1),
(568, '?Fuel Cell Engineers?', 1),
(569, '?Fuel Cell Technicians?', 1),
(570, '?Fundraisers?', 1),
(571, '?Funeral Attendants', 1),
(572, '?Funeral Directors', 1),
(573, '?Furnace, Kiln, Oven, Drier, and Kettle Operators and Tenders', 1),
(574, '?Furniture Finishers', 1),
(575, '?Gaming and Sports Book Writers and Runners', 1),
(576, '?Gaming Cage Workers', 1),
(577, '?Gaming Change Persons and Booth Cashiers', 1),
(578, '?Gaming Dealers', 1),
(579, '?Gaming Managers', 1),
(580, '?Gaming Service Workers, All Other', 1),
(581, '?Gaming Supervisors', 1),
(582, '?Gaming Surveillance Officers and Gaming Investigators', 1),
(583, '?Gas Appliance Repairers', 1),
(584, '?Gas Compressor and Gas Pumping Station Operators', 1),
(585, '?Gas Compressor Operators', 1),
(586, '?Gas Distribution Plant Operators', 1),
(587, '?Gas Plant Operators', 1),
(588, '?Gas Processing Plant Operators', 1),
(589, '?Gas Pumping Station Operators', 1),
(590, '?Gaugers', 1),
(591, '?Gem and Diamond Workers', 1),
(592, '?General and Operations Managers', 1),
(593, '?General Farmworkers', 1),
(594, '?Genetic Counselors?', 1),
(595, '?Geneticists?', 1),
(596, '?Geodetic Surveyors?', 1),
(597, '?Geographers', 1),
(598, '?Geographic Information Systems Technicians?', 1),
(599, '?Geography Teachers, Postsecondary', 1),
(600, '?Geological and Petroleum Technicians', 1),
(601, '?Geological Data Technicians', 1),
(602, '?Geological Sample Test Technicians', 1),
(603, '?Geologists', 1),
(604, '?Geoscientists, Except Hydrologists and Geographers', 1),
(605, '?Geospatial Information Scientists and Technologists?', 1),
(606, '?Geothermal Production Managers?', 1),
(607, '?Geothermal Technicians?', 1),
(608, '?Glass Blowers, Molders, Benders, and Finishers', 1),
(609, '?Glass Cutting Machine Setters and Set-Up Operators', 1),
(610, '?Glaziers', 1),
(611, '?Government Property Inspectors and Investigators', 1),
(612, '?Government Service Executives', 1),
(613, '?Grader, Bulldozer, and Scraper Operators', 1),
(614, '?Graders and Sorters, Agricultural Products', 1),
(615, '?Graduate Teaching Assistants', 1),
(616, '?Graphic Designers', 1),
(617, '?Green Marketers?', 1),
(618, '?Grinding and Polishing Workers, Hand', 1),
(619, '?Grinding, Honing, Lapping, and Deburring Machine Set-Up Operators', 1),
(620, '?Grinding, Lapping, Polishing, and Buffing Machine Tool Setters, Operators, and Tenders, Metal and P', 1),
(621, '?Grips and Set-Up Workers, Motion Picture Sets, Studios, and Stages', 1),
(622, '?Grounds Maintenance Workers, All Other', 1),
(623, '?Hairdressers, Hairstylists, and Cosmetologists', 1),
(624, '?Hand and Portable Power Tool Repairers', 1),
(625, '?Hand Compositors and Typesetters', 1),
(626, '?Hazardous Materials Removal Workers', 1),
(627, '?Health and Safety Engineers, Except Mining Safety Engineers and Inspectors', 1),
(628, '?Health Diagnosing and Treating Practitioners, All Other', 1),
(629, '?Health Educators', 1),
(630, '?Health Specialties Teachers, Postsecondary', 1),
(631, '?Health Technologists and Technicians, All Other', 1),
(632, '?Healthcare Practitioners and Technical Workers, All Other', 1),
(633, '?Healthcare Support Workers, All Other', 1),
(634, '?Hearing Aid Specialists?', 1),
(635, '?Heat Treating Equipment Setters, Operators, and Tenders, Metal and Plastic', 1),
(636, '?Heat Treating, Annealing, and Tempering Machine Operators and Tenders, Metal and Plastic', 1),
(637, '?Heaters, Metal and Plastic', 1),
(638, '?Heating and Air Conditioning Mechanics', 1),
(639, '?Heating Equipment Setters and Set-Up Operators, Metal and Plastic', 1),
(640, '?Heating, Air Conditioning, and Refrigeration Mechanics and Installers', 1),
(641, '?Helpers, Construction Trades, All Other', 1),
(642, '?Helpers--Brickmasons, Blockmasons, Stonemasons, and Tile and Marble Setters', 1),
(643, '?Helpers--Carpenters', 1),
(644, '?Helpers--Electricians', 1),
(645, '?Helpers--Extraction Workers', 1),
(646, '?Helpers--Installation, Maintenance, and Repair Workers', 1),
(647, '?Helpers--Painters, Paperhangers, Plasterers, and Stucco Masons', 1),
(648, '?Helpers--Pipelayers, Plumbers, Pipefitters, and Steamfitters', 1),
(649, '?Helpers--Production Workers', 1),
(650, '?Helpers--Roofers', 1),
(651, '?Highway Maintenance Workers', 1),
(652, '?Highway Patrol Pilots', 1),
(653, '?Historians', 1),
(654, '?History Teachers, Postsecondary', 1),
(655, '?Histotechnologists and Histologic Technicians?', 1),
(656, '?Hoist and Winch Operators', 1),
(657, '?Home Appliance Installers', 1),
(658, '?Home Appliance Repairers', 1),
(659, '?Home Economics Teachers, Postsecondary', 1),
(660, '?Home Health Aides', 1),
(661, '?Hospitalists?', 1),
(662, '?Hosts and Hostesses, Restaurant, Lounge, and Coffee Shop', 1),
(663, '?Hotel, Motel, and Resort Desk Clerks', 1),
(664, '?Housekeeping Supervisors', 1),
(665, '?Human Factors Engineers and Ergonomists?', 1),
(666, '?Human Resources Assistants, Except Payroll and Timekeeping', 1),
(667, '?Human Resources Managers', 1),
(668, '?Human Resources Managers?', 1),
(669, '?Human Resources Managers, All Other', 1),
(670, '?Human Resources, Training, and Labor Relations Specialists, All Other', 1),
(671, '?Hunters and Trappers', 1),
(672, '?Hydroelectric Plant Technicians?', 1),
(673, '?Hydroelectric Production Managers?', 1),
(674, '?Hydrologists', 1),
(675, '?Immigration and Customs Inspectors', 1),
(676, '?Industrial Ecologists?', 1),
(677, '?Industrial Engineering Technicians', 1),
(678, '?Industrial Engineering Technologists?', 1),
(679, '?Industrial Engineers', 1),
(680, '?Industrial Machinery Mechanics', 1),
(681, '?Industrial Production Managers', 1),
(682, '?Industrial Safety and Health Engineers', 1),
(683, '?Industrial Truck and Tractor Operators', 1),
(684, '?Industrial-Organizational Psychologists', 1),
(685, '?Infantry', 1),
(686, '?Infantry Officers', 1),
(687, '?Informatics Nurse Specialists?', 1),
(688, '?Information and Record Clerks, All Other', 1),
(689, '?Information Security Analysts?', 1),
(690, '?Information Technology Project Managers?', 1),
(691, '?Inspectors, Testers, Sorters, Samplers, and Weighers', 1),
(692, '?Installation, Maintenance, and Repair Workers, All Other', 1),
(693, '?Instructional Coordinators', 1),
(694, '?Instructional Designers and Technologists?', 1),
(695, '?Insulation Workers, Floor, Ceiling, and Wall', 1),
(696, '?Insulation Workers, Mechanical', 1),
(697, '?Insurance Adjusters, Examiners, and Investigators', 1),
(698, '?Insurance Appraisers, Auto Damage', 1),
(699, '?Insurance Claims and Policy Processing Clerks', 1),
(700, '?Insurance Claims Clerks', 1),
(701, '?Insurance Policy Processing Clerks', 1),
(702, '?Insurance Sales Agents', 1),
(703, '?Insurance Underwriters', 1),
(704, '?Intelligence Analysts?', 1),
(705, '?Interior Designers', 1),
(706, '?Internists, General', 1),
(707, '?Interpreters and Translators', 1),
(708, '?Interviewers, Except Eligibility and Loan', 1),
(709, '?Investment Fund Managers?', 1),
(710, '?Investment Underwriters?', 1),
(711, '?Irradiated-Fuel Handlers', 1),
(712, '?Janitorial Supervisors', 1),
(713, '?Janitors and Cleaners, Except Maids and Housekeeping Cleaners', 1),
(714, '?Jewelers', 1),
(715, '?Jewelers and Precious Stone and Metal Workers', 1),
(716, '?Job Printers', 1),
(717, '?Judges, Magistrate Judges, and Magistrates', 1),
(718, '?Judicial Law Clerks?', 1),
(719, '?Keyboard Instrument Repairers and Tuners', 1),
(720, '?Kindergarten Teachers, Except Special Education', 1),
(721, '?Labor Relations Specialists?', 1),
(722, '?Laborers and Freight, Stock, and Material Movers, Hand', 1),
(723, '?Landscape Architects', 1),
(724, '?Landscaping and Groundskeeping Workers', 1),
(725, '?Lathe and Turning Machine Tool Setters, Operators, and Tenders, Metal and Plastic', 1),
(726, '?Laundry and Drycleaning Machine Operators and Tenders, Except Pressing', 1),
(727, '?Laundry and Dry-Cleaning Workers', 1),
(728, '?Law Clerks', 1),
(729, '?Law Teachers, Postsecondary', 1),
(730, '?Lawn Service Managers', 1),
(731, '?Lawyers', 1),
(732, '?Lay-Out Workers, Metal and Plastic', 1),
(733, '?Legal Secretaries', 1),
(734, '?Legal Support Workers, All Other', 1),
(735, '?Legislators', 1),
(736, '?Letterpress Setters and Set-Up Operators', 1),
(737, '?Librarians', 1),
(738, '?Library Assistants, Clerical', 1),
(739, '?Library Science Teachers, Postsecondary', 1),
(740, '?Library Technicians', 1),
(741, '?License Clerks', 1),
(742, '?Licensed Practical and Licensed Vocational Nurses', 1),
(743, '?Licensing Examiners and Inspectors', 1),
(744, '?Life Scientists, All Other', 1),
(745, '?Life, Physical, and Social Science Technicians, All Other', 1),
(746, '?Lifeguards, Ski Patrol, and Other Recreational Protective Service Workers', 1),
(747, '?Loading Machine Operators, Underground Mining', 1),
(748, '?Loan Counselors', 1),
(749, '?Loan Counselors?', 1),
(750, '?Loan Interviewers and Clerks', 1),
(751, '?Loan Officers', 1),
(752, '?Locker Room, Coatroom, and Dressing Room Attendants', 1),
(753, '?Locksmiths and Safe Repairers', 1),
(754, '?Locomotive Engineers', 1),
(755, '?Locomotive Firers', 1),
(756, '?Lodging Managers', 1),
(757, '?Log Graders and Scalers', 1),
(758, '?Logging Equipment Operators', 1),
(759, '?Logging Tractor Operators', 1),
(760, '?Logging Workers, All Other', 1),
(761, '?Logisticians', 1),
(762, '?Logistics Analysts?', 1),
(763, '?Logistics Engineers?', 1),
(764, '?Logistics Managers?', 1),
(765, '?Loss Prevention Managers?', 1),
(766, '?Low Vision Therapists, Orientation and Mobility Specialists, and Vision Rehabilitation Therapists?', 1),
(767, '?Machine Feeders and Offbearers', 1),
(768, '?Machinists', 1),
(769, '?Magnetic Resonance Imaging Technologists?', 1),
(770, '?Maids and Housekeeping Cleaners', 1),
(771, '?Mail Clerks and Mail Machine Operators, Except Postal Service', 1),
(772, '?Mail Clerks, Except Mail Machine Operators and Postal Service', 1),
(773, '?Mail Machine Operators, Preparation and Handling', 1),
(774, '?Maintenance and Repair Workers, General', 1),
(775, '?Maintenance and Repair Workers, General?', 1),
(776, '?Maintenance Workers, Machinery', 1),
(777, '?Makeup Artists, Theatrical and Performance', 1),
(778, '?Management Analysts', 1),
(779, '?Managers, All Other', 1),
(780, '?Manicurists and Pedicurists', 1),
(781, '?Manufactured Building and Mobile Home Installers', 1),
(782, '?Manufacturing Engineering Technologists?', 1),
(783, '?Manufacturing Engineers?', 1),
(784, '?Manufacturing Production Technicians?', 1),
(785, '?Mapping Technicians', 1),
(786, '?Marine Architects', 1),
(787, '?Marine Cargo Inspectors', 1),
(788, '?Marine Engineers', 1),
(789, '?Marine Engineers and Naval Architects', 1),
(790, '?Market Research Analysts', 1),
(791, '?Market Research Analysts and Marketing Specialists?', 1),
(792, '?Marketing Managers', 1),
(793, '?Marking and Identification Printing Machine Setters and Set-Up Operators', 1),
(794, '?Marking Clerks', 1),
(795, '?Marriage and Family Therapists', 1),
(796, '?Massage Therapists', 1),
(797, '?Material Moving Workers, All Other', 1),
(798, '?Materials Engineers', 1),
(799, '?Materials Inspectors', 1),
(800, '?Materials Scientists', 1),
(801, '?Mates- Ship, Boat, and Barge', 1),
(802, '?Mathematical Science Occupations, All Other', 1),
(803, '?Mathematical Science Teachers, Postsecondary', 1),
(804, '?Mathematical Technicians', 1),
(805, '?Mathematicians', 1),
(806, '?Meat, Poultry, and Fish Cutters and Trimmers', 1),
(807, '?Mechanical Door Repairers', 1),
(808, '?Mechanical Drafters', 1),
(809, '?Mechanical Engineering Technicians', 1),
(810, '?Mechanical Engineering Technologists?', 1),
(811, '?Mechanical Engineers', 1),
(812, '?Mechanical Inspectors', 1),
(813, '?Mechatronics Engineers?', 1),
(814, '?Media and Communication Equipment Workers, All Other', 1),
(815, '?Media and Communication Workers, All Other', 1),
(816, '?Medical and Clinical Laboratory Technicians', 1),
(817, '?Medical and Clinical Laboratory Technologists', 1),
(818, '?Medical and Health Services Managers', 1),
(819, '?Medical and Public Health Social Workers', 1),
(820, '?Medical Appliance Technicians', 1),
(821, '?Medical Assistants', 1),
(822, '?Medical Equipment Preparers', 1),
(823, '?Medical Equipment Repairers', 1),
(824, '?Medical Records and Health Information Technicians', 1),
(825, '?Medical Scientists, Except Epidemiologists', 1),
(826, '?Medical Secretaries', 1),
(827, '?Medical Transcriptionists', 1),
(828, '?Meeting and Convention Planners', 1),
(829, '?Mental Health and Substance Abuse Social Workers', 1),
(830, '?Mental Health Counselors', 1),
(831, '?Merchandise Displayers and Window Trimmers', 1),
(832, '?Metal Fabricators, Structural Metal Products', 1),
(833, '?Metal Molding, Coremaking, and Casting Machine Operators and Tenders', 1),
(834, '?Metal Molding, Coremaking, and Casting Machine Setters and Set-Up Operators', 1),
(835, '?Metal Workers and Plastic Workers, All Other', 1),
(836, '?Metal-Refining Furnace Operators and Tenders', 1),
(837, '?Meter Mechanics', 1),
(838, '?Meter Readers, Utilities', 1),
(839, '?Methane Landfill Gas Generation System Technicians?', 1),
(840, '?Methane/Landfill Gas Collection System Operators?', 1),
(841, '?Microbiologists', 1),
(842, '?Microsystems Engineers?', 1),
(843, '?Middle School Teachers, Except Special and Vocational Education', 1),
(844, '?Midwives?', 1),
(845, '?Military Enlisted Tactical Operations and Air--Weapons Specialists and Crew Members, All Other', 1),
(846, '?Military Officer Special and Tactical Operations Leaders--Managers, All Other', 1),
(847, '?Milling and Planing Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(848, '?Millwrights', 1),
(849, '?Mine Cutting and Channeling Machine Operators', 1),
(850, '?Mining and Geological Engineers, Including Mining Safety Engineers', 1),
(851, '?Mining Machine Operators, All Other', 1),
(852, '?Mixing and Blending Machine Setters, Operators, and Tenders', 1),
(853, '?Mobile Heavy Equipment Mechanics, Except Engines', 1),
(854, '?Model and Mold Makers, Jewelry', 1),
(855, '?Model Makers, Metal and Plastic', 1),
(856, '?Model Makers, Wood', 1),
(857, '?Models', 1),
(858, '?Mold Makers, Hand', 1),
(859, '?Molders, Shapers, and Casters, Except Metal and Plastic', 1),
(860, '?Molding and Casting Workers', 1),
(861, '?Molding, Coremaking, and Casting Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(862, '?Molecular and Cellular Biologists?', 1),
(863, '?Morticians, Undertakers, and Funeral Directors?', 1),
(864, '?Motion Picture Projectionists', 1),
(865, '?Motor Vehicle Inspectors', 1),
(866, '?Motor Vehicle Operators, All Other', 1),
(867, '?Motorboat Mechanics', 1),
(868, '?Motorboat Operators', 1),
(869, '?Motorcycle Mechanics', 1),
(870, '?Multi-Media Artists and Animators', 1),
(871, '?Multiple Machine Tool Setters, Operators, and Tenders, Metal and Plastic', 1),
(872, '?Municipal Clerks', 1),
(873, '?Municipal Fire Fighters', 1),
(874, '?Municipal Fire Fighting and Prevention Supervisors', 1),
(875, '?Museum Technicians and Conservators', 1),
(876, '?Music Arrangers and Orchestrators', 1),
(877, '?Music Composers and Arrangers?', 1),
(878, '?Music Directors', 1),
(879, '?Music Directors and Composers', 1),
(880, '?Music Therapists?', 1),
(881, '?Musical Instrument Repairers and Tuners', 1),
(882, '?Musicians and Singers', 1),
(883, '?Musicians, Instrumental', 1),
(884, '?Nannies?', 1),
(885, '?Nanosystems Engineers?', 1),
(886, '?Nanotechnology Engineering Technicians?', 1),
(887, '?Nanotechnology Engineering Technologists?', 1),
(888, '?Natural Sciences Managers', 1),
(889, '?Naturopathic Physicians?', 1),
(890, '?Network and Computer Systems Administrators?', 1),
(891, '?Network and Computer Systems Administrators', 1),
(892, '?Network Systems and Data Communications Analysts', 1),
(893, '?Neurodiagnostic Technologists?', 1),
(894, '?Neurologists?', 1),
(895, '?Neuropsychologists and Clinical Neuropsychologists?', 1),
(896, '?New Accounts Clerks', 1),
(897, '?Non-Destructive Testing Specialists?', 1),
(898, '?Nonelectrolytic Plating and Coating Machine Operators and Tenders, Metal and Plastic', 1),
(899, '?Nonelectrolytic Plating and Coating Machine Setters and Set-Up Operators, Metal and Plastic', 1),
(900, '?Nonfarm Animal Caretakers', 1),
(901, '?Nuclear Engineers', 1),
(902, '?Nuclear Equipment Operation Technicians', 1),
(903, '?Nuclear Medicine Physicians?', 1),
(904, '?Nuclear Medicine Technologists', 1),
(905, '?Nuclear Monitoring Technicians', 1),
(906, '?Nuclear Power Reactor Operators', 1),
(907, '?Nuclear Technicians', 1),
(908, '?Numerical Control Machine Tool Operators and Tenders, Metal and Plastic', 1),
(909, '?Numerical Tool and Process Control Programmers', 1),
(910, '?Nurse Anesthetists?', 1),
(911, '?Nurse Midwives?', 1),
(912, '?Nurse Practitioners?', 1),
(913, '?Nursery and Greenhouse Managers?', 1),
(914, '?Nursery and Greenhouse Managers', 1),
(915, '?Nursery Workers', 1),
(916, '?Nursing Aides, Orderlies, and Attendants', 1),
(917, '?Nursing Assistants?', 1),
(918, '?Nursing Instructors and Teachers, Postsecondary', 1),
(919, '?Obstetricians and Gynecologists', 1),
(920, '?Occupational Health and Safety Specialists', 1),
(921, '?Occupational Health and Safety Technicians', 1),
(922, '?Occupational Therapist Aides', 1),
(923, '?Occupational Therapist Assistants', 1),
(924, '?Occupational Therapists', 1),
(925, '?Office and Administrative Support Workers, All Other', 1),
(926, '?Office Clerks, General', 1),
(927, '?Office Machine and Cash Register Servicers', 1),
(928, '?Office Machine Operators, Except Computer', 1),
(929, '?Offset Lithographic Press Setters and Set-Up Operators', 1),
(930, '?Online Merchants?', 1),
(931, '?Operating Engineers', 1),
(932, '?Operating Engineers and Other Construction Equipment Operators', 1),
(933, '?Operations Research Analysts', 1),
(934, '?Ophthalmic Laboratory Technicians', 1),
(935, '?Ophthalmic Medical Technicians?', 1),
(936, '?Ophthalmic Medical Technologists?', 1),
(937, '?Ophthalmologists?', 1),
(938, '?Optical Instrument Assemblers', 1),
(939, '?Opticians, Dispensing', 1),
(940, '?Optometrists', 1),
(941, '?Oral and Maxillofacial Surgeons', 1),
(942, '?Order Clerks', 1),
(943, '?Order Fillers, Wholesale and Retail Sales', 1),
(944, '?Orderlies?', 1),
(945, '?Ordinary Seamen and Marine Oilers', 1),
(946, '?Orthodontists', 1),
(947, '?Orthoptists?', 1),
(948, '?Orthotists and Prosthetists', 1),
(949, '?Outdoor Power Equipment and Other Small Engine Mechanics', 1),
(950, '?Packaging and Filling Machine Operators and Tenders', 1),
(951, '?Packers and Packagers, Hand', 1),
(952, '?Painters and Illustrators', 1),
(953, '?Painters, Construction and Maintenance', 1),
(954, '?Painters, Transportation Equipment', 1),
(955, '?Painting, Coating, and Decorating Workers', 1),
(956, '?Pantograph Engravers', 1),
(957, '?Paper Goods Machine Setters, Operators, and Tenders', 1),
(958, '?Paperhangers', 1),
(959, '?Paralegals and Legal Assistants', 1),
(960, '?Park Naturalists', 1),
(961, '?Parking Enforcement Workers', 1),
(962, '?Parking Lot Attendants', 1),
(963, '?Parts Salespersons', 1),
(964, '?Paste-Up Workers', 1),
(965, '?Pathologists?', 1),
(966, '?Patient Representatives?', 1),
(967, '?Patternmakers, Metal and Plastic', 1),
(968, '?Patternmakers, Wood', 1),
(969, '?Paving, Surfacing, and Tamping Equipment Operators', 1),
(970, '?Payroll and Timekeeping Clerks', 1),
(971, '?Pediatricians, General', 1),
(972, '?Percussion Instrument Repairers and Tuners', 1),
(973, '?Personal and Home Care Aides', 1),
(974, '?Personal Care and Service Workers, All Other', 1),
(975, '?Personal Financial Advisors', 1),
(976, '?Personnel Recruiters', 1),
(977, '?Pest Control Workers', 1),
(978, '?Pesticide Handlers, Sprayers, and Applicators, Vegetation', 1),
(979, '?Petroleum Engineers', 1),
(980, '?Petroleum Pump System Operators', 1),
(981, '?Petroleum Pump System Operators, Refinery Operators, and Gaugers', 1),
(982, '?Petroleum Refinery and Control Panel Operators', 1),
(983, '?Pewter Casters and Finishers', 1),
(984, '?Pharmacists', 1),
(985, '?Pharmacy Aides', 1),
(986, '?Pharmacy Technicians', 1),
(987, '?Philosophy and Religion Teachers, Postsecondary', 1),
(988, '?Phlebotomists?', 1),
(989, '?Photoengravers', 1),
(990, '?Photoengraving and Lithographing Machine Operators and Tenders', 1),
(991, '?Photographers', 1),
(992, '?Photographers, Scientific', 1),
(993, '?Photographic Hand Developers', 1),
(994, '?Photographic Process Workers', 1),
(995, '?Photographic Process Workers and Processing Machine Operators?', 1),
(996, '?Photographic Processing Machine Operators', 1),
(997, '?Photographic Reproduction Technicians', 1),
(998, '?Photographic Retouchers and Restorers', 1),
(999, '?Photonics Engineers?', 1),
(1000, '?Photonics Technicians?', 1),
(1001, '?Physical Medicine and Rehabilitation Physicians?', 1),
(1002, '?Physical Scientists, All Other', 1),
(1003, '?Physical Therapist Aides', 1),
(1004, '?Physical Therapist Assistants', 1),
(1005, '?Physical Therapists', 1),
(1006, '?Physician Assistants', 1),
(1007, '?Physicians and Surgeons, All Other', 1),
(1008, '?Physicists', 1),
(1009, '?Physics Teachers, Postsecondary', 1),
(1010, '?Pile-Driver Operators', 1),
(1011, '?Pilots, Ship', 1),
(1012, '?Pipe Fitters', 1),
(1013, '?Pipelayers', 1),
(1014, '?Pipelaying Fitters', 1),
(1015, '?Plant and System Operators, All Other', 1),
(1016, '?Plant Scientists', 1),
(1017, '?Plasterers and Stucco Masons', 1),
(1018, '?Plastic Molding and Casting Machine Operators and Tenders', 1),
(1019, '?Plastic Molding and Casting Machine Setters and Set-Up Operators', 1),
(1020, '?Plate Finishers', 1),
(1021, '?Platemakers', 1),
(1022, '?Plating and Coating Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(1023, '?Plumbers', 1),
(1024, '?Plumbers, Pipefitters, and Steamfitters', 1),
(1025, '?Podiatrists', 1),
(1026, '?Poets and Lyricists', 1),
(1027, '?Poets, Lyricists and Creative Writers?', 1),
(1028, '?Police and Sheriffs Patrol Officers', 1),
(1029, '?Police Detectives', 1),
(1030, '?Police Identification and Records Officers', 1),
(1031, '?Police Patrol Officers', 1),
(1032, '?Police, Fire, and Ambulance Dispatchers', 1),
(1033, '?Political Science Teachers, Postsecondary', 1),
(1034, '?Political Scientists', 1),
(1035, '?Postal Service Clerks', 1),
(1036, '?Postal Service Mail Carriers', 1),
(1037, '?Postal Service Mail Sorters, Processors, and Processing Machine Operators', 1),
(1038, '?Postmasters and Mail Superintendents', 1),
(1039, '?Postsecondary Teachers, All Other', 1),
(1040, '?Potters', 1),
(1041, '?Pourers and Casters, Metal', 1),
(1042, '?Power Distributors and Dispatchers', 1),
(1043, '?Power Generating Plant Operators, Except Auxiliary Equipment Operators', 1),
(1044, '?Power Plant Operators', 1),
(1045, '?Precious Metal Workers?', 1),
(1046, '?Precision Agriculture Technicians?', 1),
(1047, '?Precision Devices Inspectors and Testers', 1),
(1048, '?Precision Dyers', 1),
(1049, '?Precision Etchers and Engravers, Hand or Machine', 1),
(1050, '?Precision Instrument and Equipment Repairers, All Other', 1),
(1051, '?Precision Lens Grinders and Polishers', 1),
(1052, '?Precision Mold and Pattern Casters, except Nonferrous Metals', 1),
(1053, '?Precision Pattern and Die Casters, Nonferrous Metals', 1),
(1054, '?Precision Printing Workers', 1),
(1055, '?Prepress Technicians and Workers', 1),
(1056, '?Prepress Technicians and Workers?', 1),
(1057, '?Preschool Teachers, Except Special Education', 1),
(1058, '?Press and Press Brake Machine Setters and Set-Up Operators, Metal and Plastic', 1),
(1059, '?Pressers, Delicate Fabrics', 1),
(1060, '?Pressers, Hand', 1),
(1061, '?Pressers, Textile, Garment, and Related Materials', 1),
(1062, '?Pressing Machine Operators and Tenders- Textile, Garment, and Related Materials', 1),
(1063, '?Pressure Vessel Inspectors', 1),
(1064, '?Preventive Medicine Physicians?', 1),
(1065, '?Print Binding and Finishing Workers?', 1),
(1066, '?Printing Machine Operators', 1),
(1067, '?Printing Press Machine Operators and Tenders', 1),
(1068, '?Printing Press Operators?', 1),
(1069, '?Private Detectives and Investigators', 1),
(1070, '?Private Sector Executives', 1),
(1071, '?Probation Officers and Correctional Treatment Specialists', 1),
(1072, '?Procurement Clerks', 1),
(1073, '?Producers', 1),
(1074, '?Producers and Directors', 1),
(1075, '?Product Safety Engineers', 1),
(1076, '?Production Helpers', 1),
(1077, '?Production Inspectors, Testers, Graders, Sorters, Samplers, Weighers', 1),
(1078, '?Production Laborers', 1),
(1079, '?Production Workers, All Other', 1),
(1080, '?Production, Planning, and Expediting Clerks', 1),
(1081, '?Professional Photographers', 1),
(1082, '?Program Directors', 1),
(1083, '?Proofreaders and Copy Markers', 1),
(1084, '?Property, Real Estate, and Community Association Managers', 1),
(1085, '?Prosthodontists', 1),
(1086, '?Protective Service Workers, All Other', 1),
(1087, '?Psychiatric Aides', 1),
(1088, '?Psychiatric Technicians', 1),
(1089, '?Psychiatrists', 1),
(1090, '?Psychologists, All Other', 1),
(1091, '?Psychology Teachers, Postsecondary', 1),
(1092, '?Public Address System and Other Announcers', 1),
(1093, '?Public Relations Managers', 1);
INSERT INTO `workingas` (`Work_Id`, `Work_Name`, `Status`) VALUES
(1094, '?Public Relations Specialists', 1),
(1095, '?Public Transportation Inspectors', 1),
(1096, '?Pump Operators, Except Wellhead Pumpers', 1),
(1097, '?Punching Machine Setters and Set-Up Operators, Metal and Plastic', 1),
(1098, '?Purchasing Agents and Buyers, Farm Products', 1),
(1099, '?Purchasing Agents, Except Wholesale, Retail, and Farm Products', 1),
(1100, '?Purchasing Managers', 1),
(1101, '?Quality Control Analysts?', 1),
(1102, '?Quality Control Systems Managers?', 1),
(1103, '?Radar and Sonar Technicians', 1),
(1104, '?Radiation Therapists', 1),
(1105, '?Radio and Television Announcers', 1),
(1106, '?Radio Frequency Identification Device Specialists?', 1),
(1107, '?Radio Mechanics?', 1),
(1108, '?Radio Mechanics', 1),
(1109, '?Radio Operators', 1),
(1110, '?Radiologic Technicians', 1),
(1111, '?Radiologic Technicians?', 1),
(1112, '?Radiologic Technologists', 1),
(1113, '?Radiologic Technologists and Technicians', 1),
(1114, '?Radiologists?', 1),
(1115, '?Rail Car Repairers', 1),
(1116, '?Rail Transportation Workers, All Other', 1),
(1117, '?Rail Yard Engineers, Dinkey Operators, and Hostlers', 1),
(1118, '?Railroad Brake, Signal, and Switch Operators', 1),
(1119, '?Railroad Conductors and Yardmasters', 1),
(1120, '?Railroad Inspectors', 1),
(1121, '?Railroad Yard Workers', 1),
(1122, '?Rail-Track Laying and Maintenance Equipment Operators', 1),
(1123, '?Range Managers', 1),
(1124, '?Real Estate Brokers', 1),
(1125, '?Real Estate Sales Agents', 1),
(1126, '?Receptionists and Information Clerks', 1),
(1127, '?Recreation and Fitness Studies Teachers, Postsecondary', 1),
(1128, '?Recreation Workers', 1),
(1129, '?Recreational Therapists', 1),
(1130, '?Recreational Vehicle Service Technicians', 1),
(1131, '?Recycling and Reclamation Workers?', 1),
(1132, '?Recycling Coordinators?', 1),
(1133, '?Reed or Wind Instrument Repairers and Tuners', 1),
(1134, '?Refractory Materials Repairers, Except Brickmasons', 1),
(1135, '?Refrigeration Mechanics', 1),
(1136, '?Refuse and Recyclable Material Collectors', 1),
(1137, '?Registered Nurses', 1),
(1138, '?Registered Nurses?', 1),
(1139, '?Regulatory Affairs Managers?', 1),
(1140, '?Regulatory Affairs Specialists?', 1),
(1141, '?Rehabilitation Counselors', 1),
(1142, '?Reinforcing Iron and Rebar Workers', 1),
(1143, '?Religious Workers, All Other', 1),
(1144, '?Remote Sensing Scientists and Technologists?', 1),
(1145, '?Remote Sensing Technicians?', 1),
(1146, '?Reporters and Correspondents', 1),
(1147, '?Reservation and Transportation Ticket Agents', 1),
(1148, '?Reservation and Transportation Ticket Agents and Travel Clerks', 1),
(1149, '?Residential Advisors', 1),
(1150, '?Respiratory Therapists', 1),
(1151, '?Respiratory Therapy Technicians', 1),
(1152, '?Retail Loss Prevention Specialists?', 1),
(1153, '?Retail Salespersons', 1),
(1154, '?Riggers', 1),
(1155, '?Risk Management Specialists?', 1),
(1156, '?Robotics Engineers?', 1),
(1157, '?Robotics Technicians?', 1),
(1158, '?Rock Splitters, Quarry', 1),
(1159, '?Rolling Machine Setters, Operators, and Tenders, Metal and Plastic', 1),
(1160, '?Roof Bolters, Mining', 1),
(1161, '?Roofers', 1),
(1162, '?Rotary Drill Operators, Oil and Gas', 1),
(1163, '?Rough Carpenters', 1),
(1164, '?Roustabouts, Oil and Gas', 1),
(1165, '?Sailors and Marine Oilers', 1),
(1166, '?Sales Agents, Financial Services', 1),
(1167, '?Sales Agents, Securities and Commodities', 1),
(1168, '?Sales and Related Workers, All Other', 1),
(1169, '?Sales Engineers', 1),
(1170, '?Sales Managers', 1),
(1171, '?Sales Representatives, Agricultural', 1),
(1172, '?Sales Representatives, Chemical and Pharmaceutical', 1),
(1173, '?Sales Representatives, Electrical--Electronic', 1),
(1174, '?Sales Representatives, Instruments', 1),
(1175, '?Sales Representatives, Mechanical Equipment and Supplies', 1),
(1176, '?Sales Representatives, Medical', 1),
(1177, '?Sales Representatives, Services, All Other', 1),
(1178, '?Sales Representatives, Wholesale and Manufacturing, Except Technical and Scientific Products', 1),
(1179, '?Sales Representatives, Wholesale and Manufacturing, Technical and Scientific Products', 1),
(1180, '?Sawing Machine Operators and Tenders', 1),
(1181, '?Sawing Machine Setters and Set-Up Operators', 1),
(1182, '?Sawing Machine Setters, Operators, and Tenders, Wood', 1),
(1183, '?Sawing Machine Tool Setters and Set-Up Operators, Metal and Plastic', 1),
(1184, '?Scanner Operators', 1),
(1185, '?Screen Printing Machine Setters and Set-Up Operators', 1),
(1186, '?Sculptors', 1),
(1187, '?Search Marketing Strategists?', 1),
(1188, '?Secondary School Teachers, Except Special and Vocational Education', 1),
(1189, '?Secretaries, Except Legal, Medical, and Executive', 1),
(1190, '?Securities and Commodities Traders?', 1),
(1191, '?Securities, Commodities, and Financial Services Sales Agents', 1),
(1192, '?Security and Fire Alarm Systems Installers', 1),
(1193, '?Security Guards', 1),
(1194, '?Security Management Specialists?', 1),
(1195, '?Security Managers?', 1),
(1196, '?Segmental Pavers', 1),
(1197, '?Self-Enrichment Education Teachers', 1),
(1198, '?Semiconductor Processors', 1),
(1199, '?Separating, Filtering, Clarifying, Precipitating, and Still Machine Setters, Operators, and Tenders', 1),
(1200, '?Septic Tank Servicers and Sewer Pipe Cleaners', 1),
(1201, '?Service Station Attendants', 1),
(1202, '?Service Unit Operators, Oil, Gas, and Mining', 1),
(1203, '?Set and Exhibit Designers', 1),
(1204, '?Set Designers', 1),
(1205, '?Sewers, Hand', 1),
(1206, '?Sewing Machine Operators', 1),
(1207, '?Sewing Machine Operators, Garment', 1),
(1208, '?Sewing Machine Operators, Non-Garment', 1),
(1209, '?Shampooers', 1),
(1210, '?Shear and Slitter Machine Setters and Set-Up Operators, Metal and Plastic', 1),
(1211, '?Sheet Metal Workers', 1),
(1212, '?Sheriffs and Deputy Sheriffs', 1),
(1213, '?Ship and Boat Captains', 1),
(1214, '?Ship Carpenters and Joiners', 1),
(1215, '?Ship Engineers', 1),
(1216, '?Shipping, Receiving, and Traffic Clerks', 1),
(1217, '?Shoe and Leather Workers and Repairers', 1),
(1218, '?Shoe Machine Operators and Tenders', 1),
(1219, '?Shop and Alteration Tailors', 1),
(1220, '?Shuttle Car Operators', 1),
(1221, '?Signal and Track Switch Repairers', 1),
(1222, '?Silversmiths', 1),
(1223, '?Singers', 1),
(1224, '?Sketch Artists', 1),
(1225, '?Skin Care Specialists', 1),
(1226, '?Slaughterers and Meat Packers', 1),
(1227, '?Slot Key Persons', 1),
(1228, '?Social and Community Service Managers', 1),
(1229, '?Social and Human Service Assistants', 1),
(1230, '?Social Science Research Assistants', 1),
(1231, '?Social Sciences Teachers, Postsecondary, All Other', 1),
(1232, '?Social Scientists and Related Workers, All Other', 1),
(1233, '?Social Work Teachers, Postsecondary', 1),
(1234, '?Social Workers, All Other', 1),
(1235, '?Sociologists', 1),
(1236, '?Sociology Teachers, Postsecondary', 1),
(1237, '?Software Developers, Applications?', 1),
(1238, '?Software Developers, Systems Software?', 1),
(1239, '?Software Quality Assurance Engineers and Testers?', 1),
(1240, '?Soil and Plant Scientists', 1),
(1241, '?Soil Conservationists', 1),
(1242, '?Soil Scientists', 1),
(1243, '?Solar Energy Installation Managers?', 1),
(1244, '?Solar Energy Systems Engineers?', 1),
(1245, '?Solar Photovoltaic Installers?', 1),
(1246, '?Solar Sales Representatives and Assessors?', 1),
(1247, '?Solar Thermal Installers and Technicians?', 1),
(1248, '?Solderers', 1),
(1249, '?Solderers and Brazers?', 1),
(1250, '?Soldering and Brazing Machine Operators and Tenders', 1),
(1251, '?Soldering and Brazing Machine Setters and Set-Up Operators', 1),
(1252, '?Sound Engineering Technicians', 1),
(1253, '?Spa Managers?', 1),
(1254, '?Special Education Teachers, Kindergarten and Elementary School?', 1),
(1255, '?Special Education Teachers, Middle School?', 1),
(1256, '?Special Education Teachers, Middle School', 1),
(1257, '?Special Education Teachers, Preschool?', 1),
(1258, '?Special Education Teachers, Preschool, Kindergarten, and Elementary School', 1),
(1259, '?Special Education Teachers, Secondary School', 1),
(1260, '?Special Education Teachers, Secondary School?', 1),
(1261, '?Special Forces', 1),
(1262, '?Special Forces Officers', 1),
(1263, '?Speech-Language Pathologists', 1),
(1264, '?Speech-Language Pathology Assistants?', 1),
(1265, '?Sports Medicine Physicians?', 1),
(1266, '?Spotters, Dry Cleaning', 1),
(1267, '?Statement Clerks', 1),
(1268, '?Station Installers and Repairers, Telephone', 1),
(1269, '?Stationary Engineers', 1),
(1270, '?Stationary Engineers and Boiler Operators', 1),
(1271, '?Statistical Assistants', 1),
(1272, '?Statisticians', 1),
(1273, '?Stevedores, Except Equipment Operators', 1),
(1274, '?Stock Clerks and Order Fillers', 1),
(1275, '?Stock Clerks- Stockroom, Warehouse, or Storage Yard', 1),
(1276, '?Stock Clerks, Sales Floor', 1),
(1277, '?Stone Cutters and Carvers', 1),
(1278, '?Stone Sawyers', 1),
(1279, '?Stonemasons', 1),
(1280, '?Storage and Distribution Managers', 1),
(1281, '?Stringed Instrument Repairers and Tuners', 1),
(1282, '?Strippers', 1),
(1283, '?Structural Iron and Steel Workers', 1),
(1284, '?Structural Metal Fabricators and Fitters', 1),
(1285, '?Substance Abuse and Behavioral Disorder Counselors', 1),
(1286, '?Subway and Streetcar Operators', 1),
(1287, '?Supply Chain Managers?', 1),
(1288, '?Surgeons', 1),
(1289, '?Surgical Assistants?', 1),
(1290, '?Surgical Technologists', 1),
(1291, '?Survey Researchers', 1),
(1292, '?Surveying and Mapping Technicians', 1),
(1293, '?Surveying Technicians', 1),
(1294, '?Surveyors', 1),
(1295, '?Sustainability Specialists?', 1),
(1296, '?Switchboard Operators, Including Answering Service', 1),
(1297, '?Tailors, Dressmakers, and Custom Sewers', 1),
(1298, '?Talent Directors', 1),
(1299, '?Tank Car, Truck, and Ship Loaders', 1),
(1300, '?Tapers', 1),
(1301, '?Tax Examiners, Collectors, and Revenue Agents', 1),
(1302, '?Tax Preparers', 1),
(1303, '?Taxi Drivers and Chauffeurs', 1),
(1304, '?Teacher Assistants', 1),
(1305, '?Teachers and Instructors, All Other', 1),
(1306, '?Team Assemblers', 1),
(1307, '?Technical Directors--Managers', 1),
(1308, '?Technical Writers', 1),
(1309, '?Telecommunications Engineering Specialists?', 1),
(1310, '?Telecommunications Equipment Installers and Repairers, Except Line Installers', 1),
(1311, '?Telecommunications Facility Examiners', 1),
(1312, '?Telecommunications Line Installers and Repairers', 1),
(1313, '?Telemarketers', 1),
(1314, '?Telephone Operators', 1),
(1315, '?Tellers', 1),
(1316, '?Terrazzo Workers and Finishers', 1),
(1317, '?Textile Bleaching and Dyeing Machine Operators and Tenders', 1),
(1318, '?Textile Cutting Machine Setters, Operators, and Tenders', 1),
(1319, '?Textile Knitting and Weaving Machine Setters, Operators, and Tenders', 1),
(1320, '?Textile Winding, Twisting, and Drawing Out Machine Setters, Operators, and Tenders', 1),
(1321, '?Textile, Apparel, and Furnishings Workers, All Other', 1),
(1322, '?Therapists, All Other', 1),
(1323, '?Tile and Marble Setters', 1),
(1324, '?Timing Device Assemblers, Adjusters, and Calibrators', 1),
(1325, '?Tire Builders', 1),
(1326, '?Tire Repairers and Changers', 1),
(1327, '?Title Examiners and Abstractors', 1),
(1328, '?Title Examiners, Abstractors, and Searchers', 1),
(1329, '?Title Searchers', 1),
(1330, '?Tool and Die Makers', 1),
(1331, '?Tool Grinders, Filers, and Sharpeners', 1),
(1332, '?Tour Guides and Escorts', 1),
(1333, '?Tour Guides and Escorts?', 1),
(1334, '?Tractor-Trailer Truck Drivers', 1),
(1335, '?Traffic Technicians', 1),
(1336, '?Train Crew Members', 1),
(1337, '?Training and Development Managers?', 1),
(1338, '?Training and Development Managers', 1),
(1339, '?Training and Development Specialists?', 1),
(1340, '?Training and Development Specialists', 1),
(1341, '?Transformer Repairers', 1),
(1342, '?Transit and Railroad Police', 1),
(1343, '?Transportation Attendants, Except Flight Attendants?', 1),
(1344, '?Transportation Attendants, Except Flight Attendants and Baggage Porters', 1),
(1345, '?Transportation Engineers?', 1),
(1346, '?Transportation Inspectors', 1),
(1347, '?Transportation Managers', 1),
(1348, '?Transportation Planners?', 1),
(1349, '?Transportation Security Screeners?', 1),
(1350, '?Transportation Vehicle, Equipment and Systems Inspectors, Except Aviation', 1),
(1351, '?Transportation Workers, All Other', 1),
(1352, '?Transportation, Storage, and Distribution Managers', 1),
(1353, '?Travel Agents', 1),
(1354, '?Travel Clerks', 1),
(1355, '?Travel Guides', 1),
(1356, '?Travel Guides?', 1),
(1357, '?Treasurers, Controllers, and Chief Financial Officers', 1),
(1358, '?Tree Trimmers and Pruners', 1),
(1359, '?Truck Drivers, Heavy', 1),
(1360, '?Truck Drivers, Heavy and Tractor-Trailer', 1),
(1362, '?Tutors?', 1),
(1363, '?Typesetting and Composing Machine Operators and Tenders', 1),
(1364, '?Umpires, Referees, and Other Sports Officials', 1),
(1365, '?Upholsterers', 1),
(1366, '?Urban and Regional Planners', 1),
(1367, '?Urologists?', 1),
(1368, '?Ushers, Lobby Attendants, and Ticket Takers', 1),
(1369, '?Validation Engineers?', 1),
(1370, '?Valve and Regulator Repairers', 1),
(1371, '?Veterinarians', 1),
(1372, '?Veterinary Assistants and Laboratory Animal Caretakers', 1),
(1373, '?Veterinary Technologists and Technicians', 1),
(1374, '?Video Game Designers?', 1),
(1375, '?Vocational Education Teachers Postsecondary', 1),
(1376, '?Vocational Education Teachers, Middle School', 1),
(1377, '?Vocational Education Teachers, Secondary School', 1),
(1378, '?Waiters and Waitresses', 1),
(1379, '?Watch Repairers', 1),
(1380, '?Water and Liquid Waste Treatment Plant and System Operators', 1),
(1381, '?Water Resource Specialists?', 1),
(1382, '?Water/Wastewater Engineers?', 1),
(1383, '?Weatherization Installers and Technicians?', 1),
(1384, '?Web Administrators?', 1),
(1385, '?Web Developers?', 1),
(1386, '?Weighers, Measurers, Checkers, and Samplers, Recordkeeping', 1),
(1387, '?Welder-Fitters', 1),
(1388, '?Welders and Cutters', 1),
(1389, '?Welders, Cutters, and Welder Fitters?', 1),
(1390, '?Welders, Cutters, Solderers, and Brazers', 1),
(1391, '?Welders, Production', 1),
(1392, '?Welding Machine Operators and Tenders', 1),
(1393, '?Welding Machine Setters and Set-Up Operators', 1),
(1394, '?Welding, Soldering, and Brazing Machine Setters, Operators, and Tenders', 1),
(1395, '?Welfare Eligibility Workers and Interviewers', 1),
(1396, '?Well and Core Drill Operators', 1),
(1397, '?Wellhead Pumpers', 1),
(1398, '?Wholesale and Retail Buyers, Except Farm Products', 1),
(1399, '?Wind Energy Engineers?', 1),
(1400, '?Wind Energy Operations Managers?', 1),
(1401, '?Wind Energy Project Managers?', 1),
(1402, '?Wind Turbine Service Technicians?', 1),
(1403, '?Woodworkers, All Other', 1),
(1404, '?Woodworking Machine Operators and Tenders, Except Sawing', 1),
(1405, '?Woodworking Machine Setters and Set-Up Operators, Except Sawing', 1),
(1406, '?Woodworking Machine Setters, Operators, and Tenders, Except Sawing', 1),
(1407, '?Word Processors and Typists', 1),
(1408, '?Writers and Authors', 1),
(1409, '?Zoologists and Wildlife Biologists', 1),
(1410, 'Doctor', 1),
(1411, 'Test Profession', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminuser`
--
ALTER TABLE `adminuser`
  ADD PRIMARY KEY (`ADMIN_ID`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citylist`
--
ALTER TABLE `citylist`
  ADD PRIMARY KEY (`City_Id`);

--
-- Indexes for table `Clientdetails`
--
ALTER TABLE `Clientdetails`
  ADD PRIMARY KEY (`Client_Id`);

--
-- Indexes for table `communitylist`
--
ALTER TABLE `communitylist`
  ADD PRIMARY KEY (`Community_Id`);

--
-- Indexes for table `countrylist`
--
ALTER TABLE `countrylist`
  ADD PRIMARY KEY (`Country_Id`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
  ADD PRIMARY KEY (`Degree_Id`);

--
-- Indexes for table `emailsalertssms`
--
ALTER TABLE `emailsalertssms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emailsalertssmstype`
--
ALTER TABLE `emailsalertssmstype`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `languagelist`
--
ALTER TABLE `languagelist`
  ADD PRIMARY KEY (`Language_Id`);

--
-- Indexes for table `mailbox`
--
ALTER TABLE `mailbox`
  ADD PRIMARY KEY (`Mail_Id`);

--
-- Indexes for table `mailsubtypes`
--
ALTER TABLE `mailsubtypes`
  ADD PRIMARY KEY (`MailSubType_Id`);

--
-- Indexes for table `mailtypes`
--
ALTER TABLE `mailtypes`
  ADD PRIMARY KEY (`MailType_Id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `notificationtype`
--
ALTER TABLE `notificationtype`
  ADD PRIMARY KEY (`Notification_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`Page_id`);

--
-- Indexes for table `partnerprofile`
--
ALTER TABLE `partnerprofile`
  ADD PRIMARY KEY (`PartnerProfile_Id`);

--
-- Indexes for table `paymentinfo`
--
ALTER TABLE `paymentinfo`
  ADD PRIMARY KEY (`Payment_Id`);

--
-- Indexes for table `profilepictures`
--
ALTER TABLE `profilepictures`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `recentvisitors`
--
ALTER TABLE `recentvisitors`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `religionlist`
--
ALTER TABLE `religionlist`
  ADD PRIMARY KEY (`Religion_Id`);

--
-- Indexes for table `servicetypes`
--
ALTER TABLE `servicetypes`
  ADD PRIMARY KEY (`Service_Id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `settingtype`
--
ALTER TABLE `settingtype`
  ADD PRIMARY KEY (`Setting_Id`);

--
-- Indexes for table `sliderimages`
--
ALTER TABLE `sliderimages`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `specialization`
--
ALTER TABLE `specialization`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `statelist`
--
ALTER TABLE `statelist`
  ADD PRIMARY KEY (`State_Id`);

--
-- Indexes for table `subcommunitylist`
--
ALTER TABLE `subcommunitylist`
  ADD PRIMARY KEY (`SubCommunityList_Id`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `tbl_message_thread`
--
ALTER TABLE `tbl_message_thread`
  ADD PRIMARY KEY (`thread_id`);

--
-- Indexes for table `tbl_network`
--
ALTER TABLE `tbl_network`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_online`
--
ALTER TABLE `tbl_users_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD PRIMARY KEY (`User_Id`);

--
-- Indexes for table `workingas`
--
ALTER TABLE `workingas`
  ADD PRIMARY KEY (`Work_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminuser`
--
ALTER TABLE `adminuser`
  MODIFY `ADMIN_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `citylist`
--
ALTER TABLE `citylist`
  MODIFY `City_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=691;

--
-- AUTO_INCREMENT for table `Clientdetails`
--
ALTER TABLE `Clientdetails`
  MODIFY `Client_Id` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `communitylist`
--
ALTER TABLE `communitylist`
  MODIFY `Community_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=554;

--
-- AUTO_INCREMENT for table `countrylist`
--
ALTER TABLE `countrylist`
  MODIFY `Country_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `degree`
--
ALTER TABLE `degree`
  MODIFY `Degree_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `emailsalertssms`
--
ALTER TABLE `emailsalertssms`
  MODIFY `id` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `emailsalertssmstype`
--
ALTER TABLE `emailsalertssmstype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `languagelist`
--
ALTER TABLE `languagelist`
  MODIFY `Language_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `mailbox`
--
ALTER TABLE `mailbox`
  MODIFY `Mail_Id` bigint(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `mailsubtypes`
--
ALTER TABLE `mailsubtypes`
  MODIFY `MailSubType_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mailtypes`
--
ALTER TABLE `mailtypes`
  MODIFY `MailType_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `Id` bigint(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `notificationtype`
--
ALTER TABLE `notificationtype`
  MODIFY `Notification_id` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `Page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `partnerprofile`
--
ALTER TABLE `partnerprofile`
  MODIFY `PartnerProfile_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT for table `paymentinfo`
--
ALTER TABLE `paymentinfo`
  MODIFY `Payment_Id` bigint(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `profilepictures`
--
ALTER TABLE `profilepictures`
  MODIFY `Id` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `recentvisitors`
--
ALTER TABLE `recentvisitors`
  MODIFY `Id` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `religionlist`
--
ALTER TABLE `religionlist`
  MODIFY `Religion_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `servicetypes`
--
ALTER TABLE `servicetypes`
  MODIFY `Service_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `Id` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT for table `settingtype`
--
ALTER TABLE `settingtype`
  MODIFY `Setting_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sliderimages`
--
ALTER TABLE `sliderimages`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `specialization`
--
ALTER TABLE `specialization`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `statelist`
--
ALTER TABLE `statelist`
  MODIFY `State_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `subcommunitylist`
--
ALTER TABLE `subcommunitylist`
  MODIFY `SubCommunityList_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
  MODIFY `msg_id` bigint(234) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2837601614;

--
-- AUTO_INCREMENT for table `tbl_message_thread`
--
ALTER TABLE `tbl_message_thread`
  MODIFY `thread_id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_network`
--
ALTER TABLE `tbl_network`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tbl_users_online`
--
ALTER TABLE `tbl_users_online`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=833;

--
-- AUTO_INCREMENT for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userprofile`
--
ALTER TABLE `userprofile`
  MODIFY `User_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=469;

--
-- AUTO_INCREMENT for table `workingas`
--
ALTER TABLE `workingas`
  MODIFY `Work_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1412;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
