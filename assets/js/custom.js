var counter=1,dir=0,div_no,i,diff;

function next_previous(dir)
{
	
	
	if(dir)
	{
		$("#div"+counter).css("display","none");
		$("#p_"+counter).css("color","black");
		$("#p_"+counter).css("background","none repeat scroll 0% 0% #ECE9E9");
		
		if(counter!=5)
		{
			counter++;
		}
		
		$("#p_"+counter).css("color","white");
		$("#p_"+counter).css("background","none repeat scroll 0% 0% rgb(248, 75, 76)");
		$("#div"+counter).css("display","block");
		
	}else
	{
		$("#div"+counter).css("display","none");
		$("#p_"+counter).css("color","black");
		$("#p_"+counter).css("background","none repeat scroll 0% 0% #ECE9E9");
		if(counter!=1)
		{
			counter--;
		}
		$("#p_"+counter).css("color","white");
		$("#p_"+counter).css("background","none repeat scroll 0% 0% rgb(248, 75, 76)");
		$("#div"+counter).css("display","block");		
		
	}
	
	
	if(counter==5)
	{
		$("#next_button").css("display","none");
		$("#prev_button").css("display","none");
		$("#submit").css("display","block");
		
	}else
	{
		$("#next_button").css("display","block");
		$("#prev_button").css("display","block");
		$("#submit").css("display","none");
		
	}
	
}


function change_div(div_no)
{
	
	if(div_no>counter)
	{
		diff=div_no-counter
		for(i=0;i<diff;i++)
		{
			next_previous(1);
			
		}
		
		
	}else
	{
		
		diff=counter-div_no;
		for(i=0;i<diff;i++)
		{
			next_previous(0);
			
		}
		
	}
}

