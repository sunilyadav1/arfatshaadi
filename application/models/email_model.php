<?php
class Email_model extends CI_Model {
	 public function GetInboxDetails($userid,$mailtype){
		 $sql = $this->db->select('*');
		 $this->db->from('mailbox');
		 $this->db->where('User_IdTo',$userid);
		 $this->db->where('MailType_Id',$mailtype);
		 $this->db->where('Deleted_fromuser',0);
		 $query = $this->db->get();
		 //echo $this->db->last_query(); exit;
		if($query->num_rows() ==''){
				$return =  0;		
		}else{
				$return = $query->result();	
				
				
		}
		return $return;
	}	 
	public function GetSentDetails($userid,$mailtype){
		 $sql = $this->db->select('*');
		 $this->db->from('mailbox');
		 $this->db->where('User_IdFrom',$userid);
		 $this->db->where('MailType_Id',$mailtype);
		 $this->db->where('Deleted_Touser',0);
		 $query = $this->db->get();
		
		if($query->num_rows() ==''){
				$return =  0;		
		}else{
				$return = $query->result();	
				
				
		}
		return $return;
	}
public function GetAllUser($userid){
		$sql = $this->db->select('*');
		$this->db->from('userprofile');
		$this->db->where('User_Id !=',$userid);
		$query = $this->db->get();
		return $query->result();
	}
	
	
	public function GetUserDetails($userid){
		$sql = $this->db->select('*');
		$this->db->from('userprofile');
		$this->db->where('User_Id',$userid);
		$query = $this->db->get();
		return $query->result();
	}
	public function Getcounts($userid){
		$Inboxcount = $this->db->select('*');
		$this->db->from('mailbox');
		$this->db->where('User_IdTo',$userid);
		$query = $this->db->get();
		$data['Inboxcount']  = $query->num_rows();
		
		$Sentcount = $this->db->select('*');
		$this->db->from('mailbox');
		$this->db->where('User_IdFrom',$userid);
		$query = $this->db->get();
		$data['Sentcount']  = $query->num_rows();
		
		$Deletedcount = $this->db->select('*');
		$this->db->from('mailbox');
		$this->db->where('Deleted_fromuser',$userid);
		$query = $this->db->get();
		$data['Deletedcount']  = $query->num_rows();
		return $data;
	}	
}