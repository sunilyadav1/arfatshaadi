<?php

class Admin_model extends CI_Model {

	public function login($email, $pass){
		$this->db->select('*');
		 $this->db->from('adminuser');
		 $this->db->where('email',$email);
		 $this->db->where('password',$pass);
		 $query = $this->db->get();
		 if($query->num_rows() ==''){
				return 'failure';		
		}else{
				$results = $query->result();		
				$result = $results[0];
				return $result;	
		}
		
	
	}
	
	public function getImages(){
			 $this->db->select('*');
		   $this->db->from('sliderimages');
		   $query = $this->db->get();
		   if($query->num_rows() ==''){
				return '';			
			}else{
			return $query->result();				
			}
		}
	 public function insert_sliderimages($id,$image_name){
	
		   
		   $data = array(
				'slider_id' => $id,
				'slider_image' => $image_name,
				'status' => 1
				);
			$this->db->insert('sliderimages', $data);
			$id = $this->db->insert_id();
			return true;
	   
	 }
	   public function update_sliderimages($id,$status)
	   {
			if($status == 2)
			{
				$where2 = "slider_id = $id";
				if ($this->db->delete('sliderimages', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "slider_id = $id";
				$data = array(
					'status' => $status
				);
				if ($this->db->update('sliderimages', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	   }	
	 public function get_country_details(){
		$this->db->select('*');
	   $this->db->from('countrylist');
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 public function getcountry($id){
		$this->db->select('*');
	   $this->db->from('countrylist');
	   $this->db->where('Country_Id',$id); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 public function add_country($Country_Name,$id){
		   $data =array(
				'Country_Name' 	=> $Country_Name,
				);
			if($id == 0){
				$this->db->insert('countrylist',$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				$where = "Country_Id = $id";
				$this->db->update('countrylist',$data,$where);
			}
			redirect('home/manage_country', 'refresh'); 
			
	   }
	public function update_country($id,$status){
		if($status == 2)
			{
				$where2 = "Country_Id = $id";
				if ($this->db->delete('countrylist', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "Country_Id = $id";
				$data = array(
					'status' => $status
				);
				if ($this->db->update('countrylist', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	
	/** end of country**/
	// start of state///
	public function get_state_details(){
		$this->db->select('*');
	   $this->db->from('statelist');
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 
	function insert_state($page,$data,$state)
  {
  $data=array('Country_Id'=>$data,
  'State_Name'=>$state);
	    $this->load->database();
       $this->db->insert('statelist', $data);
  
  }
  public function add_state($conName,$State_Name,$id){
  
		   $data =array('Country_Id' =>$conName,
				'State_Name' 	=> $State_Name,
				);
			if($id == 0){
				$this->db->insert('statelist',$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				$where = "State_Id = $id";
				$this->db->update('statelist',$data,$where);
			}
			redirect('home/manage_state', 'refresh'); 
			
	   }
  
	
	public function update_state($id,$status){
		if($status == 2)
			{
				$where2 = "State_Id = $id";
				if ($this->db->delete('statelist', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "State_Id = $id";
				$data = array(
					'Status' => $status
				);
				if ($this->db->update('statelist', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	public function getstate($id){
		$this->db->select('*');
	   $this->db->from('statelist');
	   $this->db->where('State_Id',$id); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
// for city  
	public function get_city_details(){
		$this->db->select('*');
	   $this->db->from('citylist');
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 
	
	
	public function update_city($id,$status){
		if($status == 2)
			{
				$where2 = "City_Id = $id";
				if ($this->db->delete('citylist', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "City_Id = $id";
				$data = array(
					'Status' => $status
				);
				if ($this->db->update('citylist', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	public function getcity($id){
		$this->db->select('*');
	   $this->db->from('citylist');
	   $this->db->where('City_Id',$id); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
    public function add_city($conName,$State_Name,$city,$id){
  
		   $data =array('Country_Id' =>$conName,
				'State_Id' 	=> $State_Name,
				'City_Name'=>$city,
				);
			if($id == 0){
				$this->db->insert('citylist',$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				$where = "City_Id = $id";
				$this->db->update('citylist',$data,$where);
			}
			redirect('home/manage_cities', 'refresh'); 
			
	   }
  
	// religion 
	 public function get_religion_details(){
		$this->db->select('*');
	   $this->db->from('religionlist');
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 public function getreligion($id){
		$this->db->select('*');
	   $this->db->from('religionlist');
	   $this->db->where('Religion_Id',$id); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 public function add_religion($Religion_Name,$id){
		   $data =array(
				'Religion_Name' 	=> $Religion_Name,
				);
			if($id == 0){
				$this->db->insert('religionlist',$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				$where = "Religion_Id = $id";
				$this->db->update('religionlist',$data,$where);
			}
			redirect('home/manage_religion', 'refresh'); 
			
	   }
	public function update_religion($id,$status){
		if($status == 2)
			{
				$where2 = "Religion_Id = $id";
				if ($this->db->delete('religionlist', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "Religion_Id = $id";
				$data = array(
					'status' => $status
				);
				if ($this->db->update('religionlist', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	//for community
	public function get_community_details(){
		$this->db->select('*');
	   $this->db->from('communitylist');
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 
	
  public function add_community($Religion,$Community_Name,$id){
  
		   $data =array('Religion_Id' =>$Religion,
				'Community_Name' 	=> $Community_Name,
				);
			if($id == 0){
				$this->db->insert('communitylist',$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				$where = "Community_Id = $id";
				$this->db->update('communitylist',$data,$where);
			}
			redirect('home/manage_community', 'refresh'); 
			
	   }
  
	
	public function update_community($id,$status){
		if($status == 2)
			{
				$where2 = "Community_Id = $id";
				if ($this->db->delete('communitylist', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "Community_Id = $id";
				$data = array(
					'Status' => $status
				);
				if ($this->db->update('communitylist', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	public function getcommunity($id){
		$this->db->select('*');
	   $this->db->from('communitylist');
	   $this->db->where('Community_Id',$id); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	//for subcommunity///////////
	public function get_subcommunity_details(){
		$this->db->select('*');
	   $this->db->from('subcommunitylist');
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	
	public function update_subcommunity($id,$status){
		if($status == 2)
			{
				$where2 = "SubCommunityList_Id = $id";
				if ($this->db->delete('subcommunitylist', $where2)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$where = "SubCommunityList_Id = $id";
				$data = array(
					'Status' => $status
				);
				if ($this->db->update('subcommunitylist', $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	public function getsubcommunity($id){
		$this->db->select('*');
	   $this->db->from('subcommunitylist');
	   $this->db->where('SubCommunityList_Id',$id); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
    public function add_subcommunity($conName,$State_Name,$city,$id){
  
		   $data =array('Religion_Id' =>$conName,
				'Community_Id' 	=> $State_Name,
				'SubCommunity_Name'=>$city,
				);
			if($id == 0){
				$this->db->insert('subcommunitylist',$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				$where = "SubCommunityList_Id = $id";
				$this->db->update('SubCommunityList',$data,$where);
			}
			redirect('home/manage_subcommunity', 'refresh'); 
			
	   }
  //chat try ..................................still need changes //
  function get_last_item()
  {
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get('chats', 1);
    return $query->result();
  }
  
  
  function insert_message($message)
  {
    $this->message = $message;
    $this-> time = time();  
    $this->db->insert('chats', $this);
  }

  function get_chat_after($time)
  {
    $this->db->where('time >', $time)->order_by('time', 'DESC')->limit(10); 
    $query = $this->db->get('chats');
    
    $results = array();
    
    foreach ($query->result() as $row)
    {
      $results[] = array($row->message,$row->time);
    }
    
    return array_reverse($results);
  }
  
  
}
