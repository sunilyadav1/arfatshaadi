<?php
class Action_model extends CI_Model {
	
		public function login($email, $pass){
		 $this->db->select('*');
		 $this->db->from('adminuser');
		 $this->db->where('email',$email);
		 $this->db->where('password',$pass);
		 $query = $this->db->get();
		 if($query->num_rows() ==''){
				return 'failure';		
		}else{
				$results = $query->result();		
				$result = $results[0];
				return $result;	
		}
	}
	public function get_table_details($table){
		$this->db->select('*');
	   $this->db->from($table);
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 public function getpart_table_deatils($table,$column,$value){
		$this->db->select('*');
	   $this->db->from($table);
	   $this->db->where($column,$value); 
	   $query = $this->db->get();
	   
	   if($query->num_rows() ==''){
			return '';			
		}else{
		return $query->result();				
		}
	 }
	 public function add_record($table,$data,$id,$where=null){
		
			if($id == 0){
				$this->db->insert($table,$data);
				$lastid=$this->db->insert_id();
				if(!empty($lastid))
				{
					return true;
				}
				else {
					  
					  return false;
				  }
			}else{
				
				$this->db->update($table,$data,$where);
			}	
	   }
	public function update_record($table,$id,$status,$where){
		if($status == 2)
			{	
				if ($this->db->delete($table, $where)) {
					return true;
				} else {
					return false;
				}
			}
			else
			{
				$data = array(
					'Status' => $status
				);
				if ($this->db->update($table, $data, $where)) {
					return true;
				} else {
					return false;
				}
			}
	}
	public function get_user_details(){
		// $sql ="select * from `userprofile`  u LEFT JOIN `religionlist` r on u.Religion = r.Religion_Id  LEFT JOIN 
		// `languagelist` l on u.MotherTongue = l.Language_Id LEFT JOIN `workingas` w on	u.WorkingAs = w.Work_Id LEFT JOIN  `communitylist` cm on u.Community = cm.Community_Id 
		// LEFT JOIN `subcommunitylist` sm on u.SubCommunity = sm.SubCommunityList_Id  LEFT JOIN HeightList h on u.Height = h.Height_Id LEFT JOIN  `degree` d on u.EducationLevel = d.Degree_Id
		// LEFT JOIN  `countrylist` c1 on u.LivingIn = c1.Country_Id LEFT JOIN  `statelist` s1 on u.LivingState = s1.State_Id LEFT JOIN  `citylist` t1 on u.LivingCity = t1.City_Id 
		// LEFT JOIN `settings` s on u.User_Id = s.User_id
		// LEFT JOIN `familyinfo` f on u.User_Id = f.User_id LEFT JOIN  specialization sp on u.EducationField = sp.Id";


		$sql ="select * from `userprofile`  u LEFT JOIN `religionlist` r on u.Religion = r.Religion_Id  LEFT JOIN 
		`languagelist` l on u.MotherTongue = l.Language_Id LEFT JOIN `workingas` w on	u.WorkingAs = w.Work_Id LEFT JOIN  `communitylist` cm on u.Community = cm.Community_Id 
		LEFT JOIN `subcommunitylist` sm on u.SubCommunity = sm.SubCommunityList_Id  LEFT JOIN HeightList h on u.Height = h.Height_Id LEFT JOIN  `degree` d on u.EducationLevel = d.Degree_Id
		LEFT JOIN  `countrylist` c1 on u.LivingIn = c1.Country_Id LEFT JOIN  `statelist` s1 on u.LivingState = s1.State_Id LEFT JOIN  `citylist` t1 on u.LivingCity = t1.City_Id 
		LEFT JOIN `familyinfo` f on u.User_Id = f.User_id LEFT JOIN  specialization sp on u.EducationField = sp.Id";
	
	
		$query = $this->db->query($sql);



		if($query->num_rows() ==''){
			return '';		
		}else{
			$result =$query->result_array();
			return $result;
		}
		
	}
	
	public function get_user_online(){
		$sql ="select * from `userprofile`";
		$query = $this->db->query($sql);

		if($query->num_rows() ==''){
			return '';		
		}else{
			$result =$query->result_array();
			return $result;
		}
	}
	
	public function full_profile($User_Id){
	
	$sql ="select * from `userprofile`  u LEFT JOIN `religionlist` r on u.Religion = r.Religion_Id  LEFT JOIN 
	`languagelist` l on u.MotherTongue = l.Language_Id LEFT JOIN `workingas` w on	u.WorkingAs = w.Work_Id LEFT JOIN  `communitylist` cm on u.Community = cm.Community_Id 
	LEFT JOIN `subcommunitylist` sm on u.SubCommunity = sm.SubCommunityList_Id  LEFT JOIN HeightList h on u.Height = h.Height_Id LEFT JOIN  `degree` d on u.EducationLevel = d.Degree_Id
	LEFT JOIN  `countrylist` c1 on u.LivingIn = c1.Country_Id LEFT JOIN  `statelist` s1 on u.LivingState = s1.State_Id LEFT JOIN  `citylist` t1 on u.LivingCity = t1.City_Id 
	LEFT JOIN `settings` s on u.User_Id = s.User_id
	LEFT JOIN `familyinfo` f on u.User_Id = f.User_id LEFT JOIN  specialization sp on u.EducationField = sp.Id where u.User_Id =".$User_Id." ";
	
	$query = $this->db->query($sql);

		if($query->num_rows() ==''){
				return '';		
		}else{
			
				$result =$query->result_array();
				
				return $result;
		}
					
	}
	public function full_partnerprofile($User_Id){
	
	$sql ="select * from `partnerprofile`  u LEFT JOIN `religionlist` r on u.Religion = r.Religion_Id  LEFT JOIN 
	`languagelist` l on u.MotherTongue = l.Language_Id LEFT JOIN `workingas` w on	u.ProfessionArea = w.Work_Id LEFT JOIN  `communitylist` cm on u.Community = cm.Community_Id 
	 LEFT JOIN  `degree` d on u.Education = d.Degree_Id
	LEFT JOIN  `countrylist` c1 on u.CountryLivingIn = c1.Country_Id LEFT JOIN  HeightList h on u.HeightFrom = h.Height_Id
	LEFT JOIN 	`statelist` s1 on u.LivingState = s1.State_Id LEFT JOIN  `citylist` t1 on u.LivingCity = t1.City_Id
	LEFT JOIN `familyinfo` f on u.User_Id = f.User_id LEFT JOIN  specialization sp on u.Education_Field = sp.Id where u.User_Id =".$User_Id." ";
	
	$query = $this->db->query($sql);

		if($query->num_rows() ==''){
				return '';		
		}else{
			
				$result =$query->result_array();
				
				return $result;
		}
					
	}


	

	


}
