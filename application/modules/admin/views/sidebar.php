<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Place Management</li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/UserReport"><i ></i><span class="hidden-tablet"> Users</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/UserOnline"><i ></i><span class="hidden-tablet"> Online Users </span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/SliderImages"><i ></i><span class="hidden-tablet"> Slider Images</span></a></li>
						
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_pages"><i ></i><span class="hidden-tablet"> Pages</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_country"><i ></i><span class="hidden-tablet"> Countries</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_state"><i ></i><span class="hidden-tablet"> States</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_cities"><i ></i><span class="hidden-tablet"> Cities</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_religion"><i ></i><span class="hidden-tablet"> Religion</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_community"><i ></i><span class="hidden-tablet"> Community</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_subcommunity"><i ></i><span class="hidden-tablet"> Sub Community</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_language"><i ></i><span class="hidden-tablet"> Language</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_degree"><i ></i><span class="hidden-tablet"> Degree</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_specialization"><i ></i><span class="hidden-tablet"> Specialization</span></a></li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_profession"><i ></i><span class="hidden-tablet"> Profession</span></a></li>
						
					
						<li class="nav-header hidden-tablet">Settings</li>
						<li><a class="ajax-link" href="<?php echo WEB_DIR; ?>index.php/home/manage_settings "><i ></i><span class="hidden-tablet"> Settings</span></a></li>
							
						
					</ul>
					
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			