<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<title>Milan Rishtha</title>
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="<?php echo WEB_DIR; ?>css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	  
	</style>
	<link rel="stylesheet" href="<?php echo WEB_DIR_FRONT; ?>dist/css/bootstrap.min.css">
	<!-- <link href="<?php echo WEB_DIR; ?>css/bootstrap-responsive.css" rel="stylesheet"> -->
	<link href="<?php echo WEB_DIR; ?>css/rajesh.css" rel="stylesheet">
	<link href="<?php echo WEB_DIR; ?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="<?php echo WEB_DIR_FRONT; ?>dist/css/bootstrap-table.css" rel="stylesheet">
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="<?php echo WEB_DIR; ?>images/favicon.ico">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
</head>

<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?php echo WEB_DIR;?>"> <span>ARFAT SHAADI</span></a>
				
				<!-- theme selector starts -->
				
				<!-- theme selector ends -->
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> <?php echo $this->session->userdata('username'); ?></span>
						<span class="caret"></span>
					</a>
					<a href="<?php echo WEB_URL; ?>home/logout">Logout</a>
					<ul class="dropdown-menu">
						
						<li></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		