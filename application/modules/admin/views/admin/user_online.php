<?php
$this->load->view('header');
?>

<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="<?php echo WEB_DIR;?>">Home</a> /

					</li>

					<li>
						<a href="#">Online Users</a>

					</li>
					<!-- <li style="float:right;">
						<a href="<?php echo WEB_URL; ?>home/AddUser" class="btn btn-success">Add New User</a>
					</li> -->
				</ul>

			</div>

		
			<div class="row-fluid sortable">

				<div class="box span12">
					<table data-toggle="table"  data-cache="false" data-pagination="true" 
					data-show-refresh="true" data-search="true" id="table-pagination" data-sort-order="desc">
						<thead>
							<tr style="background:rgb(244, 244, 244)">
								<th data-field="id"  data-sortable="true">User ID</th>
								<th data-field="name">User Name</th>
								<th data-field="email">Email Id</th>
								<th data-field="MobileNo">Mobile No</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="result_ol">
						
						</tbody>
					</table>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>

<script>
$("document").ready(function(){
$.ajax({
		url : "<?php echo WEB_URL; ?>home/UserOnlineAjax",
		data : {},
		type : "post",
		success : function(data){
			$("#result_ol").html(data);
		}
	});
});
setInterval(function() {
	$.ajax({
		url : "<?php echo WEB_URL; ?>home/UserOnlineAjax",
		data : {},
		type : "post",
		success : function(data){
			
		console.log(data);
			$("#result_ol").html(data);
		}
	});
}, 1000 * 60 * 10);

</script>