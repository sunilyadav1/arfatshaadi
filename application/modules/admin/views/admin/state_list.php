<?php
$this->load->view('header');
?>
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="<?php echo WEB_DIR;?>">Home</a> <span class="divider">/</span>

					</li>

					<li>

						<a href="#">states</a>

					</li>

				</ul>

			</div>

			

			<div class="row-fluid sortable">

				<div class="box span12">
						<?php 
						 if($this->uri->segment(3) != 0){
							$state = $this->admin_model->getstate($this->uri->segment(3));
							
							$state_name = $state[0]->State_Name;
							$state_id = $state[0]->State_Id;
							$title = "Edit state";
						 }else{
							$state_name = "";
							$state_id = 0;
							$title = "Add state";
						 }
						 ?>
						 
						
					<div class="box-header well" data-original-title>

						<h2><i class="icon-edit"></i><?=$title;?></h2>

						<div class="box-icon">

							
						</div>

					</div>

					<div class="box-content">

					<form class="form-horizontal"  name="f2" action="<?php echo WEB_URL;?>home/add_state/<?=$state_id;?>"   method="post">
						 <fieldset>
						
							
													  <div class="control-group">
							  <label class="control-label" for="textarea2">select country </label>
							  <div class="controls">
							  <select name="Con">
									<option value=""> select country</option>									
									<?php 
									foreach($result as $country){?>
									<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    <?php  //for type name     ?>
									<?php } ?>
								</select>
								
							  </div>
							</div>
							  
							  <div class="control-group">
								 <label class="control-label" for="textarea2">state Name</label>
								<div class="controls">

									<input class="input-file uniform_on" name="State_Name" id="State_Name" value="<?=$state_name;?>" type="text" required>
								</div>
								
								</div>
							<div class="form-actions">

							  <input type="submit" class="btn btn-primary" value="<?=$title;?>">

							  <button type="reset" class="btn">Cancel</button>

							</div>	
						</fieldset>
					</form>
					
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								
								  <th>State_name</th>
								  <th>status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  
						  <tbody>
							
							<?php
							if (!empty($state_data)) {
							for($i=0;$i< count($state_data);$i++) { 
							?>
							<tr>
								<td><?php echo $state_data[$i]->State_Name; ?></td>
								
								<td><?php if($state_data[$i]->Status==1) { echo "Active";}else {echo "InActive";} ?></td>
								
								<td class="center">
									
									<a class="btn btn-info" href="<?php echo WEB_URL;?>home/manage_state/<?php echo $state_data[$i]->State_Id; ?>" >
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<?php if($state_data[$i]->Status==0) { ?>
									 <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_state_list/<?php echo $state_data[$i]->State_Id; ?>/1">Active</a>
									 <?php }else{?>
									  <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_state_list/<?php echo $state_data[$i]->State_Id; ?>/0">InActive</a>
									  <?php } ?>
									  
									<a class="btn btn-danger btn-setting" href="<?php echo WEB_URL; ?>home/update_state_list/<?php echo $state_data[$i]->State_Id;?>/2" onClick="return confirm('Are you sure you want to delete?');">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<?php
								}
								}
							?>
							
							
							
						  </tbody>
					  </table>    

					</div>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>
