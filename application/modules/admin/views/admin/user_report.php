<?php
$this->load->view('header');
?>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-table.css">
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="<?php echo WEB_DIR;?>">Home</a> /

					</li>

					<li>
						<a href="#">Users</a>

					</li>
					<li style="float:right;margin-top:-7px">
						<a href="<?php echo WEB_URL; ?>home/AddUser" class="btn btn-success">Add New User</a>
					</li>
				</ul>

			</div>

		
			<div class="row-fluid sortable">

				<div class="box span12">
					<table data-toggle="table"  data-cache="false" data-pagination="true" 
					data-show-refresh="true" data-search="true" id="table-pagination" data-sort-order="desc">
						<thead>
							<tr style="background:rgb(244, 244, 244)">
								<th data-field="id"  data-sortable="true">User ID</th>
								<th data-field="name">User Name</th>
								<th data-field="email">Email Id</th>
								<th data-field="MobileNo">Mobile No</th>
								<th data-field="Religion">Religion</th>
								<th data-field="Community">Community</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php  if($result != ""){
								foreach($result as $row){	?>
									<tr>
										<td><?=$row['User_Id']?></td>
										<td><?=$row['Name']?></td>
										<td><?=$row['Email']?></td>
										<td><?=$row['MobileNo']?></td>
										<td><?=$row['Religion_Name'];?></td>
										<td><?=$row['Community_Name'];?></td>
										<td><a href="<?php echo WEB_URL; ?>home/AddUser/<?=$row['User_Id'];?>">Edit</a>&nbsp;&nbsp;&nbsp;
                                           <a href="<?php echo WEB_URL; ?>home/DeleteUser/<?=$row['User_Id'];?>">Delete</a>
										<?php if(isset($row['TimeStamp']) && $row['TimeStamp'] != ""){
											$your_date = $row['TimeStamp'];
											$now = time();
											$datediff = $now - $your_date;
											$ol = floor($datediff/60);
										
											if($ol < 15){
												echo '<img src="http://localhost/milanrishta/images/ol.png" width="30px">';
											}
										}?>
										</td>
									</tr>
						<?php 	} 
						} ?>
						</tbody>
					</table>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-table.js"></script>
<script>
setInterval(function() {
	location.reload();
}, 1000 * 60 * 15); 
</script>