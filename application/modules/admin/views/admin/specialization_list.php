<?php
$this->load->view('header');
?>
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="<?php echo WEB_DIR;?>">Home</a> <span class="divider">/</span>

					</li>

					<li>

						<a href="#">specialization</a>

					</li>

				</ul>

			</div>

			

			<div class="row-fluid sortable">

				<div class="box span12">
						<?php 
						 if($this->uri->segment(3) != 0){
							$specialization = $this->action_model->getpart_table_deatils('specialization','Id',$this->uri->segment(3));
							
							$specialization_name = $specialization[0]->SName;
							$specialization_id = $specialization[0]->Id;
							$title = "Edit specialization";
						 }else{
							$specialization_name = "";
							$specialization_id = 0;
							$title = "Add specialization";
						 }
						 ?>
						 
						
					<div class="box-header well" data-original-title>

						<h2><i class="icon-edit"></i><?=$title;?></h2>

						<div class="box-icon">

							
						</div>

					</div>

					<div class="box-content">

					<form class="form-horizontal"  name="f2" action="<?php echo WEB_URL;?>home/add_specialization/<?=$specialization_id;?>"   method="post">
						 <fieldset>
						
							
													  <div class="control-group">
							  <label class="control-label" for="textarea2">select degree </label>
							  <div class="controls">
							  <select name="degree">
									<option value=""> select degree</option>									
									<?php 
									foreach($result as $degree){?>
									<option value="<?php echo $degree->Degree_Id; ?>"><?php echo $degree->Degree_Name;?></option>    <?php  //for type name     ?>
									<?php } ?>
								</select>
								
							  </div>
							</div>
							  
							  <div class="control-group">
								 <label class="control-label" for="textarea2"> specialization</label>
								<div class="controls">

									<input class="input-file uniform_on" name="spec" id="spec" value="<?=$specialization_name;?>" type="text" required>
								</div>
								
								</div>
							<div class="form-actions">

							  <input type="submit" class="btn btn-primary" value="<?=$title;?>">

							  <button type="reset" class="btn">Cancel</button>

							</div>	
						</fieldset>
					</form>
					
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								
								  <th>specialization_name</th>
								  <th>status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  
						  <tbody>
							
							<?php
							if (!empty($special)) {
							for($i=0;$i< count($special);$i++) { 
							?>
							<tr>
								<td><?php echo $special[$i]->SName; ?></td>
								
								<td><?php if($special[$i]->Status==1) { echo "Active";}else {echo "InActive";} ?></td>
								
								<td class="center">
									
									<a class="btn btn-info" href="<?php echo WEB_URL;?>home/manage_specialization/<?php echo $special[$i]->Id; ?>" >
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<?php if($special[$i]->Status==0) { ?>
									 <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_specialization/<?php echo $special[$i]->Id; ?>/1">Active</a>
									 <?php }else{?>
									  <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_specialization/<?php echo $special[$i]->Id; ?>/0">InActive</a>
									  <?php } ?>
									  
									<a class="btn btn-danger btn-setting" href="<?php echo WEB_URL; ?>home/update_specialization/<?php echo $special[$i]->Id;?>/2" onClick="return confirm('Are you sure you want to delete?');">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<?php
								}
								}
							?>
							
							
							
						  </tbody>
					  </table>    

					</div>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>
