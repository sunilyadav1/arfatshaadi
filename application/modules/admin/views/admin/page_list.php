<?php
$this->load->view('header');
?>
<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
	
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="<?php echo WEB_DIR;?>">Home</a> <span class="divider">/</span>

					</li>

					<li>

						<a href="#">Pages</a>

					</li>

				</ul>

			</div>

			

			<div class="row-fluid sortable">

				<div class="box span12">
						<?php 
						 if($this->uri->segment(3) != 0){
							$page = $this->action_model->getpart_table_deatils('pages','Page_id',$this->uri->segment(3));
							
							$Page_Heading = $page[0]->Page_Heading;
							$Page_id = $page[0]->Page_id;
							$Page_text = $page[0]->Page_text;
							$title = "Edit Page";
						 }else{
							$Page_Heading = "";
							$Page_text= "";
							$Page_id = 0;
							$title = "Add Page";
						 }
						 ?>
					<div class="box-header well" data-original-title>

						<h2><i class="icon-edit"></i><?=$title;?></h2>

						<div class="box-icon">

							
						</div>

					</div>

					<div class="box-content">

					<form class="form-horizontal"  name="f2" action="<?php echo WEB_URL;?>home/add_page/<?=$Page_id;?>" method="post">
						 <fieldset>
						
							<div class="control-group">
								 <label class="control-label" for="textarea2">Page Heading</label>
								<div class="controls">

									<input class="input-file uniform_on" name="Page_Heading" id="Page_Heading" value="<?=$Page_Heading;?>" type="text" required>
								</div>
							</div>
							<div class="control-group">
								 <label class="control-label" for="textarea2">Page Text</label>
								<div class="controls">
<textarea class="ckeditor" name="Page_text" id="Page_text"><?=$Page_text;?></textarea>
																</div>
							</div>
							<div class="form-actions">

							  <!-- <input type="submit" class="btn btn-primary" value="<?=$title;?>">
							   -->
                              <input type="submit" class="btn btn-primary" value="Save">
							  <button type="reset" class="btn">Cancel</button>

							</div>	
						</fieldset>
					</form>
					
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								
								  <th>Page_Heading</th>
								  <th>status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  
						  <tbody>
							
							<?php
							if (!empty($result)) {
							for($i=0;$i< count($result);$i++) { 
							?>
							<tr>
								<td><?php echo $result[$i]->Page_Heading; ?></td>
								
								<td><?php if($result[$i]->Status==1) { echo "Active";}else {echo "InActive";} ?></td>
								
								<td class="center">
									
									<a class="btn btn-info" href="<?php echo WEB_URL;?>home/manage_pages/<?php echo $result[$i]->Page_id; ?>">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<?php if($result[$i]->Status==0) { ?>
									 <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_page/<?php echo $result[$i]->Page_id; ?>/1">Active</a>
									 <?php }else{?>
									  <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_page/<?php echo $result[$i]->Page_id; ?>/0">InActive</a>
									  <?php } ?>
									  
									<!--<a class="btn btn-danger btn-setting" href="<?php echo WEB_URL; ?>home/update_page/<?php echo $result[$i]->Page_id;?>/2" onClick="return confirm('Are you sure you want to delete?');">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>-->
								</td>
							</tr>
							<?php
								}
								}
							?>
							
							
							
						  </tbody>
					  </table>    

					</div>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>
