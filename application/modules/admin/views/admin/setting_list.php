<?php
$this->load->view('header');
?>
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>
						<a href="<?php echo WEB_DIR;?>">Home</a> <span class="divider">/</span>
					</li>

					<li>

						<a href="#">Settings</a>

					</li>

				</ul>

			</div>

			

			<div class="row-fluid sortable">

				<div class="box span12">
						<?php 
					
							$admin = $this->action_model->get_table_details('adminuser');
							//print_r ($admin);
							//exit;
							if (!empty($admin)) {
							for($i=0;$i< count($admin);$i++) { 
							$adminid[$i]=$admin[$i]->ADMIN_ID;
							$name[$i]=$admin[$i]->NAME;
							$email[$i]=$admin[$i]->EMAIL;
							$password[$i]=$admin[$i]->PASSWORD;
							}
							}							
						 ?>
					<div class="box-header well" data-original-title>

						<h2><i class="icon-edit"></i>Admin profile</h2>

						<div class="box-icon">

							
						</div>

					</div>

					<div class="box-content">

					<?php for($j=0;$j<count($admin);$j++){?>
					<form class="form-horizontal"  name="f2" action="<?php echo WEB_URL;?>home/update_admin/<?=$adminid[$j];?>" method="post">
						 <fieldset>
						
							<div class="control-group">
								 <label class="control-label" for="textarea2">Admin Name</label>
								<div class="controls">

									<input class="input-file uniform_on" name="Admin_Name" id="Admin_Name" value="<?=$name[$j];?>" type="text" required>
								</div>
							</div>
							<div class="control-group">
								 <label class="control-label" for="textarea2">Admin Email</label>
								<div class="controls">

									<input class="input-file uniform_on" name="Admin_Email" id="Admin_Email" value="<?=$email[$j];?>" type="text" required>
								</div>
							</div>
							<div class="control-group">
								 <label class="control-label" for="textarea2">Admin Password</label>
								<div class="controls">

									<input class="input-file uniform_on" name="Admin_Password" id="Admin_Password" value="<?=$password[$j];?>" type="password" required>
								</div>
							</div>
							
							
							<div class="form-actions">

							  <input type="submit" class="btn btn-primary" value="update">

							  <button type="reset" class="btn">Cancel</button>

							</div>	
						</fieldset>
					</form>
					<?php
					}?>
					   

					</div>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>
