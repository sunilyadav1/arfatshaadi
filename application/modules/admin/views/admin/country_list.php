<?php
$this->load->view('header');
?>
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="<?php echo WEB_DIR;?>">Home</a> <span class="divider">/</span>

					</li>

					<li>

						<a href="#">Countries</a>

					</li>

				</ul>

			</div>

			

			<div class="row-fluid sortable">

				<div class="box span12">
						<?php 
						 if($this->uri->segment(3) != 0){
							$country = $this->action_model->getpart_table_deatils('countrylist','Country_Id',$this->uri->segment(3));
							
							$country_name = $country[0]->Country_Name;
							$country_id = $country[0]->Country_Id;
							$title = "Edit Country";
						 }else{
							$country_name = "";
							$country_id = 0;
							$title = "Add Country";
						 }
						 ?>
					<div class="box-header well" data-original-title>

						<h2><i class="icon-edit"></i><?=$title;?></h2>

						<div class="box-icon">

							
						</div>

					</div>

					<div class="box-content">

					<form class="form-horizontal"  name="f2" action="<?php echo WEB_URL;?>home/add_record/<?=$country_id;?>" method="post">
						 <fieldset>
						
							<div class="control-group">
								 <label class="control-label" for="textarea2">Country Name</label>
								<div class="controls">

									<input class="input-file uniform_on" name="Country_Name" id="Country_Name" value="<?=$country_name;?>" type="text" required>
								</div>
							</div>
							<div class="form-actions">

							  <input type="submit" class="btn btn-primary" value="<?=$title;?>">

							  <button type="reset" class="btn">Cancel</button>

							</div>	
						</fieldset>
					</form>
					
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								
								  <th>Country_Name</th>
								  <th>status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  
						  <tbody>
							
							<?php
							if (!empty($result)) {
							for($i=0;$i< count($result);$i++) { 
							?>
							<tr>
								<td><?php echo $result[$i]->Country_Name; ?></td>
								
								<td><?php if($result[$i]->status==1) { echo "Active";}else {echo "InActive";} ?></td>
								
								<td class="center">
									
									<a class="btn btn-info" href="<?php echo WEB_URL;?>home/manage_country/<?php echo $result[$i]->Country_Id; ?>">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<?php if($result[$i]->status==0) { ?>
									 <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_record/<?php echo $result[$i]->Country_Id; ?>/1">Active</a>
									 <?php }else{?>
									  <a class="btn btn-info" href="<?php echo WEB_URL;?>home/update_record/<?php echo $result[$i]->Country_Id; ?>/0">InActive</a>
									  <?php } ?>
									  
									<a class="btn btn-danger btn-setting" href="<?php echo WEB_URL; ?>home/update_record/<?php echo $result[$i]->Country_Id;?>/2" onClick="return confirm('Are you sure you want to delete?');">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<?php
								}
								}
							?>
							
							
							
						  </tbody>
					  </table>    

					</div>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>
