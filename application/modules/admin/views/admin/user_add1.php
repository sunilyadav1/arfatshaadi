<style>.multiselect-container {width: 400px;  overflow-y: scroll;}.btn .caret {margin-left: 0;float: right;margin-top: 10px;}.multiselect{width: 400px; height: 34px; text-align: left;color: #999;  padding: 6px 0px 6px 12px; border: 2px solid #ebebeb; background-color: #ffffff;}.form-control {  width: 100% !important;  height: 34px !important;  padding: 6px 12px !important;  font-size: 14px !important;  line-height: 1.428571429 !important;  color: #555 !important;  vertical-align: middle !important;  background-color: #fff !important;  border: 1px solid #ccc !important;  border-radius: 4px !important;  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);  -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;}
</style>
<?php
$this->load->view('header');
$user_id = $this->uri->segment(3);
if(isset($user_id) && $user_id != ""){
	$UserDetails =  $this->action_model->full_profile($user_id); 
	$PartnerDetails =  $this->action_model->full_partnerprofile($user_id);
	
}else{
	$UserDetails ="";
	$PartnerDetails="";
}

?>
<div class="container-fluid">
	<div class="row-fluid">
		<?php
			//$this->load->view('sidebar');
		?>		
		<div id="content" class="span10">

			<!-- content starts -->

		<div>

				<ul class="breadcrumb">

					<li>

						<a href="#">Home</a>/

					</li>

					<li>

						<a href="#">Add User</a>

					</li>

				</ul>

			</div>

		
			<div class="row-fluid sortable">

				<div class="box span12">
					<div class="col-md-12  offset-0">
						<div class="cstyle10"></div>
						<ul class="nav nav-tabs" id="myTab">
								<li  class="active"><a data-toggle="tab" href="#basic"><span class="rates"></span><span class="hidetext">Basic Information</span>&nbsp;</a></li>
								<li  class=""><a data-toggle="tab" href="#partner"><span class="preferences"></span><span class="hidetext">Partner Preferrence</span>&nbsp;</a></li>
								<li  class=""><a data-toggle="tab" href="#family"><span class="preferences"></span><span class="hidetext">Family Information</span>&nbsp;</a></li>
						
								<li  class=""><a data-toggle="tab" href="#password"><span class="preferences"></span><span class="hidetext">Password Change</span>&nbsp;</a></li>
						
						</ul>	
						<div class="tab-content">
							<div id="basic" class="tab-pane fade active in" style="padding:30px;">
								<form action="<?php echo WEB_URL;?>home/updateUserProfile" method="POST">
								<!-- LEFT CONTENT -->
									<div class="col-md-8 basic_info pagecontainer2 offset-0">

										<div class="padding30 grey">
										
										
											<strong class="size16px bold dark left"> Basic Info</strong><hr/>
											
										<div id="basic-info">
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Name:	</span></div>
											</div>
											<div class="col-md-6">
												<div class="margtop15">
												<input type="text" class="form-control"  name="Name" id="Name" value="<?php echo isset($UserDetails[0]['Name']) ? $UserDetails[0]['Name'] : ""; ?>">
												 <input type="hidden" name="id" value="<?php echo isset($user_id) ? $user_id : 0; ?>">
												</div>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Email:	</span></div>
											</div>
											<div class="col-md-6">
												<div class="margtop15">
												<input type="text" class="form-control"  name="Email" id="Email" value="<?php echo isset($UserDetails[0]['Email']) ? $UserDetails[0]['Email'] : ""; ?>">
												
												</div>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Profile Created By:	</span></div>
											</div>
											<div class="col-md-6">
												<div class="margtop15">
											   <select class="form-control"  name="ProfileFor" id="profilefor">
													<?php $profilefor = array("Self","Son","Daughter","Brother","Sister","Friend","Relative"); ?>
													  <option value="" label="Select">select</option>
													  <?php for($i=0;$i<count($profilefor);$i++){
													  
														if($UserDetails != "" && $UserDetails[0]['ProfileFor'] == $profilefor[$i]){$sel ="selected=selected"; }else{$sel="";}?>
													  <option value="<?=$profilefor[$i];?>" <?=$sel;?>><?=$profilefor[$i];?></option>
													 <?php } ?>
												</select>
												</div>
											</div>
											
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Gender :	</span></div>
											</div>
											<div class="col-md-6">
												<div class="margtop15">
															<?php if($UserDetails != ""){
															if($UserDetails[0]['Gender'] == 2){ echo "Female";}else{echo "Male";}
															}else{
															echo "<label class='radio-inline'>Female :<input type='radio' name='Gender' value='1'></label>
															<label class='radio-inline'>Male :<input type='radio' name='Gender' value='2'></label>";
															}?>
													
												</div>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
									
											<div class="col-md-6 textright">
												<div class="margtop15"><span class="dark">Date Of Birth :</span></div>
											</div>
											<div class="col-md-6">
												
												<input type="date" name="DOB"  class="form-control" id="DOB" value="<?php echo isset($UserDetails[0]['DOB']) ? $UserDetails[0]['DOB'] : ""; ?>">
												
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop15"><span class="dark">Marital status :</span></div>
											</div>
											<div class="col-md-6">
										
											<?php $marital_status = array("Never married","Divorced","Widowed","Awaiting_Divorced","Annulled");?>
											<select class="form-control" name="MaritalStatus" id="MaritalStatus">				
												<option value="">Doesn't Matter</option>
												<?php for($i=0;$i<count($marital_status);$i++){
													if(isset($UserDetails)){														if($UserDetails[0]['MaritalStatus'] == $marital_status[$i]){$select = "selected=selected";}else{$select="";}													}else{ $select ="";}?>
												
													<option value="<?=$marital_status[$i];?>" <?=$select;?>><?=$marital_status[$i];?></option>
												<?php } ?> 					  						
											</select>
											
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop15"><span class="dark">Height :</span></div>
											</div>
											<div class="col-md-6">
												<select name="Height" class="form-control">
												<?php $HeightFrom = $this->action_model->get_table_details('HeightList');	?>
												<option value="">Select Height</option>								
													<?php 
													foreach($HeightFrom as $value){
													if($UserDetails[0]['Height'] == $value->Height_Id){$sel = "selected=selected";}else{$sel="";}													?>
													<option value="<?php echo $value->Height_Id; ?>" <?=$sel;?>><?php echo $value->Height;?></option>    
													<?php } ?>
												</select>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											
											<div class="col-md-6 textright">
												<div class="margtop15"><span class="dark">Body Type :</span></div>
											</div>
											<div class="col-md-6">
												<div class="w50percent">
													<?php $Slim="";$Average="";$Athletic="";$Heavy="";
													if($UserDetails != ""){
													switch($UserDetails[0]['BodyType']){
														case 'Slim' :
														$Slim = "checked=checked";
														break;
														case 'Average' :
														$Average = "checked=checked";
														break;
														case 'Athletic' :
														$Athletic = "checked=checked";
														break;
														case 'Heavy' :
														$Heavy = "checked=checked";
														break;
														default :
														break;
													}}?>
														<div class="radio">
														  <label>
															<input type="radio" name="BodyType" id="Average" value="Average" <?=$Average;?>>
															 Average
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="BodyType" id="Slim" value="Slim" <?=$Slim;?>>
															 Slim
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="BodyType" id="Athletic" value="Athletic" <?=$Athletic;?>>
															 Athletic
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="BodyType" id="Heavy" value="Heavy" <?=$Heavy;?>>
															 Heavy
														  </label>
														</div>
													</div>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Body Weight :</span></div>
											</div>
											<div class="col-md-6">
													<div class="w50percent">
														<input type="text" class="form-control" name="Weight" id="Weight" placeholder="" value="<?php echo isset($UserDetails[0]['Weight']) ?$UserDetails[0]['Weight'] :"";?>">
													 Kg
													</div>

													
												
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											
											<br/>
										
											<?php $veryfair="";$fair="";$dark="";$white="";
													if($UserDetails != ""){
													switch($UserDetails[0]['SkinTone']){
														case 'very fair' :
														$veryfair = "checked=checked";
														break;
														case 'fair' :
														$fair = "checked=checked";
														break;
														case 'Black' :
														$dark = "checked=checked";
														break;
														case 'Wheatish' :
														$white = "checked=checked";
														break;
														default :
														break;
													}}?>
											<div class="col-md-6 textright"> 
												<div class="margtop15"><span class="dark">Skin Tone :</span></div>
											</div>
											<div class="col-md-6">
												<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="SkinTone" id="optionsRadios1"  value="very fair" <?=$veryfair;?>>
															 Very Fair
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="SkinTone" id="optionsRadios1" value="fair" <?=$fair;?>>
															 Fair
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="SkinTone" id="optionsRadios1" value="Wheatish" <?=$white;?>>
															 Wheatish
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="SkinTone" id="optionsRadios1" value="Black" <?=$dark;?>>
															 Dark
														  </label>
														</div>
													</div>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Any Disability? :</span></div>
											</div>
											<div class="col-md-6">
													<div class="w50percent">
													<?php $yes="";$no=""; 
													if($UserDetails != ""){
													if($UserDetails[0]['Disability'] == 1){$yes ="checked=checked";}else{$no ="checked=checked";}
													}
													?>
														<div class="radio">
														  <label>
															<input type="radio" name="Disability" id="optionsRadios1" value="1" <?=$yes;?>>
															Yes
														  </label>
														</div>
													</div>

													<div class="w50percentlast">	
														<div class="radio">
														  <label>
															<input type="radio" name="Disability" id="optionsRadios2" value="2" <?=$no;?>>
															 No
														  </label>
														</div>
													</div>
												
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											
										
										
											</div>
											
											
											<br/>
											<br/>
											
											
											<strong class="size16px bold dark left">Religious Background</strong><hr/>
											<div class="clearfix"></div>
											<div class="line4"></div>
											<div id="religious">
											
											<div class="col-md-6 textright">
												<div class="margtop15"><span class="dark">Religion:</span></div>
											</div>
											<div class="col-md-6">
												<div class="margtop15">
												<?php echo isset($UserDetails[0]['Religion_Name']) ? $UserDetails[0]['Religion_Name'] : ""; ?>
													<?php $religion = $this->action_model->get_table_details('religionlist'); ?>
						
												<select class="form-control" name="Religion" id="religion1" onchange="myFunction('communitylist','religion1','GetCommunities','community1')" onload="myFunction('communitylist','religion1','GetCommunities','community1')">
													<option value="">Doesn't Matter</option>
													<?php foreach($religion as $value){
														if($UserDetails[0]['Religion'] == $value->Religion_Id){$select = "selected=selected";}else{$select="";}?>
													
													<option value="<?php echo $value->Religion_Id; ?>" <?=$select;?>><?php echo $value->Religion_Name;?></option>    
													<?php } ?> 
												</select>
												</div>
											</div>
											<div class="col-md-6 textleft margtop15">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Mother Tongue:</span></div>
											</div>
											<div class="col-md-6">						
												<div class="margtop7">
												<?php echo isset($UserDetails[0]['Language_Name']) ? $UserDetails[0]['Language_Name'] : ""; ?>
												<?php    $language = $this->action_model->get_table_details('languagelist'); ?>
										
												<select class="form-control" name="MotherTongue" id="Pmother_tongue" >
													<option value="">Doesn't Matter</option>
													<?php 
													foreach($language as $value){
														if($UserDetails[0]['MotherTongue'] == $value->Language_Id){$select = "selected=selected";}else{$select="";}?>
											
													<option value="<?php echo $value->Language_Id; ?>" <?=$select;?>><?php echo $value->Language_Name;?></option>    
													<?php } ?>
										   
												</select>
												</div>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="margtop7"><span class="dark">Community:</span></div>
											</div>
											<div class="col-md-6">						
												<div class="margtop7">
												<?php  $com = $this->action_model->get_table_details('communitylist'); ?>
												<select class="form-control" name="Community" id="community1">			
													<?php echo isset($UserDetails[0]['Community_Name']) ? $UserDetails[0]['Community_Name'] : ""; ?>
												</select>
												</div>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
									
										
											
											</div>
											
										
											
											<br/>
											<br/>
											<strong class="size16px bold dark left">Education & Career</strong><hr/>
											<div class="clearfix"></div>
											<div class="line4"></div>		
											
											
											<div id="education">
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Education:</span></div>
											</div>
											<div class="col-md-6">
												<?php $degree = $this->action_model->get_table_details('degree');?>
												<select class="form-control m10" name="EducationLevel" id="EducationLevel">
													<option value="">select degree</option>
													<?php 
													foreach($degree as $value){ 
													if($UserDetails[0]['EducationLevel'] == $value->Degree_Id){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?php echo $value->Degree_Id; ?>" <?=$sel;?>><?php echo $value->Degree_Name;?></option>    
													<?php } ?>   
												</select>
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Specialization:</span></div>
											</div>
											<div class="col-md-6 textleft">
												<?php $spec = $this->action_model->get_table_details('specialization');?>
												<select class="form-control m10" name="EducationField" id="EducationField">
													<option value="">select specialization</option>
													<?php 
													foreach($spec as $value){
													if($UserDetails[0]['EducationField'] == $value->Id){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?php echo $value->Id; ?>" <?=$sel;?>><?php echo $value->SName;?></option>    
													<?php } ?>
												</select>
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Working With:</span></div>
											</div>
											<div class="col-md-6">
												<select class="form-control m10" name="WorkingWith" id="WorkingWith">
													<?php $sel1=""; $sel2=""; $sel3=""; switch($UserDetails[0]['WorkingWith']){
														case 1 :
															$sel1 ="selected=selected";
															break;
														case 2 :
															$sel2 ="selected=selected";
															break;
														case 3 :
															$sel3 ="selected=selected";
															break;
														default :
														break;
													}?>
													<option value="">Select working with</option>
													<option value="1" <?=$sel1;?>>not working</option>
													<option value="2" <?=$sel2;?>>Government</option>
													<option value="3" <?=$sel3;?>>Private</option>
											   </select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Working As:</span></div>
											</div>
											<div class="col-md-6">
												<?php  $spec = $this->action_model->get_table_details('workingas'); ?>
											
												<select class="form-control m10" name="WorkingAs" id="WorkingAs">
													<option value="Doesn't Matter">select working as</option>
													<?php 
													foreach($spec as $value){
													if($UserDetails[0]['WorkingAs'] == $value->Work_Id){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?php echo $value->Work_Id; ?>" <?=$sel;?>><?php echo $value->Work_Name;?></option>    
													<?php } ?>
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Annual Income:</span></div>
											</div>
											<div class="col-md-6">
											<?php $AnualIncome=  array("Upto INR 1 Lakh","INR 1 Lakh to 2 Lakh","INR 2 Lakh to 4 Lakh","INR 4 Lakh to 7 Lakh",
											"INR 7 Lakh to 10 Lakh","INR 10 Lakh to 15 Lakh","INR 15 Lakh to 20 Lakh","INR 20 Lakh to 30 Lakh","INR 30 Lakh to 50 Lakh",
											"INR 50 Lakh to 75 Lakh","INR 75 Lakh to 1 Crore","Dont want to specify");											?>
												<select class="form-control" name="AnualIncome" id="AnualIncome">
												<option value="">Select</option>
											<?php for($i=0;$i<count($AnualIncome);$i++){ 
												 if($UserDetails[0]['AnualIncome'] != ""){													if($UserDetails[0]['AnualIncome'] == $AnualIncome[$i]){$sel="selected=selected";}else{$sel="";}												 }?>
												
												<option value="<?=$AnualIncome[$i];?>" <?=$sel;?>><?=$AnualIncome[$i];?></option>
											<?php }?>	
										   </select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											</div>
											
											<br/>
											<br/>
											<strong class="size16px bold dark left">Lifestyle</strong><hr/>
											<div class="clearfix"></div>
											<div class="line4"></div>		
											
											
											<div id="lifestyle">
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Diet:</span></div>
											</div>
											<?php $veg=""; $Nonveg=""; $occa=""; $egg ="";$Jain =""; 
											if($UserDetails != ""){
											switch($UserDetails[0]['Diet']){
														case "veg" :
															$veg ="checked=checked";
															break;
														case "Non veg" :
															$Nonveg ="checked=checked";
															break;
														case "Occasionally Non-Veg" :
															$occa ="checked=checked";
															break;
														case "Eggetarian" :
															$egg ="checked=checked";
															break;
														case "Jain" :
															$Jain ="checked=checked";
															break;
														default :
														break;
													}
													}?>
											<div class="col-md-8">
												<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Diet" id="optionsRadios1" value="veg" <?=$veg;?>>
															 Veg
														  </label>
														</div>
													</div>
														<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Diet" id="optionsRadios1" value="Non veg" <?=$Nonveg;?>>
															 Non-Veg
														  </label>
														</div>
													</div>
														<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Diet" id="optionsRadios1" value="Occasionally Non-Veg" <?=$occa;?>>
															 Occasionally Non-Veg
														  </label>
														</div>
													</div>
													<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Diet" id="optionsRadios1" value="Eggetarian" <?=$egg;?>>
															 Eggetarian
														  </label>
														</div>
													</div>
														<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Diet" id="optionsRadios1" value="Jain" <?=$Jain;?>>
															 Jain 
														  </label>
														</div>
													</div>
														
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Drink:</span></div>
											</div>
											<div class="col-md-6">
												<div class="w50percent">
												<?php $yes=""; $no="";
												if($UserDetails != "" && $UserDetails[0]['Drink'] == 1){$yes="checked=checked";}else{$no="checked=checked";} ?>
													<div class="radio">
														  <label>
															<input type="radio" name="Drink" id="optionsRadios1" value="1" <?=$yes;?>>
															 Yes
														  </label>
														</div>
													</div>
													<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Drink" id="optionsRadios1" value="2" <?=$no;?>>
															 No
														  </label>
														</div>
													</div>
													
											</div>
											<div class="col-md-2 textleft">
											</div>
											<div class="clearfix"></div>
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Smoke:</span></div>
											</div>
											<?php $yes=""; $no="";if($UserDetails !="" && $UserDetails[0]['Smoke'] == 1){$yes="checked=checked";}else{$no="checked=checked";} ?>
											<div class="col-md-6">
												<div class="w50percent">
													<div class="radio">
														  <label>
															<input type="radio" name="Smoke" id="optionsRadios1" value="1" <?=$yes;?>>
															 Yes
														  </label>
														</div>
													</div>
													<div class="w50percent">
														<div class="radio">
														  <label>
															<input type="radio" name="Smoke" id="optionsRadios1" value="2" <?=$no;?>>
															 No
														  </label>
														</div>
													</div>
													
											</div>
											<div class="col-md-2 textleft">
											</div>
											<div class="clearfix"></div>
											</div>
											
											<br/>
											<br/>
											<strong class="size16px bold dark left">Location</strong><hr/>
											<div class="clearfix"></div>
											<div class="line4"></div>		
											
											<div id="location">
											<br/>
											<div class="col-md-6 textright">
												<div class="mt7"><span class="dark">Country Living In:</span></div>
											</div>
											
											<div class="col-md-6">
												<?php  $country = $this->action_model->get_table_details('countrylist'); ?>
												<select class="form-control"  name="LivingIn" id="LivingCountry" onchange="GetList('LivingCountry','LivingState','Country_Id','statelist','State_Id','State_Name')">			
													<option value="">Select Country</option>
													<?php 
													foreach($country as $country){
														 if($UserDetails[0]['LivingIn'] == $country->Country_Id){$sel="selected=selected";}else{$sel="";}?>
												
													<option value="<?php echo $country->Country_Id; ?>" <?=$sel;?>><?php echo $country->Country_Name;?></option>    
													<?php } ?>
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt7"><span class="dark">State Living In:</span></div>
											</div>
											<div class="col-md-6" id="">
												
												<select class="form-control" name="LivingState" id="LivingState" onchange="GetList('LivingState','LivingCity','State_Id','citylist','City_Id','City_Name')">												<option id="HideState"><?php echo isset($UserDetails[0]['State_Name']) ? $UserDetails[0]['State_Name'] : "";?></option>
												</select>
											</div>
											
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt7"><span class="dark">City Living In:</span></div>
											</div>
											<div class="col-md-6">
												
												<select name="LivingCity" id="LivingCity" class="form-control">
												<option id="hideCity"><?php echo isset($UserDetails[0]['City_Name']) ? $UserDetails[0]['City_Name'] : "";?></option>
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											
											
										
											
											<div class="col-md-6 textright">
												<div class="mt7"><span class="dark">Zip/Pin Code:</span></div>
											</div>
											<div class="col-md-6">
												<input type="text" name="PinNo" id="PinNo" class="form-control" 
												value="<?php echo isset($UserDetails[0]['PinNo']) ? $UserDetails[0]['PinNo'] : "";?>">
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											</div>
											
											<br/>
											<br/>
											
											<strong class="size16px bold dark left">More About Yourself, Partner and Family</strong><hr/>
											<div class="clearfix"></div>
											<div class="line4"></div>	
											<div id="About">
												
												<div class="col-md-2 textright">
												</div>
												<div class="col-md-8">
													<span class="size12">Personality, Family Details, Career, Partner Expectations etc.</span>
													<textarea rows="3" name="About" id="About" class="form-control margtop10" style="height: 75px; resize: none;">
													<?php echo isset($UserDetails[0]['About']) ? $UserDetails[0]['About'] :"";?></textarea>
												</div>
												<div class="col-md-2 textleft">
												</div>
												<div class="clearfix"></div>
												<div class="col-md-12" style="text-align: center;">
													<button type="submit" class="btn-search margtop30">Save</button>
												</div>
											</div>
									
										</div>
								
									</div>
								</form>
						
							</div>
														<script type="text/javascript" src="<?php echo WEB_DIR_FRONT; ?>dist/js/bootstrap-multiselect.js"></script>							<link rel="stylesheet" href="<?php echo WEB_DIR_FRONT; ?>dist/css/bootstrap-multiselect.css" type="text/css"/>	
							<div id="partner" class="tab-pane fade" style="padding:30px;">
								<form action="<?php echo WEB_URL;?>home/UpdatePartnerProfile" method="POST" id="multiselectForm">			<!-- LEFT CONTENT -->			<div class="col-md-8 pagecontainer2 offset-0">				<div class="padding30 grey">									<span class="size16px bold dark left"> Basic Info</span>					<div class="roundstep active right" data-toggle="collapse" data-target="#basic-info">1</div>					<div class="clearfix"></div>					<div class="line4"></div>										 <div class="form-group">						<label class="control-label col-sm-4" for="email"> Age From</label>						<div class="col-sm-8">						 <select name="AgeFrom" class="form-control">							<?php for($i=18;$i<62;$i++){ 							if($PartnerDetails != ""){if($PartnerDetails[0]['AgeFrom'] == $i){$select = "selected=selected";}else{$select="";}							}else{$select="";}?>								<option value="<?=$i;?>" <?=$select;?>><?=$i;?></option>							<?php } ?>						</select>						</div>					  </div>  						  <br/><br/>					 <div class="form-group">						<label class="control-label col-sm-4" for="email"> Age To</label>						<div class="col-sm-8">							<select name="AgeTo" class="form-control">							<?php for($j=18;$j<62;$j++){ 							if($PartnerDetails != ""){if($PartnerDetails[0]['AgeTo'] == $j){$select = "selected=selected";}else{$select="";}							}else{$select="";}?>								<option value="<?=$j;?>" <?=$select;?>><?=$j;?></option>							<?php } ?>							</select>						</div>					  </div>  	  					  <br/><br/>					  <div class="form-group">						<label class="control-label col-sm-4" for="email"> Height From</label>						<div class="col-sm-8">													<select name="HeightFrom" class="form-control">							<?php  $HeightFrom = $this->action_model->get_table_details('HeightList');	?>							<option value="">Select Height From</option>																<?php 								foreach($HeightFrom as $value){								if($PartnerDetails != ""){								if($PartnerDetails[0]['HeightFrom'] == $value->Height_Id){$select = "selected=selected";}else{$select="";}								}else{$select="";}?>								<option value="<?php echo $value->Height_Id; ?>" <?=$select;?>><?php echo $value->Height;?></option>    								<?php } ?>							</select>													</div>					  </div> 					<br/><br/>					  					 <div class="form-group">						<label class="control-label col-sm-4" for="email"> Height To</label>						<div class="col-sm-8">							<select name="HeightTo" class="form-control">							<?php  $HeightTo = $this->action_model->get_table_details('HeightList');	?>							<option value="">Select Height From</option>																<?php 								foreach($HeightTo as $value){								if($PartnerDetails != ""){								if($PartnerDetails[0]['HeightTo'] == $value->Height_Id){$select = "selected=selected";}else{$select="";								}}else{$select="";}?>								<option value="<?php echo $value->Height_Id; ?>" <?=$select;?>><?php echo $value->Height;?></option>    								<?php } ?>							</select>													</div>					  </div>  <br/><br/>					  					  <div class="form-group">						<label class="control-label col-sm-4" for="email">Marital Status</label>						<div class="col-sm-8">						<?php $marital_status = array("Never married","Divorced","Widowed","Awaiting_Divorced","Annulled");?>							<select class="form-control" name="MaritalStatus[]" multiple>												<option value="">Doesn't Matter</option>								<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['MaritalStatus'] != ""){										foreach($marital_status as $value){										$MaritalStatus = explode(",",$PartnerDetails[0]['MaritalStatus']);												if(in_array($value,$MaritalStatus)){													echo '<option value="'.$value.'" selected>'.$value.'</option>';												}else{													echo '<option value="'.$value.'">'.$value.'</option>';												}										}									}else{										foreach($marital_status as $value){											echo '<option value="'.$value.'">'.$value.'</option>';										}									} ?> 					  													</select>						</div>					  </div> 	<br/><br/>					  					  <div class="form-group">						<label class="control-label col-sm-4" for="email">select partner Religion</label>						<?php $religion = $this->action_model->get_table_details('religionlist'); ?>						<div class="col-sm-8">							<select class="form-control" name="Religion[]" id="religion" onchange="GetList('religion','Community','Religion_Id','communitylist','Community_Id','Community_Name')" multiple >								<option value="">Doesn't Matter</option>								<?php 								if($PartnerDetails != "" && $PartnerDetails[0]['Religion_Id'] != ""){								$Religion = explode(",",$PartnerDetails[0]['Religion']);																foreach($religion as $value){									if(in_array($value->Religion_Id,$Religion)){																	?>								<option value="<?php echo $value->Religion_Id; ?>" selected><?php echo $value->Religion_Name;?></option>    								<?php }else{?>								<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    															<?php }								}}else{								foreach($religion as $value){ 												echo '<option value="'.$value->Religion_Id.'">'.$value->Religion_Name.'</option>'; 											 }								} ?> 							</select>						</div>					  </div><br/><br/>					  					  <div class="form-group" id="Community_Dev">						<label class="control-label col-sm-4" for="email">select partner Community</label>						<?php  $com = $this->action_model->get_table_details('communitylist'); ?>						<div class="col-sm-8">								<select class="form-control" name="Community[]" id="Community" multiple>									<option value="">Doesn't Matter</option>											<?php 											if($PartnerDetails != "" && $PartnerDetails[0]['Community_Id'] !=""){											$Community = explode(",",$PartnerDetails[0]['Community']);												for($i=0;$i<count($Community);$i++){											foreach($com as $values){											if($Community[$i] == $values->Community_Id){											 echo '<option value="'.$values->Community_Id.'" selected>'.$values->Community_Name.'</option> ';											}else{continue;}											 }}}else{											 foreach($states as $state){ 												echo '<option value="'.$values->Community_Id.'">'.$values->Community_Name.'</option>'; 											 }} ?>																</select>						</div>					  </div> 		<br/><br/>					  					 <div class="form-group">						<label class="control-label col-sm-4" for="email">Select Partner profile created by</label>						<div class="col-sm-8">						<?php $profile_created_by = array("self","son","Daughter","parents");?>														<select class="form-control" name="ProfileCreatedby[]" multiple>									<option value="">Doesn't Matter</option>									<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['ProfileCreatedby'] != ""){									foreach($profile_created_by as $value){										$ProfileCreatedby = explode(",",$PartnerDetails[0]['ProfileCreatedby']);																				if(in_array($value,$ProfileCreatedby)){ ?>									<option value="<?= $value;?>" selected><?=$value;?></option>									<?php }else{?> 									<option value="<?= $value;?>"><?=$value;?></option>																		<?php } ?>									<?php  }}else{									foreach($profile_created_by as $value){ ?>									<option value="<?= $value;?>" ><?=$value;?></option>																		<?php }}?> 								</select>						</div>					  </div>					  <br/><br/>					   <div class="form-group">						<label class="control-label col-sm-4" for="email">Country Grew up</label>												<?php     $countrys = $this->action_model->get_table_details('countrylist'); ?>						<div class="col-sm-8">							<select class="form-control" name="CountryGrewUp[]" id="CountryGrewUp"  multiple>								<option value="">Doesn't Matter</option>								<?php $CountryGrewUp = explode(",",$PartnerDetails[0]['CountryGrewUp']);								if($PartnerDetails != "" && $PartnerDetails[0]['CountryGrewUp'] != ""){									foreach($countrys as $country){									if(in_array($country->Country_Id, $CountryGrewUp)){ ?>									<option value="<?php echo $country->Country_Id; ?>" selected><?php echo $country->Country_Name;?></option>    																	<?php	}else{ ?>								<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    																	<?php }?>																	<?php }}else{ foreach($countrys as $country){?>										<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    																	<?php }} ?>							 </select>						</div>					  </div>					  <br/><br/>						<div class="form-group">						<label class="control-label col-sm-4" for="email">Mother Tongue</label>						<?php    $language = $this->action_model->get_table_details('languagelist'); ?>						<div class="col-sm-8">							<select class="form-control" name="MotherTongue[]" multiple>								<option value="">Doesn't Matter</option>								<?php 									if($PartnerDetails != ""){										$MotherTongue = explode(",",$PartnerDetails[0]['MotherTongue']);										foreach($language as $value){											if(in_array($value->Language_Id,$MotherTongue)){ ?>																				<option value="<?php echo $value->Language_Id; ?>" selected><?php echo $value->Language_Name;?></option>    								<?php }else{ ?>								<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    																<?php } ?>								<?php }}else{								foreach($language as $value){ ?>																				<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>   								<?php }								} ?>							</select>       													</div>					  </div><br/><br/>					<div class="form-group">						<label class="control-label col-sm-4" for="email">Living Country</label>						<?php     $countrys = $this->action_model->get_table_details('countrylist'); ?>						<div class="col-sm-8">							<select class="form-control" name="CountryLivingIn[]" id="living_in"  onchange="GetList('living_in','LivingState','Country_Id','statelist','State_Id','State_Name')" multiple>								<option value="">Doesn't Matter</option>								<?php $CountryLivingIn = explode(",",$PartnerDetails[0]['CountryLivingIn']);																if($PartnerDetails != "" && $PartnerDetails[0]['CountryLivingIn'] != ""){																	foreach($countrys as $country){																		if(in_array($country->Country_Id,$CountryLivingIn)){ ?>										<option value="<?php echo $country->Country_Id; ?>" selected><?php echo $country->Country_Name;?></option>    									<?php }else{ ?>									<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    																	<?php } ?>									<?php }}else{ } ?>							 </select>						</div>						</div>		<br/><br/>												<div class="form-group" id="LivingState_Dev">						<label class="control-label col-sm-4" for="email">Living State</label>						<div class="col-sm-8">							<select class="form-control" name="LivingState[]" id="LivingState" onChange="GetList('LivingState','LivingCity','State_Id','citylist','City_Id','City_Name')" multiple>								<?php  $states = $this->action_model->get_table_details('statelist');									if($PartnerDetails != "" && $PartnerDetails[0]['LivingState'] != ""){									      $LivingState = explode(",",$PartnerDetails[0]['LivingState']);																					foreach($states as $state){										if(in_array($state->State_Id,$LivingState)){											echo '<option value="'.$state->State_Id.'" selected>'.$state->State_Name.'</option>';  											}else{continue;}								 }}else{} ?>							</select>						</div>						</div>											<br/><br/>							<div class="form-group" id="LivingCity_Dev">						<label class="control-label col-sm-4" for="email">Living City</label>						<div class="col-sm-8">							<select class="form-control" name="LivingCity[]" id="LivingCity" multiple>							<?php $citys = $this->action_model->get_table_details('citylist');																if($PartnerDetails != "" && $PartnerDetails[0]['LivingCity'] != ""){									      $LivingCity = explode(",",$PartnerDetails[0]['LivingCity']);									      										foreach($citys as $city){										if(in_array($city->City_Id, $LivingCity)){											echo '<option value="'.$city->City_Id.'" selected=selected>'.$city->City_Name.'</option>';}else{$select="";}								 }}else{								 foreach($citys as $city){ 								 echo '<option value="'.$city->City_Id.'">'.$city->City_Name.'</option>';								} } ?>											  							</select>						</div>						</div>							<br/><br/>					 					<div class="form-group">						<label class="control-label col-sm-4" for="residencyStatus">Education level</label>						<?php  $degree = $this->action_model->get_table_details('degree'); ?>						<div class="col-sm-8">							<select class="form-control" name="Education[]" multiple>								<option value="">Doesn't Matter</option>								<?php 								if($PartnerDetails != "" && $PartnerDetails[0]['Education'] != ""){								 $Education = explode(",",$PartnerDetails[0]['Education']);	    								foreach($degree as $value){								if(in_array($value->Degree_Id,$Education)){									?>								<option value="<?php echo $value->Degree_Id; ?>" selected><?php echo $value->Degree_Name;?></option>    								<?php }else{?> <option value="<?php echo $value->Degree_Id; ?>"><?php echo $value->Degree_Name;?></option>    <?php }								}}else{								foreach($degree as $value){ ?>								<option value="<?php echo $value->Degree_Id; ?>"><?php echo $value->Degree_Name;?></option>   								<?php }								} ?>	  							</select>						</div>					 </div>		<br/><br/>					 								 					<div class="form-group">						<label class="control-label col-sm-4" for="residencyStatus">Education Fields</label>						<?php $spec = $this->action_model->get_table_details('specialization'); ?>						<div class="col-sm-8">							<select class="form-control" name="Education_Field[]" multiple>								<option value="">Doesn't Matter</option>								<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['Education_Field'] != ""){									foreach($spec as $value){										$Education_Field = explode(",",$PartnerDetails[0]['Education_Field']);										for($i=0;$i<count($Education_Field);$i++){										if($Education_Field[$i] == $value->Id){ ?>									<option value="<?=$value->Id;?>" selected><?=$value->SName;?></option>									<?php }else{ echo '<option value="'.$value->Id.'">'.$value->SName.'</option>';} ?>									<?php } }}else{									foreach($spec as $value){ ?>									<option value="<?=$value->Id;?>"><?=$value->SName;?></option>									<?php }}?> 							</select>						</div>					 </div>	         <br/><br/>					 					<div class="form-group">						<label class="control-label col-sm-4" for="residencyStatus">Work Area</label>						<?php  $spec = $this->action_model->get_table_details('workingas'); ?>						<div class="col-sm-8">							<select class="form-control" name="ProfessionArea[]" multiple>								<option value="">Doesn't Matter</option>								<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['ProfessionArea'] != ""){									foreach($spec as $value){										$ProfessionArea = explode(",",$PartnerDetails[0]['ProfessionArea']);										if(in_array($value->Work_Id,$ProfessionArea)){ ?>											<option value="<?=$value->Work_Id;?>" selected><?=$value->Work_Name;?></option>									<?php }else{?> <option value="<?=$value->Work_Id;?>"><?=$value->Work_Name;?></option><?php } ?>									<?php }}else{									foreach($spec as $value){ ?>									<option value="<?=$value->Work_Id;?>"><?=$value->Work_Name;?></option>								<?php }}?> 							</select>						</div>					 </div>	<br/><br/>					 					<div class="form-group">								<?php $AnualIncome=  array("Upto INR 1 Lakh","INR 1 Lakh to 2 Lakh","INR 2 Lakh to 4 Lakh","INR 4 Lakh to 7 Lakh",											"INR 7 Lakh to 10 Lakh","INR 10 Lakh to 15 Lakh","INR 15 Lakh to 20 Lakh","INR 20 Lakh to 30 Lakh","INR 30 Lakh to 50 Lakh",											"INR 50 Lakh to 75 Lakh","INR 75 Lakh to 1 Crore","Dont want to specify");											?>											<label class="control-label col-sm-4" for="residencyStatus">Annual Income</label>						<div class="col-sm-8">						   							<select class="form-control" name="AnualIncome[]" multiple>								<option value="">Select</option>									<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['AnualIncome'] != ""){									foreach($AnualIncome as $value){										$anualIncome = explode(",",$PartnerDetails[0]['AnualIncome']);										if(in_array($value,$anualIncome)){ ?>									<option value="<?=$value;?>" selected><?=$value;?></option>									<?php }else{ ?> <option value="<?=$value;?>"><?=$value;?></option> <?php } ?>									<?php }}else{									foreach($AnualIncome as $value){ ?>									<option value="<?= $value;?>" <?=$select;?>><?=$value;?></option>																		<?php }}?> 						   </select>						</div>					</div>					<br/><br/>					<div class="form-group">											<label class="control-label col-sm-4" for="residencyStatus">Select Disablities</label>						<div class="col-sm-8"> 							<select class="form-control" name="disability">								<option value="">Doesn't Matter</option>>								<option value="2">No</option>    								<option value="1">yes</option> 							   </select>						</div>					</div>					<br/><br/>					<div class="form-group">											<label class="control-label col-sm-4" for="residencyStatus">Diet</label>						<?php $DietList = array("veg","Non veg","Occasionally Non-Veg","Eggetarian","Jain");?>						<div class="col-sm-8"> 						<select class="form-control" name="Diet[]" multiple>								<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['Diet'] != ""){									foreach($DietList as $value){										$Diet = explode(",",$PartnerDetails[0]['Diet']);																				if(in_array($value,$Diet)){ ?>									<option value="<?=$value;?>" selected><?=$value;?></option>									<?php }else{ ?> <option value="<?=$value;?>"><?=$value;?></option> <?php } ?>									<?php }}else{									foreach($DietList as $value){ ?>										<option value="<?= $value;?>"><?=$value;?></option>																		<?php }}?> 						</select>													</div>					</div>					<br/><br/>					<div class="form-group">											<label class="control-label col-sm-4" for="residencyStatus">Body Type</label>						<?php $bodyType = array("slim","average","Athletic","heavy");?>						<div class="col-sm-8"> 							<select class="form-control" name="BodyType[]" multiple>								<?php 									if($PartnerDetails != "" && $PartnerDetails[0]['BodyType'] != ""){									foreach($bodyType as $value){										$BodyType = explode(",",$PartnerDetails[0]['BodyType']);																				if(in_array($value,$BodyType)){ ?>									<option value="<?=$value;?>" selected><?=$value;?></option>									<?php }else{ ?> <option value="<?=$value;?>"><?=$value;?></option> <?php } ?>									<?php }}else{									foreach($bodyType as $value){ ?>									<option value="<?=$value;?>"><?=$value;?></option>																		<?php }}?> 							</select>													</div>					</div>							<br/><br/>					<div class="form-group">											<label class="control-label col-sm-4" for="residencyStatus">Smoke</label>						<?php $yes="";$no=""; if($PartnerDetails != ""){						if($PartnerDetails[0]['Smoke'] == 1){$yes ="checked=checked";}else{$no ="checked=checked";}						}?>													<div class="col-sm-8"> 							<input type="radio" name="Smoke" value="1" <?=$yes;?>>Yes							<input type="radio" name="Smoke" value="2" <?=$no;?>>No							<input type="radio" name="Smoke" value="">Doesn't Matter						</div>					</div>	<br/><br/>										<div class="form-group">											<label class="control-label col-sm-4" for="residencyStatus">Drinks</label>						<?php $yes="";$no=""; if($PartnerDetails != ""){						if($PartnerDetails[0]['Drink'] == 1){$yes ="checked=checked";}else{$no ="checked=checked";} 						}?>												<div class="col-sm-8"> 							<input type="radio" name="Drink" value="1" <?=$yes;?>>Yes							<input type="radio" name="Drink" value="2" <?=$no;?>>No							<input type="radio" name="Drink" value="">Doesn't Matter						</div>					</div>	<br/><br/>										<div class="form-group">											<label class="control-label col-sm-4" for="residencyStatus">select Dosham</label>						<?php $yes="";$no=""; if($PartnerDetails != ""){						if($PartnerDetails[0]['Dosham'] == 1){$yes ="checked=checked";}else{$no ="checked=checked";}						}?>												<div class="col-sm-8"> 							<input type="radio" name="Dosham" value="1" <?=$yes;?>>Yes							<input type="radio" name="Dosham" value="2" <?=$no;?>>No						</div>					</div>	<br/><br/>						<div class='form-group'>						<div class='col-sm-6 col-sm-offset-3'>							<input type='submit' class='btn save_btn' value='save' id="submit">						</div>					</div>						</div>					</div>			</form>
							</div>
							
							<div id="family" class="tab-pane fade" style="padding:30px;">
								<form action="<?php echo WEB_URL;?>home/FamilyinfoUpdate" method="POST" class="form-horizontal fv-form fv-form-bootstrap">
										<strong class="size16px bold dark left">Family</strong><hr/>
											<div class="clearfix"></div>
											<div class="line4"></div>	
											<div id="family">
											
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Father's Status:</span></div>
											</div>
											 <input type="hidden" name="id" value="<?php echo isset($user_id) ? $user_id : 0; ?>">
											<div class="col-md-6">
												<select class="form-control mt7" name="father_status" id="father_status">
												<?php $father_status = array("Employed","Business","Professional","Retired","Not Employed","Passed Away"); ?>
													<option value="">select father's status</option>
													<?php for($i=0;$i<count($father_status);$i++){
														if($UserDetails != "" && $UserDetails[0]['father_status'] == $father_status[$i]){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?=$father_status[$i];?>" <?=$sel;?>><?=$father_status[$i];?></option>
													<?php } ?>
										
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Mother's Status:</span></div>
											</div>
											<div class="col-md-6">
											<?php $mother_status = array("Homemaker","Employed","Business","Professional","Retired","Not Employed","Passed Away"); ?>
												<select class="form-control mt15" name="mother_status" id="mother_status">
													<option value="">select mother's status</option>
													<?php for($i=0;$i<count($mother_status);$i++){
														if($UserDetails[0]['mother_status'] == $mother_status[$i]){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?=$mother_status[$i];?>" <?=$sel;?>><?=$mother_status[$i];?></option>
													<?php } ?>
										
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt20"><span class="dark">No. of Brothers:</span></div>
											</div>
											<div class="col-md-6">
											<span class="size12">Total No. of Brothers</span>
												<select class="form-control" name="num_bro" id="num_bro">
												<option value="">select no of brothers</option>
														<?php for($i=0;$i<10;$i++){
														if($UserDetails[0]['num_bro'] == $i){$sel="selected=selected";}else{$sel="";}?>
												<option value="<?=$i;?>"  <?=$sel;?>><?=$i;?></option>
												<?php } ?>	</select>
											</div>
											<div class="col-md-6 textleft">
											<span class="size12">Of Which Married</span>
												<select class="form-control" name="num_bro_married" id="num_bro_married">
												<option value="">select no of brothers married</option>
												<?php for($i=0;$i<10;$i++){
												 if($UserDetails[0]['num_bro_married'] == $i){$sel="selected=selected";}else{$sel="";}?>
												<option value="<?=$i;?>"  <?=$sel;?>><?=$i;?></option>
												<?php } ?>	</select>
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt20"><span class="dark">No. of Sisters:</span></div>
											</div>
											<div class="col-md-6">
												<span class="size12">Total No. of Sisters</span>
												<select  class="form-control" name="num_sis" id="num_sis">
												<option value="">select sisters</option>
												<?php for($i=0;$i<10;$i++){
												 if($UserDetails[0]['num_sis'] == $i){$sel="selected=selected";}else{$sel="";}?>
												<option value="<?=$i;?>" <?=$sel;?>><?=$i;?></option>
												<?php } ?>
												</select>
											</div>
											<div class="col-md-6 textleft">
												<span class="size12">Of Which Married</span>
												<select  class="form-control" name="num_sis_married" id="num_sis_married">
												<option value="">select no of sisters married</option>
														<option value="select"></option>
												<?php for($i=0;$i<10;$i++){
												 if($UserDetails[0]['num_sis_married'] == $i){$sel="selected=selected";}else{$sel="";}?>
												<option value="<?=$i;?>" <?=$sel;?>><?=$i;?></option>
												<?php } ?>	</select>
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt20"><span class="dark">Native Place:</span></div>
											</div>
											<div class="col-md-6">
												<span class="size10">(Parent's birth place or Origin Place)</span>
												<input  class="form-control" type="text" name="native" id="nativea" 
												value="<?php isset($UserDetails[0]['native']) ? $UserDetails[0]['native'] : "";?>"> 
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Affluence Level:</span></div>
											</div>
											<div class="col-md-6">
											<?php $level = array("Upper Middle Class","Middle Clas","Lower Middle Class"); ?>
												<select  class="form-control m10" name="level" id="level">
													<option value="">Select Affluence Level</option>
													<?php for($i=0;$i<count($level);$i++){
													 if($UserDetails != "" && $UserDetails[0]['level'] == $level[$i]){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?=$level[$i];?>" <?=$level[$i];?> <?=$sel;?>><?=$level[$i];?></option>
													<?php } ?>
													
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											
											<br/>
											<div class="col-md-6 textright">
												<div class="mt15"><span class="dark">Family Values:</span></div>
											</div>
											<div class="col-md-6">
											<?php $family_value = array("Traditional","Moderate","Liberal"); ?>
												<select  class="form-control" name="family_value" id="family_value">
												<option value="">Select family value</option>
												<?php for($i=0;$i<count($level);$i++){ 
												 if($UserDetails[0]['family_value'] == $family_value[$i]){$sel="selected=selected";}else{$sel="";}?>
													<option value="<?=$family_value[$i];?>" <?=$sel;?>><?=$family_value[$i];?></option>
												<?php } ?>
												</select>
											</div>
											<div class="col-md-6 textleft">
											</div>
											<div class="clearfix"></div>
											</div>
								</form>
							</div>
							
							<div id="password" class="tab-pane fade" style="padding:30px;">
								<form action="<?php echo WEB_URL;?>home/ChangePassword" id="PasswordForm" method="POST" class="form-horizontal fv-form fv-form-bootstrap">
									<div class="col-md-8 pagecontainer2 offset-0">
									 <input type="hidden" name="id" value="<?php echo isset($user_id) ? $user_id : 0; ?>">
										<div class="form-group">
											<label class="col-sm-4">Password</label>
											<div class="col-sm-6">
												<input type="password" name="password" id="password" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4">ReType Password</label>
											<div class="col-sm-6">
												<input type="password" name="Epassword" id="Epassword" class="form-control">
											</div>
										</div>
										<div class="col-sm-4 col-sm-offset-7"><input type="submit" class="btn btn-primary submit_btn" name="bt_modif" id="bt_modif" value="Chaneg Password"></div>
										<br/></br/>
									</div>
								</form>
							</div>
						</div>
					</div>
			
			
		</div>
		
		
	</div>

				</div><!--/span-->



			</div><!--/row-->





					<!-- content ends -->

			</div><!--/#content.span10-->
	</div>
	<?php
		$this->load->view('footer');
	?>
</div>

<script>$(document).ready(function() {$('#multiselectForm').find('[name="MaritalStatus[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="ProfileCreatedby[]"]')            .multiselect({                // Re-validate the multiselect field when it is changed                onChange: function(element, checked) {                    adjustByScrollHeight();                },                onDropdownShown: function(e) {                    adjustByScrollHeight();                },                onDropdownHidden: function(e) {                    adjustByHeight();                }            })            .end()$('#multiselectForm').find('[name="WorkingWith[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="Diet[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="BodyType[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="ProfessionArea[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="AnualIncome[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="Community[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()$('#multiselectForm').find('[name="Education[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()	 $('#multiselectForm').find('[name="MotherTongue[]"]')            .multiselect({                // Re-validate the multiselect field when it is changed                onChange: function(element, checked) {                    adjustByScrollHeight();                },                onDropdownShown: function(e) {                    adjustByScrollHeight();                },                onDropdownHidden: function(e) {                    adjustByHeight();                }            })            .end()	 $('#multiselectForm').find('[name="Religion[]"]')		.multiselect({			// Re-validate the multiselect field when it is changed			onChange: function(element, checked) {				adjustByScrollHeight();			},			onDropdownShown: function(e) {				adjustByScrollHeight();			},			onDropdownHidden: function(e) {				adjustByHeight();			}		})		.end()    $('#multiselectForm').find('[name="CountryLivingIn[]"]')            .multiselect({                // Re-validate the multiselect field when it is changed                onChange: function(element, checked) {                    adjustByScrollHeight();                },                onDropdownShown: function(e) {                    adjustByScrollHeight();                },                onDropdownHidden: function(e) {                    adjustByHeight();                }            })            .end()$('#multiselectForm').find('[name="Education_Field[]"]').multiselect({	// Re-validate the multiselect field when it is changed	onChange: function(element, checked) {		adjustByScrollHeight();	},	onDropdownShown: function(e) {		adjustByScrollHeight();	},	onDropdownHidden: function(e) {		adjustByHeight();	}}).end()      $('#multiselectForm').find('[name="LivingState[]"]')            .multiselect({                // Re-validate the multiselect field when it is changed                onChange: function(element, checked) {                    adjustByScrollHeight();                },                onDropdownShown: function(e) {                    adjustByScrollHeight();                },                onDropdownHidden: function(e) {                    adjustByHeight();                }            })            .end() 	 $('#multiselectForm').find('[name="LivingCity[]"]')            .multiselect({                // Re-validate the multiselect field when it is changed                onChange: function(element, checked) {                    adjustByScrollHeight();                },                onDropdownShown: function(e) {                    adjustByScrollHeight();                },                onDropdownHidden: function(e) {                    adjustByHeight();                }            })            .end() 	$('#multiselectForm').find('[name="CountryGrewUp[]"]')            .multiselect({                // Re-validate the multiselect field when it is changed                onChange: function(element, checked) {                    adjustByScrollHeight();                },                onDropdownShown: function(e) {                    adjustByScrollHeight();                },                onDropdownHidden: function(e) {                    adjustByHeight();                }            })            .end() 			    // You don't need to care about these methods    function adjustByHeight() {        var $body   = $('body'),            $iframe = $body.data('iframe.fv');        if ($iframe) {            // Adjust the height of iframe when hiding the picker            $iframe.height($body.height());        }    }    function adjustByScrollHeight() {        var $body   = $('body'),            $iframe = $body.data('iframe.fv');        if ($iframe) {            // Adjust the height of iframe when showing the picker            $iframe.height($body.get(0).scrollHeight);        }    }});
function myFunction(table,id,functionurl,dev){
	 var religion = $("#"+id).val();
	 if(religion != ""){
		$.ajax({
			url: "<?php echo WEB_URL_FRONT; ?>home/"+functionurl,
			data: {religion : religion},
			type: "POST",
			success: function(data) {
				$("#"+dev).html(data);
			}
		  });
	 }
}
function GetList(currentdev,nextdev,column,table,display1,display2){
	 var val = $("#"+currentdev).val();
	 if(val != ""){
		$.ajax({
			url: "<?php echo WEB_URL_FRONT; ?>home/GetList",
			data: {column:column , value : val,table:table,display1:display1,display2:display2},
			type: "POST",
			success: function(data) {
				$("#"+nextdev+"_Dev").css('display','block');
				$("#"+nextdev).html(data);
			}
		  });
	 }
}
</script>