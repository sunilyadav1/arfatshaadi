<?php session_start();
$this->load->view("include/header");
$id = $this->session->userdata('User_Id');
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->full_profile($id);
?>
<style type="text/css">
    a.prev_1 {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        cursor: pointer;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    a.prev_1:hover {
        background-color: #dddddd;
    }

    a.next_1:hover {
        background-color: #dddddd;
    }

    a.next_1 {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        cursor: pointer;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }

    /*.pagination_btn{
          position: relative;
      top: -203px;
      float: right;
    }
    */
    .pagination_container {
        border-right: none;
    }

    }
</style>
<div class="container">
<!--  Contact Us Field  -->
<!--  Contact Us Field  -->
<!-- CONTENT -->
<div class="container">
<div class="container pagecontainer offset-0">

<!-- SLIDER -->
<div class="col-md-8 pagecontainer2 offset-0">

<ul class="nav nav-tabs" id="myTab">
    <li onclick="mySelectUpdate()" class="active">
<!--        <a data-toggle="tab" href="#My-Match">-->
<!--            <span class="summary"></span>-->
<!--<!--            <span class="hidetext">My Match</span>-->
<!--        </a>-->
    </li>
</ul>
<div class="tab-content4">
<!-- TAB 1 -->

<div class="padding20 newmatches">
    <div class="panel panel-default">
        <div class="panel-heading">New Matches</div>
        <div class="panel-body">
            <div class='contentNewmatch'>
                <div class="pagination_btn" style="position:relative; top: -53px; float: right;"><a
                        class="prev_1">&laquo;</a> <a class="next_1">&raquo;</a></div>
                <ul>
                    <?php $result = $this->action_model->preferred_match($id, $Gender, 'new_match');

                    if ($result == 'failure') {
                        ?>
                        <div class="alert alert-info">
                            No matches found !
                        </div>

                    <?php
                    } else {
						echo'<div class="alert alert-info">
							 '.count($result['result']).' New matches found
						</div>';
                        foreach ($result['result'] as $value) {
                           
                                if ($value['ProfilePic'] == "") {
                                    $ppic = "no-profile.gif";
                                } else {
									// if(file_exists(WEB_DIR . "images/profiles/" . $value['ProfilePic'])){
									// 	$ppic = $value['ProfilePic'];
									// }else{
									// 	 $ppic = "no-profile.gif";
									// }
                                    $ppic = $value['ProfilePic'];
                                }
								$pimg = WEB_DIR . "images/profiles/" . $ppic;
                            ?>
							
                            <li class="matches">
                                <div class="padding10 pagination_container">
                                    <div class="col-md-2 text-center  offset-0">
                                        <a href="#"><img src="<?php echo $pimg; ?>" alt="" class="fwimg img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-10 searchmt offset-0" >
                                        <div class="col-md-12">
											<div class="col-md-9">
                                            <h4 class="opensans dark bold margtop1 lh1"><?= $value['Name']; ?>
                                            <span style="font-size: 10px;">(<?php $DOB = $this->action_model->birthda($value['DOB']);
										echo isset($DOB) ? $DOB." Years ," : "";?> 
										<?php echo isset($value['Community_Name']) ? $value['Community_Name']."," : ""; ?>
                                            <?php $HeightValue = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$value['Height']);
										echo isset($HeightValue[0]->Height) ? $HeightValue[0]->Height."," : ""; ?>
                                            <?php echo isset($value['SubCommunity_Name']) ? $value['SubCommunity_Name']."," : ""; ?>
                                            
                                            <?php echo isset($value['Language_Name']) ? $value['Language_Name'] : ""; ?>)<span></h4>
                                            <p class="hpadding20" style= "height: 40px; overflow: hidden;"><?php echo isset($value['About']) ? $value['About'] : ""; ?></p>

                                            </div>
											<div class="col-md-3 center">
												<a href="<?= WEB_URL; ?>home/profile/<?= $value['User_Id']; ?>"
												   target="_blank" class="bluebtn center">View
													Profile</a>
											</div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
								<div class="line7"></div>

                            </li>
                        <?php
                        }
                    } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="line2"></div>
<div class="padding20 mymatches">
    <div class="panel panel-default">
        <div class="panel-heading">My Matches</div>
        <div class="panel-body">
            <div class='contentMymatch'>
                <div class="pagination_btn" style="position:relative; top: -53px; float: right;"><a
                        class="prev_1">&laquo;</a> <a class="next_1">&raquo;</a></div>
                <ul>
                    <?php $result1 = $this->action_model->preferred_match($id, $Gender, 'preferred_match');
                    if ($result1 == 'failure') {
                        ?>
                        <div class="alert alert-info">
                            No matches found !
                        </div>

                    <?php
                    } else {
					echo'<div class="alert alert-info">
							 '.count($result1['result']).' Preferred matches found
						</div>';
                        foreach ($result1['result'] as $value) {
                                if ($value['ProfilePic'] == "") {
                                    $ppic = "no-profile.gif";
                                } else {
									// if(file_exists(WEB_DIR . "images/profiles/" . $value['ProfilePic'])){
									// 	$ppic = $value['ProfilePic'];
									// }else{
									// 	 $ppic = "no-profile.gif";
									// }
                                    $ppic = $value['ProfilePic'];
                                }
								$pimg = WEB_DIR . "images/profiles/" . $ppic;
							
                            ?>
							
							<li class="matches">
                                <div class="padding10 pagination_container">
                                    <div class="col-md-2 text-center  offset-0">
                                        <a href="#"><img src="<?php echo $pimg; ?>" alt="" class="fwimg img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-10 searchmt offset-0" >
                                        <div class="col-md-12">
											<div class="col-md-9">
                                            <h4 class="opensans dark bold margtop1 lh1"><?= $value['Name']; ?>
                                            <span style="font-size: 10px;">(<?php $DOB = $this->action_model->birthda($value['DOB']);
										echo isset($DOB) ? $DOB." Years ," : "";?> 
										<?php $HeightValue = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$value['Height']);
										echo isset($HeightValue[0]->Height) ? $HeightValue[0]->Height."," : ""; ?>
										<?php echo isset($value['Community_Name']) ? $value['Community_Name']."," : ""; ?>
                                            
                                            <?php echo isset($value['SubCommunity_Name']) ? $value['SubCommunity_Name']."," : ""; ?>
                                            
                                            <?php echo isset($value['Language_Name']) ? $value['Language_Name'] : ""; ?>)<span></h4>
                                            <p style= "height: 40px; overflow: hidden;"><?php echo isset($value['About']) ? $value['About'] : ""; ?></p>

                                            </div>
											<div class="col-md-3 center">
												<a href="<?= WEB_URL; ?>home/profile/<?= $value['User_Id']; ?>"
                                               target="_blank" class="bluebtn">View
                                                Profile
												</a>
											</div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
							</li>
                        <?php
                        }
                    } ?>

                </ul>
            </div>
        </div>
    </div>
</div>
<div class="line2"></div>

<div class="padding20 recentVisitors">
    <div class="panel panel-default">
        <div class="panel-heading">Recent Profile Visitors</div>
        <div class="panel-body">
            <div class='contentrecentprofile'>
                <div class="pagination_btn" style="position:relative; top: -53px; float: right;"><a
                        class="prev_1">&laquo;</a> <a class="next_1">&raquo;</a></div>
                <ul>
                    <?php $result2 = $this->action_model->recentVisitors($id);
                    if ($result2 == 'failure') {
                        ?>
                        <div class="alert alert-info">
                            No recent visitors found !
                        </div>

                    <?php
                    } else {
						echo'<div class="alert alert-info">
							 '.count($result2['result']).' Recent profile visitors found
						</div>';
                        foreach ($result2['result'] as $value) {
								if ($value['ProfilePic'] == "") {
                                    $ppic = "no-profile.gif";
                                } else {
									// if(file_exists(WEB_DIR . "images/profiles/" . $value['ProfilePic'])){
									// 	$ppic = $value['ProfilePic'];
									// }else{
									// 	 $ppic = "no-profile.gif";
									// }
                                        $ppic = $value['ProfilePic'];
                                }
								$pimg = WEB_DIR . "images/profiles/" . $ppic;
						?>
						
                            <li class="matches">
                                <div class="padding10 pagination_container">
                                    <div class="col-md-2 text-center  offset-0">
                                        <a href="#"><img src="<?php echo $pimg; ?>" alt="" class="fwimg img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-10 searchmt offset-0" >
                                        <div class="col-md-12">
											<div class="col-md-9">
                                            <h4 class="opensans dark bold margtop1 lh1"><?= $value['Name']; ?>
                                            <span style="font-size: 10px;">(<?php $DOB = $this->action_model->birthda($value['DOB']);
										echo isset($DOB) ? $DOB." Years ," : "";?> 
										<?php echo isset($value['Community_Name']) ? $value['Community_Name']."," : ""; ?>
                                            <?php $HeightValue = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$value['Height']);
										echo isset($HeightValue[0]->Height) ? $HeightValue[0]->Height."," : ""; ?>
                                            <?php echo isset($value['SubCommunity_Name']) ? $value['SubCommunity_Name']."," : ""; ?>
                                            
                                            <?php echo isset($value['Language_Name']) ? $value['Language_Name'] : ""; ?>)<span></h4>
                                            <p class="hpadding20" style="height: 54px; overflow: hidden;"><?php echo isset($value['About']) ? $value['About'] : ""; ?></p>

                                            </div>
											<div class="col-md-3 center">
												<a href="<?= WEB_URL; ?>home/profile/<?= $value['User_Id']; ?>"
												   target="_blank" class="bluebtn center">View
													Profile</a>
											</div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

							<div class="line7"></div>
                            </li>
                        <?php
                        }
                    } ?>

                </ul>
            </div>

        </div>
    </div>
</div>

</div>


</div>
<?php $this->load->view("include/sidebar"); ?>
</div>
<?php $this->load->view("include/footer"); ?>

<script>
$(document).ready(function(){
	$('.contentNewmatch').each(function () {
        var foo = $(this);
        $(this).find('ul li:gt(4)').hide();
        $(this).find('.next_1').click(function () {
            var last = $('ul', foo).children('li:visible:last');
            last.nextAll(':lt(5)').show();
            last.next().prevAll().hide();
        });
        $(this).find('.prev_1').click(function () {
            var first = $('ul', foo).children('li:visible:first');
            first.prevAll(':lt(5)').show();
            first.prev().nextAll().hide();
        });
    });


    $('.contentMymatch').each(function () {
        var foo = $(this);
        $(this).find('ul li:gt(4)').hide();
        $(this).find('.next_1').click(function () {
            var last = $('ul', foo).children('li:visible:last');
            last.nextAll(':lt(5)').show();
            last.next().prevAll().hide();
        });
        $(this).find('.prev_1').click(function () {
            var first = $('ul', foo).children('li:visible:first');
            first.prevAll(':lt(5)').show();
            first.prev().nextAll().hide();
        });
    });


    $('.contentrecentprofile').each(function () {
        var foo = $(this);
        $(this).find('ul li:gt(4)').hide();
        $(this).find('.next_1').click(function () {
            var last = $('ul', foo).children('li:visible:last');
            last.nextAll(':lt(5)').show();
            last.next().prevAll().hide();
        });
        $(this).find('.prev_1').click(function () {
            var first = $('ul', foo).children('li:visible:first');
            first.prevAll(':lt(5)').show();
            first.prev().nextAll().hide();
        });
    });
});
</script>