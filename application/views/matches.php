<?php $this->load->view("include/header");
$id = $this->session->userdata('User_Id'); 
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->getpart_table_deatils("userprofile","User_Id",$id);
$PartnerDetails =  $this->action_model->full_partnerprofile($id); 

?>
<div class="container">
	<div class="container breadcrub">
	    
		<div class="brlines"></div>
	</div>	

	<!-- CONTENT -->
	<div class="container">
	<!--  Contact Us Field  -->
<!--  Contact Us Field  -->
		<div class="container pagecontainer offset-0">	
			
			<!-- SLIDER -->
			<div class="col-md-9 pagecontainer2 offset-0">
				<div class="cstyle10"></div>
		
				<ul class="nav nav-tabs" id="myTab">
					<li onclick="mySelectUpdate()" class="active match_padding col-md-3"><a href="<?php echo WEB_URL; ?>home/match"><span class="rates"></span><span class="hidetext">Preferred Matches</span>&nbsp;</a></li>
					<li onclick="mySelectUpdate()" class="match_padding col-md-3"><a href="<?php echo WEB_URL; ?>home/NewMatch"><span class="preferences"></span><span class="hidetext">New Matches</span>&nbsp;</a></li>
					<li onclick="loadScript()" class="match_padding col-md-3"><a href="<?php echo WEB_URL; ?>home/broaderMatch"><span class="maps"></span><span class="hidetext">broader Matches</span>&nbsp;</a></li>
					<li onclick="loadScript()" class="match_padding col-md-3"><a href="<?php echo WEB_URL; ?>home/maybe"><span class="maps"></span><span class="hidetext">Maybe List</span>&nbsp;</a></li>
				
				</ul>			
				<div class="tab-content4">
					<!-- TAB 1 -->	   	
					
					<div id="preferred" class="tab-pane fade active in padding20">
						<?php 
						  $result = $this->action_model->preferred_match($id, $Gender, 'preferred_match');
						  //print_r($result);exit;
						  
						 if($result == 'failure'){ ?>
							<div class="alert alert-info">
							  No matches found !
							</div>
							
						<?php }else{  
						foreach($result['result'] as $value){
							if ($value['ProfilePic'] == "") {
								$ppic = "no-profile.gif";
							} else {
								// if(file_exists(WEB_DIR . "images/profiles/" . $value['ProfilePic'])){
								// 	$ppic = $value['ProfilePic'];
								// }else{
								// 	 $ppic = "no-profile.gif";
								// }
								$ppic = $value['ProfilePic'];
							}
							$pimg = WEB_DIR . "images/profiles/" . $ppic;
						 
						 $datediff = time() - strtotime($value['DateCreated']);
						 $days =  floor($datediff/(60*60*24));
						 if($days <= 2 ){
							$date_created = "day";
						 }elseif($days <= 7){
							$date_created = "week";
						 }elseif($days <= 30){
							$date_created = "month";
						 }else{
							$date_created = "all";
						 }
						 if($value['MaritalStatus'] == "Never married"){$value['MaritalStatus'] ="Never_married";}
						?>
						<div class="padding10 match_pagesview <?=$value['MaritalStatus'];?> <?=$date_created; ?>" id="result_<?=$value['User_Id'];?>">
							<div class="col-md-2 text-center  offset-0">
								<a href="#"><img src="<?php echo $pimg; ?>" alt="" class="fwimg img-thumbnail"></a>
							</div>
							<div class="col-md-10 offset-0">
								<div class="col-md-12">
									<div class="col-md-9">
										<h4 class="opensans dark bold margtop1 lh1"><?=$value['Name'];?></h4>
<?php $DOB = $this->action_model->birthda($value['DOB']);
echo isset($DOB) ? $DOB." Years ," : "";?> 
<?php echo isset($value['Community_Name']) ? $value['Community_Name']."," : ""; ?>
<?php $HeightValue = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$value['Height']);
echo isset($HeightValue[0]->Height) ? $HeightValue[0]->Height."," : ""; ?>
<?php echo isset($value['SubCommunity_Name']) ? $value['SubCommunity_Name']."," : ""; ?>
<?php echo isset($value['Language_Name']) ? $value['Language_Name'] : ""; ?>

<?php if($value['About'] !=""){?>
<p class="hpadding20" style="height: 48px;overflow: hidden;"><?=$value['About'];?></p>
<a href="<?= WEB_URL; ?>home/profile/<?= $value['User_Id']; ?>"
target="_blank" style="color:#428BCA;font-weight:bold;">Read More</a>
										<?php } ?>
									</div>
									
									<div class="col-md-3 connection_div text-center">
										<p class="text-center"> Connect with her?</p>
										<a href="<?=WEB_URL;?>home/profile/<?=$value['User_Id'];?>" target="_blank" class="mb-10 btn btn-block btn-success connect_user">Yes</a>
										<a href="<?=WEB_URL;?>home/profile/<?=$value['User_Id'];?>" target="_blank" class="btn btn-danger connect_user_no">No</a>
										<a href="<?=WEB_URL;?>home/profile/<?=$value['User_Id'];?>" target="_blank" class="btn btn-info connect_user_maybe"> Maybe</a>
										
									</div>									
								</div>									
							</div>
							<div class="clearfix"></div>
						</div>
						
						<?php }
						} ?>
					</div>
					<!-- TAB 2 -->
					
					
					<!-- TAB 6 -->						
				</div>
			</div>
			<!-- END OF SLIDER -->			
			<div class="col-md-3" style="margin-top: -20px;">
				<div class="pagecontainer2 mt20 needassistancebox" style="border:none;">
					<div class="list-group">
						<a href="#" class="list-group-item active" style="text-align:center;"> Refine Search</a>
						
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo" style="font-weight:bold;">Photo Settings
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="All"> All </p>
								<p><input type="checkbox" name="All"> Visible to all</p>
								<p><input type="checkbox" name="All"> Protected photos </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo1" style="font-weight:bold;">Recently joined
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo1" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Within a day </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Within a week </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Within a month </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo2" style="font-weight:bold;">Recently Viewed
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo2" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="Unviewed"> Unviewed Matches</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="Viewed" class="FilterCheck"> Viewed Matches </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo3" style="font-weight:bold;">Marital Status
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo3" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All </p>
								<p><input type="checkbox" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="Never_married"> Never Married </p>
								<p><input type="checkbox" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value='Divorced'> Divorced </p>
								<p><input type="checkbox" name="refined_options[]" onclick="javascript:MatchFilter()" value='Widowed'> Widowed </p>
								<p><input type="checkbox" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value='Awaiting_Divorced'>Awaiting Divorced </p>
								<p><input type="checkbox" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value='Annulled'>Annulled</p>
							  </div>
						</div>
						
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo4" style="font-weight:bold;">Active Members
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo4" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Within a day </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Within a week </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Within a month </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo5" style="font-weight:bold;">Annual Income
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo5" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Don,t want to Specify </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Up to 1 Lakh </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> 1 Lakh to 2 Lakh </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> 2 Lakh to 4 Lakh </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> 4 Lakh to 8 Lakh </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo6" style="font-weight:bold;">Religion
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo6" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Muslim </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo7" style="font-weight:bold;">Community
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo7" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Sunni </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Sunni Hanafi </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Bengali </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Shia </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Shafi </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo8" style="font-weight:bold;">Mother Tongue
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo8" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Urdu </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Hindi </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Bengali </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Malayalam </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo9" style="font-weight:bold;">Country Living In
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo9" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> India </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Pakistan </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> UAE </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Saudi Arabia </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> USA </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo10" style="font-weight:bold;">Country Grew up In
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo10" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> India </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Pakistan </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> UAE </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Saudi Arabia </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> USA </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo11" style="font-weight:bold;">Education
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo11" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Bachelors - Commerce </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Bachelors - Arts </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Bachelors - Science </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> UnderGraduate </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo12" style="font-weight:bold;">Working With
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo12" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Private Company </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Government/Public Sector </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Defence/Civil Services </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Business/Self Employed </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo13" style="font-weight:bold;">Profession Area
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo13" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Non- Working </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Not Specified </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Accounting Banking & Finance </p>
								<p><a href="#"> more + </a></p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo14" style="font-weight:bold;">Profile Created By
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo14" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Sibling </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="week" class="FilterCheck"> Parents/Gaurdian </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Self </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Other </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="month" class="FilterCheck"> Friend </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo15" style="font-weight:bold;">Smoking
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo15" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Non-Smoking </p>
							</div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo16" style="font-weight:bold;">Drinking
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo16" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Never Drinks </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Drinks Occasional </p>
							</div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo17" style="font-weight:bold;">Eating Habits
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo17" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="refined_options[]" class="FilterCheck"  onclick="javascript:MatchFilter()" value="all"> All</p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Non- Veg </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Veg </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Eggetarian </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Vegan </p>
								<p><input type="radio" name="refined_options[]" onclick="javascript:MatchFilter()" value="day" class="FilterCheck"> Jain </p>
							</div>
						</div>
				</div>
			
				</div>
			</div>
			
		</div>
		<!-- END OF container-->
		
	</div>


<?php $this->load->view("include/footer"); ?>
<script>
function MatchFilter(){
	var res = [];
	$(".FilterCheck:checked").each(function(){	
	   var res1 = $(this).val();														 
	   res.push(res1);					 									  
	});
	
	var result = res.join('.');

	$(".padding20").fadeOut().filter(function() {
		$(".padding20").filter("."+result).fadeIn("slow");
	});
}

</script>