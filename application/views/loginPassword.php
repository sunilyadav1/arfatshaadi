<?php session_start();
$this->load->view("include/header"); 
$id = $this->session->userdata('User_Id'); 
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->full_profile($id);
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class="active"><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="#"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="#"  class="tehnical">Photographs</a></li>
									<li class=""><a href="#"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="#" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="#"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="#"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="#"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="#"  class="tehnical">Milanrishta.com Alerts</a></li>
									<li class=""><a href="#"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="#"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Login & Password</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>How do I Login and Logout from Milanrishta.com?  
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
										The Login and Logout options are found at the top right corner of the Milanrishta.com website.
										<div>&nbsp;</div>Click here to login to Milanrishta.com.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>Can I be permanently logged into my account to avoid logging in everytime I visit A Milanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Yes, you can be logged into your account permanently by selecting the “Stay Signed in” option next to the “Sign in” button.
										<div>&nbsp;</div>NOTE: We recommend that you do NOT use the ‘Stay Signed in’ feature if you are signing in from a shared computer.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>Can other Members of Milanrishta.com come to know when I am logged into Milanrishta.com?
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Yes. The  Who is Online feature lists Members who are currently online. This feature helps online Members to get noticed
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>I forgot my password, what should I do? How can I login to theMilanrishta.com Matrimonial Service? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											If you have forgotten your password, we can send it to you via email.  Click here and enter your email; you will receive your Login and Password credentials immediately.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>How can I change my password?
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Click here to visit the ‘My Settings’ page and change your password. The ‘My Settings’ page can be accessed by clicking on your display name on the top right, after you login.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>How can I change my contact information? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Click here to edit your contact information.
									  </div>
									</div>
								  </div>
								  <div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



