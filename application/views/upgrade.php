<?php session_start();
$this->load->view("include/header");
$id = $this->session->userdata('User_Id');
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->full_profile($id);
?>

<style>
.TableHead{
	background-color: #A7CAD6;
    height: 40px;
	text-align:center;
	}
.select_bullet{margin-left:40px;}
</style>
<div class="container">
	<ol class="breadcrumb">
		<li><a href="<?php echo WEB_URL;?>home">Home</a></li>
		<li class="active">Self Service Plans</li>
	</ol>
	<table class="table-bordered table-hover col-sm-12">
		<thead>
			<tr class="TableHead">
				<th>Membership Plan</th>
				<th>1 Week</th>
				<th>1 Month</th>
				<th>3 Months</th>
				<th>6 Months</th>
				<th>12 Months</th>
			</tr>
		</thead>
		<tbody>
		<form action="<?php echo WEB_URL;?>home/PaymentOption" method="POST">
			<tr>
				<td class="selfservice_dotted_bdr" valign="top">
				<div class="premium_container">
					Premium Benefits</h4>

					<ul class="select_bullet">
					<li><span class="select_bullet_text"><span id="contactmatchplus" bt-xtitle="" title="" class=""><a href="javascript: void(0);" class="gray">Connect directly with Matches </a>&nbsp;</span></span></li><li><span class="select_bullet_text"><span id="privacysettingplus" bt-xtitle="" title="">
					<a href="javascript: void(0);" class="gray">Identify more Relevant Matches </a>&nbsp;</span></span></li><li><span class="select_bullet_text"><span id="spotlightplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Be featured under Spotlight </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="boldlistingplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Standout with Bold Listing  </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="boldlistingplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Unlimited Chatings  </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="boldlistingplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Unlimited Emails  </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="boldlistingplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Unlimited Contact Views   </a>&nbsp;</span></span></li>
					</ul>
				</div>
				</td>
				<td class="">
			
					<input type="radio" name="productcode" id="ssp_bplus" value="1">

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs.">450/-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/EBD48B1423B08CC36E1B381B79F8A14A"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/112.png" width="140px"/></a></div>																						

				</td>
				<td class="">
			
					<input type="radio" name="productcode" id="ssp_splus" value="2">

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 1,500/-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/61656E80EDBC6735E5FE9B494F0CB8D1"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/112.png" width="140px"/></a></div>																						

				</td>
				<td class="">
			
					<input type="radio" name="productcode" id="ssp_gplus" value="3">

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 2,500/-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/71DC69A9275E8D19F85F3082120AA4FF"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/112.png" width="140px"/></a></div>																						

				</td>
				<td class="">
				
					<input type="radio" name="productcode" id="ssp_dplus" value="4">

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 3,800/-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/2B84D256BF02F0E9DB60DA0F4F571AAC"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/112.png" width="140px"/></a></div>																						

				</td>
				<td class="">
				<input type="radio" name="productcode" id="ssp_pplus" value="5">

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 5,200</span>
				<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/47959"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/112.png" width="140px"/></a></div>																						


				</td>
								
				</tr>
				<tr>
				<td class="selfservice_dotted_bdr" valign="top">
				<div class="premium_container">
					Personalized Matchmaking Service 

					<ul class="select_bullet">
					<li><span class="select_bullet_text"><span id="contactmatchplus" bt-xtitle="" title="" class=""><a href="javascript: void(0);" class="gray">Handpicked Matches </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="privacysettingplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Sending profiles by Mails </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="spotlightplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Quicker Responses from Prospects </a>&nbsp;</span></span></li>
					<li><span class="select_bullet_text"><span id="boldlistingplus" bt-xtitle="" title=""><a href="javascript: void(0);" class="gray">Personal Introductions & Meetings  </a>&nbsp;</span></span></li>
					</ul>
				</div>
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="">
			
					<input type="radio" name="productcode" id="SSP_G" value="6">

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 6000 /-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/C20E5C8CD5630DA68BCC61608B32DCE0"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/111.png" /></a></div>
				</td>
				<td class="">
				

						<input type="radio" name="productcode" checked="checked" id="ssp_d" value="7"> 

				
					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 10,000/-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/2B702A59CF2A6C2802B7D6A0DD88CF89"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/111.png" /></a></div>
				</td>
				<td class="">
				
					<input type="radio" name="productcode" id="SSP_P" value="8"> 

					<span class="price"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> 20,000/-</span>
					<div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/3F8056FA8BE6809F279FE4FC576BE77A"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/111.png" /></a></div>
				</td>
								
				</tr>
				
		</tbody>
	</table>
	<!--<input type="submit" value="Upgrade Now" class="btn btn-success" style="float:right;margin-top:20px;">-->
	</form>
</div>


<?php $this->load->view("include/footer"); ?>