<?php $this->load->helper('html');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login with Facebook</title>
<?php  $base_url=$this->config->item('base_url'); ?>
<script src="<?php echo WEB_url;?>js/jquery.min.js" type="text/javascript"></script>
</head>

<body>
<?php 
$ses_user= $this->session->all_userdata();

if(empty($ses_user['User_Id']))   { 
echo img(array('src'=>$base_url.'images/facebook.png','id'=>'facebook','style'=>'cursor:pointer;float:left;margin-left:550px;'));

 }  else{
	
 echo '<img src="https://graph.facebook.com/'. $ses_user['UID'] .'/picture" width="30" height="30"/><div>'.$ses_user['Name'].'</div>';	
	echo "<fb:login-button autologoutlink='true' perms='email,publish_stream' ></fb:login-button>";
	
}
	?>

<div id="fb-root"></div>

   <script type="text/javascript">
window.fbAsyncInit = function() {
	FB.init({appId: '558623904280461', status: true, cookie: true, xfbml: true, version    : 'v2.2'});
	
	FB.Event.subscribe('auth.login', function(response) {
		login();
	}); 
	FB.Event.subscribe('auth.logout', function(response) {
		$.ajax({
		  url: "http://localhost/facebook/index.php/fbci/logout",
			data:response,
			type:"post",
			beforeSend:function(){},
			success:function(data){
				parent.location.reload();
			}
		 });
	 
	});

	FB.getLoginStatus(function(response) {
		if (response.session) {
			greet();
		}
	});
};
	$(function() {
			var e = document.createElement('script');
			e.type = 'text/javascript';
			e.src = document.location.protocol +
				'//connect.facebook.net/en_US/all.js';
			e.async = true; 
			document.getElementById('fb-root').appendChild(e);
		});
		 
	//Onclick for fb login
 $('#facebook').click(function(e) {
    FB.login(function(response) {
	  if(response.authResponse) {
		  if (response.status === 'connected') {
			FB.api('/me', function(response) {
				var json = JSON.stringify(response);
				$.ajax({
				  url: "http://localhost/facebook/index.php/fbci/fblogin",
					data:response,
					type:"post",
					beforeSend:function(){},
					success:function(data){
						parent.location.reload();
					}
				 });
			});
		  }
	  }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
});
   </script>
</body>
</html>