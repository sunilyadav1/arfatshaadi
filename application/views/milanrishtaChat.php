<?php
session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class="active"><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaChat"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="#"  class="tehnical">AMilanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Milanrishta Chat</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>Who can I see online on my Shaadi Chat list?  
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
										On the Milanrishta Chat list you can see Accepted Members, Shortlists and Matches who are online. Using Milanrishta Chat is completely FREE. 
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>What additional benefits do I get as a Premium Member?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										As a Premium Member, you can initiate Chats and highlight your Profile in order to get faster responses.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>Can I search for Members who are currently Online onMilanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Yes, you can search for Online Members by using the Who is Online search.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>If I am already chatting with someone and do not wish to be disturbed by getting more Chats what can I do?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											You can appear offline to all Members while continuing to chat with Members you prefer. You can do this by changing your Chat Status from “I am Online” to "Invisible".
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>I do not wish to receive Chat requests when I am browsing onMilanrishta.com what do I do?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											You can go offline from Milanrishta Chat by changing your Chat status from "I am Online" to "Offline". Please note that this will decrease your chances of being contacted, as Members will not see you online on their Milanrishta Chat list.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>The form seems to be a bit lengthy. Do I need to fill it entirely? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											We understand that it may become tedious for you to fill in a long form in one go. However, matrimonial decision are important decisions and hence one should convey detailed information about oneself to Interested Members. The more information you provide about yourself the more likely you are to be contacted by other Members of Milanrishta.com. So please do take the time and effort to complete your Profile.<div>&nbsp;</div>
											Register Now with Milanrishta.com and create your Profile.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										   <strong>7. </strong>How can I stop chatting with a particular Member?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSeven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											You can “Ignore” or “Decline” a particular Member if you do not wish to receive any Chats from them. These Members will also not be able to see you online on Milanrishta Chat.
									  </div>
									</div>
								  </div>
								 <div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading panel_heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



