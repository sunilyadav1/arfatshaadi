<?php $this->load->view("include/header"); ?>
<?php if(isset($data) == "failure"){ ?>
	<div class="alert alert-danger" role="alert">No preferred matched.. Please fill your Partner preference..</div>
<?php } 
$id = $this->session->userdata('User_Id');

$profile = $this->action_model->getpart_table_deatils('partnerprofile','User_Id',$id);
if($profile==''){
$profile_id = 0;
$title="Add partner preferences";

}
else{
$profile_id=$profile[0]->User_Id;
$title="Update partner preferences";
}?>
<div class="box-header well row">
	<div class="col-md-1"></div>
	<h4 class="col-md-10"><i class="glyphicon glyphicon-user"></i> <?=$title;?></h4>
	<div class="col-md-1"></div>
	<div class="box-icon">

		
	</div>

</div>
<div class="col-md-2"></div>
<div  class="form col-md-8 partner_profile" id="sign_up_form" style="display:block;">
<form id="contactform" action="<?php echo WEB_URL; ?>save_partner_profile/<?=$profile_id;?>"  method="post" class="form-horizontal" role="form"> 
			
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">User Name</label>
    <div class="col-sm-8">
     <input id="user_id" name="user_id" placeholder="First and last name" required="" tabindex="1" type="text" value="<?php echo $id;?>" readonly class="form-control"> 
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4" for="email"> Age From</label>
    <div class="col-sm-8">
     <select name="ageFrom" class="form-control">
		<?php for($i=18;$i<62;$i++){ ?>
			<option value="<?=$i;?>"><?=$i;?></option>
		<?php } ?>
	</select>
    </div>
  </div>  	
 <div class="form-group">
    <label class="control-label col-sm-4" for="email"> Age To</label>
    <div class="col-sm-8">
		<select name="ageTo" class="form-control">
		<?php for($j=18;$j<62;$j++){ ?>
			<option value="<?=$i;?>"><?=$i;?></option>
		<?php } ?>
		</select>
    </div>
  </div>  	  
  <div class="form-group">
    <label class="control-label col-sm-4" for="email"> Height From</label>
    <div class="col-sm-8">
		<select name="heightFrom" class="form-control">
		<option value="" selected class="form-control">select height</option>
		<?php for($l=134;$l<190;$l+=4){ ?>
			<option value="<?=$l;?>"><?=$l;?> cm</option>
		<?php } ?>
		</select>
    </div>
  </div>  				
 <div class="form-group">
    <label class="control-label col-sm-4" for="email"> Height To</label>
    <div class="col-sm-8">
		<select name="heightTo" class="form-control">
		<?php for($l=134;$l<190;$l+=4){ ?>
			<option value="<?=$l;?>"><?=$l;?> cm</option>
		<?php } ?>
		
		</select>
    </div>
  </div>  	 			
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Marital Status</label>
    <div class="col-sm-8">
		<select class="form-control" name="marital_status">				
			<option value="">Doesn't Matter</option>
			<option value="Never married">Never married</option>
			<option value="Divorced">Divorced</option>
			<option value="Widowed">Widowed</option>
			<option value="Awaiting_Divorced">Awaiting Divorced</option>
			<option value="Annulled">Annulled</option>  						  						
		</select>
    </div>
  </div> 		
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">select partner Religion</label>
	<?php $religion = $this->action_model->get_table_details('religionlist'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="religion">
			<option value="">Doesn't Matter</option>
			<?php foreach($religion as $value){?>
			<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
			<?php } ?> 
		</select>
    </div>
  </div> 			
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">select partner Community</label>
	<?php  $com = $this->action_model->get_table_details('communitylist'); ?>
    <div class="col-sm-8">
			<select class="form-control" name="community">
				<option value="">Doesn't Matter</option>
				<?php 
						foreach($com as $values){?>
						<option value="<?php echo $values->Community_Id; ?>"><?php echo $values->Community_Name;?></option>    
						<?php } ?>
			
			</select>
    </div>
  </div> 				
 <div class="form-group">
    <label class="control-label col-sm-4" for="email">Select Partner profile created by</label>
    <div class="col-sm-8">
		<select class="form-control" name="profile_created_by">
				<option value="">Doesn't Matter</option>
				<option value="1">self</option>
				<option value="2">son </option>
				<option value="3">Daughter</option>  
			</select>
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-4" for="email">Country Grew up</label>
	<?php   $country = $this->action_model->get_table_details('countrylist'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="countryGrewUp">
			<option value="">Doesn't Matter</option>
			<?php 
			foreach($country as $country){?>
			<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
			<?php } ?>
		
		</select>
    </div>
  </div>
	<div class="form-group">
    <label class="control-label col-sm-4" for="email">Mother Tongue</label>
	<?php    $language = $this->action_model->get_table_details('languagelist'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="mother_tongue" >
			<option value="">Doesn't Matter</option>
			<?php 
			foreach($language as $value){?>
			<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
			<?php } ?>
   
		</select>
    </div>
  </div>
	<div class="form-group">
    <label class="control-label col-sm-4" for="email">Living Country</label>
	<?php     $country = $this->action_model->get_table_details('countrylist'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="living_in" id="living_in">
			<option value="">Doesn't Matter</option>
			<?php 
			foreach($country as $country){?>
			<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
			<?php } ?>
         </select>
    </div>
  </div>		
<div class="form-group" id="StateList" style="display:none;">
    <label class="control-label col-sm-4" for="email">Living State</label>
    <div class="col-sm-8">
		<select class="form-control" name="LivingState" id="LivingState">
			
		
		</select>
    </div>
</div>		
<div class="form-group" id="CityList" style="display:none;">
    <label class="control-label col-sm-4" for="email">Living City</label>
	<?php     $city = $this->action_model->get_table_details('citylist'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="LivingCity" id="LivingCity">
		
						  
		</select>
    </div>
  </div>	
<div class="form-group">
    <label class="control-label col-sm-4" for="residencyStatus">Residency Status</label>
    <div class="col-sm-8">
		<select class="form-control" name="residencyStatus">
			<option value="">Doesn't Matter</option>
			<option value="Citizen / Permanent Resident">Citizen / Permanent Resident</option>
			<option value="Work / Student Visa">Work / Student Visa</option>
						  
		</select>
    </div>
 </div>				
<div class="form-group">
    <label class="control-label col-sm-4" for="residencyStatus">Education level</label>
	<?php  $degree = $this->action_model->get_table_details('degree'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="education">
			<option value="">Doesn't Matter</option>
			<?php 
			foreach($degree as $value){?>
			<option value="<?php echo $value->Degree_Id; ?>"><?php echo $value->Degree_Name;?></option>    
			<?php } ?>	  
		</select>
    </div>
 </div>				
<div class="form-group">
    <label class="control-label col-sm-4" for="residencyStatus">working with</label>
	
    <div class="col-sm-8">
		<select class="form-control" name="workinWith">
			<option value="">Doesn't Matter</option>
							<option value="1">not working</option>
							<option value="2">Government</option>
							<option value="3">Private</option>
		</select>
    </div>
 </div>					
<div class="form-group">
    <label class="control-label col-sm-4" for="residencyStatus">Education Fields</label>
	<?php $spec = $this->action_model->get_table_details('specialization'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="education_field">
			<option value="">Doesn't Matter</option>
			<?php 
			foreach($spec as $value){?>
			<option value="<?php echo $value->S_Id; ?>"><?php echo $value->Course_Name;?></option>    
			<?php } ?>
		</select>
    </div>
 </div>	          
<div class="form-group">
    <label class="control-label col-sm-4" for="residencyStatus">Work Area</label>
	<?php  //$spec = $this->action_model->get_table_details('workingas'); ?>
    <div class="col-sm-8">
		<select class="form-control" name="professionArea" id="edu_field">
			<option value="">Doesn't Matter</option>
			<?php 
			//foreach($spec as $value){?>
			<option value="<?php echo $value->Work_Id; ?>"><?php echo $value->Work_Name;?></option>    
			<?php //} ?>
		</select>
    </div>
 </div>				
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">Annual Income</label>
	<div class="col-sm-8">
	   
		<select class="form-control" name="annualIncome">
			<option value="">Doesn't Matter</option>
			<option value="1- 2 laks">1- 2 laks</option>
			<option value="3-4 laks">3-4 laks</option>
			<option value="5-6 laks">5-6 laks</option>
	   </select>
	</div>
</div>
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">Select Disablities</label>
	<div class="col-sm-8"> 
		<select class="form-control" name="disability">
			<option value="">Doesn't Matter</option>>
			<option value="2">No</option>    
			<option value="1">yes</option> 	
	   </select>
	</div>
</div>
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">Diet</label>
	<div class="col-sm-8"> 
		<input type="radio" name="diet" value="veg">veg
		<input type="radio" name="diet" value="Non veg">Non veg
		<input type="radio" name="diet" value="Occasionally Non-Veg">Occasionally Non-Veg
		<input type="radio" name="diet" value="Eggetarian">Eggetarian
		<input type="radio" name="diet" value="Jain">Jain

	</div>
</div>
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">Body Type</label>
	<div class="col-sm-8"> 
		<input type="radio" name="body_type" value="slim">slim
		<input type="radio" name="body_type" value="average">average
		<input type="radio" name="body_type" value="Athletic">Athletic
		<input type="radio" name="body_type" value="heavy">heavy
	</div>
</div>		
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">Smoke</label>
	<div class="col-sm-8"> 
		<input type="radio" name="smoke" value="1">Yes
		<input type="radio" name="smoke" value="2">No
		<input type="radio" name="smoke" value="">Doesn't Matter
	</div>
</div>				
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">Drinks</label>
	<div class="col-sm-8">
		<input type="radio" name="drink" value="1">Yes
		<input type="radio" name="drink" value="2">No
		<input type="radio" name="drink" value="">Doesn't Matter
	</div>
</div>				
<div class="form-group">					
	<label class="control-label col-sm-4" for="residencyStatus">select Dosham</label>
	<div class="col-sm-8"> 
		<input type="radio" name="Dosham" value="1">Yes
		<input type="radio" name="Dosham" value="2" checked>No
	</div>
</div>			
<div class='form-group'>
	<div class='col-sm-6 col-sm-offset-3'>
		<input type='submit' class='btn save_btn' value='save' id="submit">
	</div>
</div>			
</form> 
</div>
<div class="col-md-2"></div>
<script>
$(document).ready(function() {
	$("#living_in").change(function(){
		$("#StateList").css("display","none");
		$("#CityList").css("display","nnone");
		 var living_in = $("#living_in").val();
		 if(living_in != ""){
			$.ajax({
				url: "<?php echo WEB_URL; ?>HOME/GetStates",
				data: {living_in : living_in},
				type: "POST",
				success: function(data) {
					$("#StateList").css("display","block");
					$("#LivingState").html(data);
				}
			  });
		 }
	});
	$(document).on("click",'#LivingState',function() { 
		 var living_in = $("#LivingState").val();
		 if(living_in != ""){
			$.ajax({
				url: "<?php echo WEB_URL; ?>HOME/GetCities",
				data: {living_in : living_in},
				type: "POST",
				success: function(data) {
					$("#CityList").css("display","block");
					$("#LivingCity").html(data);
				}
			  });
		 }
	});
});
</script>
<?php $this->load->view("include/footer"); ?>
