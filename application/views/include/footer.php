<!-- FOOTER -->
</div>
<div>&nbsp;</div><div>&nbsp;</div>
		<div class="footerbgblack">
			<div class="container">		
				
				<div class="col-md-3">
					<span class="ftitle">Let's socialize</span>
					<div class="scont">
						<a href="http://www.facebook.com" class="social1b"><img src="<?php echo WEB_DIR; ?>images/icon-facebook.png" alt=""/></a>
						<a href="http://www.twitter.com" class="social2b"><img src="<?php echo WEB_DIR; ?>images/icon-twitter.png" alt=""/></a>
						<a href="http://plus.google.com" class="social3b"><img src="<?php echo WEB_DIR; ?>images/icon-gplus.png" alt=""/></a>
						<a href="http://www.youtube.com" class="social4b"><img src="<?php echo WEB_DIR; ?>images/icon-youtube.png" alt=""/></a>
						<br/><br/>
						<img src="<?php echo WEB_DIR; ?>images/footer-logo.jpg" alt="" style="width: 200px;margin-bottom: 5px;"/><br/>
						&copy; 2013  |  All Rights Reserved 
						<br/><br/>
						
					</div>
				</div>
				<!-- End of column 1-->
				
				<div class="col-md-3">
					<span class="ftitle">Need Help?</span>
					<br/><br/>
					<ul class="footerlist">
						<li><a href="#">Member Login</a></li>
						<li><a href="/search">Partner Search</a></li>
						<li><a href="#">How to Use MilanRishta.com</a></li>
						<li><a href="#">Premium Memberships</a></li>
						<li><a href="#">Customer Support</a></li>
						<li><a href="<?php echo WEB_URL."home/Contact"; ?>">Contact Us</a></li>
						<li><a href="#">Success Stories</a></li>
					</ul>
				</div>
				<!-- End of column 2-->		
				
				<div class="col-md-3">
					<span class="ftitle">Wedding Specialists</span>
					<br/><br/>
					<ul class="footerlist">
						<li><a href="#">Weddings</a></li>
						<li><a href="#">Mobile</a></li>
						<li><a href="#">WeddingMatch Centres</a></li>
						<li><a href="#">WeddingMatch Labs</a></li>
						<li><a href="#">Wedding Planning</a></li>
						<li><a href="#">Select WeddingMatch</a></li>
					</ul>				
				</div>
				<!-- End of column 3-->		
				
				<div class="col-md-3 grey">
					<span class="ftitle">Newsletter</span>
					<div class="relative">
						<input type="email" class="form-control fccustom2" id="exampleInputEmail1" placeholder="Enter email">
						<button type="submit" class="btn btn-default btncustom">Submit<img src="<?php echo WEB_DIR; ?>images/arrow.png" alt=""/></button>
					</div>
					<br/><br/>
					<span class="ftitle">Customer support</span><br/>
					<span class="pnr">+91-7259872851<br/>+91-9342627372</span><br/>
					<span class="grey2"> info@milanrishta.com</span>
				</div>			
				<!-- End of column 4-->			
			
			

				
				
			</div>	
		</div>
		
		<div class="footerbg3black">
			<div class="container center grey"> 
			<a href="#">Blog</a> | 
			<a href="<?php echo WEB_URL."home/about"; ?>">About Us</a> | 
			<a href="<?php echo WEB_URL."home/terms"; ?>">Terms of Use</a> | 
			<a href="<?php echo WEB_URL."home/faq"; ?>">Faq</a> | 
			<a href="#">Privacy Policy</a> | 
			<a href="#">Security Tips</a> | 
			<a href="#">Report Misuse</a>
			</div>
		</div>
		
		

		
		
	</div>
	</div>
	</div>
	<!-- / WRAP -->
	 <?php if ($this->session->userdata('user_logged_in')) { ?>
	<div class="maincontentdiv" style="height: 25px; bottom: 0px;">
	<div class="chatfriendheader">
		<span class="dew">friends(<?php if(isset($onlinefriends) && count($onlinefriends) > 1){ echo count($onlinefriends);}else{echo "0";}?>)</span>
		<em class="ShchatBarScreenMinimize" style="  margin-left: 10px;"></em>
	</div>
	<div class="ShchatBarBoxToolbar1 clearfix">
		<span class="ShchatBarBoxToolbarSettings"><em class="showChatSettings SettingIconClicked"></em>
			<div class="ShchatBarBoxToolbarSettingsOption">
				<ul>
					<li class="status_change" id="<?php echo isset($user_data) ? $user_data['User_Id'] : ""; ?>" datastr="1">make online</li>
					<li class="status_change" id="<?php echo isset($user_data) ? $user_data['User_Id'] : ""; ?>" datastr="0">make offline</li>
				</ul>
			</div>
		</span>
	</div>	
	<div class="friendslist onlinefriendslist">
		<ul data="<?php echo WEB_URL; ?>" name="<?php echo strtotime(date("Y-m-d H:i:s")); ?>" datastr="<?php echo isset($user_data) ? $user_data['User_Id'] : ""; ?>">		
		<?php 
		if(isset($onlinefriends) && count($onlinefriends) > 1){
				foreach($onlinefriends as $ol){ 
				if ($ol->ProfilePic == "") {
					$ppic = "no-profile.gif";
				} else {
					if(file_exists(WEB_DIR . "images/profiles/" . $ol->ProfilePic)){
						$ppic = $ol->ProfilePic;
					}else{
						 $ppic = "no-profile.gif";
					}
				}
				$img = WEB_DIR . "images/profiles/" . $ppic;
				?>
			<li class="clearfix switchFriendsChatWindow" rel="<?php echo $ol->Email; ?>" name="<?php //echo $ol->thread_id; ?>" id="olFrnList-<?php echo $ol->User_Id; ?>" datastr="<?php echo $ol->Name; ?>" datacheck="currentOnlineStatus <?php echo $ol->status_online;?>">
				<em class="userpic">
					<img src="<?php echo $img; ?>" style="width:50px;height:50px;">
				</em>
				<span id="ChatUserName"><?php echo $ol->Name; ?>
				(<span><?php if(isset($ol->Gender) && $ol->Gender != ""){
				if($ol->Gender == 2){echo "Female";}else{echo "Male";}
				} ?></span>)<br/>
				<span style="font-size: 11px;color: #807F7F;"><?php 
				if(isset($ol->Country_Name) && $ol->Country_Name != ""){
				echo $ol->Country_Name;
				} 
				if(isset($ol->Community_Name) && $ol->Community_Name != ""){
				echo ",".$ol->Community_Name;
				} 
				?>
				</span>
				</span>
				<span class="right"><em class="currentOnlineStatus <?php echo $ol->status_online;?>"></em></span>
			</li>
		<?php }	 ?>
		</ul>
		<audio controls="controls" style="display:none;" id="soundHandle"></audio>  <!--this tag is for chat sound	-->
	</div>
	<div id="FrmXSet34234" class="FrmXSet34234"></div>
	<span id="sControlSpan" style="display: none;" target="0" name="<?php echo isset($user_data) ? $user_data['User_Id'] : ""; ?>">
		<li class="clearfix switchFriendsChatWindow" rel="{email}" name="{thread}" id="olFrnList-{reciver}" datastr="{name}" datacheck="currentOnlineStatus {class}">
				 <em class="userpic">
					<img src="{profile_pic}"  style="width:50px;height:50px;">
				</em> 
				<span id="ChatUserName">{name}</span>
				<span class="right"><em class="currentOnlineStatus {class}"></em></span>
			</li>
	</span>
	<div class="PipeLoaderIm" style="display: none;" >
	<?php //$pim = file_get_contents(WEB_DIR."images/profiles/".$this->db->get_where('userprofile',array('User_Id'=>$user_data['User_Id']))->row()->ProfilePic);
			//echo $pdata = base64_encode($pim);
	?>
	</div>
	<script id="initiator"> var scriptinitiator = []; </script>
</div>
<?php }	 ?>
<!----chat div---->

<div id="tmpMsg" style="display:none">
<div id="tmpMessage" style="height:200px">
<form method="post" id="frmMsg" action="<?php echo WEB_URL; ?>ajaxhandler/send_msg">
<textarea id="txtusermsg" name="txtusermsg" ></textarea>
<input name="data" type="hidden" value="<?php echo isset($uid['User_Id']) ? $uid['User_Id'] : "".'_'.$this->session->userdata("mymodid"); ?>">
<input type="submit" value="Send" name="btnsendmsg" id="btnsendmsg"/>	
</form>
</div>
</div>
	<?php } ?>
	
	
   
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo WEB_DIR; ?>dist/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script src="<?php echo WEB_DIR; ?>js/shrchatrsrkv.js"></script>

<script>
// online user check
setInterval(function() {
	$.ajax({
		url : "<?php echo WEB_URL; ?>home/UpdateTimeStamp",
		data : {},
		type : "post",
		success : function(data){
		}
	});
}, 1000 * 60 * 10); 

//Tab activation script
$("document").ready(function(){
	var url = window.location.href;
	var index = url.lastIndexOf("#")+1;
	var filename = url.substr(index);
	var result = filename.split("/");

	if(result.length == 1){
		$('.active').removeClass('active in');
		$('#'+filename).addClass('active in');
		$('.'+filename).addClass('active');
	}else{
		$('#index').addClass('active in');
		$('.index').addClass('active');
	} 
});
 
jQuery(document).ready(function($) {
	$(document).on('click','.chatfriendheader em.ShchatBarScreenMinimize',function(){
		$(this).parents('.chatfriendheader').siblings(".onlinefriendslist").toggle();
		if ($(this).parents('.chatfriendheader').siblings(".onlinefriendslist").css('display') == 'none') {
			$(this).parents('div.maincontentdiv').css({'height':'25px','bottom':0,'top':''});
			$(".navbar-wrapper2").css('margin', '');
			$(".pagecontainer2").css("width","");
			$(".pagecontainer ").css("margin","");
		}
		else
		{
			$(this).parents('div.maincontentdiv').css({'height':'100%','top':0,'bottom':''});
			$(".navbar-wrapper2").css('margin', '0px auto 0px -120px');
			$(".pagecontainer2").css("width","680px");
			$(".pagecontainer ").css("margin","0px auto 0px -70px");
		}
		
	});

	$("form#frmMsg").on('submit',function(){
		var that = $(this),
		contents= that.serialize();
		$.ajax({
			url: "<?php echo WEB_URL; ?>ajaxhandler/send_msg",
			cache: false,
			type: "POST",
			data: contents,
			dataType: "json",
			success: function(data){
				if(data.status=="200")
				{
					$("form#frmMsg").append(data.msg);
					$("#txtusermsg").val("");
					$(".alert").delay(1000).fadeOut();
				}
				else{
					$("#frmMsg").append(data.msg);
				}
			}
		});
		return false;
	});
});

       
		</script>
  </body>
</html>