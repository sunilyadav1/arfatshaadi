<!DOCTYPE html>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Couple matching</title>
	
    <!-- Bootstrap -->
    <link href="<?php echo WEB_DIR;?>dist/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php echo WEB_DIR;?>assets/css/custom.css" rel="stylesheet" media="screen">
	
    <!-- Carousel -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo WEB_DIR;?>assets/js/html5shiv.js"></script>
      <script src="<?php echo WEB_DIR;?>assets/js/respond.min.js"></script>
    <![endif]-->
	
    <!-- Fonts -->	
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>	
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css'>	
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/font-awesome.css" media="screen" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="assets/css/font-awesome-ie7.css" media="screen" /><![endif]-->
	
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>css/fullscreen.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" href="<?php echo WEB_DIR;?>dist/css/bootstrap-datepicker.css">
	
    <!-- jQuery -->	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/jquery.v2.0.3.js"></script>
<style>
.multiselect-container {width: 100%; height: 200px; overflow-y: scroll;}
.btn .caret {margin-left: 0;float: right;margin-top: 10px;}
.multiselect{width: 300px; height: 34px; text-align: left;color: #999;  padding: 6px 0px 6px 12px; border: 2px solid #ebebeb; background-color: #ffffff;}
</style>
  </head>
  <body id="top">
    <?php $this->load->helper('html');?>
	<!-- Top wrapper -->
	<div class="navbar-wrapper2 ">
      <div class="container">
		<div class="navbar mtnav">

			<div class="container offset-3">
			  <!-- Navigation-->
			  <div class="navbar-header">
				<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a href="<?php echo WEB_URL; ?>" class="navbar-brand"><img src="<?php echo WEB_DIR; ?>images/logo1.png"  class="logo" style="height:85px;"/></a>
			  </div>
			  <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left" style="margin-top:35px;margin-left: 75px;">
					<li class="dropdown">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown">Members Login<strong class="caret"></strong></a>
							<div class="dropdown-menu">
								<form method="post" accept-charset="UTF-8" id="header-login">
									<span style="color:red;font-size:11px;font-weight:bold;" id="login_error"></span>
									<input type="text" name="email" id="email-header" style="margin-bottom: 10px;" class="form-control" placeholder="Email">
									<input type="password" name="password"  id="password-header" style="margin-bottom: 10px;" class="form-control" placeholder="Password">
									<input type="checkbox" name="remember-me" id="remember-me" value="1">
									<label class="string optional" style="margin-bottom: 10px;" for="user_remember_me"> Remember me</label>
									<button type="submit" style="margin-bottom: 10px;" class="btn-block btn btn-success">Login</button>
									<p class="text-center"><a href="<?php echo WEB_DIR; ?>index.php/home/forgot">Forgot Password?</a></p>
								</form>
							</div>
					</li>
					<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Help <strong class="caret"></strong></a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
						  <li class="dropdown-header">Need Assistance?</li>	
						  <li><a>+91-9448334317</a></li>
						  <li><a>+91-9901969497</a></li>
						</ul>
				  </li>	
				  <li style="margin-top:-20px;">
					<ul style="list-style: none !important;"><strong style="color: #107ABD;">Franchise Contact:</strong>
					<li>+91-9448334317</li><li>info@arfatshaadi.com</li></ul>
				  </li>
				</ul>
			  </div>
			  <!-- /Navigation-->			  
			</div>
		
        </div>
      </div>
    </div>
	<!-- / Top wrapper -->
	<div id="dajy" class="fullscreen-container mtslide sliderbg fixed">
			<div class="fullscreenbanner">
				<ul>
				<?php $sliderImages = $this->action_model->GetSliderImages(); 
				if($sliderImages != ""){
					foreach($sliderImages as $sliderImage){ ?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="300"> 										
						<img src="<?php echo WEB_DIR; ?>images/slider/<?=$sliderImage->slider_image;?>" alt=""/>
						<div class="tp-caption scrolleffect sft"
							 data-x="120"
							 data-y="400"
							 data-speed="1000"
							 data-start="800"
							 data-easing="easeOutExpo"  >
						</div>	
					</li>	
						
					<?php } }?>
					
					 <!-- FADE 
					<li data-transition="fade" data-slotamount="1" data-masterspeed="300"> 										
						
						<img src="<?php echo WEB_DIR; ?>images/slider/wed4.jpg" alt=""/>
						<div class="tp-caption scrolleffect sft"
							 data-x="120"
							 data-y="100"
							 data-speed="1000"
							 data-start="800"
							 data-easing="easeOutExpo"  >
						</div>	
					</li>-->	



				</ul>
				<div class="tp-bannertimer none"></div>
			</div>
		</div>

		<script type="text/javascript">

			var tpj=jQuery;
			tpj.noConflict();

			tpj(document).ready(function() {

			if (tpj.fn.cssOriginal!=undefined)
				tpj.fn.css = tpj.fn.cssOriginal;

				tpj('.fullscreenbanner').revolution(
					{
						delay:9000,
						startwidth:1170,
						startheight:600,

						onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

						thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
						thumbHeight:50,
						thumbAmount:3,

						hideThumbs:0,
						navigationType:"bullet",				// bullet, thumb, none
						navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

						navigationStyle:false,				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


						navigationHAlign:"left",				// Vertical Align top,center,bottom
						navigationVAlign:"bottom",					// Horizontal Align left,center,right
						navigationHOffset:30,
						navigationVOffset:30,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,

						touchenabled:"on",						// Enable Swipe Function : on/off


						stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
						stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

						hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
						hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
						hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


						fullWidth:"on",							// Same time only Enable FullScreen of FullWidth !!
						fullScreen:"off",						// Same time only Enable FullScreen of FullWidth !!


						shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

					});


		});
		</script>
		
	<!-- WRAP -->
		<div class="wrap cstyle03">
			
		<div class="container mt-200 sign-up-right mb-30 z-index100">		
		  <div class="row">
			<div class="col-md-8">
			</div>
			<div class="col-md-4" style="margin-top: -41px;">
				<div class="bs-example bs-example-tabs cstyle04">
					<div class="container breadcrub">
						<div class="brlines"></div>
					</div>	
					<ul class="nav nav-tabs" id="myTab">
						<li onclick="mySelectUpdate()" class="active col-xs-6" style="padding:0;text-align:center; border-radius: 10px 0px 0px 0px;"><a data-toggle="tab" style="border-radius: 9px 0px 0px 0px;" href="#car"><span class=""></span>Register Free</a></li>
						<li onclick="mySelectUpdate()" class="col-xs-6" style="padding:0;text-align:center;border-radius: 0px 10px 0px 0px;"><a data-toggle="tab" style="border-radius: 0px 9px 0px 0px;" href="#vacations"><span class=""></span>Search</a></li>
					</ul>
					
					<div class="tab-content3" id="myTabContent" >
						<!--End of 1st tab -->
						
						<!--End of 2nd tab -->
						
					<div id="car" class="tab-pane active in">
							<div class="w100percent">
								<div class="wh100percent text-center">
								<?php 
									$ses_user= $this->session->all_userdata();

									if(empty($ses_user['User_Id']))   { ?>
									<img src="<?php echo WEB_DIR; ?>images/fb.png" width="213px" id="facebook"/>
									
								<?php	 }  else{
										
									 echo '<img src="https://graph.facebook.com/'. $ses_user['UID'] .'/picture" width="30" height="30"/><div>'.$ses_user['Name'].'</div>';	
										echo "<fb:login-button autologoutlink='true' perms='email' scope='email'></fb:login-button>";
										
									}
								?>

								<div id="fb-root"></div>
								
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent text-center">
									<p>----- OR -----</p>
								</div>
							</div>
						<form id="wedding-validate" action="<?php echo WEB_DIR; ?>index.php/home/submit_form" method="POST">
							<div class="w100percent">
								<div class="wh100percent textleft">
									<input type="email" id="email" name="email" class="form-control" placeholder="Email">
									<label  id="email_error" style="display:block;  font-weight: bold;font-size: 10px;color: rgb(255, 104, 104);"></label>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<input type="password" name="password" id="password" class="form-control" placeholder="Password">
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<select class="form-control mySelectBoxClass"  name="profile_for" id="profilefor">
									  <option value="" label="Select" selected="selected">Profile For</option>
									  <option value="Self">Self</option>
									  <option value="Son">Son</option>
									  <option value="Daughter">Daughter</option>
									  <option value="Brother">Brother</option>
									  <option value="Sister">Sister</option>
									  <option value="Friend">Friend</option>
									  <option value="Relative">Relative</option>							  
									</select>
								</div>
							</div>
							<div class="w50percent">
								<div class="wh100percent textleft">
									<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name">
								</div>
							</div>
							<div class="w50percent">
								<div class="wh100percent textleft right">
									<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
								</div>
							</div>
							<div class="w50percent">
								<div class="wh100percent textleft">
									<span class="opensans size13"><b>Gender</b></span><br />
									<label class="radio-inline"><input type="radio" name="gender" value="1">Male</label>
									<label class="radio-inline"><input type="radio" name="gender" value="2">Female</label>
								</div>
							</div>
							<div class="w50percent">
								<div class="wh100percent textleft">
									<input type="text" class="form-control" placeholder="Date Of Birth" name="DOB"  id="datepicker7" data-date-format="mm/dd/yyyy"/>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<?php  $religion = $this->action_model->get_table_details('religionlist'); ?>
								<select  name="religion" id="religion" class="form-control">
									<option value="select">Religion</option>
									
									<?php 
										foreach($religion as $value){?>
										<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
									<?php } ?>					  
								</select>
								</div>
							</div>
							<!--<div class="w100percent">
								<div class="wh100percent textleft">
									<select  name="community" id="community" class="form-control mySelectBoxClass">
										<option value="select">Community</option>
													  
									</select>
								</div>
							</div>-->
							<div class="w100percent">
								<div class="wh100percent textleft">
									<?php  $language = $this->action_model->get_table_details('languagelist');?>
									<select class="form-control" name="mother_tongue" id="language">
										<option value="select">Mother tongue</option>
										<?php 
										foreach($language as $value){?>
										<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
										<?php } ?>
		           
									</select>
								
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
										<?php  $country = $this->action_model->get_table_details('countrylist');?>
										<select class="form-control" name="country" id="Country" name="living_in">
										<option value="" label="" selected="selected">Country</option>
											<?php 
											foreach($country as $country){?>
											<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
											<?php } ?>
										</select>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<input type="text" class="form-control" name="phone_no" id="phoneno" placeholder="Phone Number">
								</div>
							</div>
							<div class="w100percent">
									<label class="radio-inline"><input type="checkbox" style="margin-top:3px;" name="couple">I Agree with <a href="#">Term& Conditions</a></label>
							</div>
							<div class="searchbg">
								<button type="submit" class="btn-search" id="register_btn">Register</button>
							</div>
						</form>
					</div>
						
						<!--End of 3rd tab -->
							<script type="text/javascript" src="<?php echo WEB_DIR; ?>dist/js/bootstrap-multiselect.js"></script>
							<link rel="stylesheet" href="<?php echo WEB_DIR; ?>dist/css/bootstrap-multiselect.css" type="text/css"/>
							
						<div id="vacations" class="tab-pane in" style="height:550px">
						<form id="wedding-search" action="#" method="post" >	
							<div class="w100percent">
								<div class="wh100percent textleft margin_bottom_search">
									<span class="opensans size13"><b>Looking For :</b></span><br />
									<label class="radio-inline"><input type="radio" name="couple">Bride</label>
									<label class="radio-inline"><input type="radio" name="couple">Groom</label>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft margin_bottom_search">
									<span class="opensans size13"><b>Starting Age</b></span>
									<select class="form-control" name="startage" id="startage">
									  <option value="" label="Select" selected="selected">Select</option>
									  <?php for($i=18;$i<100;$i++){?>
										<option value="<?=$i;?>"><?=$i;?></option>    
										<?php } ?>						  
									</select>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft margin_bottom_search">
									<span class="opensans size13"><b>Ending Age</b></span>
									<select class="form-control" name="endage" id="endage">
									  <option value="" label="Select" value="" label="Select" selected="selected">Select</option>
										<?php for($i=18;$i<100;$i++){?>
										<option value="<?=$i;?>"><?=$i;?></option>    
										<?php } ?>
									</select>
								</div>
							</div>
							 <div class="w100percent">
								<div class="wh100percent textleft margin_bottom_search">
								<span class="opensans size13"><b>Mother Tongue</b></span>
								<?php    $language = $this->action_model->get_table_details('languagelist'); ?>
							
									<select class="form-control" name="Pmother_tongue[]" id="Pmother_tongue" multiple>
										<option value="">Doesn't Matter</option>
										
										<?php foreach($language as $value){ ?>
											<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
										<?php } ?>
									</select>
								</div>
							  </div>
							  
							<div class="w100percent">
								<div class="wh100percent textleft margin_bottom_search">
									<span class="opensans size13"><b>Select Partner Religion</b></span>
								<?php $religion = $this->action_model->get_table_details('religionlist'); ?>
								
									<select class="form-control" name="Preligion[]" id="Preligion" onchange="GetList('Preligion','Pcommunity','Religion_Id','communitylist','Community_Id','Community_Name')"  multiple>
										<option value="">Doesn't Matter</option>
										<?php foreach($religion as $value){?>
										<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
										<?php } ?> 
									</select>
								</div>
							  </div> 
							 
							  <div class="w100percent" id="Pcommunity_Dev" style="display:none;">
									<div class="wh100percent textleft margin_bottom_search">
										<span class="opensans size13"><b>Select Partner Community</b></span>
									
										<select  name="Pcommunity[]" id="Pcommunity" class="form-control">
													  
										</select>
									</div>
								</div>
								
						<div class="w100percent">
							<div class="wh100percent textleft">
							<span class="opensans size13"><b>Living Country</b></span>
							<?php     $country = $this->action_model->get_table_details('countrylist'); ?>
						
								<select class="form-control" name="Pliving_in[]" id="Pliving_in"  onchange="GetList('Pliving_in','PLivingState','Country_Id','statelist','State_Id','State_Name')" multiple>
									<option value="">Doesn't Matter</option>
									<?php 
									foreach($country as $country){?>
									<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
									<?php } ?>
								 </select>
							</div>
						  </div>
						  
						<div class="searchbg">
								<button type="button" class="btn-search" onclick="open_login_popup(1)" >Search</button>
						</div>
						</form>
						</div>
						<!--End of 4th tab -->
					</div>
					
						
				</div>
			</div>
		  </div>
		</div>
			
			
			
		<!----login pop up---->	
			<div style="position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; z-index: 1000; background: none repeat scroll 0% 0% rgba(0, 0, 0, 0.43); display: none;" id="login_pop_up" >
				<ul style="list-style: outside none none; width: 250px; display: block; margin: 100px auto 0px; top: 10%;" >
					<li class="dropdown open">
							
							<div class="dropdown-menu" style="padding-bottom:0px;">
							   
								<form method="post" accept-charset="UTF-8" id="popup-login">
									<input type="text" name="email" id="email-popup" style="margin-bottom: 10px;" class="form-control" placeholder="Email">
									<input type="password" name="password"  id="password-popup" style="margin-bottom: 10px;" class="form-control" placeholder="Password">
									<input type="checkbox" name="remember-me" id="remember-me" value="1">
									<label class="string optional" style="margin-bottom: 10px;" for="user_remember_me"> Remember me</label>
									<button type="button" style="margin-bottom: 10px;" id="btn_popuplogin" class="btn-block btn btn-success">Login</button>
									<p class="text-center"><a href="<?php echo WEB_DIR; ?>index.php/home/forgot">Forgot Password?</a></p>
								</form>
								<p style="text-align:right;color:red;font-weight:bold;cursor:pointer;" onclick="open_login_popup(0)" >CLOSE</p>
							</div>
							
							
					</li>
				</ul>
		
			</div>
		<!----/login pop up---->	
<script type="text/javascript">
		var x=0;
		function open_login_popup(x)
		{
			if(x)
			{
				$("#login_pop_up").css("display","block");
				$("#btn_popuplogin").click(function(){
						
					var email = $("#email-popup").val();
					var password = $("#password-popup").val();
					if(email == "" || password == ""){
						alert("please fill correct login details");
						return false;
					}
					$.ajax({
						url: "<?php echo WEB_URL;?>home/check_userAjax",
						dataType:'json',
						data: {email : email,password:password},
						type: "POST",
						success: function(data) {
						console.log(data);
							location.reload();
						}
					  });
				
			
				});
				return false;
			}else{
				$("#login_pop_up").css("display","none");
			}
			
			
		}
	</script>			
			
<script>
jQuery("#searchlanguage,#Preligion,#Pliving_in,#Pmother_tongue").multiselect();
 function GetList(currentdev,nextdev,column,table,display1,display2){

	 var val = $("#"+currentdev).val();

	 if(val != ""){

		$.ajax({

			url: "<?php echo WEB_URL; ?>home/GetJsonList",
			async: false,	
			data: {column:column , value : JSON.stringify(val),table:table,display1:display1,display2:display2},

			type: "POST",

			success: function(data) {
				console.log(data);
				$("#"+nextdev+"_Dev").css('display','block');
				$("#"+nextdev).html(data);
			}

		  });

	 }

}
	  // When the browser is ready...
	   jQuery(document).ready(function($) {
		$("#religion").change(function(){
			 var religion = $("#religion").val();
			 if(religion != ""){
				$.ajax({
					url: "<?php echo WEB_URL; ?>home/GetCommunities",
					data: {religion : religion},
					type: "POST",
					success: function(data) {
						
						$("#community").html(data);
					}
				  });
			 }
		});
		
		// Setup form validation on the #register-form element
		$("#wedding-validate").validate({
		
			// Specify the validation rules
			rules: {
				firstname: "required",
				lastname: "required",
				email: {
					required: true,
					email: true
				},
				profile_for: {
					required: true,
				},
				gender: {
					required: true,
				},
				DOB: {
					required: true,
				},
				phone_no: {
					required: true,
				},
				Country: {
					required: true,
				},
				religion: {
					required: true,
				},
				mother_tongue: {
					required: true,
				},
				password: {
					required: true,
					minlength: 5
				},
				community :{required : true,}
			},
			
			// Specify the validation error messages
			messages: {
				firstname: "Please enter your first name",
				lastname: "Please enter your last name",
				profile_for: "Please Specify Profile Created for ",
				gender: "Please Select a Gender",
				DOB: "Please Select DOB",
				phone_no: "Please fill your phone No.",
				religion: "Please Select your religion",
				mother_tongue: "Please Select your MotherTongue",
				Country: "Please Select Your Country",
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				email: "Please enter a valid email address",
				community : "Please select community"
			},
			
			submitHandler: function(form) {
			 	var email = $("#email").val();
				
				$.ajax({
				  url: "<?php echo WEB_URL;?>home/EmailCheck",
					data:{email:email},
					type:"post",
					beforeSend:function(){},
					success:function(data){
					
						if(data > 0){
							$("#email_error").html("This email id already registered");
							return  false;
						}else{
							form.submit();
						}
					}
				 }); 
			}
		});

	  });
</script>
<script type="text/javascript">
window.fbAsyncInit = function() {
	FB.init({appId: '558623904280461', status: true, cookie: true, xfbml: true, version    : 'v2.2'});
	
	FB.Event.subscribe('auth.login', function(response) {
		login();
	}); 
	FB.Event.subscribe('auth.logout', function(response) {
		$.ajax({
		  url: "<?php echo WEB_URL; ?>fbci/logout",
			data:response,
			type:"post",
			beforeSend:function(){},
			success:function(data){
				parent.location.reload();
			}
		 });
	 
	});

	FB.getLoginStatus(function(response) {
		if (response.session) {
			greet();
		}
	});
};

$(function() {
	var e = document.createElement('script');
	e.type = 'text/javascript';
	e.src = document.location.protocol +
		'//connect.facebook.net/en_US/all.js';
	e.async = true; 
	document.getElementById('fb-root').appendChild(e);
});
		 
//Onclick for fb login
 $('#facebook').click(function(e) {
    FB.login(function(response) {
	  if(response.authResponse) {
		  if (response.status === 'connected') {
			FB.api('/me', function(response) {
				var json = JSON.stringify(response);
				$.ajax({
				  url: "<?php echo WEB_URL; ?>fbci/fblogin",
					data:response,
					type:"post",
					beforeSend:function(){},
					success:function(data){
					console.log(data);
					parent.location.reload();
					}
				 });
			});
		  }
	  }
 },{scope: 'email,read_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
});
</script>	
