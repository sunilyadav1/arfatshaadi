<?php 
$id = $this->session->userdata('User_Id');
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->full_profile($id);
$UserSettings = $this->action_model->getpart_table_deatils("settings","User_Id",$id);
$img = $this->session->userdata('ProfilePic');
$name = $this->session->userdata('DisplayName');	
?>
<div class="col-md-4 detailsright offset-0">

    <div class="padding10" style="border-top: 1px solid #e7e7e7;">
       

        <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
            <div id="image_preview"><img class="left mr10 img-thumbnail" src="<?= $img; ?>" alt="" id="previewing"/></div>

            <span class="opensans gray bold size15"><?= $name; ?></span>
					<span class="size12">(<?php $DOB = $this->action_model->birthda($UserDetails[0]['DOB']);
										echo isset($DOB) ? $DOB." Years" : "";?> <?php echo isset($UserDetails[0]['Community_Name']) ? $UserDetails[0]['Community_Name']."," : ''; ?>
                       
                        <?php echo isset($UserDetails[0]['Language_Name']) ? ",".$UserDetails[0]['Language_Name'] : ''; ?>
                       
                        <?php echo isset($UserDetails[0]['Country_Name']) ? ",".$UserDetails[0]['Country_Name'] : ''; ?>
						<?php echo isset($UserDetails[0]['State_Name']) ? ",".$UserDetails[0]['State_Name'] : ''; ?>
						<?php echo isset($UserDetails[0]['City_Name']) ? ",".$UserDetails[0]['City_Name'] : ''; ?>
                        )</span>

        <div id="selectImage">
        <span class="opensans bold size10 mt5" style="float: left;">Change Image </span>
			<a href="#Img-upload" data-toggle="modal"><div class="upload">
				
			</div></a>
        <input type="submit" value="Upload" class="upload_btn" class="submit"/>
		
    </div>
    </div>
    </form>
    <div class="line7"></div>

    <div class="padding10">
        <ul class="hotelpreferences margtop10 paddingbtm20" style="position: relative; top: -7px;">
            <li class="trust_title">Trust Badges!</li>
            <li class="icohp-internet"></li>
            <li class="icohp-air"></li>
            <li class="icohp-pool"></li>
            <li class="icohp-internet"></li>
            <li class="icohp-air"></li>
        </ul>
    </div>

    <div class="line7"></div>

		<div class="padding10">
					<div class="wh60percent left">
						<span class="opensans size13 grey bold">Membership: </span>
						<span class="opensans size18 text-warning bold"> Free</span><br>
					</div>
					<div class="wh40percent right">
						<table class="right">
							<tbody>
							<tr>
								<td><span class="size12 lgrey"><a href="<?php echo WEB_URL; ?>home/upgrade" class="lblue">Upgrade Now</a></span></td>
								<td><span class="size12 grey">(6 days)</span></td>
							</tr>							
						</tbody></table>
					</div>
					
					<div class="clearfix"></div>
		</div>
		<div class="line7"></div>
		<div class="padding10">
					<div class="wh70percent left">
						<span class="opensans size13 grey bold"> Call / SMS Balance </span>
					</div>
					<div class="wh30percent right">
						<table class="right">
							<tbody>
							<tr>
								<td><span class="size13 grey"><a href="<?php echo WEB_URL; ?>home/upgrade" class="lblue">Upgrade Now</a></span></td>
							</tr>							
						</tbody></table>
					</div>
					
					<div class="clearfix"></div>
		</div>
		 <div class="line7"></div>
		 <?php $ViewedProfiles = $this->action_model->ViewedProfiles($id); 
				$notificationsIgnore = $this->action_model->GetTypeCount($this->session->userdata('User_Id'),8); 		
				$notificationsDecline = $this->action_model->GetTypeCount($this->session->userdata('User_Id'),7); 
		?>
    <div class="col-md-6 bordertype3 text-center" style="border-right: 1px solid #e7e7e7;">
        <a href="<?php echo WEB_URL."home/ViewedProfiles"; ?>" class="lblue size12">Viewed Profiles <span class="badge indent0"><?php if($ViewedProfiles['ViewedProfilescount'] == 0){echo 0;}else{echo $ViewedProfiles['ViewedProfilescount'];} ?></span></a><br/>
        <a href="<?php echo WEB_URL."home/Ignored"; ?>" class="lblue size12">Ignored Members <span class="badge indent0"><?php echo $notificationsIgnore; ?></span></a><br/>
        <a href="#" class="lblue size12">Decline Members <span class="badge indent0"><?php echo $notificationsDecline; ?></span></a>
    </div>
    <div class="col-md-6 bordertype3 text-center" style="border-right: 1px solid #e7e7e7;">
        <a href="<?php echo WEB_URL; ?>home/ViewProfile" class="lblue size12">My Profile</a><br/>
        <a href="<?php echo WEB_URL; ?>home/PicUpload" class="lblue size12">+Add Photos</a><br/>
<!--        <a href="#" class="lblue size12">Privacy Settings</a>-->
        <?php echo anchor('home/privacySetting','Privacy Settings', array('class'=>'lblue size12'));?>
    </div>
    <p class="clear"></p>

</div>
<div class="col-md-4 right">

<?php $emails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'), 1);
$Iemails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'), 2);
$Remails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'), 3);
?>
<div class="pagecontainer2 mt20 needassistancebox" style="border:none;">
    <div class="list-group">
        <a href="<?php echo WEB_URL."inbox/emails";?>" class="list-group-item active">
            <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Inbox <span class="label label-primary">(Activity Factor 100%)</span><span
                class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
        </a>
        <a data-toggle="tab" href="#emails_inbox" class="list-group-item">Emails <span class="badge">
            
            <?php if ($emails == 0) {
                    echo 0;
                } else {
                    echo count($emails);
                } ?></span></a>
        <a data-toggle="tab" href="#invitation_inbox" class="list-group-item">Invitations <span class="badge"><?php if ($Iemails == 0) {
                    echo 0;
                } else {
                    echo count($Iemails);
                } ?></span></a>
        <a data-toggle="tab" href="#requests_inbox" class="list-group-item">Requests <span class="badge"><?php if ($Remails == 0) {
                    echo 0;
                } else {
                    echo count($Remails);
                } ?></span></a>
    </div>
</div>

<div class="pagecontainer2 mt20 needassistancebox" style="border:none;">
    <div class="list-group">
        <a href="<?php echo WEB_URL; ?>inbox/emails#accepted" class="list-group-item active">
            <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Accepted
        </a>
        <a href="<?php echo WEB_URL; ?>inbox/emails" class="list-group-item">Members<span class="badge">00</span></a>
        <a href="<?php echo WEB_URL; ?>inbox/emails" class="list-group-item">Requests<span class="badge">0</span></a>
    </div>
</div>
<?php $sent = $this->email_model->GetSentDetails($this->session->userdata('User_Id'), 1);
$Isent = $this->email_model->GetSentDetails($this->session->userdata('User_Id'), 2);
$Rsent = $this->email_model->GetSentDetails($this->session->userdata('User_Id'), 3);
?>
<div class="pagecontainer2 mt20 needassistancebox" style="border:none;">
    <div class="list-group">
        <a href="<?php echo WEB_URL."inbox/emails#sent";?>" class="list-group-item active">
            <span class="glyphicon glyphicon-send" aria-hidden="true"></span> Sent
            <a data-toggle="tab" href="#emails_sent" class="list-group-item">Emails <span class="badge"><?php if ($sent == 0) {
                        echo 0;
                    } else {
                        echo count($sent);
                    } ?></span></a>
            <a data-toggle="tab" href="#invitation_sent" class="list-group-item">Invitations <span class="badge"><?php if ($Isent == 0) {
                        echo 0;
                    } else {
                        echo count($Isent);
                    } ?></span></a>
            <a data-toggle="tab" href="#requests_sent" class="list-group-item">Requests <span class="badge"><?php if ($Rsent == 0) {
                        echo 0;
                    } else {
                        echo count($Rsent);
                    } ?></span></a>
    </div>
</div>

</div>

</div>
 <link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/croptit.css" type="text/css" />
<script src="<?php echo WEB_DIR;?>assets/js/cropbox.js"></script>
			<div class="modal fade" id="Img-upload">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title"><strong>Add</strong> New Profile Image</h4>
                    </div>
                    <div class="modal-body">
						<div class="row">
							<div class="image-editor col-md-12">
									<div class="imageBox">
										<div class="thumbBox"></div>
										<div class="spinner" style="display: none">Loading...</div>
									</div>
									<div class="action">
										<input type="file" id="file" style="float:left; width: 250px">
										<input type="button" id="btnZoomIn" value="+" style="float: right">
										<input type="button" id="btnZoomOut" value="-" style="float: right">
									</div>
									
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                      <button type="button" id="btnCrop" value="Crop"  class="btn btn-danger btn-embossed save-category export" data-dismiss="modal">Save</button>
                    </div>
                  </div>
                </div>
              </div>
			  
<script>
     window.onload = function() {
        var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: ''
        }
        var cropper = new cropbox(options);
        document.querySelector('#file').addEventListener('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = new cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        document.querySelector('#btnCrop').addEventListener('click', function(){
            var img = cropper.getDataURL();
			$.ajax({
				url: "<?php echo WEB_URL; ?>home/UploadProfilePic", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: {img:img}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				success: function (data)   // A function to be called if request succeeds
				{
					alert(data);
					location.reload();
				}
			});
            //document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';
        })
        document.querySelector('#btnZoomIn').addEventListener('click', function(){
            cropper.zoomIn();
        })
        document.querySelector('#btnZoomOut').addEventListener('click', function(){
            cropper.zoomOut();
        })
    };
</script>