<!DOCTYPE html>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Couple matching</title>
	
    <!-- Bootstrap -->
    <link href="<?php echo WEB_DIR;?>dist/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php echo WEB_DIR;?>assets/css/custom.css" rel="stylesheet" media="screen">
   

    <!-- Carousel -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
	
    <!-- Fonts -->	
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css'>	
	<!-- Font-Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/font-awesome.css" media="screen" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="assets/css/font-awesome-ie7.css" media="screen" /><![endif]-->
	
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>css/fullscreen.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>rs-plugin/css/settings.css" media="screen" />

    <!-- Picker UI-->	
	<link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/jquery-ui.css" />		
	
    <!-- jQuery -->	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/jquery.v2.0.3.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/custom.js"></script>

  </head>
  <body id="top" >
    
	<div class="navbar-wrapper2">
      <div class="container">
		<div class="navbar mtnav">

			<div class="container offset-3">
			  <!-- Navigation-->
			  <div class="navbar-header">
				<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a href="" class="navbar-brand"><img src="<?php echo WEB_DIR; ?>images/logo.jpg" alt="" class="logo"/></a>
			  </div>
			  <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left" style="margin-top:35px;">
				  <li class="dropdown active">
					<a class="dropdown-toggle" href="<?php echo WEB_URL."home"; ?>">Home</a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
						  <li class="dropdown-header">My Contact Details</li>	
						  <li><a>My Profile</a></li>
						  <li><a>My Photos</a></li>
						  <li><a>Partner Preference</a></li>
						  <li><a>Profile | Viewed</a></li>
						  <li><a>Recent Visitor</a></li>
						  <li><a>Ignored/Blocked</a></li>
						  <li><a>Edit Horoscope</a></li>
						  <li><a>Refer A Friend</a></li>
						</ul>
				  </li>
                <!--Updated 25thApril: Pramod: Removed DropDown for Search-->
				  <li class="dropdown">
                      <a class="dropdown-toggle" href="<?php echo WEB_URL."home";?>">Register Now</a>
				  </li>
				 
				    <li class="dropdown">
					<a href="<?php echo WEB_URL."home" ?>">Inbox</a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
							<li><a href="#">Inbox</a></li>
							<li><a href="#">Accepted</a></li>
							<li><a href="#">Sent</a></li>
							<li><a href="#">Deleted</a></li>
							<li><a href="#">Notifications</a></li>	
						</ul>
					</li>
					
				  <li class="dropdown"><a href="<?php echo WEB_URL."home"; ?>">Matches</a>
						<ul class="dropdown-menu" style="width: 200px; padding: 6px 6px;">
							<li><a href="#">Daily Recommendations</a></li>
							<li><a href="#">Preferred Matches</a></li>
							<li><a href="#">New Matches</a></li>
							<li><a href="#">Border Matches</a></li>
							<li><a href="#">2-way Matches</a></li>	
							<li><a href="#">Reverse Matches</a></li>	
							<li><a href="#">May be's & Shortlists</a></li>	
						</ul>
				  </li>
					</li>
					
				  <li class="dropdown"><a href="#">Help <strong class="caret"></strong></a>
					<ul class="dropdown-menu" style="width: 170px;padding: 6px 6px;">
					  <li class="dropdown-header">Need Assistance?</li>	
					  <li><a href="#">+91-7259872851</a></li>
					  <li><a href="#">+91-9342627372</a></li>
					</ul>
				  </li>
				 
				</ul>
			  </div>
			  <!-- /Navigation-->			  
			</div>
		
        </div>
      </div>
    </div>
	<div>&nbsp;<div>