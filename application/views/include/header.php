<!DOCTYPE html>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Couple matching</title>
	
    <!-- Bootstrap -->
    <link href="<?php echo WEB_DIR;?>dist/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php echo WEB_DIR;?>assets/css/custom.css" rel="stylesheet" media="screen">
   
	<style>.dropdown a{color: #fff}.notify_dropdown a{color: #666;}</style>
    <!-- Carousel -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
	
    <!-- Fonts -->	
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css'>	
	<!-- Font-Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/font-awesome.css" media="screen" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="assets/css/font-awesome-ie7.css" media="screen" /><![endif]-->
	
 
	<!----online users---->
	<link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/jquery-ui.css" />	
	<link rel="stylesheet" href="<?php echo WEB_DIR; ?>css/shrchat.css">
  
    <!-- jQuery -->	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/jquery.v2.0.3.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/custom.js"></script>
	
		


  </head>
  <body id="top" >
    
	<div class="navbar-wrapper2">
      <div class="container">
		<div class="navbar mtnav">

			<div class="container offset-3">
			  <!-- Navigation-->
			  <div class="navbar-header">
				<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a href="" class="navbar-brand"><img src="<?php echo WEB_DIR; ?>images/logo.jpg" alt="" class="logo"/></a>
			  </div>
			  <!-- /Navigation-->			  
			</div>
		
        </div>
      </div>
    <div class="container container_milan">
		<div class="navbar mtnav">

			<div class="container offset-3">
			  <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left main_header">
				  <li class="dropdown active">
					<a class="dropdown-toggle" href="<?php echo WEB_URL."home"; ?>">Home<b class="mt-2"></b></a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
						  <li><a href="<?php echo WEB_URL."home/ViewProfile"; ?>">My Profile</a></li>
						  <li><a href="<?php echo WEB_URL."home/PicUpload"; ?>">My Photos</a></li>
						  <li><a href="<?php echo WEB_URL."home/ViewProfile#partner"; ?>">Partner Preference</a></li>
						  <li><a href="<?php echo WEB_URL."home/ViewedProfiles"; ?>">Profile | Viewed</a></li>
						  <li><a href="<?php echo WEB_URL."home/RecentVisitors"; ?>">Recent Visitor</a></li>
						  <li><a href="<?php echo WEB_URL."home/Ignored"; ?>">Ignored/Blocked</a></li>
						</ul>
				  </li>
                <!--Updated 25thApril: Pramod: Removed DropDown for Search-->
				  <li class="dropdown">
                      <a class="dropdown-toggle" href="<?php echo WEB_URL."home/search";?>">Search<b class="mt-2"></b></a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
						  <li><a href="<?php echo WEB_URL."home/search#advance";?>">Advanced Search</a></li>
						  <li><a href="<?php echo WEB_URL."home/search#basic";?>">Basic Search</a></li>
						  <li><a href="<?php echo WEB_URL."home/search#astro";?>">Astro Search</a></li>
						  <li><a href="<?php echo WEB_URL."home/search#special";?>">Special Cases</a></li>
						</ul>
				  </li>
				  
				  <?php 
				  $emails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'),1); ?>
				    <li class="dropdown">
					<a href="<?php echo WEB_URL."inbox/emails" ?>">Inbox <span class="badge indent0"><?php if($emails == 0){ echo 0;}else{ echo count($emails);}?></span></a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
							<li><a href="<?php echo WEB_URL."inbox/emails";?>">Inbox</a></li>
							<li><a href="<?php echo WEB_URL."inbox/emails#accepted";?>">Accepted</a></li>
							<li><a href="<?php echo WEB_URL."inbox/emails#sent";?>">Sent</a></li>
							<li><a href="<?php echo WEB_URL."inbox/emails";?>">Deleted</a></li>
							<li><a href="<?php echo WEB_URL."inbox/emails";?>">Notifications</a></li>	
						</ul>
					</li>
					
				  <li class="dropdown"><a href="<?php echo WEB_URL."home/match"; ?>">Matches <span class="badge indent0"></span></a>
						<ul class="dropdown-menu" style="width: 200px; padding: 6px 6px;">
							<li><a href="<?php echo WEB_URL."home/match"; ?>">Daily Recommendations</a></li>
							<li><a href="<?php echo WEB_URL."home/match"; ?>">Preferred Matches</a></li>
							<li><a href="<?php echo WEB_URL."home/NewMatch"; ?>">New Matches</a></li>
							<li><a href="<?php echo WEB_URL."home/broaderMatch"; ?>">Border Matches</a></li>
							<li><a href="<?php echo WEB_URL."home/maybe"; ?>">May be's & Shortlists</a></li>	
						</ul>
				  </li>
				  <?php $notifications = $this->action_model->GetNotifications($this->session->userdata('User_Id')); 
				  if($notifications != "failure"){$notifications_count = count($notifications);}else{$notifications_count = 0;}?>
				 <li class="dropdown">
					<a class="dropdown-toggle" href="#" data-toggle="dropdown">Notifications <span class="badge indent0"><?=$notifications_count;?></span></a>
					<div class="dropdown-menu notification_dropdwn">
					<?php if($notifications_count != 0){
					   for($i=0;$i<count($notifications);$i++){ 
					    if ($this->session->userdata('fb_login')){ 
							$img = $notifications[0]['ProfilePic'];
						}else{
							if($notifications[0]['ProfilePic'] == ""){
								$pic ="no-profile.gif";
							}else{
								$pic = $notifications[0]['ProfilePic'];
							}
							
							$img = WEB_DIR."images/profiles/".$pic;
						}	
					?>
						<div class="notify_dropdown">
							<a href="#"><img src="<?=$img?>" class="left mr10 noti-img" alt=""/></a>
							<a href="#" class="size13"><b class="notify_dropdown1"><?=$notifications[$i]['Name'];?></b></a><br/>
							<span class="opensans gray size12"><?php echo  isset($notifications[$i]['Religion_Name']) ? $notifications[$i]['Religion_Name'] : 'not specified';?></span><span class="opensans gray size12" title="Mother Tongue">, <?php echo  isset($notifications[$i]['Language_Name']) ? $notifications[$i]['Language_Name'] : 'not specified';?></span><br/>
							<span class="opensans gray size12"><?=$notifications[$i]['Name'];?> <?=$notifications[$i]['Description'];?></span><br/>
							<a class="size12 bold" href="<?=WEB_URL;?>home/profile/<?=$notifications[$i]['User_Id'];?>" class="lblue size12">Visit Profile</a>
						</div>
						<div class="line5"></div>
					<?php }
					} ?>
					</div>
				</li>
				  <?php  if ($this->session->userdata('user_logged_in')) {
					$id = $this->session->userdata('User_Id'); 
					$img = $this->session->userdata('ProfilePic');
					$name = $this->session->userdata('DisplayName');		
				  ?>
					<li class="dropdown user user-menu open">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
						  <span class="hidden-xs"><img class="img-circle" src="<?=$img;?>" width="30px" height="30px"> <?=$name;?></span>
						</a>
						<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
						 <!-- Menu Footer-->
									<li><a href="#">My Orders</a></li>
									<li><a href="<?php echo WEB_URL; ?>home/privacySetting">Privacy Setting</a></li>
									<?php if ($this->session->userdata('fb_login')) { ?>
									<li><fb:login-button autologoutlink='true' perms='email'></fb:login-button></li>
									<div id="fb-root"></div>
									<?php }else{ ?>
									<li><a href="<?php echo WEB_URL; ?>home/logout">Logout</a></li>
									<?php } ?>
						</ul>
					</li>	
					
					  <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Help <strong class="caret"></strong></a>
							<ul class="dropdown-menu" style="width: 170px; padding: 6px 6px;">
							  <li class="dropdown-header">Need Assistance?</li>	
							  <li><a>+91-7259872851</a></li>
							  <li><a>+91-9342627372</a></li>
							</ul>
					  </li>			  
			  <?php } ?>
				</ul>
			  </div>
			  <!-- /Navigation-->			  
			</div>
		
        </div>
      </div>
    </div>
	<div>&nbsp;<div>
