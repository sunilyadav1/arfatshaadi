<!-- FOOTER -->
</div>
<div>&nbsp;</div><div>&nbsp;</div>
		<div class="footerbgblack">
			<div class="container">		
				
				<div class="col-md-3">
					<span class="ftitle">Let's socialize</span>
					<div class="scont">
						<a href="http://www.facebook.com" class="social1b"><img src="<?php echo WEB_DIR; ?>images/icon-facebook.png" alt=""/></a>
						<a href="http://www.twitter.com" class="social2b"><img src="<?php echo WEB_DIR; ?>images/icon-twitter.png" alt=""/></a>
						<a href="http://plus.google.com" class="social3b"><img src="<?php echo WEB_DIR; ?>images/icon-gplus.png" alt=""/></a>
						<a href="http://www.youtube.com" class="social4b"><img src="<?php echo WEB_DIR; ?>images/icon-youtube.png" alt=""/></a>
						<br/><br/>
						<img src="<?php echo WEB_DIR; ?>images/logo1.png" alt="" style="width: 200px;height:50px;margin-bottom: 5px;"/><br/>
						&copy; 2013  |  All Rights Reserved 
						<br/><br/>
						
					</div>
				</div>
				<!-- End of column 1-->
				
				<div class="col-md-3">
					<span class="ftitle">Need Help?</span>
					<br/><br/>
					<ul class="footerlist">
						<li><a href="#" onclick="open_login_popup(1)">Member Login</a></li>
						<!-- <li><a href="<?php echo WEB_URL;?>home/searchingProfiles">Partner Search</a></li> -->
						<li><a href="#">Partner Search</a></li>
						<li><a href="#">How to Use MilanRishta.com</a></li>
						<!-- <li><a href="<?php echo WEB_URL;?>home/membershipsFAQ">Premium Memberships</a></li> -->
						<li><a href="#">Premium Memberships</a></li>
						<li><a href="#">Customer Support</a></li>
						<!-- <li><a href="<?php echo WEB_URL."home/Contact"; ?>">Contact Us</a></li> -->
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Success Stories</a></li>
					</ul>
				</div>
				<!-- End of column 2-->		
				
				<div class="col-md-3">
					<span class="ftitle">Wedding Specialists</span>
					<br/><br/>
					<ul class="footerlist">
						<li><a href="#">Weddings</a></li>
						<li><a href="#">Mobile</a></li>
						<li><a href="#">WeddingMatch Centres</a></li>
						<li><a href="#">WeddingMatch Labs</a></li>
						<li><a href="#">Wedding Planning</a></li>
						<li><a href="#">Select WeddingMatch</a></li>
					</ul>				
				</div>
				<!-- End of column 3-->		
				
				<div class="col-md-3 grey">
					<span class="ftitle">Newsletter</span>
					<div class="relative">
						<input type="email" class="form-control fccustom2" id="exampleInputEmail1" placeholder="Enter email">
						<button type="submit" class="btn btn-default btncustom">Submit<img src="<?php echo WEB_DIR; ?>images/arrow.png" alt=""/></button>
					</div>
					<br/><br/>
					<span class="ftitle">Customer support</span><br/>
					<span class="pnr">+91-9448334317<br/>+91-9901969497</span><br/>
					<span class="grey2"> info@arfatshaadi.com</span>
				</div>			
				<!-- End of column 4-->			
			
			

				
				
			</div>	
		</div>
		
		<div class="footerbg3black">
			<div class="container center grey"> 
			<a href="#">Blog</a> | 
			<a href="<?php echo WEB_URL."home/about"; ?>">About Us</a> | 
			<a href="<?php echo WEB_URL."home/terms"; ?>">Terms of Use</a> | 
			<a href="<?php echo WEB_URL."home/faq"; ?>">Faq</a> | 
			<a href="#">Privacy Policy</a> | 
			<a href="#">Security Tips</a> | 
			<a href="#">Report Misuse</a>
			</div>
		</div>
		
		

		
		
	</div>
	</div>
	</div>

<!----chat div---->

<div id="tmpMsg" style="display:none">
<div id="tmpMessage" style="height:200px">
<form method="post" id="frmMsg" action="<?php echo WEB_URL; ?>ajaxhandler/send_msg">
<textarea id="txtusermsg" name="txtusermsg" ></textarea>
<input name="data" type="hidden" value="<?php echo $uid['User_Id'].'_'.$this->session->userdata("mymodid"); ?>">
<input type="submit" value="Send" name="btnsendmsg" id="btnsendmsg"/>	
</form>
</div>
</div>	
	
     <!-- Javascript -->
	
    <!-- This page JS -->
	<script src="<?php echo WEB_DIR; ?>assets/js/js-index3.js"></script>	
	
 
    <!-- jQuery KenBurn Slider  -->
    <script type="text/javascript" src="<?php echo WEB_DIR; ?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	
   <!-- Nicescroll  -->	
	
    <!-- CarouFredSel -->
    <script src="<?php echo WEB_DIR; ?>assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
	
    <!-- Custom Select -->
	<script type='text/javascript' src='<?php echo WEB_DIR; ?>assets/js/jquery.customSelect.js'></script>	

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo WEB_DIR; ?>dist/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
	<script src="<?php echo WEB_DIR;?>dist/js/bootstrap-datepicker.js"></script>
   
	  <script>
	 
		  jQuery(document).ready(function($) {
			$("#header-login").validate({
				rules: {
					email: {
						required: true,
					},
					password:{
						required: true,
					},
				},
				messages: {
					email: "Please Enter Registered email id",
					password: "Please enter password",
				},
				submitHandler: function(form) {
					var email = $("#email-header").val();
					var password = $("#password-header").val();
					$.ajax({
						url: "<?php echo WEB_URL;?>home/check_userAjax",
						dataType:'json',
						data: {email : email,password:password},
						type: "POST",
						success: function(data) {
							console.log(data);
							if(data == 0){
								$("#login_error").html("Wrong email and password.");
							}else{
								location.reload();
							}
						}
					  });
				}
			});
	

		});

       
		</script>
  </body>
</html>