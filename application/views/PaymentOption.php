<?php session_start();
$this->load->view("include/header");
$id = $this->session->userdata('User_Id');
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->full_profile($id);
?>
<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "JBZaLc";

// Merchant Salt as provided by Payu
$SALT = "GQs7yium";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";

$action = '';

$posted = array();
if(!empty($_POST) && !empty($_POST['email'])) {
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  }
  
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
$data = array(
	"email" =>$_POST['email'],
	"firstname" =>$_POST['firstname'],
	"lastname" =>$_POST['lastname'],
	"User_Id" =>$_POST['User_Id'],
	"Service_Id" =>$_POST['Service_Id'],
	"Service_Id" =>$_POST['Service_Id'],
	"amount" => $_POST['amount'],
	"key" => $_POST['key'],
	"txnid" => $_POST['txnid'],
	"hash" => $_POST['hash'],
	"service_provider" => $_POST['service_provider']
  );
  $this->action_model->add_record('paymentinfo',$data,0,"");
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<script>
var hash = '<?php echo $hash ?>';
function submitPayuForm() {
  if(hash == '') {
	return;
  }
  var payuForm = document.forms.payuForm;
  payuForm.submit();
}
</script>
<body onload="submitPayuForm()">
<div class="container" >
	<ol class="breadcrumb">
		<li><a href="<?php echo WEB_URL;?>home">Home</a></li>
		<li class="active">Your Order Details</li>
	</ol>
	<?php if(isset($_POST['productcode'])){
			$_SESSION['productcode'] = $_POST['productcode'];
		 }
		 $PaymentOption = $this->action_model->getpart_table_deatils("servicetypes","Service_Id",$_SESSION['productcode']);
		if(isset($PaymentOption) && $PaymentOption !=""){ ?>
			<table class="col-sm-12" style="background-color:beige;">
				<thead>
					<tr>&nbsp;</tr><tr>&nbsp;</tr>
				</thead>
				<tbody>
					<tr>
						<td style="padding:20px 20px 0px 20px;">1) <?=$PaymentOption[0]->Service_name;?></td>
						<td style="padding:20px;float:right;"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> <?=$PaymentOption[0]->Service_Cost;?></td>
					</tr>
					<tr colspan=2>
						<td style="padding-left:20px;"><?=$PaymentOption[0]->Service_Description;?></td>
					</tr>
					<tr style="color:green;">
						
						<td style="float:right;margin-right: -95px;"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> <?=$PaymentOption[0]->Service_Discount;?></td>
						<td style="float:right;">Discount</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr colspan=2><td style="border-top:1px dotted #000000;">&nbsp;</td></tr>
					
					<tr>
						<td style="float:right;margin-right: -95px;"><img src="<?php echo WEB_DIR; ?>images/normal-rupees9x14.png" width="9" height="14" alt="Rs." title="Rs."> <?=$PaymentOption[0]->Service_PayAmount;?></td>
						<td style="float:right;font-weight:bold;">Total payment amount</td>
					</tr>
				</tbody>
			</table>
		<?php	} ?>
			<div class="" style="padding-top:20px"></div>

    <h2>Choose a payment Option</h2>
    <br/>
    <?php if($formError) { ?>
	
      <span style="color:red">Please fill all mandatory fields.</span>
      <br/>
      <br/>
    <?php } ?>
			 <form id="payuForm" action="<?php echo $action; ?>" name="payuForm" method="post" class="form-horizontal">
				  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
				  <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
				  <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
				   <input name="amount" type="hidden" class="form-control" value="<?php echo (empty($PaymentOption[0]->Service_PayAmount)) ? '' : $PaymentOption[0]->Service_PayAmount ?>" />
					<input type="hidden" name="surl" value="<?php echo WEB_URL;?>home/success" size="64" /> 
					<input type="hidden" name="furl" value="<?php echo WEB_URL;?>home/failure" size="64" />
					 <input type="hidden" name="User_Id" value="<?php echo $UserDetails[0]['User_Id']; ?>" />
					  <input type="hidden" name="Service_Id" value="<?php echo $PaymentOption[0]->Service_Id; ?>" />
				 <div class="form-group">
					  <label class="form-label col-sm-4">Payment Option *: </label>
					  <div class="col-sm-4">
						PayU <input type="radio" name="service_provider" value="payu_paisa" checked>
						PayTm <div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/EBD48B1423B08CC36E1B381B79F8A14A"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/112.png" /></a></div>																						
<input type="radio" name="service_provider" value="PayTm" style="display:none;">
					  </div>
				  </div>
				  <div class="form-group">
					  <label class="form-label col-sm-4">First Name *: </label>
					  <div class="col-sm-4">
						<input name="firstname" class="form-control" id="firstname" value="<?php echo (empty($UserDetails[0]['Name'])) ? '' : $UserDetails[0]['Name']; ?>" />
					  </div>
				  </div>
				  <div class="form-group">
					  <label class="form-label col-sm-4">Email *: </label>
					  <div class="col-sm-4">
						<input name="email" class="form-control" id="email" value="<?php echo (empty($UserDetails[0]['Email'])) ? '' : $UserDetails[0]['Email']; ?>" />
					  </div>
				  </div>
				   <div class="form-group">
					  <label class="form-label col-sm-4">Phone *: </label>
					  <div class="col-sm-4">
						<input name="phone" class="form-control" value="<?php echo (empty($UserDetails[0]['MobileNo'])) ? '' : $UserDetails[0]['MobileNo']; ?>" />
					</div>
				  </div>
				
				 <div class="form-group">
					  <label class="form-label col-sm-4">Product Info *: </label>
					  <div class="col-sm-4">
						<textarea name="productinfo" class="form-control"><?php echo (empty($PaymentOption[0]->Service_Description)) ? '' : $PaymentOption[0]->Service_Description; ?></textarea>
					</div>
				  </div> 
				
				  <div class="form-group">
					  <label class="form-label col-sm-4">Last Name: </label>
					  <div class="col-sm-4">
						<input name="lastname"  class="form-control" id="lastname" value="<?php echo (empty($UserDetails[0]['LastName'])) ? '' : $UserDetails[0]['LastName']; ?>" />
					</div>
				  </div> 
				<div class="form-group">
					  <label class="form-label col-sm-4">Address: </label>
					  <div class="col-sm-4">
						<input name="address1" class="form-control" id="address1" value="" />
					</div>
				  </div> 
					  <div class="form-group">
					  <label class="form-label col-sm-4">City: </label>
					  <div class="col-sm-4">
						<input name="city" class="form-control" id="city" value="<?php echo (empty($UserDetails[0]['City_Name'])) ? '' : $UserDetails[0]['City_Name']; ?>" />
					</div>
					</div>
					 <div class="form-group">
					  <label class="form-label col-sm-4">State: </label>
					  <div class="col-sm-4">
						<input name="state" class="form-control" id="state" value="<?php echo (empty($UserDetails[0]['State_Name'])) ? '' : $UserDetails[0]['State_Name']; ?>" />
					</div>
					</div>
					 <div class="form-group">
					  <label class="form-label col-sm-4">Country: </label>
					  <div class="col-sm-4">
						<input name="country" class="form-control" id="country" value="<?php echo (empty($UserDetails[0]['Country_Name'])) ? '' : $UserDetails[0]['Country_Name']; ?>" />
					</div>
					</div>
					 <div class="form-group">
					  <label class="form-label col-sm-4">Zipcode: </label>
					  <div class="col-sm-4">
						<input name="zipcode" class="form-control" id="zipcode" value="<?php echo (empty($UserDetails[0]['PinNo'])) ? '' : $UserDetails[0]['PinNo']; ?>" />
					</div>
					</div>
					
					  <?php if(!$hash) { ?>
						<td colspan="4"><input type="submit" value="Submit" /></td>
					  <?php } ?>
					
				</form>
	
</div>


<?php $this->load->view("include/footer"); ?>