<?php $this->load->view("include/header"); 
$id = $this->session->userdata('User_Id'); 
$UserDetails = $this->action_model->getpart_table_deatils("userprofile","User_Id",$id);
?>
<div class="container">
	<div class="container breadcrub">
		<div class="brlines"></div>
	</div>	
	<!-- CONTENT -->
	<div class="container">
		<div class="container pagecontainer offset-0">	
		<?php if(isset($result['status']) && $result['status'] == 0){?>
			<div class="alert alert-warning" role="alert">Please fill the Partner preference details to get better match
			<a href="<?php echo WEB_URL; ?>HOME/partner_profile">Partner details</a></div>
		<?php } ?>
			<!-- SLIDER -->
			<div class="col-md-8 pagecontainer2 offset-0">
				<div class="cstyle10"></div>
		
				<ul class="nav nav-tabs" id="myTab">
					<li onclick="mySelectUpdate()" class="active"><a data-toggle="tab" href="#My-Match"><span class="summary"></span><span class="hidetext">My Match</span>&nbsp;</a></li>
				</ul>			
				<div class="tab-content4">
					<!-- TAB 1 -->	
					<div id="My-Match" class="tab-pane fade active in">
					  <?php if($result == 'failure'){ ?>
							<div class="alert alert-info">
							  No matches found !
							</div>
							
						<?php }else{  
						foreach($result['result'] as $value){
						if ($this->session->userdata('fb_login')) { 
							$pimg = $value['ProfilePic'];
						}else{
							if($value['ProfilePic'] == ""){
								$ppic ="no-profile.gif";
							}else{
								$ppic = $value['ProfilePic'];
							}
							
							$pimg = WEB_DIR."images/profiles/".$ppic;
						}?>
						
						<div class="padding20">
							<div class="col-md-4 text-center  offset-0">
								<a href="#"><img src="<?php echo $pimg; ?>" alt="" class="fwimg img-thumbnail" width="80px" height="80px"></a>
							</div>
							<div class="col-md-8 offset-0">
								<div class="col-md-12">
									<h4 class="opensans dark bold margtop1 lh1"><?=$value['Name'];?></h4>
									<?=$value['Community_Name']; ?>, <?=$value['SubCommunity_Name']; ?>, <?=$value['Language_Name']; ?>
									<p class="hpadding20"><?=$value['About'];?></p>
									<div class="clearfix"></div>
									<button class="btn-search4 caps center margtop20">View Profile</button>								
								</div>									
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="line2"></div>
						<?php }
						} ?>
						
					</div>			
										
				</div>
			</div>
			<!-- END OF SLIDER -->			
			
			<!-- RIGHT INFO -->
			<div class="col-md-4 detailsright offset-0">
				<div class="padding20">
					
					<?php
						if ($this->session->userdata('fb_login')) { 
							$img = $UserDetails[0]->ProfilePic;
						}else{
							if($UserDetails[0]->ProfilePic == ""){
								$pic ="no-profile.gif";
							}else{
								$pic = $UserDetails[0]->ProfilePic;
							}
							
							$img = WEB_DIR."images/profiles/".$pic;
						}
					
					?>
					<img class="left mr20" src="<?=$img;?>" alt=""/>
					<h4 class="lh1"><?=$UserDetails[0]->Name;?> <?=$UserDetails[0]->LastName;?></h4>
					<?php $community = $this->action_model->getpart_table_deatils("communitylist","Community_Id",$UserDetails[0]->Community);
						  $subcommunity = $this->action_model->getpart_table_deatils("subcommunitylist","SubCommunityList_Id",$UserDetails[0]->SubCommunity);			
						  $MotherTongue = $this->action_model->getpart_table_deatils("languagelist","Language_Id",$UserDetails[0]->MotherTongue);						  ?>
					<span>( <?php echo  isset($community[0]->Community_Name) ? $community[0]->Community_Name : 'not specified';?>, 
					<?php echo  isset($subcommunity[0]->SubCommunity_Name) ? $subcommunity[0]->SubCommunity_Name : 'not specified';?>, 
					<?php echo  isset($MotherTongue[0]->Language_Name) ? $MotherTongue[0]->Language_Name : 'not specified';?> )</span>
				</div>
				
				<div class="line3"></div>
				
				<div class="hpadding20">
					<a href="#"><h4 class="opensans green2 text-center">Trust Badges!</h4></a>
					<ul class="hotelpreferences margtop10 paddingbtm20">
										<li class="icohp-internet"></li>
										<li class="icohp-air"></li>
										<li class="icohp-pool"></li>
										<li class="icohp-childcare"></li>
										<li class="icohp-fitness"></li>
										<li class="icohp-breakfast"></li>
										<li class="icohp-parking"></li>
									</ul>
				</div>
				
				<div class="line3 margtop20"></div>
				
				<div class="col-md-6 bordertype1 padding20">
				<h4 class="opensans green2 text-center" style="margin: 0px;">Membership</h4>
					<span class="opensans size30 bold text-warning">Gold</span><br/>
					<a href="#" class="grey">Change Plan*</a>
					
				</div>
				<div class="col-md-6 bordertype2 padding20">
					<span class="opensans size30 bold grey2">16 / 59</span><br/>
					Call / SMS Balance
				</div>
				<div class="col-md-6 bordertype3">
					<a href="#" class="lblue size12">Viewed Profiles <span class="badge indent0">14</span></a><br/>
					<a href="#" class="lblue size12">Ignored Members <span class="badge indent0">60</span></a><br/>
					<a href="#" class="lblue size12">Decline Members <span class="badge indent0">342</span></a>
				</div>
				<div class="col-md-6 bordertype3">
					<a href="#" class="lblue size12">Edit Profile</a><br/>
					<a href="#" class="lblue size12">+Add Photos</a><br/>
					<a href="#" class="lblue size12">Privacy Settings</a>
				</div>
				<div class="clearfix"></div><br/>
				
			</div>
			<!-- END OF RIGHT INFO -->
			<div class="col-md-4 right" style="margin-top: 20px;" >
				
				<div class="pagecontainer2 testimonialbox">
					<div class="cpadding0 mt-10">
						<div id="vacations" class="">
							<h4 class="opensans green2 text-center" style="margin: 0px;">Search For Match</h4>
							<div class="line3 margtop20"></div>
							<div id="car">
							<div class="w100percent">
								<div class="wh100percent textleft">
									<span class="opensans size13"><b>Looking For</b></span><br>
									<label class="radio-inline"><input type="radio" name="optradio">Bride</label>
									<label class="radio-inline"><input type="radio" name="optradio">Groom</label>
								</div>
							</div>
							<div class="w50percent">
								<div class="wh90percent textleft">
									<span class="opensans size13"><b>Starting Age</b></span>
									<select class="form-control mySelectBoxClass hasCustomSelect" style="-webkit-appearance: menulist-button; width: 179px; position: absolute; opacity: 0; height: 34px; font-size: 14px;">
									  <option selected="selected">21</option>
									  <option>22</option>
									  <option>23</option>
									  <option>24</option>
									  <option>25</option>
									  <option>26</option>
									  <option>27</option>
									  <option>28</option>
									  <option>29</option>
									  <option>30</option>
									  <option>31</option>
									  <option>32</option>
									  <option>33</option>							  
									</select><span class="customSelect form-control mySelectBoxClass" style="display: inline-block;"><span class="customSelectInner" style="width: 84px; display: inline-block;">21</span></span>
								</div>
							</div>
							<div class="w50percent">
								<div class="wh90percent textleft right">
									<span class="opensans size13"><b>Ending Age</b></span>
									<select class="form-control mySelectBoxClass hasCustomSelect" style="-webkit-appearance: menulist-button; width: 179px; position: absolute; opacity: 0; height: 34px; font-size: 14px;">
									  <option selected="selected">21</option>
									  <option>22</option>
									  <option>23</option>
									  <option>24</option>
									  <option>25</option>
									  <option>26</option>
									  <option>27</option>
									  <option>28</option>
									  <option>29</option>
									  <option>30</option>
									  <option>31</option>
									  <option>32</option>
									  <option>33</option>						  
									</select><span class="customSelect form-control mySelectBoxClass" style="display: inline-block;"><span class="customSelectInner" style="width: 84px; display: inline-block;">21</span></span>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<span class="opensans size13"><b>Religion</b></span>
									<select class="form-control mySelectBoxClass hasCustomSelect" style="-webkit-appearance: menulist-button; width: 358px; position: absolute; opacity: 0; height: 34px; font-size: 14px;">
									  <option value="" label="Select" selected="selected">Hindu</option>
									  <option>Muslim</option>
									  <option>Christian</option>
									  <option>Sikh</option>
									  <option>Parsi</option>
									  <option>Jain</option>
									  <option>Buddhist</option>
									  <option>Jewish</option>
									  <option>No Religion</option>
									  <option>Spiritual</option>
									  <option>Other</option>							  
									</select><span class="customSelect form-control mySelectBoxClass" style="display: inline-block;"><span class="customSelectInner" style="width: 84px; display: inline-block;">Hindu</span></span>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<span class="opensans size13"><b>Mother Tongue</b></span>
									<select class="form-control mySelectBoxClass hasCustomSelect" style="-webkit-appearance: menulist-button; width: 358px; position: absolute; opacity: 0; height: 34px; font-size: 14px;">
										<option value="Assamese" label="Assamese" selected="selected">Assamese</option>
										<option value="Bengali" label="Bengali">Bengali</option>
										<option value="English" label="English">English</option>
										<option value="Gujarati" label="Gujarati">Gujarati</option>
										<option value="Hindi" label="Hindi">Hindi</option>
										<option value="Kannada" label="Kannada">Kannada</option>
										<option value="Konkani" label="Konkani">Konkani</option>
										<option value="Malayalam" label="Malayalam">Malayalam</option>
										<option value="Marathi" label="Marathi">Marathi</option>
										<option value="Marwari" label="Marwari">Marwari</option>
										<option value="Odia" label="Odia">Odia</option>
										<option value="Punjabi" label="Punjabi">Punjabi</option>
										<option value="Sindhi" label="Sindhi">Sindhi</option>
										<option value="Tamil" label="Tamil">Tamil</option>
										<option value="Telugu" label="Telugu">Telugu</option>
										<option value="Urdu" label="Urdu">Urdu</option>
										<optgroup id="frm-mothertongue-optgroup-More" label="More">
										</optgroup>
										<option value="Aka" label="Aka">Aka</option>
										<option value="Arabic" label="Arabic">Arabic</option>
										<option value="Arunachali" label="Arunachali">Arunachali</option>
										<option value="Awadhi" label="Awadhi">Awadhi</option>
										<option value="Baluchi" label="Baluchi">Baluchi</option>
										<option value="Bhojpuri" label="Bhojpuri">Bhojpuri</option>
										<option value="Bhutia" label="Bhutia">Bhutia</option>
										<option value="Brahui" label="Brahui">Brahui</option>
										<option value="Brij" label="Brij">Brij</option>
										<option value="Burmese" label="Burmese">Burmese</option>
										<option value="Chattisgarhi" label="Chattisgarhi">Chattisgarhi</option>
										<option value="Chinese" label="Chinese">Chinese</option>
										<option value="Coorgi" label="Coorgi">Coorgi</option>
										<option value="Dogri" label="Dogri">Dogri</option>
										<option value="French" label="French">French</option>
										<option value="Garhwali" label="Garhwali">Garhwali</option>
										<option value="Garo" label="Garo">Garo</option>
										<option value="Haryanavi" label="Haryanavi">Haryanavi</option>
										<option value="Himachali/Pahari" label="Himachali/Pahari">Himachali/Pahari</option>
										<option value="Hindko" label="Hindko">Hindko</option>
										<option value="Kakbarak" label="Kakbarak">Kakbarak</option>
										<option value="Kanauji" label="Kanauji">Kanauji</option>
										<option value="Kashmiri" label="Kashmiri">Kashmiri</option>
										<option value="Khandesi" label="Khandesi">Khandesi</option>
										<option value="Khasi" label="Khasi">Khasi</option>
										<option value="Koshali" label="Koshali">Koshali</option>
										<option value="Kumaoni" label="Kumaoni">Kumaoni</option>
										<option value="Kutchi" label="Kutchi">Kutchi</option>
										<option value="Ladakhi" label="Ladakhi">Ladakhi</option>
										<option value="Lepcha" label="Lepcha">Lepcha</option>
										<option value="Magahi" label="Magahi">Magahi</option>
										<option value="Maithili" label="Maithili">Maithili</option>
										<option value="Malay" label="Malay">Malay</option>
										<option value="Manipuri" label="Manipuri">Manipuri</option>
										<option value="Miji" label="Miji">Miji</option>
										<option value="Mizo" label="Mizo">Mizo</option>
										<option value="Monpa" label="Monpa">Monpa</option>
										<option value="Nepali" label="Nepali">Nepali</option>
										<option value="Pashto" label="Pashto">Pashto</option>
										<option value="Persian" label="Persian">Persian</option>
										<option value="Rajasthani" label="Rajasthani">Rajasthani</option>
										<option value="Russian" label="Russian">Russian</option>
										<option value="Sanskrit" label="Sanskrit">Sanskrit</option>
										<option value="Santhali" label="Santhali">Santhali</option>
										<option value="Seraiki" label="Seraiki">Seraiki</option>
										<option value="Sinhala" label="Sinhala">Sinhala</option>
										<option value="Sourashtra" label="Sourashtra">Sourashtra</option>
										<option value="Spanish" label="Spanish">Spanish</option>
										<option value="Swedish" label="Swedish">Swedish</option>
										<option value="Tagalog" label="Tagalog">Tagalog</option>
										<option value="Tulu" label="Tulu">Tulu</option>
										<option value="Other" label="Other">Other</option>						  
									</select><span class="customSelect form-control mySelectBoxClass" style="display: inline-block;"><span class="customSelectInner" style="width: 84px; display: inline-block;">Assamese</span></span>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<span class="opensans size13"><b>Community</b></span>
									<select class="form-control mySelectBoxClass hasCustomSelect" style="-webkit-appearance: menulist-button; width: 358px; position: absolute; opacity: 0; height: 34px; font-size: 14px;">
									  <option value="" label="Select" selected="selected">Hindu</option>
									  <option>Muslim</option>
									  <option>Christian</option>
									  <option>Sikh</option>
									  <option>Parsi</option>
									  <option>Jain</option>
									  <option>Buddhist</option>
									  <option>Jewish</option>
									  <option>No Religion</option>
									  <option>Spiritual</option>
									  <option>Other</option>							  
									</select><span class="customSelect form-control mySelectBoxClass" style="display: inline-block;"><span class="customSelectInner" style="width: 84px; display: inline-block;">Hindu</span></span>
								</div>
							</div>
							<div class="w100percent">
								<div class="wh100percent textleft">
									<span class="opensans size13"><b>Country</b></span>
									<select class="form-control mySelectBoxClass hasCustomSelect" style="-webkit-appearance: menulist-button; width: 358px; position: absolute; opacity: 0; height: 34px; font-size: 14px;">
										<option value="India" label="India" selected="selected">India</option>
										<option value="USA" label="USA">USA</option>
										<option value="United Kingdom" label="UK">UK</option>
										<option value="United Arab Emirates" label="UAE">UAE</option>
										<option value="Canada" label="Canada">Canada</option>
										<option value="Australia" label="Australia">Australia</option>
										<option value="New Zealand" label="New Zealand">New Zealand</option>
										<option value="Pakistan" label="Pakistan">Pakistan</option>
										<option value="Saudi Arabia" label="Saudi Arabia">Saudi Arabia</option>
										<option value="Kuwait" label="Kuwait">Kuwait</option>
										<option value="South Africa" label="South Africa">South Africa</option>							  
									</select><span class="customSelect form-control mySelectBoxClass" style="display: inline-block;"><span class="customSelectInner" style="width: 84px; display: inline-block;">India</span></span>
								</div>
							</div>
						</div>	
						<div class="searchbg">
							<form action="list4.html">
								<button type="submit" class="btn-search">Search</button>
							</form>
						</div>
						</div> 
					</div>
				</div>
				
				<div class="pagecontainer2 mt20 needassistancebox">
					<div class="cpadding1">
						<span class="icon-help"></span>
						<h3 class="opensans">Need Assistance?</h3>
						<p class="size14 grey">Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
						<p class="opensans size30 lblue xslim">1-866-599-6674</p>
					</div>
				</div><br/>
				
				<div class="pagecontainer2 mt20 alsolikebox"><br/>
					<h4 class="opensans green2 text-center" style="margin: 0px;">Profile Visitors</h4>
					<div class="line3 margtop20"></div>
					<div class="cpadding1">
						<div class="clearfix"></div>
					</div>
					<div class="cpadding1 ">
						<a href="#"><img src="images/user-avatar.jpg" class="left mr20" alt=""/></a>
						<a href="#" class="dark"><b>Sarfeena Banno</b></a><br/>
						<span class="opensans gray size13">Muslim, Sunni, Hindi</span><br/>
						<a href="#" class="lblue size12">Visit Profile</a>
					</div>
					<div class="line5"></div>
					<div class="cpadding1 ">
						<a href="#"><img src="images/user-avatar.jpg" class="left mr20" alt=""/></a>
						<a href="#" class="dark"><b>Sarfeena Banno</b></a><br/>
						<span class="opensans gray size13">Muslim, Sunni, Hindi</span><br/>
						<a href="#" class="lblue size12">Visit Profile</a>
					</div>
					<div class="line5"></div>			
					<div class="cpadding1 ">
						<a href="#"><img src="images/user-avatar.jpg" class="left mr20" alt=""/></a>
						<a href="#" class="dark"><b>Sarfeena Banno</b></a><br/>
						<span class="opensans gray size13">Muslim, Sunni, Hindi</span><br/>
						<a href="#" class="lblue size12">Visit Profile</a>
					</div>
					<br/>
				
					
				</div>				
			
			</div>
		</div>
		<!-- END OF container-->
		
		
		
		
	</div>


<?php $this->load->view("include/footer"); ?>