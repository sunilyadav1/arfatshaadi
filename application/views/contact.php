<?php session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>


<div class="container">
	
	<!-- CONTENT -->
	<div class="container">
	 
		
		<div class="container pagecontainer offset-0">	
	
			<!-- SLIDER -->
			<div class="col-md-8 col-md-offset-2" style="border:none;">
				 <div class="panel panel-default">
						  <div class="panel-heading"><h4>Contact Us</h4></div>
						  <div class="panel-body">
									    <div class='contentNewmatch'>
									    	<div class="contact_form">
											<?php if(isset($status) && $status != ""){ ?>
											<div class="alert alert-success"><strong><?php echo $status; ?></strong></div>
											<?php } ?>
							  <form id="my-contact-form" action="<?php echo WEB_URL; ?>home/contact" method="post" class="form-horizontal"novalidate="true"  role="form"> 
							<div><input name="my-contact-form" type="hidden" value="1"  class="form-control"></div> 
							<fieldset> 

							<div class="form-group"> 
							<label for="username" class="col-sm-4 control-label">Your name <sup class="text-danger">* </sup>: </label> 
							<div class="col-sm-8"> 
							<input id="username" name="username" type="text" value="<?php echo isset($UserDetails[0]['Name']) ? $UserDetails[0]['Name'] : ""; ?>" required="required" class="form-control"> 
							</div> 
							</div> 
							<div class="form-group"> 
							<label for="useremail" class="col-sm-4 control-label">Your email <sup class="text-danger">* </sup>: </label> 
							<div class="col-sm-8"> 
							<input id="useremail" name="useremail" type="email" value="<?php echo isset($UserDetails[0]['Email']) ? $UserDetails[0]['Email'] : "";?>" required="required" class="form-control"> 
							</div> 
							</div> 
							<div class="form-group"> 
							<label for="userphone" class="col-sm-4 control-label">Your phone <sup class="text-danger">* </sup>: </label> 
							<div class="col-sm-8"> 
							<input id="userphone" name="userphone" type="text" value="<?php echo isset($UserDetails[0]['MobileNo']) ? $UserDetails[0]['MobileNo'] : "";?>" required="required" class="form-control"> 
							</div> 
							</div> 
							<div class="form-group"> 
							<label for="subject" class="col-sm-4 control-label">Subject : </label> 
							<div class="col-sm-8"> 
							<select id="subject" name="subject"  class="form-control"> 
							<option value="Support" >Support</option> 
							<option value="Sales" >Sales</option> 
							<option value="Other" >Other</option> 
							</select> 
							</div> 
							</div> 
							<div class="form-group"> 
							<label for="message" class="col-sm-4 control-label">Your message <sup class="text-danger">* </sup>: </label> 
							<div class="col-sm-8"> 
							<textarea id="message" name="message" cols="30" rows="4" required="required" class="form-control"></textarea> 
							</div> 
							</div> 
							 
							<div class="form-group"> 
							<div class="col-sm-offset-4 col-sm-8"> 
							<input type="submit" name="submit-btn" value="Submit" class="btn btn-success">
							</div> 
							</div> 
							</fieldset> 
							</form> 
								 </div>                                           
									    </div>
 
						  </div>
						</div>
				
				


			</div>


			<!-- END OF SLIDER -->			
			
			

			<div class="col-md-4 right">

			
				
				
				
				

			
			</div>

		</div>
		<!-- END OF container-->
	
	</div>


    
 

<?php $this->load->view("include/footer"); ?>



