<?php
session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class="active"><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="#"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="#"  class="tehnical">AMilanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Privacy and Security Tips</p><div>&nbsp;</div>
                                <hr/>
								<div class="row">
									<div class="col-md-12">
										<p>Milanrishta.com aims to provide you with a safe & secure environment in which you search and find your life partner.</p>
										<ul class="faq">
											<li>Our Customer Relations team ensures that every profile put up atMilanrishta.com is screened for irrelevant and/or inappropriate content.</li>
											<li>We also have strict abuse-prevention and reporting systems for those that do get through our screening systems.</li>
										</ul>
										<p>However, in ensuring your safety & privacy, we're limited to actions that are within our control. Therefore, it is necessary for you to exercise some simple precautions for your privacy & for a safe and secure experience.
										<br />Here are some simple guidelines YOU can follow to protect your privacy.</p>
										<p><strong>1. Guard your anonymity</strong></p>
											Our system of anonymous contacting ( sending invitation to connect, accepting/declining), and private messaging (Communicate section) is to ensure that your identity is protected until YOU decide to reveal it.
										<p><strong>Do</strong></p>
											<ul class="faq">
												<li>Remember that you are in control of your online experience at all times. You can remain completely anonymous until YOU choose not to.</li>
											</ul>
											
										<p><strong>Don't</strong></p>
											<ul class="faq">
												<li>Don't include your personal contact information like name, email address, home address, telephone numbers, place of work or any other identifying information in your initial messages.</li>
												<li>Don't send contact information until your instincts tell you that this is someone you can trust. It's okay to take your time.</li>
											</ul>
											
										<p><strong>2.  Start slow - Use emails initially</strong></p>
											Set up a separate email account for communicating. Trust your instincts and start by sharing only your email address and communicating solely via email.
										<p><strong>Do</strong></p>
											<ul class="faq">
												<li>Look for odd behavior or inconsistencies. Serious people will respect your space and allow you to take your time.</li>
												<li>Ask a friend to read the emails you receive - an unbiased observer can spot warning signs you missed.</li>
												<li>Stop communicating with anyone who pressures you for personal information or attempts in any way to trick you into revealing it.</li>
											</ul>
										<p><strong>Don't</strong></p>
											<ul class="faq">
												<li>Don't use signature lines in your emails that include your phone numbers and addresses.</li>
												<li>Don't use your regular or official email id for communicating with a person you don't know well.</li>
											</ul>
										
										<p><strong>3. Request for a photo</strong></p>
											A photo will give you a good idea of the person's appearance, which may prove helpful in achieving a gut feeling.
										<p></strong>Do</strong></p>
											<ul class="faq">
												<li>You can use the Photo Request option onMilanrishta.com. SinceMilanrishta.com offers free scanning & upload services to its members, there's no reason someone shouldn't be able to provide you a photo.</li>
												<li>In fact, it's best to view several images of someone in various settings: casual, formal, indoor and outdoors. If all you hear are excuses about why you can't see a photo, consider that he or she has something to hide.</li>
											</ul>
										
										<p><strong>4. Chat on the phone</strong></p>
											A phone call can reveal much about a person's communication and social skills. Consider your security and do not reveal your personal phone number to a stranger.
										<p><strong>Do</strong></p>
											<ul class="faq">
												<li>Use a pre paid mobile phone number or use local telephone blocking techniques to prevent your phone number from appearing in Caller ID. Only furnish your phone number when you feel completely comfortable and have some background information on the other person.</li>
												<li>If someone gives you a phone number with a strange area code, check it out to make sure it's not a charge number before you make the call.</li>
											</ul>
										
										<p><strong>5. Meet when YOU are ready</strong></p>
											The beauty of meeting online is that you can collect information gradually, later choosing whether to pursue the relationship in the offline world. You never are obligated to meet anyone. Go at your own pace!
										<p><strong>Do</strong></p>
										<ul class="faq">
												<li>Remember that you are in control when it comes to taking an online relationship offline.</li>
												<li>Even if you decide to arrange a meeting, you always have the right to change your mind. It's possible that your decision to keep the relationship at the anonymous level is based on a hunch that you can't logically explain. Trust yourself. Go with your instincts.</li>
											</ul>
										
										<p><strong>6. Meet in a safe place</strong></p>
											When you choose to meet offline it is a good idea to try and include either or both of your families. But if you trust the person enough to meet alone always tell a friend or your family where you are going and when you will return.
										<p><strong>Do</strong></p>
											<ul class="faq">
												<li>For the first meeting it is always good not to meet the other person alone. Take a friend or relative along and ask him/her to do the same.</li>
												<li>In case you do decide to meet him/her alone leave the name, address and telephone number of the person you are going to meet with your friend or family member. Take a cellular phone along with you.</li>
											</ul>
										<p><strong>Don't</strong></p>
											<ul class="faq">
												<li>Never arrange for your prospective match to pick you up or drop you at home.</li>
												<li>Do not go to a secluded place or a movie alone at the first meeting.</li>
											</ul>
											
										<p><strong>7. Watch for warning signs</strong></p>
											Watching for warning signs and acting upon it is the surest way to avoid an uncomfortable situation.
										<p><strong>Do</strong></p>
										<ul class="faq">
												<li>Ask a lot of questions and watch for inconsistencies. This will help you detect liars & cons, and it will help you find out if you're compatible.</li>
												<li>Pay attention to displays of anger, intense frustration or attempts to pressure or control you. Acting in a resentful manner, making demeaning or disrespectful comments, or any physically inappropriate behavior are all warning signs.</li>
												<li>Involve your family or your close friends in your search for a life partner and do not take a decision unilaterally.</li>
											</ul>
										<p><strong>Don't</strong></p>
											<ul class="faq">
												<li>Don't ignore the following behavior specially if it is without an acceptable explanation:</li>
												<li>Provides inconsistent information about age, interests, appearance, marital status, profession, employment, etc.</li>
												<li>Fails to provide direct answers to direct questions</li>
												<li>Appears significantly different in person from his or her online persona</li>
												<li>Never introduces you to friends, professional associates or family members</li>
											</ul>
										
										<p><strong>8. Beware of money scams</strong></p>
											Watch out for money scams. There are just too many con artists and scam artists around the world, and they are everywhere.
										<p><strong>Do</strong></p>
											<ul class="faq">
												<li>Be wary of those who try to ask money from you for whatever reason. But it would be safer to cut off the communication. Remember a genuine person will not ask you for money in any circumstance. If someone asks you for money, use common sense and never give in to such requests.</li>
												<li>In case someone asks you for money report the situation to us.</li>
											</ul>
										<p><strong>Don't</strong></p>
											<ul class="faq">
												<li>Take all the time you need to decide on a trustworthy person and pay careful attention along the way. Be responsible about romance, and don't fall for the oldest con tricks of people who shower love and affection at the first instance and disappear later. Don't become prematurely close to someone, even if that intimacy only occurs online.</li>
											</ul>
										<p>Using your own good judgment is your best bet because ultimately you are responsible for your personal experience. Trust your instincts and then move ahead with the right person!</p>
										<p>If you do have an unpleasant experience you can always report it to the authorities.</p>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div><div>&nbsp;</div><div>&nbsp;</div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading panel_heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>
