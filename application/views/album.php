<?php $this->load->view("include/header"); 
$photos = $this->action_model->GetAlbum($this->uri->segment(3));
?>
	
<div class="container breadcrub">
	<div class="brlines"></div>
</div>	

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Photo album</h1>
		</div>
		  <?php if($photos != "failure"){
				for($i=0;$i<count($photos);$i++){?>
		<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			<div data-toggle="modal" data-target="#myModal" id="myModal_<?=$photos[$i]['Id'];?>" class="thumbnail" style="background-image:url('<?php echo WEB_DIR; ?>images/profiles/<?=$photos[$i]['ProfilePic'];?>');background-repeat: no-repeat;background-position: center;height:175px;" align="center"></div>
		</div>
		<?php } 
		  }else{ ?>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;No photos available</p>
		<?php } ?>
	</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header" style="border-bottom:0px;background:none;">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
	</div>
	<div class="modal-body" align="center">
	  
	</div>
	
  </div>
  
</div>
</div>
</div> 
<?php $this->load->view("include/footer"); ?>
<script>
	$("document").ready(function(){
		$(".thumbnail").click(function(){
				var id= $(this).attr("id");
				$.ajax({
					url:"<?php echo WEB_URL; ?>home/GetImages",
					type:"post",
					data:{id:id},
					success:function(data){
						$(".modal-body").html(data);
					}
				});
		
		});
	});
</script>