<?php $this->load->view("include/header");
$id = $this->session->userdata('User_Id'); 
$Gender = $this->session->userdata('Gender');
$UserDetails = $this->action_model->getpart_table_deatils("userprofile","User_Id",$id);
 ?>
<style>
.tab-pane{margin:25px;}
.btn .caret {margin-left: 0;float: right;margin-top: 10px;}
.multiselect{width: 450px; height: 34px; text-align: left;color: #999;  padding: 6px 0px 6px 12px; border: 2px solid #ebebeb; background-color: #ffffff;}
</style>

<script type="text/javascript" src="<?php echo WEB_DIR; ?>dist/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo WEB_DIR; ?>dist/css/bootstrap-multiselect.css" type="text/css"/>		

<div class="container">
<!--  Contact Us Field  -->
<!--  Contact Us Field  -->
	<div class="container breadcrub">
	    
		<div class="brlines"></div>
	</div>	

	<!-- CONTENT -->
	<div class="container">
		<div class="container pagecontainer offset-0">	

			<!-- SLIDER -->
			<div class="col-md-8 pagecontainer2 offset-0">
				<div class="cstyle10"></div>
		
				<ul class="nav nav-tabs" id="myTab">
					<li  class="index active"><a data-toggle="tab" href="#index"><span class="rates"></span><span class="hidetext">Basic Search</span>&nbsp;</a></li>
					<li  class="advance"><a data-toggle="tab" href="#advance"><span class="preferences"></span><span class="hidetext">Advanced Search</span>&nbsp;</a></li>
					<li  class="astro"><a data-toggle="tab" href="#astro"><span class="maps"></span><span class="hidetext">Astro Search</span>&nbsp;</a></li>
					<li  class="special"><a data-toggle="tab" href="#special"><span class="maps"></span><span class="hidetext">Special Case Search</span>&nbsp;</a></li>
				</ul>			
				<div class="tab-content4">
					<div id="index" class="tab-pane fade active in">
						<form id="basicform" action="<?php echo WEB_URL;?>home/search_match" name="page2" method="post" class="form-horizontal">
						<div class="form-group">
							<label class="form-label col-sm-4"> Age From</label>
							<div class="col-sm-4">
							<select class="form-control" name="ageFrom">	
							<option value="">Select age from</option>							
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>
							</select>
							</div>
							<div class="col-sm-4">
							
							<select class="form-control" name="ageTo">	
								<option value="">Select age To</option>								
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
						
						<div class="form-group">
							<label class="form-label col-sm-4"> Height From</label>
							<div class="col-sm-4">
							<select class="form-control" name="heightFrom">	
							<?php  $HeightFrom = $this->action_model->get_table_details('HeightList');	?>
							<option value="">Select Height From</option>								
								<?php 
								foreach($HeightFrom as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
							<div class="col-sm-4">
							<select class="form-control" name="heightTo">	
							<option value="">Select Height To</option>	
							<?php  $HeightTo = $this->action_model->get_table_details('HeightList'); ?>
							<option value="">Select Height From</option>								
								<?php 
								foreach($HeightTo as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
						
						<div class="form-group">
							<label class="form-label col-sm-4">Marital Status</label>
							<div class="col-sm-8">
							<select class="form-control" name="marital_status[]" id="marital_status" multiple>			
								<option value="">Select marital status</option>
								<option value="Never married" selected>Never married</option>
								<option value="Divorced">Divorced</option>
								<option value="Widowed">Widowed</option>
								<option value="Awaiting_Divorced">Awaiting Divorced</option>
								<option value="Annulled">Annulled</option>  
							</select>
							</div>
						</div>
						
						<div class="form-group" style="display:none;" id="children">
							<label class="form-label col-sm-4">Have Children</label>
							<div class="col-sm-8">
							<label for="children-"><input type="radio" name="children" id="children-" value="" checked="checked" class="rad_btn">Doesn't Matter</label> <label for="children-No"><input type="radio" name="children" id="children-No" value="No" class="rad_btn">No</label> <label for="children-NoYesNotlivingtogether"><input type="radio" name="children" id="children-NoYesNotlivingtogether" value="No|Yes. Not living together" class="rad_btn">Ok. As long as they don't live together</label>
							</div>
						</div>
						
						<div class="form-group">
							<label class="form-label col-sm-4">Select Religion</label>
							<div class="col-sm-8">
							<?php  $religion = $this->action_model->get_table_details('religionlist'); ?>
							<select class="form-control" name="religion[]" id="religion" onchange="GetList('religion','community','Religion_Id','communitylist','Community_Id','Community_Name')" multiple>	
								<option value="">Select religion</option>
								<?php 
								foreach($religion as $value){?>
								<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="form-label col-sm-4">Select Mother Tongue</label>
							<div class="col-sm-8">
							<?php  $language = $this->action_model->get_table_details('languagelist'); ?>
							<select class="form-control" name="mother_tongue[]" id="mother_tongue" multiple>			
								<?php 
								foreach($language as $value){?>
								<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>
						
						<div class="form-group" id="community_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select Community</label>
							<div class="col-sm-8">
							
							<select class="form-control" name="community[]" id="community" multiple>			
								
							</select>
							</div>
						</div>							
						
						<div class="form-group">
							<label class="control-label col-sm-4" for="email">Living Country</label>
							<?php     $country = $this->action_model->get_table_details('countrylist'); ?>
							<div class="col-sm-8">
								<select class="form-control" name="CountryLivingIn[]" id="living_in"  onchange="GetList('living_in','LivingState','Country_Id','statelist','State_Id','State_Name')" multiple>
									<option value="">Doesn't Matter</option>
									<?php foreach($country as $country){ ?>
									<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
									<?php } ?>
								 </select>
							</div>
							</div>		

						<div class="form-group" id="LivingState_Dev" style="display:none;">
						<label class="control-label col-sm-4" for="email">Living State</label>
						<div class="col-sm-8">
							<select class="form-control" name="LivingState[]" id="LivingState" onChange="GetList('LivingState','LivingCity','State_Id','citylist','City_Id','City_Name')" multiple>
								
							
							</select>
						</div>
						</div>		

						<div class="form-group" id="LivingCity_Dev" style="display:none;">
						<label class="control-label col-sm-4" for="email">Living City</label>
						<div class="col-sm-8">
							<select class="form-control" name="LivingCity[]" id="LivingCity" multiple>
							
											  
							</select>
						</div>
						</div>	
						
						<input class="btn btn-primary" name="submit" id="submit" tabindex="5" value="Search" type="submit" > 	 
						</form> 
					</div>
					<!-- TAB 1 -->
								
					<div id="advance" class="tab-pane fade">
							<form id="advanceform" action="<?php echo WEB_URL;?>home/search_match" name="page2" method="post" class="form-horizontal">
							<div class="form-group">
							<label class="form-label col-sm-4"> Age From</label>
							<div class="col-sm-4">
							<select class="form-control" name="ageFrom">	
							<option value="">Select age from</option>							
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>
							</select>
							</div>
							<div class="col-sm-4">
							
							<select class="form-control" name="ageTo">	
								<option value="">Select age To</option>								
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4"> Height From</label>
							<div class="col-sm-4">
							<select class="form-control" name="heightFrom">	
							<option value="">Select Height From</option>								
								<?php  $HeightFrom1 = $this->action_model->get_table_details('HeightList'); 
								foreach($HeightFrom1 as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
							<div class="col-sm-4">
							<select class="form-control" name="heightTo">	
							<option value="">Select Height To</option>	
								<?php  $HeightTo1 = $this->action_model->get_table_details('HeightList'); 
								foreach($HeightTo1 as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4">Marital Status</label>
							<div class="col-sm-8">
							<select class="form-control" name="marital_status[]" id="marital_status1" multiple>			
								<option value="">Select marital_status</option>
								<option value="Never married">Never married</option>
								<option value="Divorced">Divorced</option>
								<option value="Widowed">Widowed</option>
								<option value="Awaiting_Divorced">Awaiting Divorced</option>
								<option value="Annulled">Annulled</option>  
							</select>
							</div>
						</div>
						<div class="form-group" style="display:none;" id="children1">
							<label class="form-label col-sm-4">Have Children</label>
							<div class="col-sm-8">
							<label for="children-"><input type="radio" name="children" id="children-1" value="" checked="checked" class="rad_btn">Doesn't Matter</label> <label for="children-No"><input type="radio" name="children" id="children-No" value="No" class="rad_btn">No</label> <label for="children-NoYesNotlivingtogether"><input type="radio" name="children" id="children-NoYesNotlivingtogether" value="No|Yes. Not living together" class="rad_btn">Ok. As long as they don't live together</label>
							</div>
						</div>
						
									
						<div class="form-group">
							<label class="form-label col-sm-4">Select Religion</label>
							<div class="col-sm-8">
							<?php  $religion = $this->action_model->get_table_details('religionlist'); ?>
							<select class="form-control" name="religion[]" id="religion1" onchange="GetList('religion1','community1','Religion_Id','communitylist','Community_Id','Community_Name')" multiple>			
								<option value="">Select religion</option>
								<?php 
								foreach($religion as $value){?>
								<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group">
							<label class="form-label col-sm-4">Select Mother Tongue</label>
							<div class="col-sm-8">
							<?php  $language = $this->action_model->get_table_details('languagelist'); ?>
							<select class="form-control" name="mother_tongue[]" id="mother_tongue1" multiple>			
								<?php 
								foreach($language as $value){?>
								<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group" id="community1_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select Community</label>
							<div class="col-sm-8">
							
							<select class="form-control" name="community[]" id="community1" multiple>			
								
							</select>
							</div>
						</div>							
						<div class="form-group">
							<label class="form-label col-sm-4">Select Country</label>
							<div class="col-sm-8">
							<?php  $country = $this->action_model->get_table_details('countrylist'); ?>
							<select class="form-control"  name="LivingCountry[]" id="LivingCountry1" onchange="GetList('LivingCountry1','LivingState1','Country_Id','statelist','State_Id','State_Name')" multiple>			
								<option value="">Select Country</option>
								<?php 
								foreach($country as $country){?>
								<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>		
						  <div class="form-group" id="LivingState1_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select State</label>		
							<div class="col-sm-8">					
							<select class="form-control" name="LivingState[]" id="LivingState1" onchange="GetList('LivingState1','LivingCity1','State_Id','citylist','City_Id','City_Name')" multiple>
								<option value="">Select State</option>
							</select>
							</div>
						</div>
						 <div class="form-group" id="LivingCity1_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select City</label>		
							<div class="col-sm-8">		
							<select name="LivingCity[]" id="LivingCity1" multiple>
							<option value="">Select City</option>
							</select>
							</div>
						</div>	 
							<div class="form-group">
								<label class="control-label col-sm-4" for="residencyStatus">Work Area</label>
								<?php  $spec = $this->action_model->get_table_details('workingas'); ?>
								<div class="col-sm-8">
									<select class="form-control" name="professionArea[]" id="edu_field" multiple>
										<option value="">Doesn't Matter</option>
										<?php 
										foreach($spec as $value){?>
										<option value="<?php echo $value->Work_Id; ?>"><?php echo $value->Work_Name;?></option>    
										<?php } ?>
									</select>
								</div>
							 </div>
							 <div class="form-group">					
								<label class="control-label col-sm-4" for="residencyStatus">Body Type</label>
								<div class="col-sm-8"> 
									<?php $bodyType = array("slim","average","Athletic","heavy"); ?>
										<select name="body_type[]" id="body_type" class="form-control" multiple>
											<?php foreach($bodyType as $value){ ?>
												<option value="<?=$value;?>"><?=$value;?></option>
											<?php } ?>
										</select>
										
								</div>
							</div>
							<div class="form-group">					
								<label class="control-label col-sm-4" for="residencyStatus">Skin Tone</label>
								<select name="skin_tone[]" id="skin_tone" class="form-control" multiple>
									<?php $SkinTone = array("very fair","fair","Black","Wheatish"); 
										foreach($SkinTone as $value){ 
											echo '<option value="'.$value.'">'.$value.'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">					
								<label class="control-label col-sm-4" for="residencyStatus">Diet</label>
								<div class="col-sm-8"> 
									<select name="diet[]" id="diet" class="form-control" multiple>
									<?php $DietList = array("veg","Non veg","Occasionally Non-Veg","Eggetarian","Jain"); 
										foreach($DietList as $value){ 
											echo '<option value="'.$value.'">'.$value.'</option>';
										}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">					
								<label class="control-label col-sm-4" for="residencyStatus">Smoke</label>
								<div class="col-sm-8"> 
									<input type="radio" name="smoke" value="1">Yes
									<input type="radio" name="smoke" value="2">No
									<input type="radio" name="smoke" value="1,2">Doesn't Matter
								</div>
							</div>	
							<div class="form-group">					
								<label class="control-label col-sm-4" for="residencyStatus">Drinks</label>
								<div class="col-sm-8"> 
									<input type="radio" name="drink" value="1">Yes
									<input type="radio" name="drink" value="2">No
									<input type="radio" name="drink" value="1,2">Doesn't Matter
								</div>
							</div>	
											
							<input class="btn btn-primary" name="submit" id="submit" tabindex="5" value="Search" type="submit" > 	 
							</form>     
							
					</div>
					<!-- TAB 2 -->		
					
														
					<div id="astro" class="tab-pane fade ">
						<form id="astroform" action="<?php echo WEB_URL;?>home/search_match" name="page2" method="post" class="form-horizontal">
						<div class="form-group">
							<label class="form-label col-sm-4">Find Matches based on</label>
							<div class="col-sm-8">
								<input type="radio" name="Dosham" value="1" checked>Ashtakoota
								<input type="radio" name="Dosham" value="2">Dashakoota
								<input type="radio" name="Dosham" value="1,2">Does't matter
							</div>
						</div>				
						<div class="form-group">
							<label class="form-label col-sm-4">Looking for</label>
							<div class="col-sm-8">
								<input checked="checked" value="2" type="radio" name="gender">
								<label style=" margin-left: 10px; "  >Bride</label>
								<input  value="1" type="radio" name="gender">
								<label >Groom</label>
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4">Select Mother Tongue</label>
							<div class="col-sm-8">
							<?php  $language = $this->action_model->get_table_details('languagelist'); ?>
							<select class="form-control" name="mother_tongue[]" id="mother_tongue2" multiple>			
								<?php 
								foreach($language as $value){?>
								<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group">
							<label class="form-label col-sm-4">Select Religion</label>
							<div class="col-sm-8">
							<?php  $religion = $this->action_model->get_table_details('religionlist'); ?>
							<select class="form-control" name="religion[]" id="religion2" onchange="GetList('religion2','community2','Religion_Id','communitylist','Community_Id','Community_Name')" multiple>			
								<?php 
								foreach($religion as $value){?>
								<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
						<div class="form-group" id="community2_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select Community</label>
							<div class="col-sm-8">
							
							<select class="form-control" name="community[]" id="community2" multiple>			
								
							</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4">Select Country</label>
							<div class="col-sm-8">
							<?php  $country = $this->action_model->get_table_details('countrylist'); ?>
							<select class="form-control"  name="LivingCountry[]" id="LivingCountry3" onchange="GetList('LivingCountry3','LivingState3','Country_Id','statelist','State_Id','State_Name')" multiple>			
								<option value="">Select Country</option>
								<?php 
								foreach($country as $country){?>
								<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>		
						  <div class="form-group" id="LivingState3_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select State</label>		
							<div class="col-sm-8">					
							<select class="form-control" name="LivingState[]" id="LivingState3" onchange="GetList('LivingState3','LivingCity3','State_Id','citylist','City_Id','City_Name')" multiple>
								<option value="">Select State</option>
							</select>
							</div>
						</div>
						 <div class="form-group" id="LivingCity3_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select City</label>		
							<div class="col-sm-8">		
							<select name="LivingCity[]" id="LivingCity3" multiple>
							<option value="">Select City</option>
							</select>
							</div>
						</div>	 
						
						<div class="form-group">
							<label class="form-label col-sm-4"> Age From</label>
							<div class="col-sm-4">
							<select class="form-control" name="ageFrom">	
							<option value="">Select age from</option>							
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>							   
							</select>
							</div>
							<div class="col-sm-4">
							
							<select class="form-control" name="ageTo">	
								<option value="">Select age To</option>								
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>	
							</select>
							</div>
						</div>
										
					<div class="form-group">
							<label class="form-label col-sm-4">Height From</label>
							<div class="col-sm-4">
							<select class="form-control" name="heightFrom">	
							<option value="">Select Height From</option>								
								<?php  $HeightFrom2 = $this->action_model->get_table_details('HeightList'); 
								foreach($HeightFrom2 as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
							<div class="col-sm-4">
							<select class="form-control" name="heightTo">	
							<option value="">Select Height To</option>	
								<?php  $HeightTo2 = $this->action_model->get_table_details('HeightList'); 
								foreach($HeightTo2 as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
										
						<div class="form-group">
							<label class="form-label col-sm-4">Marital Status</label>
							<div class="col-sm-8">
							<select class="form-control" name="marital_status[]" id="marital_status2" multiple>			
								<option value="">Select marital status</option>
								<option value="Never married" selected>Never married</option>
								<option value="Divorced">Divorced</option>
								<option value="Widowed">Widowed</option>
								<option value="Awaiting_Divorced">Awaiting Divorced</option>
								<option value="Annulled">Annulled</option>  
							</select>
							</div>
						</div>	
						   
						<input class="btn btn-primary" name="submit" id="submit" tabindex="5" value="Search" type="submit" > 	 
						</form> 
					</div>		
					<!-- TAB 3 -->	
					
					<div id="special" class="tab-pane fade ">
						<form id="specialcaseform" action="<?php echo WEB_URL;?>home/search_match" name="page2" method="post" class="form-horizontal">
						<div class="form-group">
							<label class="form-label col-sm-4">Select Disability </label>
							<div class="col-sm-8">
								<input type="radio" name="disability" value="1">Yes
								<input type="radio" name="disability" value="2" checked>No
								<input type="radio" name="disability" value="1,2">Both
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4">HIV</label>
							<div class="col-sm-8">
								<input type="radio" name="HIV" value="1">Yes
								<input type="radio" name="HIV" value="2" checked>No
								<input type="radio" name="HIV" value="1,2" checked>Both
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4">Looking For</label>
							<div class="col-sm-8">
								<input checked="checked" value="2" type="radio" name="gender">
								<label style=" margin-left: 10px;">Bride</label>
								<input  value="1" type="radio" name="gender">
								<label >Groom</label>
							</div>
						</div>	

						<div class="form-group">
							<label class="form-label col-sm-4">Select Mother Tongue</label>
							<div class="col-sm-8">
							<?php  $language = $this->action_model->get_table_details('languagelist'); ?>
							<select class="form-control" name="mother_tongue[]" id="mother_tongue3" multiple>			
								<?php 
								foreach($language as $value){?>
								<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group">
							<label class="form-label col-sm-4">Select Religion</label>
							<div class="col-sm-8">
							<?php  $religion = $this->action_model->get_table_details('religionlist'); ?>
							<select class="form-control" name="religion[]" id="religion3" onchange="GetList('religion3','community3','Religion_Id','communitylist','Community_Id','Community_Name')" multiple >			
								<?php 
								foreach($religion as $value){?>
								<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
						<div class="form-group" id="community3_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select Community</label>
							<div class="col-sm-8">
							
							<select class="form-control" name="community[]" id="community3" multiple>			
								
							</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4">Select Country</label>
							<div class="col-sm-8">
							<?php  $country = $this->action_model->get_table_details('countrylist'); ?>
							<select class="form-control"  name="LivingCountry[]" id="LivingCountry2" onchange="GetList('LivingCountry2','LivingState2','Country_Id','statelist','State_Id','State_Name')" multiple>			
								<option value="">Select Country</option>
								<?php 
								foreach($country as $country){?>
								<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>		
						  <div class="form-group" id="LivingState2_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select State</label>		
							<div class="col-sm-8">					
							<select class="form-control" name="LivingState[]" id="LivingState2" onchange="GetList('LivingState2','LivingCity2','State_Id','citylist','City_Id','City_Name')" multiple>
							<option value="">Select State</option>
							</select>
							</div>
						</div>
						 <div class="form-group" id="LivingCity2_Dev" style="display:none;">
							<label class="form-label col-sm-4">Select City</label>		
							<div class="col-sm-8">		
							<select name="LivingCity2[]" id="LivingCity2" multiple>
							<option value="">Select City</option>
							</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="form-label col-sm-4"> Age From</label>
							<div class="col-sm-4">
							<select class="form-control" name="ageFrom">	
							<option value="">Select age from</option>							
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>							   
							</select>
							</div>
							<div class="col-sm-4">
							
							<select class="form-control" name="ageTo">	
								<option value="">Select age To</option>								
								<?php 
								for($i=18;$i<100;$i++){?>
								<option value="<?=$i;?>"><?=$i;?></option>    
								<?php } ?>	
							</select>
							</div>
						</div>
										
					<div class="form-group">
							<label class="form-label col-sm-4"> Height From</label>
							<div class="col-sm-4">
							<select class="form-control" name="heightFrom">	
							<option value="">Select Height From</option>								
								<?php  $HeightFrom3= $this->action_model->get_table_details('HeightList'); 
								foreach($HeightFrom3 as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
							<div class="col-sm-4">
							<select class="form-control" name="heightTo">	
							<option value="">Select Height To</option>	
								<?php  $HeightTo3 = $this->action_model->get_table_details('HeightList'); 
								foreach($HeightTo3 as $value){?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>
							</select>
							</div>
						</div>	
										
						<div class="form-group">
							<label class="form-label col-sm-4">Marital Status</label>
							<div class="col-sm-8">
							<select class="form-control" name="marital_status[]" id="marital_status3" multiple>			
								<option value="">Select marital_status</option>
								<option value="Never married" selected>Never married</option>
								<option value="Divorced">Divorced</option>
								<option value="Widowed">Widowed</option>
								<option value="Awaiting_Divorced">Awaiting Divorced</option>
								<option value="Annulled">Annulled</option>  
							</select>
							</div>
						</div>					  
						<input class="btn btn-primary" name="submit" id="submit" tabindex="5" value="Search" type="submit" > 	 
						 </form>     
					</div>		
					<!-- TAB 4 -->						
				</div>
			</div>
			<!-- END OF SLIDER -->			
			<!-- RIGHT INFO -->
			<?php $this->load->view("include/sidebar"); ?>
			<!-- END OF RIGHT INFO -->
			
		</div>
		<!-- END OF container-->
	</div>
</div>
<script>
$(document).ready(function(){
	$("#marital_status,#marital_status1").change(function(){
		if($("#marital_status").val() != "Never married"){
			$("#children").css("display","block");
		}else{
			$("#children").css("display","none");
		}
		if($("#marital_status1").val() != "Never married"){
			$("#children1").css("display","block");
		}else{
			$("#children1").css("display","none");
		}
	});

$('#religion,#religion1,#religion2,#religion3,#LivingState,#LivingState1,#LivingState2,#LivingState3,LivingCity,#LivingCity1,#LivingCity2,#LivingCity3').multiselect();
$('#community,#community1,#community2,#community3,#living_in,#LivingCountry1,#LivingCountry2,#LivingCountry3,#edu_field,#body_type,#diet,#skin_tone').multiselect();
$("#mother_tongue,#mother_tongue1,#mother_tongue2,#mother_tongue3,#marital_status,#marital_status1,#marital_status2,#marital_status3").multiselect();

// You don't need to care about these methods
function adjustByHeight() {
	var $body   = $('body'),
		$iframe = $body.data('iframe.fv');
	if ($iframe) {
		// Adjust the height of iframe when hiding the picker
		$iframe.height($body.height());
	}
}

function adjustByScrollHeight() {
	var $body   = $('body'),
		$iframe = $body.data('iframe.fv');
	if ($iframe) {
		// Adjust the height of iframe when showing the picker
		$iframe.height($body.get(0).scrollHeight);
	}
}
});
function GetList(currentdev,nextdev,column,table,display1,display2){
	 var val = $("#"+currentdev).val();
	 if(val != ""){
		$.ajax({
			url: "<?php echo WEB_URL; ?>home/GetJsonList",
			async: false,	
			data: {column:column , value : JSON.stringify(val),table:table,display1:display1,display2:display2},
			type: "POST",
			success: function(data) {
				console.log(data);
				$("#"+nextdev+"_Dev").css('display','block');
				$("#"+nextdev).html(data);
				$("#"+nextdev).multiselect({includeSelectAllOption: true});
				$("#"+nextdev).multiselect('rebuild');
			}
		  });
	 }
}
</script>
<?php $this->load->view("include/footer"); ?>
