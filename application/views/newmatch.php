<style type="text/css">
    .more{cursor:pointer;}
	.ContentDev{height:200px; overflow-y:auto;}
</style>
<?php $this->load->view("include/header");
$id = $this->session->userdata('User_Id'); 
$Gender = $this->session->userdata('Gender');
$PartnerDetails =  $this->action_model->full_partnerprofile($id);
 ?>
<div class="container">
	<div class="container breadcrub">
	    
		<div class="brlines"></div>
	</div>	

	<!-- CONTENT -->
	<div class="container">
	<!--  Contact Us Field  -->
<!--  Contact Us Field  -->
		<div class="container pagecontainer offset-0">	
			
			<!-- SLIDER -->
			<div class="col-md-9 pagecontainer2 offset-0">
				<div class="cstyle10"></div>
		
				<ul class="nav nav-tabs" id="myTab">
					<li onclick="mySelectUpdate()" class="col-md-3 match_padding"><a href="<?php echo WEB_URL; ?>home/match"><span class="rates"></span><span class="hidetext">Preferred Matches</span>&nbsp;</a></li>
					<li onclick="mySelectUpdate()" class="col-md-3 active match_padding"><a href="<?php echo WEB_URL; ?>home/NewMatch"><span class="preferences"></span><span class="hidetext">New Matches</span>&nbsp;</a></li>
					<li onclick="loadScript()" class="col-md-3 match_padding"><a href="<?php echo WEB_URL; ?>home/broaderMatch"><span class="maps"></span><span class="hidetext">broader Matches</span>&nbsp;</a></li>
					<li onclick="loadScript()" class="match_padding col-md-3"><a href="<?php echo WEB_URL; ?>home/maybe"><span class="maps"></span><span class="hidetext">Maybe List</span>&nbsp;</a></li>
					
				</ul>			
				<div class="tab-content4">
				<div id="FilterResult"></div>		
					
					<!-- TAB 3 -->					
					<div id="FirstResult" class="tab-pane fade active in padding20">
					
						<?php 
						$result1=$this->action_model->preferred_match($id,$Gender,'new_match');
					
						 if($result1 == 'failure'){ ?>
							<div class="alert alert-info">
							  No matches found !
							</div>
							
						<?php }else{  
						foreach($result1['result'] as $value){
							
						if ($this->session->userdata('fb_login')) { 
							$pimg = $value['ProfilePic'];
						}else{
							if($value['ProfilePic'] == ""){
								$ppic ="no-profile.gif";
							}else{
								$ppic = $value['ProfilePic'];
							}
							
							$pimg = WEB_DIR."images/profiles/".$ppic;
						} 
						$datediff = time() - strtotime($value['DateCreated']);
						 $days =  floor($datediff/(60*60*24));
						 if($days <= 2 ){
							$date_created = "day";
						 }elseif($days <= 7){
							$date_created = "week";
						 }elseif($days <= 30){
							$date_created = "month";
						 }else{
							$date_created = "all";
						 }
						 if($value['MaritalStatus'] == "Never married"){$value['MaritalStatus'] ="Never_married";}
						?>
						<div class="padding10 match_pagesview <?=$value['MaritalStatus'];?> <?php echo 'religion_'.$value['Religion'];?> <?=$date_created; ?> " id="result_<?=$value['User_Id'];?>">
							<div class="col-md-2 text-center  offset-0">
								<a href="#"><img src="<?php echo $pimg; ?>" alt="" class="fwimg img-thumbnail img-thumbnail" width="80px" height="80px"></a>
							</div>
							<div class="col-md-10 offset-0">
								<div class="col-md-12">
									<div class="col-md-9">
										<h4 class="opensans dark bold margtop1 lh1"><?=$value['Name'];?></h4>
										<?php $DOB = $this->action_model->birthda($value['DOB']);
echo isset($DOB) ? $DOB." Years ," : "";?> 
<?php echo isset($value['Community_Name']) ? $value['Community_Name']."," : ""; ?>
<?php $HeightValue = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$value['Height']);
echo isset($HeightValue[0]->Height) ? $HeightValue[0]->Height."," : ""; ?>
<?php echo isset($value['SubCommunity_Name']) ? $value['SubCommunity_Name']."," : ""; ?>
<?php echo isset($value['Language_Name']) ? $value['Language_Name'] : ""; ?>
										<p class="hpadding20" style="height: 48px;overflow: hidden;"><?=$value['About'];?></p>
										<a href="<?= WEB_URL; ?>home/profile/<?= $value['User_Id']; ?>"
                                               target="_blank" style="color:#428BCA;font-weight:bold;">Read More</a>
									</div>
									<div class="col-md-3 connection_div text-center">
										<p class="text-center"> Connect with her?</p>
										<a href="<?=WEB_URL;?>home/profile/<?=$value['User_Id'];?>" target="_blank" class="mb-10 btn btn-block btn-success connect_user">View Profile</a>
										<a href="<?=WEB_URL;?>home/profile/<?=$value['User_Id'];?>" target="_blank" class="btn btn-danger connect_user_no">No</a>
										<a href="<?=WEB_URL;?>home/profile/<?=$value['User_Id'];?>" target="_blank" class="btn btn-info connect_user_maybe"> Maybe</a>										
									</div>	
								</div>									
							</div>
							<div class="clearfix"></div>
						</div>
						
						<?php }
						} ?>		
					</div>
					
					
					
					<!-- TAB 6 -->						
				</div>
			</div>
			<!-- END OF SLIDER -->			
			<div class="col-md-3" style="margin-top: -20px;">
				<div class="pagecontainer2 mt20 needassistancebox" style="border:none;">
					<div class="list-group">
						<a href="#" class="list-group-item active" style="text-align:center;"> Refine Search</a>
						
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo" style="font-weight:bold;">Photo Settings
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="PhotoSetting" value="all"  > All </p>
								<p><input type="checkbox" name="PhotoSetting" value="10"  > Visible to all</p>
								<p><input type="checkbox" name="PhotoSetting" value="8"  > Protected photos </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo1" style="font-weight:bold;">Recently joined
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo1" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="RecentlyJoined"  value="all"  class="FilterCheck"  > All</p>
								<p><input type="radio" name="RecentlyJoined"  value="day"  class="FilterCheck"> Within a day </p>
								<p><input type="radio" name="RecentlyJoined"  value="week" class="FilterCheck"> Within a week </p>
								<p><input type="radio" name="RecentlyJoined"  value="month" class="FilterCheck"> Within a month </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo2" style="font-weight:bold;">Recently Viewed
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo2" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="RecentlyViewed" class="FilterCheck" value="Unviewed"> Unviewed Matches</p>
								<p><input type="radio" name="RecentlyViewed"  value="Viewed" class="FilterCheck"> Viewed Matches </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo3" style="font-weight:bold;">Marital Status
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo3" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="MaritalStatus" class="FilterCheck"   value="all"> All </p>
								<p><input type="checkbox" name="MaritalStatus" class="FilterCheck"   value="Never married"> Never Married </p>
								<p><input type="checkbox" name="MaritalStatus" class="FilterCheck"   value='Divorced'> Divorced </p>
								<p><input type="checkbox" name="MaritalStatus" class="FilterCheck"   value='Widowed'> Widowed </p>
								<p><input type="checkbox" name="MaritalStatus" class="FilterCheck"   value='Awaiting_Divorced'>Awaiting Divorced </p>
								<p><input type="checkbox" name="MaritalStatus" class="FilterCheck"   value='Annulled'>Annulled</p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo4" style="font-weight:bold;">Active Members
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo4" class="in refine_search" style="padding: 10px;">
								<p><input type="radio" name="ActiveMembers" class="FilterCheck"   value="all"> All</p>
								<p><input type="radio" name="ActiveMembers"  value="day" class="FilterCheck"> Within a day </p>
								<p><input type="radio" name="ActiveMembers"  value="week" class="FilterCheck"> Within a week </p>
								<p><input type="radio" name="ActiveMembers"  value="month" class="FilterCheck"> Within a month </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo5" style="font-weight:bold;">Annual Income
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo5" class="in refine_search" style="padding: 10px;">
								<ul>
									<li><input type="checkbox" name="AnnualIncome" class="FilterCheck"   value="all"> All</li>
									<?php $AnualIncome=  array("Upto INR 1 Lakh","INR 1 Lakh to 2 Lakh","INR 2 Lakh to 4 Lakh","INR 4 Lakh to 7 Lakh",
											"INR 7 Lakh to 10 Lakh","INR 10 Lakh to 15 Lakh","INR 15 Lakh to 20 Lakh","INR 20 Lakh to 30 Lakh","INR 30 Lakh to 50 Lakh",
											"INR 50 Lakh to 75 Lakh","INR 75 Lakh to 1 Crore","Dont want to specify");
											
										if($PartnerDetails != "" && $PartnerDetails[0]['AnualIncome'] != ""){
											$anualIncome = explode(",",$PartnerDetails[0]['AnualIncome']);
											foreach($AnualIncome as $value){
												if(in_array($value,$anualIncome)){$check = "checked";}else{$check ="";}
									?>
													<li><input type="checkbox" name="AnnualIncome" class="FilterCheck" <?=$check;?>  value="<?=$value;?>"><?=$value;?></li>
									<?php 	} 
									}else{ 
										foreach($AnualIncome as $value){
											echo '<li><input type="radio" name="AnnualIncome" class="FilterCheck"   value="'.$value.'">'.$value.'</li';
										} 
									} ?>
								</ul>
								<p class="more"> more + </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev" id="ContentDevFilter1">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo6" style="font-weight:bold;">Religion
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							
							  <div id="demo6" class="in refine_search " style="padding: 10px;">
						
								<ul>
									<li><input type="checkbox" name="Religion" class="FilterCheck"   value="all"> All</li>
									<?php $religion = $this->action_model->get_table_details('religionlist');
										if(isset($post['religion']) != "" && $post['religion'] != ""){
											
											foreach($religion as $value){
												if(in_array($value->Religion_Id,$post['religion'])){$check = "checked";}else{$check ="";}
									?>
													<li><input type="checkbox" name="Religion"  class="FilterCheck" <?=$check;?>  value="<?=$value->Religion_Id;?>"><?=$value->Religion_Name;?></li>
									<?php 	} 
									}else{ 
									foreach($religion as $value){
										echo '<li><input type="checkbox" name="Religion" class="FilterCheck"   value="'.$value->Religion_Id.'">'.$value->Religion_Name.'</li';
									} 
									} ?>
								</ul>
								<p class="more"> more + </p>
							 </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev" id="ContentDevFilter2">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo7" style="font-weight:bold;">Community
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo7" class="in refine_search" style="padding: 10px;">
							  <ul>
								<li><input type="checkbox" name="Community" class="FilterCheck"   value="all"> All</li>
								   <?php $com = $this->action_model->get_table_details('communitylist');
									if(isset($post['community']) != "" && $post['community'] !=""){
									
									foreach($com as $values){
										if(in_array($values->Community_Id,$post['community'])){$check = "checked";}else{$check ="";}
										 echo '<li><input type="checkbox" name="Community" class="FilterCheck" '.$check.'  value="'.$values->Community_Id.'"> '.$values->Community_Name.'</li> ';
								
									 }}else{
									 foreach($com as $values){
										echo '<li><input type="checkbox" name="Community" class="FilterCheck"  value="'.$values->Community_Id.'"> '.$values->Community_Name.'</li> ';
								 
									 }} 
									?>
								</ul>
								<p class="more"> more +</p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo8" style="font-weight:bold;">Mother Tongue
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo8" class="in refine_search" style="padding: 10px;">
								 <ul>
									<li><input type="checkbox" name="MotherTongue" class="FilterCheck"   value="all"> All</li>
								   <?php  $language = $this->action_model->get_table_details('languagelist'); 
									if(isset($post['mother_tongue']) != "" && $post['mother_tongue'] !=""){
									
									foreach($language as $value){
										if(in_array($value->Language_Id,$post['mother_tongue'])){$check = "checked";}else{$check ="";}
										 echo '<li><input type="checkbox" name="MotherTongue" class="FilterCheck" '.$check.'  value="'.$value->Language_Id.'"> '.$value->Language_Name.'</li> ';
								
									 }}else{
									 foreach($language as $value){
										echo '<li><input type="checkbox" name="MotherTongue" class="FilterCheck"  value="'.$value->Language_Id.'"> '.$value->Language_Name.'</li> ';
								 
									 }} 
									?>
								</ul>
								<p class="more"> more +</p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo9" style="font-weight:bold;">Country Living In
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo9" class="in refine_search" style="padding: 10px;">
							  <ul>
									<li><input type="checkbox" name="CountryLivingIn" class="FilterCheck"   value="all"> All</li>
								 
								 <?php $countrys = $this->action_model->get_table_details('countrylist'); 
								 
									if(isset($post['CountryLivingIn']) != "" && $post['CountryLivingIn'] !=""){
									foreach($countrys as $value){
										if(in_array($value->Country_Id,$post['CountryLivingIn'])){$check = "checked";}else{$check ="";}
										 echo '<li><input type="checkbox" name="CountryLivingIn" class="FilterCheck" '.$check.'  value="'.$value->Country_Id.'"> '.$value->Country_Name.'</li> ';
								
									 }}else{
									 foreach($countrys as $value){
										echo '<li><input type="checkbox" name="CountryLivingIn" class="FilterCheck"  value="'.$value->Country_Id.'"> '.$value->Country_Name.'</li> ';
								 
									 }} 
									?>
									</ul>
								<p class="more"> more +</p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo10" style="font-weight:bold;">Country Grew up In
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo10" class="in refine_search" style="padding: 10px;">
								<ul>
									<li><input type="checkbox" name="GrewUp" class="FilterCheck"   value="all"> All</li>
								 
								 <?php $countrys = $this->action_model->get_table_details('countrylist'); 
								 
									if(isset($post) && $PartnerDetails[0]['CountryGrewUp'] !=""){
										$CountryGrewUp = explode(",",$PartnerDetails[0]['CountryGrewUp']);
									foreach($countrys as $value){
										if(in_array($value->Country_Id,$CountryGrewUp)){$check = "checked";}else{$check ="";}
										 echo '<li><input type="checkbox" name="GrewUp" class="FilterCheck" '.$check.'  value="'.$value->Country_Id.'"> '.$value->Country_Name.'</li> ';
								
									 }}else{
									 foreach($countrys as $value){
										echo '<li><input type="checkbox" name="GrewUp" class="FilterCheck"  value="'.$value->Country_Id.'"> '.$value->Country_Name.'</li> ';
								 
									 }} 
									?>
									</ul>
								<p class="more"> more +</p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;" class="ContentDev">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo11" style="font-weight:bold;">Education
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo11" class="in refine_search" style="padding: 10px;">
									<ul>
									<li><input type="checkbox" name="Education" class="FilterCheck"   value="all"> All</li>
								 
								 <?php $degree = $this->action_model->get_table_details('degree');
								 
									if($PartnerDetails != "" && $PartnerDetails[0]['Education'] !=""){
									$Education = explode(",",$PartnerDetails[0]['Education']);
									foreach($degree as $value){
										if(in_array($value->Degree_Id,$Education)){$check = "checked";}else{$check ="";}
										 echo '<li><input type="checkbox" name="Education" class="FilterCheck" '.$check.'  value="'.$value->Degree_Id.'"> '.$value->Degree_Name.'</li> ';
								
									 }}else{
									 foreach($degree as $value){
										echo '<li><input type="checkbox" name="Education" class="FilterCheck"  value="'.$value->Degree_Id.'"> '.$value->Degree_Name.'</li> ';
								 
									 }} 
									?>
									</ul>
								<p class="more"> more +</p>
							  </div>
						</div>
						
						<div style="border: 1px solid #BCBCBC;" class="ContentDev">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo13" style="font-weight:bold;">Profession Area
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo13" class="in refine_search" style="padding: 10px;">
								<ul>
									<li><input type="checkbox" name="ProfessionArea" class="FilterCheck"   value="all"> All</li>
								 
								 <?php $spec = $this->action_model->get_table_details('workingas');
								 
									if(isset($post['working_as']) != "" && $post['working_as'] !=""){
									foreach($spec as $value){
										if(in_array($value->Work_Id,$post['working_as'])){$check = "checked";}else{$check ="";}
										 echo '<li><input type="checkbox" name="ProfessionArea" class="FilterCheck" '.$check.'  value="'.$value->Work_Id.'"> '.$value->Work_Name.'</li> ';
								
									 }}else{
									 foreach($spec as $value){
										echo '<li><input type="checkbox" name="ProfessionArea" class="FilterCheck"  value="'.$value->Work_Id.'"> '.$value->Work_Name.'</li> ';
								 
									 }} 
									?>
								</ul>
								<p class="more"> more +</p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo14" style="font-weight:bold;">Profile Created By
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo14" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="CreatedBy" class="FilterCheck"   value="all"> All</p>
								<p><input type="checkbox" name="CreatedBy"  value="sibling" class="FilterCheck"> Sibling </p>
								<p><input type="checkbox" name="CreatedBy"  value="parents" class="FilterCheck"> Parents/Gaurdian </p>
								<p><input type="checkbox" name="CreatedBy"  value="self" class="FilterCheck"> Self </p>
								<p><input type="checkbox" name="CreatedBy"  value="other" class="FilterCheck"> Other </p>
								<p><input type="checkbox" name="CreatedBy"  value="friends" class="FilterCheck"> Friend </p>
							  </div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo15" style="font-weight:bold;">Smoking
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo15" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="Smoking" class="FilterCheck"   value="1"> Yes</p>
								<p><input type="checkbox" name="Smoking"  value="2" class="FilterCheck"> Non-Smoking </p>
							</div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo16" style="font-weight:bold;">Drinking
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo16" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="Drinking"  value="2" class="FilterCheck"> Never Drinks </p>
								<p><input type="checkbox" name="Drinking"  value="1" class="FilterCheck"> Drinks Occasional </p>
							</div>
						</div>
						<div style="border: 1px solid #BCBCBC;">
							<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div data-toggle="collapse" data-target="#demo17" style="font-weight:bold;">Eating Habits
								<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
							</div></a>
							  <div id="demo17" class="in refine_search" style="padding: 10px;">
								<p><input type="checkbox" name="Diet" class="FilterCheck" value="all"> All</p>
								<p><input type="checkbox" name="Diet"  value="veg" class="FilterCheck"> Veg </p>
								<p><input type="checkbox" name="Diet"  value="Non veg" class="FilterCheck"> Non veg </p>
								<p><input type="checkbox" name="Diet"  value="Occasionally Non-Veg" class="FilterCheck"> Occasionally Non-Veg </p>
								<p><input type="checkbox" name="Diet"  value="Eggetarian" class="FilterCheck"> Eggetarian </p>
								<p><input type="checkbox" name="Diet" value="Jain" class="FilterCheck"> Jain </p>
							</div>
						</div>
				</div>
			
				</div>
				</div>
			
		</div>
		<!-- END OF container-->
		
		
		
		
	</div>

  

<?php $this->load->view("include/footer"); ?>
<script>
$('input[type="checkbox"]').on('change', function(e) {
    var data = {}; 
    var dataStrings = []; 

    $('input[type="checkbox"]').each(function() { 
        if (this.checked) { 
            if (data[this.name] === undefined) data[this.name] = []; 
            data[this.name].push(this.value); 
        }
    });

	$.ajax({
		url: "<?php echo WEB_URL; ?>home/SearchFilter",
		data:data,
		type: "POST",
		success: function(data) {
			$('#broader').css('display','none');
			$('#FilterResult').html(data);
		}
	  });
});
$(document).ready(function(){
    $('.ContentDev').each(function () {
        var foo = $(this);
        $(this).find('ul li:gt(4)').hide();
        $(this).find('.more').click(function () {
            var last = $('ul', foo).children('li:visible:last');
            last.nextAll(':lt(5)').show();
        });
	});
});
</script>