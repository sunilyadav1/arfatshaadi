<?php
	session_start();
	$this->load->view("include/header");
	$user_id = $this->session->userdata('User_Id');
	$EmailSmsAlert = $this->action_model->getpart_table_deatils("emailsalertssms","User_Id",$user_id);
	
?>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/bootstrap-editable.css">
	<script src="<?php echo WEB_DIR;?>assets/js/angular.min.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/bootstrap-editable.js"></script>
	<script src="<?php echo WEB_DIR;?>dist/js/bootstrap.js"></script>
	
<div class="container">
    <div>&nbsp;</div>
    <!--  BreadCrumb for User Profile  -->
    <ol class="breadcrumb">
        <li><?php echo anchor('home/','Home');?></li>
        <li><?php echo anchor('home/', $full_name);?></li>
        <li class="active">Privacy Settings</li>
    </ol>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-2">
						<span href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo" style="font-weight:bold;">Settings
						</div></span>
						  <div class="parrent pull-left">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="<?php echo WEB_URL;?>home/AccountSettings"  class="tehnical">Account Settings</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/ContactFilters"  class="tehnical">Contact Filters</a></li>
									<li class="active"><a href="<?php echo WEB_URL;?>home/EmailAlerts" class="tehnical">Email / SMS Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/privacySetting"  class="tehnical">Privacy Options</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                       <div class="tab-pane fade active in">
					   <form action="<?php echo WEB_URL; ?>home/AddEmailAlerts" method="post">
						<div class="media">
						<input type="hidden" name="id" value="<?php echo isset($EmailSmsAlert[0]->id) ? $EmailSmsAlert[0]->id : 0;?>">
                        <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
							<div class="media-body">
                                <h4>Email - SMS Alerts</h4>
								<small>Manage your subscriptions to MilanRishta Alerts on email listed below. You can also subscribe to MilanRishta</small>
                                <hr/>
                                <h5 class="privacy_email"> My Alerts </h5>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Match Mail & Daily Recommendations Mail</p>
										<small class="">Personalized matches for you delivered via email as often as you like. A very effective match-making tool.</small><br />
										<input type="checkbox" ><small class="mt-2"> Send me Broader Matches if there are no new Preferred Matches</small>
									</div>
									<div class="col-md-5">
									<?php $Daily=""; $Thriceaweek=""; $Weekly=""; $UnSubscribe="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->DialyRecommendationsMail){
											case '1': $Daily="checked=checked"; break;
											case '3': $Thriceaweek="checked=checked"; break;
											case '2': $Weekly="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Daily"><input type="radio" name="DialyRecommendationsMail" id="DialyRecommendationsMail-Daily" value="1" <?=$Daily;?>> Daily</label><br/>
										<label for="checkmate-Thriceaweek"><input type="radio" name="DialyRecommendationsMail" id="DialyRecommendationsMail-Thriceaweek" <?=$Thriceaweek;?> value="3"> Tri-Weekly</label><br/>
										<label for="checkmate-Weekly"><input type="radio" name="DialyRecommendationsMail" id="DialyRecommendationsMail-Weekly" value="2" <?=$Weekly;?>> Weekly</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="DialyRecommendationsMail" id="DialyRecommendationsMail-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Premium Match Mail</p>
										<small class="">An email notification containing your Matches who have upgraded to a Premium Membership.</small>
									</div>
									<div class="col-md-5">
									<?php $Weekly=""; $UnSubscribe="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->PremiumMatchMail){
							
											case '2': $Weekly="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Daily"><input type="radio" name="PremiumMatchMail" id="PremiumMatchMail-Daily" value="1" <?=$Weekly;?>> Weekly</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="PremiumMatchMail" id="PremiumMatchMail-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label><br/>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Recent Visitors Email</p>
										<small class="">An email notification of Members who have recently Viewed your Profile.</small>
									</div>
									<div class="col-md-5">
									<?php $Daily=""; $UnSubscribe="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->RecentVisitorsMail){
							
											case '1': $Daily="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Daily"><input type="radio" name="RecentVisitorsMail" id="RecentVisitorsMail-Daily" value="1" <?=$Daily;?>> Daily</label><br/>
										<label for="checkmate-Thriceaweek"><input type="radio" name="RecentVisitorsMail" id="RecentVisitorsMail-Thriceaweek" value="4" <?=$UnSubscribe;?>> UnSubscribe</label><br/>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Members who Shortlisted you Email</p>
										<small class="">An email notification of Members who have recently Shortlisted your Profile.</small>
									</div>
									<div class="col-md-5">
									<?php $Weekly=""; $UnSubscribe="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->MembersShortlistedMail){
							
											case '2': $Weekly="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Daily"><input type="radio" name="MembersShortlistedMail" id="MembersShortlistedMail-Daily" value="2" <?=$Weekly;?>> Weekly</label><br/>
										<label for="checkmate-Thriceaweek"><input type="radio" name="MembersShortlistedMail" id="MembersShortlistedMail-Thriceaweek" <?=$UnSubscribe;?> value="4"> UnSubscribe</label><br/>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Viewed Profiles Email</p>
										<small class="">An email reminder containing Profiles you have Viewed recently but have not yet invited to Connect.</small>
									</div>
									<div class="col-md-5">
									<?php $Weekly=""; $UnSubscribe="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->ViewedProfilesMail){
							
											case '2': $Weekly="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="ViewedProfilesMail-Daily"><input type="radio" name="ViewedProfilesMail" id="ViewedProfilesMail-Daily" value="2" <?=$Weekly;?>> Weekly</label><br/>
										<label for="ViewedProfilesMail-UnSubscribe"><input type="radio" name="ViewedProfilesMail" id="ViewedProfilesMail-UnSubscribe" <?=$UnSubscribe;?> value="4"> UnSubscribe</label><br/>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Similar Profiles Email</p>
										<small class="">An email containing Profiles that are similar to the ones you have liked recently.</small>
									</div>
									<div class="col-md-5">
									<?php $BiWeekly=""; $UnSubscribe="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->SimilarProfilesMail){
							
											case '5': $BiWeekly="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Daily"><input type="radio" name="SimilarProfilesMail" id="SimilarProfilesMail-Bi-Weekly" value="5" <?=$BiWeekly;?>>Bi-Weekly</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="SimilarProfilesMail" id="SimilarProfilesMail-UnSubscribe" value="4"> UnSubscribe</label><br/>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Contact Alert</p>
										<small class="">Alerts you receive every time someone contacts you or you receive a response to a contact initiated by you. Get them in your mailbox at a frequency of your choice. Essential to keep you informed of all responses received.</small>
									</div>
									<div class="col-md-5">
									<?php $Instant=""; $UnSubscribe=""; $Daily="";
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->ContactAlert){
											case '6': $Instant="checked=checked"; break;
											case '1': $Daily="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Instant"><input type="radio" name="ContactAlert" id="ContactAlert-Instant" value="6" <?=$Instant;?>> Instant - A mail for every response</label><br/>
										<label for="checkmate-Daily"><input type="radio" name="ContactAlert" id="ContactAlert-Daily" value="1" <?=$Daily;?>> Daily - A digest of all responses received in a day</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="ContactAlert" id="ContactAlert-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Email Received Alert</p>
										<small class="">An email notification of new "Email Received" in your milanrishta.com Inbox.</small>
									</div>
									<div class="col-md-5">
									<?php $Instant=""; $UnSubscribe=""; 
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->EmailRecievedAlert){
											case '6': $Instant="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Instant"><input type="radio" name="EmailRecievedAlert" id="EmailRecievedAlert-Instant" value="6" <?=$Instant;?>> Instant</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="EmailRecievedAlert" id="EmailRecievedAlert-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">SMS Alert</p>
										<small class="">All SMS alerts will be sent to you on this mobile phone number - 9876543210</small>
									</div>
									<div class="col-md-5">
									<?php $Instant=""; $UnSubscribe=""; 
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->SMSAlert){
											case '6': $Instant="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">SMS Alert</p>
										<label for="SMSAlert-Instant"><input type="Checkbox" name="SMSAlert" id="SMSAlert-Instant" <?=$Instant;?> value="6"> For every Invitation received (max 2 per/day)</label><br/>
										<label for="SMSAlert-UnSubscribe"><input type="checkbox" name="SMSAlert" id="SMSAlert-UnSubscribe" <?=$UnSubscribe;?> value="4"> For every Accept to my Invitation (max 2 per/day)</label>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">Milanrishta.com Profile Blaster</p>
										<small class="">Perfect matches for you through profile blaster delivered via email as often as you like. The Exact match-making tool.</small>
									</div>
									<div class="col-md-5">
									<?php $Subscribe=""; $UnSubscribe=""; 
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->ProfileBlaster){
											case '7': $Subscribe="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="ProfileBlaster-Subscribe"><input type="radio" name="ProfileBlaster" id="ProfileBlaster-Subscribe" <?=$Subscribe;?> value="7"> Subscribe</label><br/>
										<label for="ProfileBlaster-UnSubscribe"><input type="radio" name="ProfileBlaster" id="ProfileBlaster-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label>
									</div>
								</div>
								<div class="line10"></div>
								 <h5 class="privacy_email"> Milanrishta.com Newsletters </h5>
								 
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">MilanRishta  Specials</p>
										<small class="">Invitations, Discounts, and Offers just for MilanRishta.com members. Offers range from free holidays to discounted jewellery. Delivered not more than twice a month.</small>
									</div>
									<div class="col-md-5">
									<?php $Occasionally=""; $UnSubscribe=""; 
										if($EmailSmsAlert != ""){switch($EmailSmsAlert[0]->MilanRishtaSpecials){
											case '8': $Occasionally="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Occasionally"><input type="radio" name="MilanRishtaSpecials" id="MilanRishtaSpecials-Occasionally" value="8" <?=$Occasionally;?>> Occasionally - Not more than twice a month</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="MilanRishtaSpecials" id="MilanRishtaSpecials-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label>
									</div>
								</div>
								<hr/> 
								<div class="row">
									<div class="col-md-7">
										<p class="size12 bold">MilanRishta InSite</p>
										<small class="">Advice and updates from MilanRishta.com to help you get the most out of your MilanRishta.com experience. A must-have.</small>
									</div>
									<div class="col-md-5">
									<?php $Monthly=""; $UnSubscribe=""; 
										if($EmailSmsAlert != ""	){switch($EmailSmsAlert[0]->MilanRishtaSpecials){
											case '9': $Monthly="checked=checked"; break;
											case '4': $UnSubscribe="checked=checked"; break;
											}}
									?>
										<p class="size12 green bold">Email Alert</p>
										<label for="checkmate-Monthly"><input type="radio" name="MilanRishtaInSite" id="MilanRishtaInSite-Monthly" value="9" <?=$Monthly;?>> Monthly</label><br/>
										<label for="checkmate-UnSubscribe"><input type="radio" name="MilanRishtaInSite" id="MilanRishtaInSite-UnSubscribe" value="4" <?=$UnSubscribe;?>> UnSubscribe</label>
									</div>
								</div>
								<div class="line10"></div>
								<h5 class="privacy_email mt10"> Milanrishta.com Times Newsletters </h5>
								 
								<div class="row">
									<div class="col-md-7">
										<small class="">Great reads on love, being single, fashion trends, celebrates, wedding couture. Also subscribe to newsletters like Fashion Passion, Homemaker, Travelogue and HealthLine.</small>
									</div>
									<div class="col-md-5">
										<p><a href="#">Click here</a> to manage your subscriptions</p>
									</div>
								</div>
								<div class="line10"></div>
								<hr />
								<div class="row text-center">
									<button class="btn-search4"> Submit</button>&nbsp; or&nbsp; <button class="btn-search5"> Reset </button>
								</div>
<div class="line10"></div><div class="line10"></div><div class="line10"></div>								

                            </div>
                        </div>
					 </form>				  
				  </div>
				
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script>
var app = angular.module("app", ["xeditable"]);

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});

app.controller('Ctrl', function($scope) {
  $scope.user = {
    name: 'awesome user'
  };
});
</script>
<script>
$(function () {
  $('[data-toggle="popover"]').popover()
})
</script>
<?php $this->load->view("include/footer"); ?>