<?php session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class=""><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaChat"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class="active"><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaAlerts"  class="tehnical">A Milanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Memberships on Milanrishta.com</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>What are the benefits of a FREE Membership?  
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
											<p>Registration onMilanrishta.com is FREE !!. For absolutely no cost, you can register and create your Profile. Some of the benefits you get as a free Member are:</p>
											<ul class="faq">
												<li>Create your Profile onMilanrishta.com</li>
												<li>Add upto 10 photos to your Profile (privacy options available)</li>
												<li>Add your contact details (privacy options available)</li>
												<li>Add additional Profile information like Family details, Horoscope details, Hobbies/Interests, etc</li>
												<li>Set your Partner Preferences to get the right Matches</li>
												<li>Receive Daily Matches on your Email id</li>
												<li>Search for Profiles using upto 26 parameters like Age, Height, Religion, etc</li>
												<li>Browse Matches as per your Partner Preferences</li>
												<li>Contact / Express Interest in Members you like</li>
												<li>Get Notified and Respond (Accept/Decline) to Members who contact you</li>
												<li>Shortlist the Profiles that you like and view them later</li>
												<li>Ignore Profiles that you don’t like and hide them from your Search results/Matches</li>
											</ul>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>What is a Premium Membership? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										A paid Membership is called a Premium Membership on Milanrishta.com. Premium Members enjoy additional features by which they can promote their Profile on Milanrishta.com to other Members.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>What are the primary benefits of becoming a Premium Member?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										<p>As a Premium Member you can Promote their Profile to other Members in the following ways:-</p>
											<ul class="faq">
												<li>Post your Profile on the Member's Wall and get noticed</li>
												<li>Send an Email with your Profile to the Member's email id</li>
												<li>Connect instantly via Milanrishta Chat and get faster responses</li>
												<li>Send an SMS to the Member's mobile phone</li>
											</ul>
										<p>Becoming a Premium Member increases your chances of getting a response. Our data shows that the finding a partner is 12 times more likely if you upgrade to a Premium Membership.</p>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>What are the different Membership Plans onMilanrishta.com?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											<p>We Have 2 kinds of Services One Premium Plus Membership & Personalized Srvices.</p>
											<p><strong>Premium Plus Plan:</strong> This is subcategorized into Gold Plus (3 months) , Diamond Plus (6 months), Platinum Plus (9months)1 month Charting free in all membership plans...</p>
											<p>Premium Plus Members can additionally promote their Profile via ‘Bold Listing’ and ‘Spotlight’</p>
											<p><strong>Personalized Service:</strong> Which Dedicate a Personal relationship Assistend Who take care of your profile on Behalf of you.& Arrang the meeting with both peoples.</p>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>What is Bold Listing?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											<p>Bold Listing makes your Profile stand out in a Search result page amongst thousands of other Profiles and increases your chances of being contacted.</p>
											<p>Your Profile will be made bold whenever it appears in:</p>
											<ul class="faq">
												<li>Search results.</li>
												<li>Preferred & Broader Matches</li>
												<li>Reverse Matches</li>
												<li>2-way Matches</li>
											</ul>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>What isMilanrishta.com Spotlight?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											TheMilanrishta.com Spotlight features Profiles right on top of relevant Search results. Being featured here increases your chances of being contacted by 20 times!
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										   <strong>7. </strong>What are the additional costs involved to Upgrade to a Premium Membership?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSeven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Prices Listed on Milanrishta.com are inclusive of all taxes viz. Service Tax. There are no additional costs.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
										   <strong>8. </strong>How can I renew / upgrade my Membership? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEight" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Upgrading to a Premium Membership is easy. Click here to view our Membership options and select one to Upgrade!
									  </div>
									</div>
								  </div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div><div>&nbsp;</div><div>&nbsp;</div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading panel_heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



