<?php $this->load->view("include/header");?>
<div class="container cstyle06">
	<div class="login-fullwidith">
		<!-- Login Wrap  -->
		<div class="login-wrap">
			<h3 class="text-center"> Retrieve your password via Email / SMS </h3>
		<form id="wedding-login" method="POST" action="<?php echo base_url();?>index.php/HOME/check_user">
			<div class="login-c1">
				<div class="cpadding50">
				<?php if(isset($_GET['status']) && $_GET['status'] != ""){ ?>
					<label for="username" generated="true" class="error">Wrong login details</label>
				<?php } ?>
				<?php if(isset($success) && $success != ""){ ?>
					<label for="username" generated="true" class="error"><?=$success;?></label>
				<?php } ?>
					<input name="email" id="username" type="email" class="form-control logpadding" placeholder="Email Address">
					<br/>
					<input name="password" id="password" type="password" class="form-control logpadding" placeholder="Password">
				</div>
			</div>
			<div class="login-c2">
				<div class="logmargfix">
					<div class="chpadding50">
							<div class="alignbottom">
								<button class="btn-search4" type="submit">Submit</button>							
							</div>
							<div class="alignbottom2">
							  <div class="checkbox">
								<label>
								  <input type="checkbox">Remember
								</label>
							  </div>
							</div>
					</div>
				</div>
			</div>
			<div class="login-c3">
				<div class="left"><a href="<?php echo WEB_URL; ?>" class="whitelink"><span></span>Home</a></div>
				<div class="right"><a href="<?php echo WEB_URL; ?>HOME/forgot" class="whitelink">Lost password?</a></div>
			</div>
		</form>			
		</div>
		<!-- End of Login Wrap  -->
	</div>
</div>
<?php $this->load->view("include/footer"); ?>
<script>
	  
	  // When the browser is ready...
	  jQuery(document).ready(function($) {
	  
		// Setup form validation on the #register-form element
		$("#wedding-login").validate({
		
			// Specify the validation rules
			rules: {
				email: {
					required: true,
				},
				password: {
					required: true,
				}
			},
			
			// Specify the validation error messages
			messages: {
				email: "Please enter your Email Address",
				password: "Please enter password"
			},
			
			submitHandler: function(form) {
				form.submit();
			}
		});

	  });
	  </script>