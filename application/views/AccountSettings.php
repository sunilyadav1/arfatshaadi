<style>
.EditProfile{cursor:pointer;}
</style>
<?php session_start();
$this->load->view("include/header");
$user_id = $this->session->userdata('User_Id');
$UserDetails = $this->action_model->full_profile($user_id);
?>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/bootstrap-editable.css">
	<script src="<?php echo WEB_DIR;?>assets/js/angular.min.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/bootstrap-editable.js"></script>
	<script src="<?php echo WEB_DIR;?>dist/js/bootstrap.js"></script>
	
<div class="container">
    <div>&nbsp;</div>
    <!--  BreadCrumb for User Profile  -->
    <ol class="breadcrumb">
        <li><?php echo anchor('home/','Home');?></li>
        <li><?php echo anchor('home/', $full_name);?></li>
        <li class="active">Privacy Settings</li>
    </ol>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-2">
						<span href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo" style="font-weight:bold;">Settings
						</div></span>
						  <div class="parrent pull-left">
								<ul class="nav nav-tabs nav-stacked">
									<li class="active"><a href="<?php echo WEB_URL;?>home/AccountSettings"  class="tehnical">Account Settings</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/ContactFilters"  class="tehnical">Contact Filters</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/EmailAlerts" class="tehnical">Email / SMS Alerts</a></li>
									<li><a href="<?php echo WEB_URL;?>home/privacySetting"  class="tehnical">Privacy Options</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h4>Account Settings</h4>
                                <hr/>
								<div id="EmailDiv" class="media">
									<p class="mrltb"><strong>Email ID :</strong> <a href="#" class="privacy_settings"><?=$UserDetails[0]['Email'];?></a> 
									<i class="EditProfile right col-md-1" id="Edit_Email">Edit</i></p>
										<div class="form-group" id="Dev_Email" style="display:none;">
											<label>Change Email Id :</label>
											<input name="UserEmail" id="UserEmail" value="<?=$UserDetails[0]['Email'];?>" type="text" class="form-control">
											
											<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
											<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
										</div>
								</div><hr/>
								<div id="PasswordDiv" class="media">
									<p class="mrltb"><strong>Password :</strong> <a href="#" class="privacy_settings"><?=$UserDetails[0]['Password'];?></a> 
									<i class="EditProfile right col-md-1" id="Edit_Password">Edit</i></p>
										<div class="form-group" id="Dev_Password" style="display:none;">
											<label>Change Email Id :</label>
											<input name="Password" id="Password" value="" type="password" class="form-control">
											<input name="CPassword" id="CPassword" value="" type="password" class="form-control">
											<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
											<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
										</div>
								</div><hr/>
                                <div id="AddressDiv" class="media">
								   <p><strong class="size18 lblue">Address </strong> <i class="EditProfile right col-md-1" id="Edit_Address">Edit</i></p>
								   <div class="form-group" id="Dev_Address" style="display:none;">
										<p><strong>Address Line 1 :</strong> <input type="text" name="Address1" id="Address1" class="form-control privacy_settings" value=""></p>
										<p><strong>Address Line 2 :</strong> <input type="text" name="Address2" id="Address2" class="form-control privacy_settings" value=""></p>
										<p><strong>City :</strong> <input type="text" name="City" id="City" class="form-control privacy_settings" value="<?=$UserDetails[0]['City_Name'];?>"></p>
										<p><strong>State :</strong> <input type="text" name="State" id="State" class="form-control privacy_settings" value="<?=$UserDetails[0]['State_Name'];?>"></p>
										<p><strong>Country :</strong> <input type="text" name="Country" id="Country" class="form-control privacy_settings" value="<?=$UserDetails[0]['Country_Name'];?>"></p>
										<p><strong>Pin/Zip Code :</strong> <input type="text" name="Pincode" id="Pincode" class="form-control privacy_settings" value="<?=$UserDetails[0]['PinNo'];?>"></p><hr/>
										<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
										<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
									</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>

<?php $this->load->view("include/footer"); ?>
<script>
$(".EditProfile").click(function(){
	$(".EditProfile").css('display','none');
	var dev = $(this).attr("id").split("_");
	
	$("#"+"Dev_"+dev[1]).css("display","block");
	
});
$(".reset").click(function(){
	$(".reset").parent().css('display','none');
	$(".EditProfile").css('display','block');
});
$(".Save").click(function(){
	var Address1 = $("#Address1").val();
	var Address2 = $("#Address2").val();
	var City = $("#City").val();
	var State = $("#State").val();
	var Country = $("#Country").val();
	var Password = $("#Password").val();
	var CPassword = $("#CPassword").val();
	var Email = $("#Email").val();
	var Pincode = $("#Pincode").val();
	if(Password != "" && CPassword != ""){
		if(Password != CPassword){
			alert("Password and confirm password should be same");
			return false;
		}
	}
  $.ajax({
	url: "<?php echo WEB_URL;?>home/AddAccountSettings",
	data: {Address1 : Address1,Address2:Address2,City:City,
	State:State,Country:Country,Password:Password,Email:Email,Pincode:Pincode},
	type: "POST",
	success: function(data) {
		location.reload();
	}
  });
});
</script>