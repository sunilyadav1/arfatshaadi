<?php   session_start();
$this->load->view("include/header");
$user_id = $this->session->userdata('User_Id');
$UserDetails = $this->action_model->full_profile($user_id);  
?>
<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>dist/css/fileinput.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	
	<div class="container">
		<div>&nbsp;</div>
		<ol class="breadcrumb">
			<li><?php echo anchor('home/','Home');?></li>
			<li class="active">Pic upload</li>
		</ol>

		<div class="tab-wrap">
			<input id="input-ru" name="input-ru[]" type="file" multiple class="file-loading">
		</div>

	</div>

<?php $this->load->view("include/footer"); ?>

<script src="<?php echo WEB_DIR;?>dist/js/fileinput.js"></script>
	
<script>
$('document').ready(function(){
 $.ajax({
		url:  "<?php echo WEB_URL; ?>home/ListProfilePic", 
		type: "POST",  
		datatype:'json',			
		data: {}, 
		success: function(data)   
		{
			var data = $.parseJSON(data);
			$("#input-ru").fileinput({
				uploadUrl: "<?php echo WEB_URL; ?>home/ImageUpload",
				uploadAsync: false,
				minFileCount: 1,
				maxFileCount: data['maxFileCount'],
				overwriteInitial: false,
				initialPreview: data['initialPreview'],
				initialPreviewConfig: data['initialPreviewConfig'],
				
			});
			$('#input-ru').on('fileuploaded', function(jqXHR) {
				location.reload();
			});
			$("#input-ru").on("filepredelete", function(jqXHR) {
				var abort = true;
				if (confirm("Do you realy want to delete")) {
					abort = false;
					//location.reload();
				}
				return abort;
			});
			
		}
	});  

});
</script>
