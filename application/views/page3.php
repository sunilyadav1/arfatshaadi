<?php $this->load->view("include/header"); ?>
<div class="container cstyle06">
			<h3 class="text-center"> Contact Informations </h3>
		<form id="contactform" action="<?php echo base_url();?>/index.php/HOME/submit_form_page3" name="page2" method="post" enctype="multipart/form-data">
				<div class="w100percent">
					<div class="wh100percent textleft">
						<span class="opensans size13"><b>Select Dosham</b></span>
						<input type="radio" name="Dosham" value="1">Yes
						<input type="radio" name="Dosham" value="2" checked>No
					</div>
				</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Select community</b></span>	
					<?php $state = $this->action_model->get_table_details('communitylist'); ?>
						<select  name="community" class="form-control">
							<option value="select">select commutity</option>
							<?php 
							foreach($state as $values){?>
							<option value="<?php echo $values->Community_Id; ?>"><?php echo $values->Community_Name;?></option>    
							<?php } ?>
						
						</select>
				</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Select subcommunity</b></span>	
					<?php $sub = $this->action_model->get_table_details('subcommunitylist'); ?>
					<select class="form-control" name="subcommunity">
							<option value="select">select subcommuinty</option>
							<?php 
									foreach($sub as $value){?>
									<option value="<?php echo $value->SubCommunityList_Id; ?>"><?php echo $value->SubCommunity_Name;?></option>    
									<?php } ?>
						      
					</select>
				</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Partner Community</b></span>	
					<?php  $state = $this->action_model->get_table_details('communitylist'); ?>
						
					<select class="form-control" name="partner_community">
							<option value="select">select partner commutity</option>
							<?php 
									foreach($state as $values){?>
									<option value="<?php echo $values->Community_Id; ?>"><?php echo $values->Community_Name;?></option>    
									<?php } ?>
						
						</select>
				</div>
			
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>select disablity</b></span>
					<input type="radio" name="disability" value="1">yes
					<input type="radio" name="disability" value="2" checked>no

				</div>
			</div>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>HIV</b></span>
					<input type="radio" name="HIV" value="1">yes
					<input type="radio" name="HIV" value="2" checked>no

				</div>
			</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>About</b></span>
					<textarea rows="4" cols="50" id="about" name="about" class="form-control"> </textarea>
				</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>select your profile picture</b></span>
					<input type="file" name="profile_pic" id="profile_pic" class="form-control">
				</div>
			<input class="buttom" name="submit" id="submit" tabindex="5" value="Register" type="submit" > 
		</form>			
</div>
<?php $this->load->view("include/footer"); ?>
<script>
	  
	  // When the browser is ready...
	  jQuery(document).ready(function($) {
	  
		// Setup form validation on the #register-form element
		$("#wedding-login").validate({
		
			// Specify the validation rules
			rules: {
				email: {
					required: true,
				},
				password: {
					required: true,
				}
			},
			
			// Specify the validation error messages
			messages: {
				email: "Please enter your Email Address",
				password: "Please enter password"
			},
			
			submitHandler: function(form) {
				return false;
				//form.submit();
			}
		});

	  });
	  </script>