<?php session_start();
$this->load->view("include/header");
$user_id = $this->session->userdata('User_Id');
$UserInfo = $this->action_model->full_profile($user_id);
?>
	
<div class="container">
    <div>&nbsp;</div>
    <!--  BreadCrumb for User Profile  -->
    <ol class="breadcrumb">
        <li><?php echo anchor('home/','Home');?></li>
        <li><?php echo anchor('home/', $full_name);?></li>
        <li class="active">Privacy Settings</li>
    </ol>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-2">
						<span href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo" style="font-weight:bold;">Settings
						</div></span>
						  <div class="parrent pull-left">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="<?php echo WEB_URL;?>home/AccountSettings"  class="tehnical">Account Settings</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/ContactFilters"  class="tehnical">Contact Filters</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/EmailAlerts" class="tehnical">Email / SMS Alerts</a></li>
									<li class="active"><a href="<?php echo WEB_URL;?>home/privacySetting"  class="tehnical">Privacy Options</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h4>Privacy Options</h4>
                                <hr/>
                                <div class="well">
									<div id="AboutDiv" class="media">
										<p class="mrltb"><strong>Display Name :</strong> <a href="#" class="privacy_settings">Vijetha</a> <small>(Visible to all) </small>
										<i class="EditProfile right col-md-1" id="Edit_About">Edit</i></p>
											<div class="form-group" id="Dev_About" style="display:none;">
												<label>Display name as :</label>
												<input name="display_name_type" id="display_name_type-partial_name" value="1" class="inputRadio"  type="radio"><span class="partialName_text">Hide my last name <span class="name_example" id="partial_name_example">(<span>Vijetha L</span>)</span></span>
												<br/><input name="display_name_type" id="display_name_type-partial_name_inverse" value="2" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Hide my first name <span class="name_example" id="partial_name_inverse_example">(<span id="partial_name_inverse">L vijetha</span>)</span></span>
												<br/><input name="display_name_type" id="display_name_type-full_name" value="3" class="inputRadio" type="radio"><span class="fullName_text">Displays my full name <span class="name_example" id="full_name_example">(<span id="full_name">Vijetha L</span>)</span></span>
												<br/><input title="" bt-xtitle="" name="display_name_type" id="display_name_type-profile_id" value="4" class="inputRadio"  type="radio"><span class="username_text" id="hide_my_fullname">Hide my full name <span class="name_example">(Displays only Profile ID: <span id="profile_id">SH57743972</span>)</span></span>
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                   <div id="PhoneDiv" class="media">
										<p class="mrltb"><strong>Display Phone :</strong> <a href="#" class="privacy_settings">Visible to all Premium Members</a> <small>(Visible to all) </small>
										<i class="EditProfile right col-md-1" id="Edit_Phone">Edit</i></p>
											<div class="form-group" id="Dev_Phone" style="display:none;">
												<label>Display Phone as :</label>
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Visible to all Premium Members</span>
												<br/><input name="display_Phone_type" value="full_name" class="inputRadio" type="radio"><span class="fullName_text">Not visible to anyone</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                   
									
                                    <div id="PhotoDiv" class="media">
										<p class="mrltb"><strong>Display Photo :</strong> <a href="#" class="privacy_settings">Visible to all Premium Members</a> <small>(Visible to all) </small>
										<i class="EditProfile right col-md-1" id="Edit_Photo">Edit</i></p>
											<div class="form-group" id="Dev_Photo" style="display:none;">
												<label>Display Photo as :</label>
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Visible to all </span>
												<br/><input name="display_Phone_type"  value="partial_name_inverse" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Visible only on Invitation Sent/Accepted</span>
												<br/><input name="display_Phone_type" value="full_name" class="inputRadio" type="radio"><span class="fullName_text">Password protected</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
									
									 <div id="HoroscopeDiv" class="media">
										<p class="mrltb"><strong>Horoscope :</strong> <a href="#" class="privacy_settings">Visible to all Members</a>
										<i class="EditProfile right col-md-1" id="Edit_Horoscope">Edit</i></p>
											<div class="form-group" id="Dev_Horoscope" style="display:none;">
												<label>Display name as :</label>
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Visible to all </span>
												<br/><input name="display_Phone_type"  value="partial_name_inverse" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Visible only on Invitation Sent/Accepted</span>
												<br/><input name="display_Phone_type" value="full_name" class="inputRadio" type="radio"><span class="fullName_text">Hide from all</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                   
                                   <div id="VisitorsDiv" class="media">
										<p class="mrltb"><strong>Visitors settings :</strong> <a href="#" class="privacy_settings">Visible to all Members</a>
										<i class="EditProfile right col-md-1" id="Edit_Visitors">Edit</i></p>
											<div class="form-group" id="Dev_Visitors" style="display:none;">
												<label>Display name as :</label>
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Let other members know that I have visited their profile</span>
												<br/><input name="display_Phone_type"  value="partial_name_inverse" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Do not let other members know that I have visited their profile</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                    <div id="ShortlistDiv" class="media">
										<p class="mrltb"><strong>Shortlist Setting :</strong> <a href="#" class="privacy_settings">Visible to all Members</a>
										<i class="EditProfile right col-md-1" id="Edit_Shortlist">Edit</i></p>
											<div class="form-group" id="Dev_Shortlist" style="display:none;">
												
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Let other members know that I have visited their profile</span>
												<br/><input name="display_Phone_type"  value="partial_name_inverse" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Do not let other members know that I have visited their profile</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                     <div id="DisturbtDiv" class="media">
										<p class="mrltb"><strong>Do Not Disturb :</strong> <a href="#" class="privacy_settings">Shaadi.com can only call me for Premium membership related offers and on behalf of Premium members who wish to Connect with me</a>
										<i class="EditProfile right col-md-1" id="Edit_Disturb">Edit</i></p>
											<div class="form-group" id="Dev_Disturb" style="display:none;">
											
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Shaadi.com can only call me for Premium membership related offers.</span>
												<br/><input name="display_Phone_type"  value="partial_name_inverse" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Shaadi.com can only call me on behalf of Premium members who wish to Connect with me</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                      <div id="ProfileDiv" class="media">
										<p class="mrltb"><strong>Profile Privacy :</strong> <a href="#" class="privacy_settings">Only visible to registered members.</a>
										<i class="EditProfile right col-md-1" id="Edit_Profile">Edit</i></p>
											<div class="form-group" id="Dev_Profile" style="display:none;">
											
												<input name="display_Phone_type"  value="partial_name" class="inputRadio"  type="radio"><span class="partialName_text">Visible to all, including unregistered visitors (Photo and Name will not be visible on profile)..</span>
												<br/><input name="display_Phone_type"  value="partial_name_inverse" class="inputRadio"  type="radio"><span class="partialNameInverse_text">Only visible to registered members.</span>
												
												<input type="submit" name="submit" value="submit" class="btn btn-primary">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
                                  
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>

<?php $this->load->view("include/footer"); ?>
<script>
$(".EditProfile").click(function(){
	$(".EditProfile").css('display','none');
	var dev = $(this).attr("id").split("_");
	
	$("#"+"Dev_"+dev[1]).css("display","block");
	
});
$(".reset").click(function(){
	$(".reset").parent().css('display','none');
	$(".EditProfile").css('display','block');
});
</script>