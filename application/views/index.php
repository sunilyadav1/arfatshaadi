<?php $this->load->view("include/home-header"); ?>
<div class="container cstyle06">	

			<div class="row anim2">
			  <div class="col-md-3">
				<h2>Best matrimony<br/>Stories</h2><br/>
				Find the best matches with the world's best matrimonial site
			  </div>
			  <div class="col-md-9">
			  <!-- Carousel -->
				<div class="wrapper">
					<div class="list_carousel">
						<ul id="foo">
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/stories.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>						
								</div>
							</li>
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/stories1.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>						
								</div>							
							</li>
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/stories2.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>						
								</div>							
							</li>
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/stories3.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>						
								</div>
							</li>
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/stories4.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>						
								</div>							
							</li>
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/stories5.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>
								</div>							
							</li>
                            <li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/wed2.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>
								</div>							
							</li>
							<li>
								<a href="#"><img src="<?php echo WEB_DIR; ?>images/wed3.jpg" alt=""/></a>
								<div class="m1">
									<h6 class="lh1 dark"><b></b></h6>
									<a href=""><h6 class="lh1 green"></h6></a>
								</div>							
							</li>

						</ul>
						<div class="clearfix"></div>
						<a id="prev_btn" class="prev" href="#"><img src="<?php echo WEB_DIR; ?>images/spacer.png" alt=""/></a>
						<a id="next_btn" class="next" href="#"><img src="<?php echo WEB_DIR; ?>images/spacer.png" alt=""/></a>
					</div>
				</div>

			  
			  </div>
			</div>	
		
		</div>

<?php $this->load->view("include/home-footer"); ?>