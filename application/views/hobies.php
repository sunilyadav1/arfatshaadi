 <!-- Bootstrap -->
    <link href="<?php echo WEB_DIR;?>dist/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php echo WEB_DIR;?>assets/css/custom.css" rel="stylesheet" media="screen">
   
	<style>.dropdown a{color: #fff}.notify_dropdown a{color: #666;}</style>
    <!-- Carousel -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
	
    <!-- Fonts -->	
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css'>	
	<!-- Font-Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/font-awesome.css" media="screen" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="assets/css/font-awesome-ie7.css" media="screen" /><![endif]-->
	
 
	<!----online users---->
	<link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/jquery-ui.css" />	
	<link rel="stylesheet" href="<?php echo WEB_DIR; ?>css/shrchat.css">
  
    <!-- jQuery -->	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/jquery.v2.0.3.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/custom.js"></script>
	<script src="<?php echo WEB_DIR;?>assets/js/jquery.cropit.js"></script>
<?php //$this->load->view("include/header");
$user_id = $this->session->userdata('User_Id'); 
?>	

<div class="container breadcrub">
	<div class="brlines"></div>
</div>	

<!-- CONTENT -->
<div class="container">
<!--<div class="row">
	<div class="col-xs-6 col-md-4"></div>
	<div class="col-xs-6 col-md-4"></div>
	<div class="col-xs-6 col-md-4">
		<div class="col-xs-6 col-md-6"><h3 class="opensans" style="font-size:16px;">Need Assistance?</h3></div>
		<div class="col-xs-6 col-md-6"><p class="opensans size30 lblue xslim"
										  style="position: relative; font-weight: 400; font-size: 15px;">
				+91-7259872851 <br/>+91-9342627372</p></div>
	</div>
</div>-->
		
	<div class="container mt25 margbottom20 offset-0">
		<div class="col-md-12  offset-0">
		<div class="cstyle10"></div>
		<ul class="nav nav-tabs" id="myTab">
				<!--<li  class="personal "><a data-toggle="tab" href="<?php echo WEB_URL?>home/edit_profile#personal"><span class="rates"></span><span class="hidetext">Basic Information</span>&nbsp;</a></li>
				<li  class="partner"><a data-toggle="tab" href="<?php echo WEB_URL?>home/edit_profile#partner"><span class="preferences"></span><span class="hidetext">Partner Preferrence</span>&nbsp;</a></li>
				<li  class="partner active"><a data-toggle="tab" href="<?php echo WEB_URL?>home/hobbies"><span class="preferences"></span><span class="hidetext">Partner Preferrence</span>&nbsp;</a></li>-->
		</ul>	
		<div class="tab-content4">
		<div id="personal" class="tab-pane fade active in">
			<form action="<?php echo WEB_URL;?>home/UpdateProfile" method="POST">
			<!-- LEFT CONTENT -->
			<div class="col-md-8 basic_info pagecontainer2 offset-0">
				<div class="padding30 grey">
					<div style="border: 1px solid #BCBCBC;">
						<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo1" style="font-weight:bold;">Hobbies
							<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
						</div></a>
						  <div id="demo1" class="in refine_search" style="padding: 10px;overflow:auto;">
						  <?php $hobbies =  $this->action_model->get_table_details('hobbies');
						  foreach($hobbies as $hobby){?>
							<p class="col-sm-4"><input type="checkbox" name="hobbies" value="<?=$hobby->Id;?>"><?=$hobby->Hobby;?></p>
						  <?php } ?>
						  </div>
					</div>
					<div style="border: 1px solid #BCBCBC;">
						<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo2" style="font-weight:bold;">Interests
							<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
						</div></a>
						  <div id="demo2" class="in refine_search" style="padding: 10px;overflow:auto;">
						  <?php $Interests =  $this->action_model->get_table_details('interests');
						  foreach($Interests as $value){?>
							<p class="col-sm-4"><input type="checkbox" name="interest" value="<?=$value->id;?>"><?=$value->interest;?></p>
						  <?php } ?>
						  </div>
					</div>
					<div style="border: 1px solid #BCBCBC;">
						<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo3" style="font-weight:bold;">Books
							<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
						</div></a>
						  <div id="demo3" class="in refine_search" style="padding: 10px;overflow:auto;">
						  <?php $reads =  $this->action_model->get_table_details('reads');
						  foreach($reads as $value){?>
							<p class="col-sm-4"><input type="checkbox" name="read" value="<?=$value->id;?>"><?=$value->read;?></p>
						  <?php } ?>
						  </div>
					</div>
					<div style="border: 1px solid #BCBCBC;">
						<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo4" style="font-weight:bold;">Movies
							<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
						</div></a>
						  <div id="demo4" class="in refine_search" style="padding: 10px;overflow:auto;">
						  <?php $Movies =  $this->action_model->get_table_details('movies');
						  foreach($Movies as $value){?>
							<p class="col-sm-4"><input type="checkbox" name="Movie" value="<?=$value->id;?>"><?=$value->Movie;?></p>
						  <?php } ?>
						  </div>
					</div>
					<div style="border: 1px solid #BCBCBC;">
						<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo5" style="font-weight:bold;">Musics
							<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
						</div></a>
						  <div id="demo5" class="in refine_search" style="padding: 10px;overflow:auto;">
						  <?php $Musics =  $this->action_model->get_table_details('music');
						  foreach($Musics as $value){?>
							<p class="col-sm-4"><input type="checkbox" name="music" value="<?=$value->id;?>"><?=$value->music;?></p>
						  <?php } ?>
						  </div>
					</div>
					<div style="border: 1px solid #BCBCBC;">
						<a href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo6" style="font-weight:bold;">Sports & Fitness
							<div class="roundstep active right"><i class="glyphicon glyphicon-chevron-down"></i></div>
						</div></a>
						  <div id="demo6" class="in refine_search" style="padding: 10px;overflow:auto;">
						  <?php $sportsfitness =  $this->action_model->get_table_details('sportsfitness');
						  foreach($sportsfitness as $value){?>
							<p class="col-sm-4"><input type="checkbox" name="Sportsfitness" value="<?=$value->id;?>"><?=$value->Sportsfitness;?></p>
						  <?php } ?>
						  </div>
					</div>
				</div>
			</div>
			</form>
		</div>
		
	</div>
	</div>

		<?php //$this->load->view("include/sidebar"); ?>
</div>
</div>
   
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo WEB_DIR; ?>dist/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script src="<?php echo WEB_DIR; ?>js/shrchatrsrkv.js"></script>
<script>

$('input[type="checkbox"]').on('change', function(e) {
    var data = {}; 
    var dataStrings = []; 

    $('input[type="checkbox"]').each(function() { 
        if (this.checked) { 
            if (data[this.name] === undefined) data[this.name] = []; 
            data[this.name].push(this.value); 
        }
    });
console.log(data);
	/**$.ajax({
		url: "<?php echo WEB_URL; ?>home/SearchFilter",
		data:data,
		type: "POST",
		success: function(data) {
			$('#FirstResult').css('display','none');
			$('#FilterResult').html(data);
		}
	  });**/
});
</script>
<?php //$this->load->view("include/footer"); ?>
