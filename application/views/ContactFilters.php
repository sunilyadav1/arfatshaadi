<style>
.EditProfile{cursor:pointer;}
</style>
<?php session_start();
$this->load->view("include/header");
$user_id = $this->session->userdata('User_Id');
$UserDetails =  $this->action_model->full_profile($user_id); 
$PartnerDetails =  $this->action_model->full_partnerprofile($user_id);
?>

<div class="container">
    <div>&nbsp;</div>
    <!--  BreadCrumb for User Profile  -->
    <ol class="breadcrumb">
        <li><?php echo anchor('home/','Home');?></li>
        <li><?php echo anchor('home/', $full_name);?></li>
        <li class="active">Privacy Settings</li>
    </ol>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-2">
						<span href="#" class="list-group-item" style="background-color:#F1F1F1;">
						<div data-toggle="collapse" data-target="#demo" style="font-weight:bold;">Settings
						</div></span>
						  <div class="parrent pull-left">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="<?php echo WEB_URL;?>home/AccountSettings"  class="tehnical">Account Settings</a></li>
									<li class="active"><a href="<?php echo WEB_URL;?>home/ContactFilters"  class="tehnical">Contact Filters</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/EmailAlerts" class="tehnical">Email / SMS Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL;?>home/privacySetting"  class="tehnical">Privacy Options</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <div class="media">
						<?php 
							if($PartnerDetails == ""){ ?>
								<div class="well">
                                    <p class="bold Dblue">No partner information given <a href="<?php echo WEB_URL;?>home/edit_profile">Click here</a> to add partner details</p>
								</div>
						<?php	}else{
						?>
                            <div class="media-body">
                                <h4>Who can contact me?</h4>
                                <hr/>
                                <div class="well">
                                    <p class="bold Dblue">Allow Members to contact me if they meet the following criteria !</p><hr/>
									<div id="AgeDiv" class="media">
									<p class="mrltb"><strong>Age :</strong> <a href="#" class="privacy_settings"><?=$PartnerDetails[0]['AgeFrom']."-".$PartnerDetails[0]['AgeTo'];?></a> 
									<i class="EditProfile right col-md-1" id="Edit_Age">Edit</i></p>
										<div class="form-group" id="Dev_Age" style="display:none;">

												 <div class="form-group">
													<label class="control-label col-sm-4" for="email"> Age From</label>
													<div class="col-sm-8">
													 <select name="ageFrom" id="ageFrom" class="form-control">
														<?php for($i=18;$i<62;$i++){ 
														if($PartnerDetails[0]['AgeFrom'] == $i){$select = "selected=selected";}else{$select="";}?>
															<option value="<?=$i;?>" <?=$select;?>><?=$i;?></option>
														<?php } ?>
													</select>
													</div>
												  </div>  	
												  <br/><br/>
												 <div class="form-group">
													<label class="control-label col-sm-4" for="email"> Age To</label>
													<div class="col-sm-8">
														<select name="ageTo" id="ageTo"  class="form-control">
														<?php for($j=18;$j<62;$j++){ 
														if($PartnerDetails[0]['AgeTo'] == $j){$select = "selected=selected";}else{$select="";}?>
															<option value="<?=$j;?>" <?=$select;?>><?=$j;?></option>
														<?php } ?>
														</select>
													</div>
												  </div>  	  
												  <br/><br/>
											<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
											<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
										</div>
									</div><hr/>
                                   
									<div id="HeightDiv" class="media">
									<?php 				$result =""; $result1="";$res=""; $res1="";
														$HeightFrom = $this->action_model->get_table_details('HeightList');
														foreach($HeightFrom as $value){
															if($PartnerDetails[0]['HeightFrom'] == $value->Height_Id){$res=$value->Height; $select = "selected=selected";}else{$select="";}
															$result.='<option value="'.$value->Height_Id.'" '.$select.'>'.$value->Height.'</option>';    
														  } 
														$HeightTo = $this->action_model->get_table_details('HeightList');
														foreach($HeightTo as $value){
															if($PartnerDetails[0]['HeightTo'] == $value->Height_Id){$res1=$value->Height; $select = "selected=selected";}else{$select="";}
															$result1.='<option value="'.$value->Height_Id.'" '.$select.'>'.$value->Height.'</option>';   
														}
														  
									?>
										<p class="mrltb"><strong>Height :</strong> <a href="#" class="privacy_settings"><?=$res." - ".$res1;?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Height">Edit</i></p>
											<div class="form-group" id="Dev_Height" style="display:none;">
												 <div class="form-group">
													<label class="control-label col-sm-4" for="email"> Height From</label>
													<div class="col-sm-8">
													
														<select name="heightFrom" id="heightFrom" class="form-control">
														
														<option value="">Select Height From</option>								
															
															<?=$result;?>
														</select>
														
													</div>
												  </div> 
												<br/><br/>					  
												 <div class="form-group">
													<label class="control-label col-sm-4" for="email"> Height To</label>
													<div class="col-sm-8">
														<select name="heightTo" id="heightTo" class="form-control">
														
														<option value="">Select Height From</option>								
															<?=$result1;?>
														</select>
														
													</div>
												  </div>  
													<br/><br/>		
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
										</div><hr/>
									
									<div id="ReligionDiv" class="media">
										<p class="mrltb"><strong>Religion :</strong> <a href="#" class="privacy_settings"><?=$PartnerDetails[0]['Religion_Name'];?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Religion">Edit</i></p>
											<div class="form-group" id="Dev_Religion" style="display:none;">
													<label class="control-label col-sm-4" for="email">select partner Religion</label>
													<?php $religion = $this->action_model->get_table_details('religionlist'); ?>
													<div class="col-sm-8">
														<select class="form-control" name="religion" id="religion">
															<option value="">Doesn't Matter</option>
															<?php foreach($religion as $value){
															if($PartnerDetails[0]['Religion_Id'] == $value->Religion_Id){$select = "selected=selected";}else{$select="";}?>
															<option value="<?php echo $value->Religion_Id; ?>" <?=$select;?>><?php echo $value->Religion_Name;?></option>    
															<?php } ?> 
														</select>
													</div>
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
									
									<div id="CommunityDiv" class="media">
										<p class="mrltb"><strong>Community :</strong> <a href="#" class="privacy_settings"><?=$PartnerDetails[0]['Community_Name'];?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Community">Edit</i></p>
											<div class="form-group" id="Dev_Community" style="display:none;">
												<label class="control-label col-sm-4" for="email">select partner Community</label>
												<?php  $com = $this->action_model->get_table_details('communitylist'); ?>
												<div class="col-sm-8">
														<select class="form-control" name="community" id="community">
															<option value="">Doesn't Matter</option>
																	<?php 
																	foreach($com as $values){
																	if($PartnerDetails[0]['Community_Id'] == $value->Community_Id){$select = "selected=selected";}else{$select="";}?>
														
																	<option value="<?php echo $values->Community_Id; ?>" <?=$select;?>><?php echo $values->Community_Name;?></option>    
																	<?php } ?>
														
														</select>
												</div>
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
									
									<div id="MaritalDiv" class="media">
										<p class="mrltb"><strong>Merital status:</strong> <a href="#" class="privacy_settings"><?=$PartnerDetails[0]['MaritalStatus'];?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Marital">Edit</i></p>
											<div class="form-group" id="Dev_Marital" style="display:none;">
												<label class="control-label col-sm-4" for="email">Marital Status</label>
													<div class="col-sm-8">
													<?php $marital_status = array("Never married","Divorced","Widowed","Awaiting_Divorced","Annulled");?>
														<select class="form-control" name="marital_status" id="marital_status">				
															<option value="">Doesn't Matter</option>
															<?php for($i=0;$i<count($marital_status);$i++){
																if($PartnerDetails[0]['MaritalStatus'] == $i){$select = "selected=selected";}else{$select="";}?>
															
																<option value="<?=$i+1;?>" <?=$select;?>><?=$marital_status[$i];?></option>
															<?php } ?> 					  						
														</select>
													</div>
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
									
									<?php $yes="";$no="";$nomatter=""; if($PartnerDetails[0]['Drink'] == 1){$yes ="checked=checked"; $value = "Include profiles who Drinks";}
									else if($PartnerDetails[0]['Drink'] == 2){$no ="checked=checked"; $value = "Include profiles who does not Drinks";}
									else{$nomatter = "checked=checked"; $value ="Does not matter";} ?>
										
								    <div id="DrinkDiv" class="media">
										<p class="mrltb"><strong>Drink :</strong> <a href="#" class="privacy_settings"><?=$value;?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Drink">Edit</i></p>
											<div class="form-group" id="Dev_Drink" style="display:none;">
													<label class="control-label col-sm-4" for="residencyStatus">Drinks</label>
											
													<div class="col-sm-8"> 
														<input type="radio" name="drink" value="1" <?=$yes;?>>Yes
														<input type="radio" name="drink" value="2" <?=$no;?>>No
														<input type="radio" name="drink" value="" <?=$nomatter;?>>Doesn't Matter
													</div>
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
									
									<?php $yes="";$no=""; $nomatter="";if($PartnerDetails[0]['Smoke'] == 1){$yes ="checked=checked"; $value = "Include profiles who smokes";}
									else if($PartnerDetails[0]['Smoke'] == 2){$no ="checked=checked"; $value = "Include profiles who does not smokes";}else{$nomatter = "checked=checked"; $value ="Does not matter";} ?>
													
									<div id="SmokeDiv" class="media">
										<p class="mrltb"><strong>Smoke :</strong> <a href="#" class="privacy_settings"><?=$value;?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Smoke">Edit</i></p>
											<div class="form-group" id="Dev_Smoke" style="display:none;">
													<label class="control-label col-sm-4" for="residencyStatus">Smoke</label>
														
													<div class="col-sm-8"> 
														<input type="radio" name="smoke" value="1" <?=$yes;?>>Yes
														<input type="radio" name="smoke" value="2" <?=$no;?>>No
														<input type="radio" name="smoke" value="" <?=$nomatter;?>>Doesn't Matter
													</div>
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
									
									<?php $yes="";$no=""; $nomatter="";if($PartnerDetails[0]['Disability'] == 1){$yes ="checked=checked"; $value = "Include profiles who has disability";}
									else if($PartnerDetails[0]['Disability'] == 2){$no ="checked=checked"; $value = "Include profiles who does not have Disability";}else{$nomatter = "checked=checked"; $value ="Does not matter";} ?>
													
									<div id="DisabilityDiv" class="media">
										<p class="mrltb"><strong>Disability :</strong> <a href="#" class="privacy_settings"><?=$value;?></a> 
										<i class="EditProfile right col-md-1" id="Edit_Disability">Edit</i></p>
											<div class="form-group" id="Dev_Disability" style="display:none;">
													<label class="control-label col-sm-4" for="residencyStatus">Select Disability</label>
													<div class="col-sm-8"> 
													   <input type="radio" name="disability" value="1" <?=$yes;?>>Yes
														<input type="radio" name="disability" value="2" <?=$no;?>>No
														<input type="radio" name="disability" value="" <?=$nomatter;?>>Doesn't Matter
														
													</div>
												<input type="submit" name="submit" value="submit" class="btn btn-primary Save">
												<input  type="reset" name="reset" value="cancel" class="btn btn-default reset">
											</div>
									</div><hr/>
								 
								</div>
                            </div>
						<?php } ?>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>

<?php $this->load->view("include/footer"); ?>
<script>
$(".EditProfile").click(function(){
	$(".EditProfile").css('display','none');
	var dev = $(this).attr("id").split("_");
	
	$("#"+"Dev_"+dev[1]).css("display","block");
	
});
$(".reset").click(function(){
	$(".reset").parent().css('display','none');
	$(".EditProfile").css('display','block');
});
$(".Save").click(function(){
	var ageFrom = $("#ageFrom").val();
	var ageTo = $("#ageTo").val();
	var heightFrom = $("#heightFrom").val();
	var heightTo = $("#heightTo").val();
	var religion = $("#religion").val();
	var community = $("#community").val();
	var marital_status = $("#marital_status").val();
	var smoke = $("input[name=smoke]:checked").val();
	var drink = $("input[name=drink]:checked").val();
	var disability = $("input[name=disability]:checked").val();
	
  $.ajax({
	url: "<?php echo WEB_URL;?>home/AddContactFilters",
	data: {ageFrom : ageFrom,ageTo:ageTo,heightFrom:heightFrom,heightTo:heightTo,religion:religion,
	community:community,marital_status:marital_status,smoke:smoke,drink:drink,disability:disability},
	type: "POST",
	success: function(data) {
	console.log(data);
		location.reload();
	}
  });
});
</script>