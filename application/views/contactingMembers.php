<?php session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class="active"><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaChat"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="#"  class="tehnical">Milanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Contacting & responding to Members onMilanrishta.com</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>How can I contact other Members onMilanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
										Contacting Members onMilanrishta.com is FREE. You can contact Members that you like by Expressing Interest in their Profiles. These Members will receive an Interest from you, in theirMilanrishta.com Inbox and will additionally receive a notification as well. These Members can choose to respond to your Interest by Accepting or Declining it.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>What additional benefits do I get as a Premium Member?
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										As a Premium Member onMilanrishta.com you can send a “Premium Interest”, i.e. along with your Interest, you can post your Profile on the Member’s Wall and send an Email with your Profile to the Member.
										<div>&nbsp;</div>Additionally you can also initiate Chats via Instant Messenger in order to get faster responses or grab more attention by sending an SMS directly to the Member's mobile phone.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>What is the meaning of Accepting, Declining or Canceling Interest with a Member onMilanrishta.com Matrimonials?   
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Members who Express Interest in you, will appear in your Inbox. We recommend that your respond to these Interests by Accepting or Declining them. Accepting an Interest indicates that you agree to communicate with the Member. Click here to access your Accepted Members list. Declining an Interest indicates that you are not interested in the Member and do not wish to communicate any further.
										<div>&nbsp;</div>While we recommend that you make it a point to respond to all Interests in your Inbox, you can also remove an Interest from your Inbox by Deleting it. In this case, the Member who sent you the Interest will not be notified.
										<div>&nbsp;</div>In some cases, you may have second thoughts after Expressing Interest in a Member. In such cases, you can Cancel your Interest and stop all further communication.
										<div>&nbsp;</div>You can find all Declined, Cancelled and Deleted Interests in your Deleted folder.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>Can I Decline a Member after having Accepted him/her? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes, you can Decline a Member that you have Accepted.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>Can I Accept a Member after Declining him/her? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes, you can Accept a Member after Declining.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>If I Express Interest in a Member, and the Member Accepts me, will I be able to Cancel the Interest later? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes, you can Cancel your Interest in this Member.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										   <strong>7. </strong>What is the 'My Filters' feature? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSeven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											‘My Filters’ helps you to filter out Members on the basis of multiple criteria like Age, Marital Status, Height, Manglik/Kuja Dosham, Religious Background, Country of Residence, etc. Interests from Members who do NOT meet your Filter criteria will appear in the ‘Filtered out’ folder in your Inbox.
											<div>&nbsp;</div>Note: Setting Filters that are too tight may considerably reduce the number of Interests that you receive in your Inbox. We recommend that you use this feature ONLY if you are receiving Interests by too many Members who don't meet your requirements.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
										   <strong>8. </strong>Can I Express Interest in Members who have Filtered me out?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEight" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes, even if you do not meet the Filter criteria of the Member, you still have a chance to Express Interest in the Member’s Profile. This Interest will appear in the Member’s Filtered out folder and may result in a delayed response.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
										   <strong>9. </strong>The registration form requires information about my time of birth and city of birth. What do I do if I am not sure of these details? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseNine" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											<p>As the name suggests, the Request Photo feature allows you to request Members to add photos to their Profile. To use this feature:</p>
											<ul class="faq">
												<li>Login to Milanrishta.com </li>
												<li>Ensure that your own photo is added to your Profile</li>
												<li>If you see a Profile that doesn’t have a photograph, you can request a photo by clicking on the ‘Request Photo’ button.</li>
												<li>We will convey your request to the Member who will automatically be added to your  Sent Requests folder.</li>
												<li>We will send you an email notification if the Member uploads his/her photo</li>
												<li>Similarly, you can check the Requests tab in your Inbox and respond to Photo Requests that you receive</li>
											</ul>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
										   <strong>10. </strong>How does the "Request Contact Details" feature work? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											<p>The "Request Contact Details" feature allows you to request contact details of Members who have either not entered contact details or have selected not to display their contact details to Premium Members. To use this feature:</p>
											<ul class="faq">
												<li>Login to Milanrishta.com </li>
												<li>If a Member has not Verified his / her phone number Click on the "Request Phone Number" link on a Member's Profile page.</li>
												<li>This Request will appear in the other Member’s Inbox and he/she can choose to approve your Request by Verifying his / her contact details.</li>
												<li>If the Member approves your Request, you will receive an email confirming that the Member has Verified his/her contact details.</li>
												<li>Similarly, if the Member requests your Contact Details, this Request will appear in your Inbox under the ‘Requests’ tab</li>
											</ul>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
										   <strong>11. </strong>I saw a Profile that I like on Milanrishta.com. Can the Customer Support Team of Milanrishta.com help me contact this Member ofMilanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEleven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											While the Customer Relations team of Milanrishta.com strives to facilitate matrimony between its Members, the team is not authorised to contact Members on your behalf. You may want to consider the Personalize  service where a team of experts manage your Profile on your behalf.
									  </div>
									</div>
								  </div>
								  <div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



