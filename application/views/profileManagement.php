<?php  session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class=""><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class="active"><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaChat"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="#"  class="tehnical">AMilanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Profile Management</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>How can I create a new Profile on Milanrishta.com?
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
										Posting your Profile onMilanrishta.com is easy and completely FREE of cost. All you need to do is fill in our FREE Registration form.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>How do I edit my Milanrishta.com Profile?
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Click here to edit your Profile. You can also access this page by clicking on ‘Edit Profile’ from your My Milanrishta page.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>How can I change my Display Name on my Profile? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										You can change your Display Name and change your Display Name settings from the Privacy Options page inside ‘Settings’.
										<div>&nbsp;</div>If you prefer not to display your full name on your Profile, you can choose to display your First Name and initial of last name e.g. Monika M or you can choose to display a custom username.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>Can I change any/all of my Profile information from the Edit Profile page? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											You can change most of the information in your Profile. However, there are some items (e.g. your date of birth, gender, marital status and religion) that can be changed only in exceptional circumstances. To change these fields please write to Customer Relations providing a valid reason for the change.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>Can I change my email after registering with Milanrishta.com and creating my Profile? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Click here to access the ‘My Account's page and edit your email.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>My matrimonial Profile is only partially visible. Why? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Whenever you create a new Profile or you modify certain information in your Profile, the Profile is screened by the Customer Relations team of Milanrishta.com. During the screening process, only your essential information is displayed. Once the screening process is completed (usually within 24 hours) your Profile will be completely visible to everybody.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										   <strong>7. </strong>Will my Profile be searchable online on Google, Yahoo etc ? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSeven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Under normal circumstances it won't be discoverable in search engines but if you have mentioned your name in your profile-about me section then there are chances of profile being discoverable in search engines via full name.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
										   <strong>8. </strong>Can I hide my matrimonial Profile temporarily? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEight" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes you can. To hide your Profile, click here and then click on the Hide / Delete Profile link.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
										   <strong>9. </strong>Can I selectively hide my Profile from only some Members? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseNine" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											No. If you choose to hide your Profile, it will be hidden from all Milanrishta.com Members.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
										   <strong>10. </strong>What is "Activity Factor"? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Activity factor gives an indication of how responsive you are. Maintaining a healthy Activity Factor increases your chances of being contacted by other Members.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
										   <strong>11. </strong>Can I add my hobbies and Interests to my Profile? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEleven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes, you can. Click here to add your Hobbies & Interests now.
											<div>&nbsp;</div>If your specific hobby or Interest is not included, please write to us and we will try to add it to the list.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTweleve">
										   <strong>12. </strong>Is it safe to add my horoscope along with the Profile? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTweleve" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes, you have complete control over your privacy.<div>&nbsp;</div>
											You can choose who you display your horoscope to by either choosing to show it to all Members or to only Members you Accept or Express Interest in. Click here to change your horoscope display settings.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">
										   <strong>13. </strong>Is it necessary to add a photo to my Profile? How can I add photos? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThirteen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Our statistics show that adding a photo to your Profile increases the number of times your Profile is viewed, by upto 7 times. You are also likely to receive 10 times as many responses if you add a Profile photo. You can add upto 10 photos to your Milanrishta.com Profile.
											<div>&nbsp;</div>Click here to add photos to your Profile.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen">
										   <strong>14. </strong> I am now happily married to another Member of Milanrishta.com. Can I share the wonderful news with other Members? How do I go about doing this?  
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFourteen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											We are thrilled that you found your life partner on Milanrishta.com. We would be more than happy to share this wonderful news with other Milanrishta.com Members, on your behalf. Your details will be posted in the Success Stories section of Milanrishta.com. Click here to send us your Success Story.
									  </div>
									</div>
								  </div>
								  <div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading panel_heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



