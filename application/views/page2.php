<?php $this->load->view("include/header"); 
$UserDetails =  $this->action_model->getpart_table_deatils("userprofile","User_Id",$this->session->userdata('User_Id'));
?>
<style>
.multiselect-container {width: 470px; height: 200px; overflow-y: scroll;}
.btn .caret {margin-left: 0;float: right;margin-top: 10px;}
.multiselect{width: 470px; height: 34px; text-align: left;color: #999;  padding: 6px 0px 6px 12px; border: 2px solid #ebebeb; background-color: #ffffff;}
</style>
<script type="text/javascript" src="<?php echo WEB_DIR; ?>dist/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo WEB_DIR; ?>dist/css/bootstrap-multiselect.css" type="text/css"/>	
<div class="container cstyle06">
			<h3 class="text-center"> </h3>
			<div id="check_points">
				<p style="background: none repeat scroll 0% 0% rgb(248, 75, 76);color:white;" id="p_1" onclick="change_div(1)" >1</p>
				<p id="p_2" onclick="change_div(2)">2</p>
				<p id="p_3" onclick="change_div(3)">3</p>
				<p id="p_4" onclick="change_div(4)" >4</p>
				<p id="p_5" onclick="change_div(5)" >5</p>
			</div>
			
			<br/>
			
			<!--First division-->
			
			<div id="div1" class="divisions" >
			<form id="contactinfo" action="#" name="page2" method="post">
				<h3 class="div_name">Location of <?php if($UserDetails[0]->Gender == 2){ echo "Bride";}else{echo "Groom";}?></h3>
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Country</b></span>	
					<?php 
						 $country = $this->action_model->get_table_details('countrylist'); ?>
						 
						<select name="country" id="country" class="form-control">
							<option value="select">select country</option>
							<?php 
							foreach($country as $country){?>
							<option value="<?php echo $country->Country_Id; ?>" ><?php echo $country->Country_Name;?></option>    
							<?php } ?>
				
						</select>
				</div>
				</div>
				
				<div class="w100percent" id="StateList" style="display:none;">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>State</b></span>	
					<?php 
						 $states = $this->action_model->getpart_table_deatils('statelist','Country_Id',$UserDetails[0]->Country); ?>
						<select name="state" id="state" class="form-control">
							<option value="select">select state</option>
							
				
						</select>
				</div>
				</div>
				
				<div class="w100percent" id="CityList" style="display:none;">
					<div class="wh100percent textleft">
						<span class="opensans size13"><b>City</b></span>
							<select class="form-control" name="city" id="city">
						
										  
						</select>
					</div>
				</div>
				<div class="w100percent">
					<div class="wh100percent textleft">
						<span class="opensans size13"><b>Pin Code</b></span>
						<input id="pincode" name="pincode" placeholder="******" class="form-control" type="text" value="<?=$UserDetails[0]->PinNo;?>"> 	
					</div>
				</div>
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Living Country</b></span>	
					<?php 
						 $country = $this->action_model->get_table_details('countrylist'); ?>
						<select name="Livingcountry" id="Livingcountry" class="form-control">
							<option value="">Select living country</option>
							<?php 
							foreach($country as $country){?>
							<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
							<?php } ?>
				
						</select>
				</div>
				</div>
				<div class="w100percent" style="display:none;" id="LivingStateList">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Living State</b></span>
						<select class="form-control" name="LivingState" id="LivingState">
							<option value="">Select Living State</option>
						</select>
				</div>
			</div>
			<div class="w100percent" id="LivingCityList" style="display:none;">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Living City</b></span>
						<select class="form-control" name="LivingCity" id="LivingCity">
							<option value="">Select Living City</option>
					    </select>
				</div>
			</div>
			<input type="submit" value="Save & Next" class="btn-search" style="margin-right: auto; margin-left: auto; margin-top: 27px; display: block; float: right;"   >
			</form>
			</div>
			
			
			
			<!--/First division-->
			
			
			
			<!--Second division-->
			<div id="div2" class="divisions" >
			<h3 class="div_name">Basic Info of <?php if($UserDetails[0]->Gender == 2){ echo "Bride";}else{echo "Groom";}?></h3>
			<form id="basicinfo" action="#" name="page2" method="post" enctype="multipart/form-data">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Marital Status</b></span>	
					<select class="form-control" name="marital_status" id="marital_status" >
						<option value="">select marital status</option>
						<option value="Never married" selected=selected>Never married</option>
						<option value="Divorced">Divorced</option>
						<option value="Widowed">Widowed</option>
						<option value="Awaiting_Divorced">Awaiting Divorced</option>
						<option value="Annulled">Annulled</option>  						  						
					</select>
				</div>
				<div class="wh100percent textleft">
				<?php $communities = $this->action_model->getpart_table_deatils('communitylist','Religion_Id',$UserDetails[0]->Religion); ?>
					<span class="opensans size13"><b>Community</b></span>	
					<select  name="community" id="community" class="form-control mySelectBoxClass">
						<option value="select">Community</option>
						<?php foreach($communities as $community){ ?>
							<option value="<?php echo $community->Community_Id; ?>"><?php echo $community->Community_Name;?></option>  
						<?php } ?>						
					</select>
				</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Height</b></span>	
					<select class="form-control" name="height" id="height">
						<option value="">Select Height</option>
								<?php  $Height = $this->action_model->get_table_details('HeightList'); 
								foreach($Height as $value){ ?>
								<option value="<?php echo $value->Height_Id; ?>" ><?php echo $value->Height;?></option>    
								<?php } ?>  						
					</select>
				</div>
				<div class="w100percent">
					<div class="wh100percent textleft">
						<span class="opensans size13"><b>Weight</b></span>
						<input type="text" value="" name="weight" id="weight" placeholder="enter numbers only" class="form-control">
					</div>
				</div>
				<br/>
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>HIV :</b></span>	
						<input type="radio" name="HIV" value="1">Yes
						<input type="radio" name="HIV" value="2" checked>no 						  						
				</div>
				</div>
				<br/>
				
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Any Disability :</b></span>
					<input type="radio" name="Disability" value="1">Yes
					<input type="radio" name="Disability" value="2" checked>No
					
				</div>
			</div>
			
			
			<br/>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Skin Tone :</b></span>
					<input type="radio" name="skin_tone" value="very fair">Very fair
					<input type="radio" name="skin_tone" value="fair">Fair
					<input type="radio" name="skin_tone" value="black">Black
					<input type="radio" name="skin_tone" value="wheatish" checked>Wheatish

				</div>
			</div>
			<br/>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Body Type :</b></span>
					<input type="radio" name="body_type" value="Slim">Slim
					<input type="radio" name="body_type" value="Average" checked>Average
					<input type="radio" name="body_type" value="Athletic">Athletic
					<input type="radio" name="body_type" value="Heavy">Heavy

				</div>
			</div>
			<br/>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Diet :</b></span>
					<input type="radio" name="diet" value="veg">veg
					<input type="radio" name="diet" value="Non veg">Non veg
					<input type="radio" name="diet" value="Occasionally Non-Veg">Occasionally Non-Veg
					<input type="radio" name="diet" value="Eggetarian">Eggetarian
					<input type="radio" name="diet" value="Jain">Jain

				</div>
			</div>
			<br/>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Smoke :</b></span>
					<input type="radio" name="smoke" value="1">Yes
					<input type="radio" name="smoke" value="2" checked>No
				</div>
			</div>
			<br/>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Drink :</b></span>
					<input type="radio" name="drink" value="1">Yes
					<input type="radio" name="drink" value="2" checked>No
				</div>
			</div>
			<br/>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>About </b></span>
					<textarea name="about" id="about" value="" class="form-control"></textarea>
				</div>
			</div>
			
			<input type="submit" value="Save & Next" class="btn-search" style="margin-right: auto; margin-left: auto; margin-top: 27px; display: block; float: right;"   >
			</form>
			</div>
				
				
			
			<!--/Second division-->
			
			
			<!--Third division-->
			<div id="div3" class="divisions" >
			<h3 class="div_name">Education & Career of <?php if($UserDetails[0]->Gender == 2){ echo "Bride";}else{echo "Groom";}?></h3>
			<form id="educationinfo" action="#" name="page2" method="post">
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Education level</b></span>
					<?php $degree = $this->action_model->get_table_details('degree');?>
					<select class="form-control" name="edu_level" id="edu_level">
						<option value="">select degree</option>
						<?php 
						foreach($degree as $value){?>
						<option value="<?php echo $value->Degree_Id; ?>"><?php echo $value->Degree_Name;?></option>    
						<?php } ?>   
					</select>
				</div>
			</div>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Education Field</b></span>
					<?php $spec = $this->action_model->get_table_details('specialization');?>
					<select class="form-control" name="edu_field" id="edu_field">
						<option value="">select specialization</option>
						<?php 
						foreach($spec as $value){?>
						<option value="<?php echo $value->Id; ?>"><?php echo $value->SName;?></option>    
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>working with</b></span>
					<select class="form-control" name="working_with" id="working_with">
					<option value="">Select working with</option>
					<option value="1">Not working</option>
					<option value="2">Government</option>
					<option value="3">Private</option>
					<option value="4">Self employed</option>
					<option value="5">business</option>
				   </select>
				</div>
			</div>
			<div class="w100percent">
				<div class="wh100percent textleft">
				<?php  $spec = $this->action_model->get_table_details('workingas'); ?>
					<span class="opensans size13"><b>Working As or Business OF</b></span>
					<select class="form-control" name="working_as" id="working_as">
						<option value="Doesn't Matter">Select working as</option>
						<?php 
						foreach($spec as $value){?>
						<option value="<?php echo $value->Work_Id; ?>"><?php echo $value->Work_Name;?></option>    
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="w100percent">
				<div class="wh100percent textleft">
				<?php $AnualIncome=  array("Upto INR 1 Lakh","INR 1 Lakh to 2 Lakh","INR 2 Lakh to 4 Lakh","INR 4 Lakh to 7 Lakh",

						"INR 7 Lakh to 10 Lakh","INR 10 Lakh to 15 Lakh","INR 15 Lakh to 20 Lakh","INR 20 Lakh to 30 Lakh","INR 30 Lakh to 50 Lakh",

						"INR 50 Lakh to 75 Lakh","INR 75 Lakh to 1 Crore","Dont want to specify");
						?>
					<span class="opensans size13"><b>Annual Income</b></span>
					<select class="form-control" name="anual_income" id="anual_income">
					
					<option value="">Select</option>
					<?php for($i=0;$i<count($AnualIncome);$i++){ ?>
						<option value="<?=$AnualIncome[$i];?>" ><?=$AnualIncome[$i];?></option>
					<?php } ?>
				   </select>
				</div>
			</div>
			<input type="submit" value="Save & Next" class="btn-search" style="margin-right: auto; margin-left: auto; margin-top: 27px; display: block; float: right;"  >
			</form>
			</div>
			
			
			
			<!--/Third division-->
			
			<!--Fourth division-->
				<div id="div4" class="divisions" >
				<h3 class="div_name">Family of <?php if($UserDetails[0]->Gender == 2){ echo "Bride";}else{echo "Groom";}?></h3>
			
				<form id="familyinfo" action="#" name="page2" method="post">
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Father's Status</b></span>	
					
						<select class="form-control" name="father_status" id="father_status">
							<option value="">select father's status</option>
							<option value="Employed">Employed</option>
							<option value="Business">Business</option>
							<option value="Professional">Professional</option>
							<option value="Retired">Retired</option>
							<option value="Not Employed">Not Employed</option>
							<option value="Passed Away">Passed Away</option>
				
						</select>
				</div>
				</div>
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Mother's Status</b></span>	
					<select class="form-control" name="mother_status" id="mother_status">
							<option value="">select mother's status</option>
							<option value="Homemaker">Home Maker</option>
							<option value="Employed">Employed</option>
							<option value="Business">Business</option>
							<option value="Professional">Professional</option>
							<option value="Retired">Retired</option>
							<option value="Not Employed">Not Employed</option>
							<option value="Passed Away">Passed Away</option>
				
						</select>
				</div>
				<div class="w100percent">
					<div class="wh100percent textleft">
						<div class="double_drop">
							<span class="opensans size13"><b>No. of Brothers</b></span>	
						<select class="form-control" name="num_bro" id="num_bro">
						<option value="">select no of brothers</option>
								<?php for($i=0;$i<10;$i++){?>
						<option value="<?=$i;?>"><?=$i;?></option>
						<?php } ?>	</select>
						</div>
						
						<div class="double_drop">
							<span class="opensans size13"><b>of which are married</b></span>	
						<select class="form-control" name="num_bro_married" id="num_bro_married">
						<option value="">select no of brothers married</option>
						<?php for($i=0;$i<10;$i++){?>
						<option value="<?=$i;?>"><?=$i;?></option>
						<?php } ?>	</select>
						</div>
						
						
					</div>
				</div>
				
				<div class="w100percent">
					<div class="wh100percent textleft">
						<div class="double_drop">
							<span class="opensans size13"><b>No. of Sisters</b></span>	
						<select  class="form-control" name="num_sis" id="num_sis">
						<option value="">select sisters</option>
						<?php for($i=0;$i<10;$i++){?>
						<option value="<?=$i;?>"><?=$i;?></option>
						<?php } ?>
						</select>
						</div>
						
						<div class="double_drop">
							<span class="opensans size13"><b>of which are married</b></span>	
						<select  class="form-control" name="num_sis_married" id="num_sis_married">
						<option value="">select no of sisters married</option>
								<option value="select"></option>
						<?php for($i=0;$i<10;$i++){?>
						<option value="<?=$i;?>"><?=$i;?></option>
						<?php } ?>	</select>
						</div>
						
						
					</div>
				</div>
				
				<div class="w100percent">
					<div class="wh100percent textleft">
						<span class="opensans size13"><b>Ancestral Origin/
Native Place :</b></span>
						<input  class="form-control" type="text" name="nativea" id="nativea"> 
					</div>
				</div>
				
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Affluence Level</b></span>	
					
					<select  class="form-control" name="level" id="level">
					<option value="">Select Affluence Level</option>
							<option value="Upper Middle Class">Upper Middle Class</option>
						<option value="Middle Clas">Middle Clas</option>
						<option value="Lower Middle Class">Lower Middle Class</option>
						</select>
				</div>
				
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Family values</b></span>	
					
					<select  class="form-control" name="family_value" id="family_value">
						<option value="Traditional">Traditional</option>
						<option value="Moderate">Moderate</option>
						<option value="Liberal">Liberal</option>
					</select>
					
				</div>
				<input type="submit" value="Save & Next" class="btn-search" style="margin-right: auto; margin-left: auto; margin-top: 27px; display: block; float: right;"  >
				</form>
			</div>
			<!--/Fourth division-->
				
			<div id="div5" class="divisions" >
			
				<h3 class="div_name">Partner Details(<?php if($UserDetails[0]->Gender == 2){ echo "Groom";}else{echo "Bride";}?> details)</h3>
				<form id="partnerinfo" action="<?php echo WEB_URL;?>home/partnerform" name="page2" method="post">
				
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b> Age From</span>
					
					 <select name="PageFrom" id="PageFrom" class="form-control">
						<?php for($i=18;$i<100;$i++){ ?>
							<option value="<?=$i;?>"><?=$i;?></option>
						<?php } ?>
					</select>
					</div>
				  </div>  
				 <div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>Age To</span>
				
						<select name="PageTo" id="PageTo" class="form-control">
						<?php for($j=18;$j<100;$j++){ ?>
							<option value="<?=$j;?>"><?=$j;?></option>
						<?php } ?>
						</select>
					</div>
				  </div>  	  
				  <div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>  Height From</span>
				<?php  $HeightFrom = $this->action_model->get_table_details('HeightList');	?>
						<select name="PheightFrom" id="PheightFrom" class="form-control">
						<option value="" selected class="form-control">select height from</option>
								<?php foreach($HeightFrom as $value){ ?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?>  			  	
						</select>
					</div>
				  </div>  				
				<div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>  Height To</b></span>
					
						<select name="PheightTo" id="PheightTo" class="form-control">
						<option value="">Select height to</option>
							   <?php  $HeightTo = $this->action_model->get_table_details('HeightList');	
								foreach($HeightTo as $value){ ?>
								<option value="<?php echo $value->Height_Id; ?>"><?php echo $value->Height;?></option>    
								<?php } ?> 			  	
						</select>
					</div>
				  </div>  
				 <div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b> Marital Status</b></span>
					<select class="form-control" name="Pmarital_status[]" id="Pmarital_status" multiple>				
						<?php $marital_status = array("Never married","Divorced","Widowed","Awaiting_Divorced","Annulled"); 
						foreach($marital_status as $value){
							echo '<option value="'.$value.'" >'.$value.'</option>';	
						} 
					    ?>
					</select>
				</div>
			  </div> 		
			  <div class="w100percent">
				<div class="wh100percent textleft">
					<span class="opensans size13"><b>select partner Religion</b></span>
				<?php $religion = $this->action_model->get_table_details('religionlist'); ?>
				
					<select class="form-control" name="Preligion[]" id="Preligion" onchange="GetList('Preligion','Pcommunity','Religion_Id','communitylist','Community_Id','Community_Name')"  multiple>
						<option value="">Doesn't Matter</option>
						<?php foreach($religion as $value){?>
						<option value="<?php echo $value->Religion_Id; ?>"><?php echo $value->Religion_Name;?></option>    
						<?php } ?> 
					</select>
				</div>
			  </div> 
			  <div class="w100percent" id="Pcommunity_Dev" style="display:none;">
					<div class="wh100percent textleft">
						<span class="opensans size13"><b>Select Partner Community</b></span>
					
						<select  name="Pcommunity[]" id="Pcommunity" class="form-control" multiple>
							<option value="">Select Community</option>
										  
						</select>
					</div>
				</div>
			
				
			 <div class="w100percent">
				<div class="wh100percent textleft">
				<span class="opensans size13"><b>Select Partner profile created by</b></span>
				
					<select class="form-control" name="Pprofile_created_by[]" id="Pprofile_created_by" multiple>
							<option value="">Doesn't Matter</option>
							<?php $profile_created_by = array("self","son","Daughter","parents"); 
									foreach($profile_created_by as $value){
										echo '<option value="'.$value.'" >'.$value.'</option>';	
									} 
							?>
						</select>
				</div>
			  </div>			  
			   <div class="w100percent">
				<div class="wh100percent textleft">
				<span class="opensans size13"><b>Country Grew up</b></span>
				<?php   $country = $this->action_model->get_table_details('countrylist'); ?>
				
					<select class="form-control" name="PcountryGrewUp[]" id="PcountryGrewUp" multiple>
						<option value="">Doesn't Matter</option>
						<?php 
						foreach($country as $country){?>
						<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
						<?php } ?>
					
					</select>
				</div>
			  </div>
				 <div class="w100percent">
				<div class="wh100percent textleft">
				<span class="opensans size13"><b>Mother Tongue</b></span>
				<?php    $language = $this->action_model->get_table_details('languagelist'); ?>
			
					<select class="form-control" name="Pmother_tongue[]" id="Pmother_tongue" multiple>
						<option value="">Doesn't Matter</option>
						
						<?php foreach($language as $value){ ?>
							<option value="<?php echo $value->Language_Id; ?>"><?php echo $value->Language_Name;?></option>    
						<?php } ?>
					</select>
				</div>
			  </div>
				<div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Living Country</b></span>
					<?php     $country = $this->action_model->get_table_details('countrylist'); ?>
				
						<select class="form-control" name="Pliving_in[]" id="Pliving_in"  onchange="GetList('Pliving_in','PLivingState','Country_Id','statelist','State_Id','State_Name')" multiple>
							<option value="">Doesn't Matter</option>
							<?php 
							foreach($country as $country){?>
							<option value="<?php echo $country->Country_Id; ?>"><?php echo $country->Country_Name;?></option>    
							<?php } ?>
						 </select>
					</div>
				  </div>		
				 <div class="w100percent" id="PLivingState_Dev" style="display:none;">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Living State</b></span>
					
						<select class="form-control" name="PLivingState[]" id="PLivingState" onChange="GetList('PLivingState','PLivingCity','State_Id','citylist','City_Id','City_Name')" multiple>
							
						
						</select>
					</div>
				</div>		
				 <div class="w100percent" id="PLivingCity_Dev" style="display:none;">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Living City</b></span>
				<?php     $city = $this->action_model->get_table_details('citylist'); ?>
				
					<select class="form-control" name="PLivingCity[]" id="PLivingCity" multiple>
					
									  
					</select>
				</div>
			  </div>	
						
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Education level</b></span>
				<?php  $degree = $this->action_model->get_table_details('degree'); ?>
			
					<select class="form-control" name="Peducation[]" id="Peducation" multiple>
						<option value="">Doesn't Matter</option>
						<?php 
						foreach($degree as $value){?>
						<option value="<?php echo $value->Degree_Id; ?>"><?php echo $value->Degree_Name;?></option>    
						<?php } ?>	  
					</select>
				</div>
			 </div>				
			  <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Education Fields</b></span>
				<?php $spec = $this->action_model->get_table_details('specialization'); ?>
			
					<select class="form-control" name="Peducation_field[]" id="Peducation_field" multiple> 
						<option value="">Doesn't Matter</option>
						<?php 
						foreach($spec as $value){?>
						<option value="<?php echo $value->Id; ?>"><?php echo $value->SName;?></option>    
						<?php } ?>
					</select>
				</div>
			 </div>	      
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Working with</b></span>
				
					<select class="form-control" name="PworkinWith[]" id="PworkinWith" multiple>
						<option value="">Doesn't Matter</option>
						<?php $workingwith = array("Not working","Government","Private","Self employed","business"); 
						for($j=1;$j<count($workingwith);$j++){ ?>
						<option value="<?php echo $j; ?>"><?php echo $workingwith[$j];?></option>    
						<?php } ?>
					</select>
				</div>
			 </div>					
			    
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Work Area</b></span>
				
					<select class="form-control" name="PworkingAs[]" id="PworkingAs" multiple>
						<option value="">Doesn't Matter</option>
						<?php 
						 $workingas = $this->action_model->get_table_details('workingas');
						foreach($workingas as $value){?>
						<option value="<?php echo $value->Work_Id; ?>"><?php echo $value->Work_Name;?></option>    
						<?php } ?>
					</select>
				</div>
			 </div>				
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Annual Income</b></span>
			
					<select class="form-control" name="PannualIncome[]" id="PannualIncome" multiple>
						<option value="">Select</option>
						<?php $AnualIncome=  array("doesn’t matter","Upto INR 1 Lakh","INR 1 Lakh to 2 Lakh","INR 2 Lakh to 4 Lakh","INR 4 Lakh to 7 Lakh",

											"INR 7 Lakh to 10 Lakh","INR 10 Lakh to 15 Lakh","INR 15 Lakh to 20 Lakh","INR 20 Lakh to 30 Lakh","INR 30 Lakh to 50 Lakh",

											"INR 50 Lakh to 75 Lakh","INR 75 Lakh to 1 Crore","Dont want to specify");
						foreach($AnualIncome as $value){ ?>
							<option value="<?php echo $value; ?>"><?php echo $value;?></option>    
						<?php } ?>
				   </select>
				</div>
			</div>
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Select Disablities</b></span>
					<select class="form-control" name="Pdisability" id="Pdisability">
						<option value="0">Doesn't Matter</option>
						<option value="2">No</option>    
						<option value="1">Yes</option> 	
				   </select>
				</div>
			</div>
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Diet</b></span>
					<select name="Pdiet[]"  class="form-control" multiple>
					<option value="0">Doesn't Matter</option>
					<?php $DietList = array("veg","Non veg","Occasionally Non-Veg","Eggetarian","Jain"); 
						foreach($DietList as $value){ 
							echo '<option value="'.$value.'">'.$value.'</option>';
						}
					?>
					</select>

				</div>
			</div>
			<div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Skin Tone</b></span>
				
				<select name="Pskin_tone[]" id="Pskin_tone" class="form-control" multiple>
				<option value="0">Doesn't Matter</option>
					<?php $SkinTone = array("very fair","fair","Black","Wheatish"); 
						foreach($SkinTone as $value){ 
							echo '<option value="'.$value.'">'.$value.'</option>';
						}
					?>
				</select>
				
				</div>
			</div>
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Body Type:  </b></span>
					<?php $bodyType = array("slim","average","Athletic","heavy"); ?>
					<select name="Pbody_type[]" class="form-control" multiple>
						<option value="0">Doesn't Matter</option>
						<?php foreach($bodyType as $value){ ?>
							<option value="<?=$value;?>"><?=$value;?></option>
						<?php } ?>
					</select>
					
				</div>
			</div>		
			<br/>
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Smoke: </b></span>
					<input type="radio" name="Psmoke" value="1">Yes
					<input type="radio" name="Psmoke" value="2">No
					<input type="radio" name="Psmoke" value="1,2" selected>Doesn't Matter
				</div>
			</div>		
<br/>			
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Drinks :</b></span>
			
					<input type="radio" name="Pdrink" value="1">Yes
					<input type="radio" name="Pdrink" value="2">No
					<input type="radio" name="Pdrink" value="1,2" selected>Doesn't Matter
				
			</div>				
			<br/>
			 <div class="w100percent">
					<div class="wh100percent textleft">
					<span class="opensans size13"><b>Select Dosham</b></span>
				
					<input type="radio" name="PDosham" value="1">Yes
					<input type="radio" name="PDosham" value="2" checked>No
					<input type="radio" name="PDosham" value="1,2" selected>Doesn't Matter
				</div>
			</div>	
				<input type="submit" value="Save & Next" class="btn-search" style="margin-right: auto; margin-left: auto; margin-top: 27px; display: block; float: right;"  >
				</form>
			</div>
			<div>
				
				
				<input id="prev_button" type="button" class="btn-search" value="Previous" style="float: right; margin-right: 245px;" onclick="next_previous(0)"/>
				
				
			</div>
			 
		
		
		
		
		<br/>
</div>
	
	<br/>
	<br/>
<script>
$(document).ready(function($) {

$("#contactinfo").validate({

	// Specify the validation rules
	rules: {
		country:{required: true,},
		state: {
			required: true,
		},
		city: {
			required: true,
		},
		Livingcountry:{required: true,},
		LivingState: {
			required: true,
		},
		LivingCity: {
			required: true,
		},
		pincode: {
			required: true,
		},
	},
	
	// Specify the validation error messages
	messages: {
		country :  "Please Select your country",
		state: "Please Select your state",
		city: "Please Select your city",
		Livingcountry: "Please Select your living country",
		LivingState: "Please Select your living state ",
		LivingCity: "Please Select your living city",
		pincode: "Please Select pincode",
	},
	
	submitHandler: function(form) {
		var country = $("#country").val();
		var state = $("#state").val();
		var city = $("#city").val();
		var Livingcountry = $("#Livingcountry").val();
		var LivingState = $("#LivingState").val();
		var LivingCity = $("#LivingCity").val();
		var pincode = $("#pincode").val();
		$.ajax({
			url: "<?php echo WEB_URL;?>home/contactform",
			dataType:'json',
			data:{country:country,state:state,city:city,Livingcountry:Livingcountry,LivingState:LivingState,LivingCity:LivingCity,pincode:pincode},
			type: "POST",
			success: function(data) {
			alert(data);
				console.log(data);
				alert("success");
			}
		  });
		  
		  next_previous(1);
	}
	
	
});
$("#basicinfo").validate({

	// Specify the validation rules
	rules: {
		community:{
			required: true,
		},
		marital_status: {
			required: true,
		},
		height: {
			required: true,
		},
		weight: {
			required: true,
		},
		about: {
			required: true,
		},
	},
	
	// Specify the validation error messages
	messages: {
		community : "Please select your community",
		marital_status: "Please Select your state",
		height: "Please Select your city",
		weight: "Please Select your living state ",
		about : "Please say something about yourself",
	},
	
	submitHandler: function(form) {
		var community = $("#community").val();
		var marital_status = $("#marital_status").val();
		var height = $("#height").val();
		var weight = $("#weight").val();
		var hiv = $("input[name=HIV]:checked").val();
		var skin_tone = $("input[name=skin_tone]:checked").val();
		var Disability = $("input[name=Disability]:checked").val();
		var body_type = $("input[name=body_type]:checked").val();
		var diet = $("input[name=diet]:checked").val();
		var smoke = $("input[name=smoke]:checked").val();
		var drink = $("input[name=drink]:checked").val();
		var about = $("#about").val();
		
		$.ajax({
			url: "<?php echo WEB_URL;?>home/basicform",
			dataType:'json',
			data:{community:community,about:about,marital_status:marital_status,height:height,weight:weight,hiv:hiv,skin_tone:skin_tone,Disability:Disability,body_type:body_type,
			diet:diet,smoke:smoke,drink:drink},
			type: "POST",
			success: function(data) {
				console.log(data);
			}
		  });
		  
		  next_previous(1);
	}
});
$("#educationinfo").validate({

	// Specify the validation rules
	rules: {
		edu_level: {
			required: true,
		},
		edu_field: {
			required: true,
		},
		working_with: {
			required: true,
		},
		working_as: {
			required: true,
		},
		anual_income: {required:true,}
	},
	
	// Specify the validation error messages
	messages: {
		edu_level: "this field is required",
		edu_field: "this field is required",
		working_with: "this field is required ",
		working_as: "this field is required",
		anual_income: "this field is required",
	},
	
	submitHandler: function(form) {
		var edu_level = $("#edu_level").val();
		var edu_field = $("#edu_field").val();
		var working_with = $("#working_with").val();
		var working_as = $("#working_as").val();
		var anual_income = $("#anual_income").val();
		$.ajax({
			url: "<?php echo WEB_URL;?>home/educationform",
			dataType:'json',
			data:{edu_level:edu_level,edu_field:edu_field,working_with:working_with,working_as:working_as,anual_income:anual_income},
			type: "POST",
			success: function(data) {
				console.log(data);
			}
		  });
		  
		  next_previous(1);
	}
});
$("#familyinfo").validate({

	// Specify the validation rules
	rules: {
		father_status: {
			required: true,
		},
		mother_status: {
			required: true,
		},
		num_bro: {
			required: true,
		},
		num_sis: {
			required: true,
		},
		level: {required:true,},
		family_value: {required:true,},
	},
	
	// Specify the validation error messages
	messages: {
		father_status: "this field is required",
		mother_status: "this field is required",
		num_bro: "this field is required ",
		num_sis: "this field is required",
		level: "this field is required",
		family_value: "this field is required",
	},
	
	submitHandler: function(form) {
		var father_status = $("#father_status").val();
		var mother_status = $("#mother_status").val();
		var num_bro = $("#num_bro").val();
		var num_sis = $("#num_sis").val();
		var num_bro_married = $("#num_bro_married").val();
		var num_sis_married = $("#num_sis_married").val();
		var nativea = $("#nativea").val();
		var level = $("#level").val();
		var family_value = $("#family_value").val();
		
		$.ajax({
			url: "<?php echo WEB_URL;?>home/familyform",
			data:{father_status:father_status,mother_status:mother_status,num_bro:num_bro,num_sis:num_sis,
			num_bro_married:num_bro_married,num_sis_married:num_sis_married,level:level,nativea:nativea,family_value:family_value},
			type: "POST",
			success: function(data) {
				  next_previous(1);
				//window.location.href = '<?php echo WEB_URL;?>home/match';
			}
		  });
		  
	}
});

});
$(document).ready(function() {
	$("#country").change(function(){
		 var country = $("#country").val();
		 if(country != ""){
			$.ajax({
				url: "<?php echo WEB_URL; ?>home/GetStates",
				data: {living_in : country},
				type: "POST",
				success: function(data) {
					$("#StateList").css("display","block");
					$("#state").html(data);
				}
			  });
		 }
	});
	$("#Livingcountry").change(function(){
		 var Livingcountry = $("#Livingcountry").val();
		
		 if(Livingcountry != ""){
			$.ajax({
				url: "<?php echo WEB_URL; ?>home/GetStates",
				data: {living_in : Livingcountry},
				type: "POST",
				success: function(data) {
					$("#LivingStateList").css("display","block");
					$("#LivingState").html(data);
				}
			  });
		 }
	});
	
	$("#state").change(function(){
		 var state = $("#state").val();
		 if(state != ""){
			$.ajax({
				url: "<?php echo WEB_URL; ?>home/GetCities",
				data: {living_in : state},
				type: "POST",
				success: function(data) {
					$("#CityList").css("display","block");
					$("#city").html(data);
				}
			  });
		 }
	});
	$("#LivingState").change(function(){
		 var LivingState = $("#LivingState").val();
		 if(LivingState != ""){
			$.ajax({
				url: "<?php echo WEB_URL; ?>home/GetCities",
				data: {living_in : LivingState},
				type: "POST",
				success: function(data) {
					$("#LivingCityList").css("display","block");
					$("#LivingCity").html(data);
				}
			  });
		 }
	});
});
$(document).ready(function() {
$("#Pskin_tone").multiselect();
$('#partnerinfo').find('[name="Pmother_tongue[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()

$('#partnerinfo').find('[name="Pmarital_status[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()

$('#partnerinfo').find('[name="Preligion[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()

$('#partnerinfo').find('[name="Pcommunity[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="Pprofile_created_by[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()

$('#partnerinfo').find('[name="PcountryGrewUp[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="PworkinWith[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="Pedu_field[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="Pbody_type[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="Pdiet[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="Peducation_field[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="Peducation[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="PworkingAs[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()
$('#partnerinfo').find('[name="PannualIncome[]"]')
.multiselect({
	// Re-validate the multiselect field when it is changed
	onChange: function(element, checked) {
		adjustByScrollHeight();
	},
	onDropdownShown: function(e) {
		adjustByScrollHeight();
	},
	onDropdownHidden: function(e) {
		adjustByHeight();
	}
})
.end()

    $('#partnerinfo').find('[name="Pliving_in[]"]')
            .multiselect({
                // Re-validate the multiselect field when it is changed
                onChange: function(element, checked) {
                    adjustByScrollHeight();
                },
                onDropdownShown: function(e) {
                    adjustByScrollHeight();
                },
                onDropdownHidden: function(e) {
                    adjustByHeight();
                }
            })
            .end()
      $('#multiselectForm').find('[name="LivingState[]"]')
            .multiselect({
                // Re-validate the multiselect field when it is changed
                onChange: function(element, checked) {
                    adjustByScrollHeight();
                },
                onDropdownShown: function(e) {
                    adjustByScrollHeight();
                },
                onDropdownHidden: function(e) {
                    adjustByHeight();
                }
            })
            .end() 
	 $('#multiselectForm').find('[name="LivingCity[]"]')
            .multiselect({
                // Re-validate the multiselect field when it is changed
                onChange: function(element, checked) {
                    adjustByScrollHeight();
                },
                onDropdownShown: function(e) {
                    adjustByScrollHeight();
                },
                onDropdownHidden: function(e) {
                    adjustByHeight();
                }
            })
            .end() 
    // You don't need to care about these methods
    function adjustByHeight() {
        var $body   = $('body'),
            $iframe = $body.data('iframe.fv');
        if ($iframe) {
            // Adjust the height of iframe when hiding the picker
            $iframe.height($body.height());
        }
    }

    function adjustByScrollHeight() {
        var $body   = $('body'),
            $iframe = $body.data('iframe.fv');
        if ($iframe) {
            // Adjust the height of iframe when showing the picker
            $iframe.height($body.get(0).scrollHeight);
        }
    }
});
function GetList(currentdev,nextdev,column,table,display1,display2){

	 var val = $("#"+currentdev).val();

	 if(val != ""){

		$.ajax({

			url: "<?php echo WEB_URL; ?>home/GetJsonList",
			async: false,	
			data: {column:column , value : JSON.stringify(val),table:table,display1:display1,display2:display2},

			type: "POST",

			success: function(data) {
				console.log(data);
				$("#"+nextdev+"_Dev").css('display','block');
				$("#"+nextdev).html(data);
				$("#"+nextdev).multiselect({includeSelectAllOption: true});
				$("#"+nextdev).multiselect('rebuild');
			}

		  });

	 }

}
</script>		

<?php $this->load->view("include/footer"); ?>
