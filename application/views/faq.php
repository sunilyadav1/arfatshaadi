<?php session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
$page = $this->action_model->getpart_table_deatils("pages","Page_id",2);
?>
<div class="container">

	<div class="container breadcrub">
			<div class="row">
		        <div class="col-xs-6 col-md-4"></div>
		        <div class="col-xs-6 col-md-4"></div>
		        <div class="col-xs-6 col-md-4">
		               <!--  <div class="col-xs-6 col-md-6"><h3 class="opensans" style="font-size:16px;">Need Assistance?</h3></div>
					    <div class="col-xs-6 col-md-6"><p class="opensans size30 lblue xslim" style="position: relative; font-weight: 400; font-size: 15px;">+91-7259872851 <br />+91-9342627372</p></div> -->
					    &nbsp;
		        </div>
	      </div>
	</div>	
	<!-- CONTENT -->
	<div class="container">
	 <div class="col-xs-6 col-md-4" style="float:right;">
            <div class="col-xs-6 col-md-6"><h3 class="opensans" style="font-size:16px;">Need Assistance?</h3></div>
            <div class="col-xs-6 col-md-6"><p class="opensans size30 lblue xslim"
                                              style="position: relative; font-weight: 400; font-size: 15px;">
                    +91-7259872851 <br/>+91-9342627372</p></div>
        </div>
		<br/><br/><br/><hr/>
		<div class="container pagecontainer offset-0">	
	
			
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class="active"><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaChat"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/milanrishtaAlerts"  class="tehnical">AMilanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>

						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Getting Started - Registering and Profile Creation</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>How can I register on Milanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
										All you need to do is, fill out the required information in the Registration Form as accurately as possible and click on the 'Submit' button. 
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>Can I register on behalf of a relative or a friend? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Yes you can! In the Registration Form, you can specify your relationship with the person on whose behalf you are registering
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>How much do I have to pay to register on Milanrishta.com Matrimonials? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Registration on Milanrishta.com Matrimonials is FREE.<br />Register Now with Milanrishta.com and create your Profile.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>What are the benefits of registering on Milanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											<p>You can avail of the following services as a free Member:-</p>
											<ul class="faq">
												<li>Create and manage your Profile on Milanrishta.com.</li>
												<li>Add Photos to your Profile</li>
												<li>Add your Partner Preferences the kind of person you are looking for</li>
												<li>Search and View Profiles without restrictions</li>
												<li>Express Interest in other Members and respond to Members who Express Interest in you</li>
												<li>Receive Chat requests and Chat with Members who are online</li>
												<li>Add your Horoscope and also create your Astro Sketch</li>
												<li>See Members who Viewed your Profile, Members who Expressed Interest in you and Members who match your Partner Preferences.</li>
												<li>Shortlist a Profile that you like. Register Now with Milanrishta.com and create your Profile.</li>
											</ul>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>How long does it take to register and create a Profile on Milanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Registration and Profile creation on Milanrishta.com can be completed in less than 10 minutes. In three easy steps you can register with Milanrishta.com and create your Profile
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>The form seems to be a bit lengthy. Do I need to fill it entirely? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											We understand that it may become tedious for you to fill in a long form in one go. However, matrimonial decision are important decisions and hence one should convey detailed information about oneself to Interested Members. The more information you provide about yourself the more likely you are to be contacted by other Members of Milanrishta.com. So please do take the time and effort to complete your Profile.<div>&nbsp;</div>
											Register Now with Milanrishta.com and create your Profile.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										   <strong>7. </strong>Can I specify more than one email while registering with Milanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSeven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body">
											No. You may specify only one email while registering. You can however change your email address later if you wish
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
										   <strong>8. </strong>My Religion/Caste/Mother tongue/Profession is not listed in the Registration form. What should I do? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEight" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											A Milanrishta.com has tried to be as comprehensive as possible while creating the many lists being used in the Registration form. However, it is possible that your particular religion / caste, mother tongue, education, profession etc may not be represented here.<div>&nbsp;</div>
											In such a case we request that you use the 'Other' option provided to you. Also, you can send an email to info@Milanrishta.com clearly listing the new addition you would like us to make to the Registration form. While we do not guarantee that your suggestion will be Accepted, we will try our best to ensure that it does.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
										   <strong>9. </strong>The registration form requires information about my time of birth and city of birth. What do I do if I am not sure of these details? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseNine" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											These details are usually of Interest for matching horoscopes. You may leave these details blank if you are not sure of answers to these questions. However, we recommend that you try and find these answers and enter them later. This information will be useful in generating automatic horoscopes for better matrimonial matches.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
										   <strong>10. </strong>What is the advantage of verifying my phone number? How can I do it? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Verifying your phone number helps you get more responses, since it builds trust, and adds authenticity to your Profile.
										<div>&nbsp;</div>You can Verify your phone number by using either our Interactive Voice Response System or via SMS in case you have provided a mobile number as your contact number.
										<div>&nbsp;</div>Click here to Verify your phone number

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
										   <strong>11. </strong>What is a Profile ID? Is it important? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseEleven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											A Profile ID is automatically generated by Milanrishta.com and uniquely identifies your Profile on Milanrishta.com. Every Member of Milanrishta.com has a unique Profile ID. Other Members can also search for your Profile using the Profile ID search feature.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTweleve">
										   <strong>12. </strong>Are there any specific DOs and DON'Ts while creating a matrimonial Profile on Milanrishta.com? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTweleve" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Do not use your Profile to display your contact details. Do not make commercial use of it and do not include content that is vulgar, pornographic or racist. See our terms of use / service agreement for more details of what type of content is prohibited on Milanrishta.com.
											<div>&nbsp;</div>Be as detailed as possible while creating your Profile. After your Profile is activated, be sure to fill in the Partner Profile and also to upload a photograph. The more information you add, the more your chances of finding a partner.
											<div>&nbsp;</div>Register Now with Milanrishta.com and create your Profile.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">
										   <strong>13. </strong>While registering, I got an error message that says that my email is already registered with Milanrishta.com and that I must specify another email. Why does this happen? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThirteen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Every Profile posted on Milanrishta.com is associated with only one unique email. You may have received this error message because you or someone else has already posted a Profile using this email. If you think your email is being used by someone else, please send us an email from the address that is under contention and let us know.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading  panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen">
										   <strong>14. </strong>Why should I set my Partner Preferences? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFourteen" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Setting your  Partner Preferences tells us more about the kind of partner you are looking for and helpsMilanrishta.com suggest better matching Profiles for you. Also, your Preferences helps other Members decide whether you be interested if they were to contact you.
											<div>&nbsp;</div>Click here to edit your Partner Preferences.

									  </div>
									</div>
								  </div>
								  <div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



