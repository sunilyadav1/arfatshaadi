<?php session_start();
$id = $this->session->userdata('User_Id'); 
if(isset($id) && $id != ""){
	$this->load->view("include/header"); 
	$Gender = $this->session->userdata('Gender');
	$UserDetails = $this->action_model->full_profile($id);
}else{
	$this->load->view("include/header-static"); 
	$UserDetails = "";
}
?>

<div class="container">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<!-- Horizontal Stack -->
    <div class="tab-wrap">
        <div class="media">
            <div class="col-md-3">
						  <span href="#" class="list-group-item" style="background-color:#F1F1F1;">
							<div style="font-weight:bold;">Help Topics</div>
						  </span>
						  <div class="parent pull-left col-md-12" style="padding: 0;">
								<ul class="nav nav-tabs nav-stacked">
									<li class=""><a href="#"  class="tehnical">AboutMilanrishta.com</a></li>
									<li class=""><a href="#"  class="tehnical">Getting Started</a></li>
									<li class=""><a href="#" class="tehnical">Login / Password</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/profileManagement"  class="tehnical">Profile Management</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/photographs"  class="tehnical">Photographs</a></li>
									<li class="active"><a href="<?php echo WEB_URL; ?>home/searchingProfiles"  class="tehnical">Searching Profiles</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/contactingMembers" class="tehnical">Contacting Members</a></li>
									<li class=""><a href="#"  class="tehnical">Milanrishta Chat</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/blockMisuse"  class="tehnical">Block and Report Misuse</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/membershipsFAQ"  class="tehnical">Memberships</a></li>
									<li class=""><a href="#"  class="tehnical">A Milanrishta.com Profile Blaster</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/PaymentOptions"  class="tehnical">Payment Options</a></li>
									<li class=""><a href="#"  class="tehnical">AMilanrishta.com Alerts</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/technicalIssues"  class="tehnical">Technical Issues</a></li>
									<li class=""><a href="<?php echo WEB_URL; ?>home/privacySecurity"  class="tehnical">Privacy & Security Tips</a></li>
									<li class=""><a href="#"  class="tehnical">Personalised Settings</a></li>
									<li class=""><a href="#"  class="tehnical">Write to Customer Relations</a></li>
									<li class=""><a href="#"  class="tehnical">Calling Customer Relations</a></li>
								</ul>
							</div>
						</div>

            <div class="parrent media-body">
                <div class="tab-content">
                     <div class="tab-pane fade active in">
                        <div class="media">
                            <div class="media-body">
                                <h3 class="green">Help / FAQs</h3>
								<p class="Dblue size13 bold">Searching and Matching Profiles</p><div>&nbsp;</div>
                                <hr/>
                                <div class="panel-group panel_group" id="accordion">
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										  <strong>1. </strong>How can I search for Profiles on Milanrishta.com? What are the search options available? 
										</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse panel_collapse collapse in">
									  <div class="panel-body panel_body">
										<p>To search for Profiles on  Milanrishta.com, you can use any of the following options: </p>
											<ul class="faq">
												<li>Basic Search allows you to search for Profiles as per basic criteria like age, height, marital status, religion, location, etc.</li>
												<li>The Advanced Search option gives you more detailed search options. The additional search options include education, profession, skin tone, manglik status, etc.</li>
												<li>The Who is Online option lets you search for Profiles who are currently online onMilanrishta.com.</li>
												<li>Search and View Profiles without restrictions</li>
												<li>The Special Cases option allows you to search for physically/mentally challenged Members.</li>
												<li>The Saved Search option allows you to save upto five searches with different criteria. You can run a Saved Search at a later time to get a list of Members who match the criteria specified.</li>
												<li>The ‘Refine Search’ option appears on the left of your page after you have performed a search. The Refine Search feature allows you to drill down and filter your results based on photo availability, marital status, mother tongue, religion, country, state, city, education, profession and more.</li>
												<li>Additionally, you can also use Profile ID search to locate particular Members on Milanrishta.com.</li>
											</ul>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										   <strong>2. </strong>In what order are my search results listed?
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										Search results are shown by default using a custom algorithm developed byMilanrishta.com. This is used to maximise responses to your Profile. You can also sort the results by ‘Newest First’ and ‘Last Logged in First’.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										   <strong>3. </strong>In the search results I can see some small icons next to some of the Profiles. What do those icons mean? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
										AMilanrishta.com uses different icons to denote the status of Profile information, e.g. whether Phone number is verified or not, whether horoscope is added or not, etc.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										   <strong>4. </strong>What is the Matches feature? How does it work? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFour" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Based on your Profile and Partner Preferences, Milanrishta.com shows you a list of Profiles under your Matches list.
											<div>&nbsp;</div>Preferred Matches: These are Members who exactly match your Partner Preferences
											<div>&nbsp;</div>Broader Matches: These are Members who match some of your Partner Preferences and are slightly outside some of your other Preferences.
											<div>&nbsp;</div>Reverse Matches: These Members have set Partner Preferences that match your Profile information.
											<div>&nbsp;</div>2-way Matches: Both of you match each others Partner Preferences.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										   <strong>5. </strong>Can I shortlist Profiles that I like onMilanrishta.com?
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseFive" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											If you like a Member’s Profile, we recommend that you contact the Member immediately by Expressing Interest. In case you want to decide later, you can add these Members to a ‘Shortlist’. You can revisit these Profiles by visiting the Shortlists page and decide to Express Interest later.
											<div>&nbsp;</div>You can create upto 10 Shortlists. You can also add the same Profile to multiple Shortlists that you have created.

									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										   <strong>6. </strong>Can I get a list of Members who have visited my Profile? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSix" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											Yes. Click here to see a list of Members who have recently viewed your Profile.
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default panel_custom">
									<div class="panel-heading panel_heading">
									  <h4 class="panel-title panel_title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										   <strong>7. </strong>What does the Ignore feature do? How is this useful? 
										</a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
									  </h4>
									</div>
									<div id="collapseSeven" class="panel-collapse panel_collapse collapse">
									  <div class="panel-body panel_body">
											While searching you may come across Profiles that you may not want to consider in your Partner Search. You can Ignore such Profiles we will ensure that these Profiles will not shown again to you onMilanrishta.com.
											<div>&nbsp;</div>By Ignoring irrelevant Profiles, you will be improving our algorithm that shows you Matches / Search Results. If you change your mind, you can access these Profiles from the Ignored Members list and Express Interest in them.
									  </div>
									</div>
								  </div>
								  <div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>
								</div>
                            </div>
                        </div>
                    </div>
				</div> <!--/.tab-content-->
            </div> <!--/.media-body-->
        </div> <!--/.media-->
    </div>
<!-- Horizontal Stack -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
        function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading panel_heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
    });
</script>
<script>
	$(".faq_content").click(function(){
		$(this).toggleClass("down"); 
	});
</script>
    
 

<?php $this->load->view("include/footer"); ?>



