<?php $this->load->view("include/header-static"); ?>
<?php
	echo isset($status) ? $status : ""; 
?>
<style>
body
{background: #FAFAFA;}
</style>
	<div class="login-fullwidith">
		
	<!-- Login Wrap  -->
	<div class="login-wrap">
		<h3 class="text-center"> Retrieve your password via Email / SMS </h3>
	<form id="wedding-lostpassword" method="POST" action="<?php echo WEB_URL; ?>home/ForgotPassword">
		<div class="login-c1">
			<div class="cpadding50">
				<input type="email" name="email" id="username" class="form-control logpadding" placeholder="Email Address">
				<p> An auto generated code will be sent to your registered Email id.....</p>
			</div>
		</div>
		<div class="login-c2">
			<div class="logmargfix">
				<div class="chpadding50">
						<div class="alignbottom1">
							<input name="forgot" class="btn-search4" style="padding: 7.5px 65px;" type="submit" value="Submit">							
						</div>
				</div>
			</div>
		</div>
		<div class="login-c3">
			<div class="left"><a href="#" class="whitelink"><span></span>Home</a></div>
			<div class="right"><a href="#" class="whitelink">Register Free?</a></div>
		</div>
	</form>			
	</div>
	<!-- End of Login Wrap  -->
	</div>
<?php $this->load->view("include/footer"); ?>
<script>
	  
// When the browser is ready...
jQuery(document).ready(function($) {

// Setup form validation on the #register-form element
$("#wedding-lostpassword").validate({

	// Specify the validation rules
	rules: {
		email: {
			required: true,
		}
	},
	
	// Specify the validation error messages
	messages: {
		email: "Please enter your Email Address"
	},
	
	submitHandler: function(form) {
		//return false;
		form.submit();
	}
});

});
</script>