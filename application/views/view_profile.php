<?php $this->load->view("include/header");
$user_id = $this->session->userdata('User_Id'); 
$UserDetails =  $this->action_model->full_profile($user_id); 
$PartnerDetails =  $this->action_model->full_partnerprofile($user_id); 
?>
	
	<div class="container breadcrub">
	    
		<div class="brlines"></div>
	</div>	

	<!-- CONTENT -->
<div class="container">
	<div class="container mt25 margbottom20 offset-0">
		<div class="col-md-12  offset-0">
		<div class="cstyle10"></div>
		<ul class="nav nav-tabs" id="myTab">
				<li class="active"><a data-toggle="tab" href="#basic"><span class="rates"></span><span class="hidetext">Basic Information</span>&nbsp;</a></li>
				<li  class=""><a data-toggle="tab" href="#partner"><span class="preferences"></span><span class="hidetext">Partner Preferrence</span>&nbsp;</a></li>
		</ul>	
		<div class="tab-content4">
		<div id="basic" class="tab-pane fade active in">
			<form action="<?php echo WEB_URL;?>home/UpdateProfile" method="POST">
			<!-- LEFT CONTENT -->
				<div class="col-md-8 pagecontainer2 basic_info offset-0">

				<div class="padding30 grey">
				
				
					<span class="size16px bold lblue left"> Basic Info</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Basic Info"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>
					
					
					<div id="basic-info">
					<div class="col-md-4 textright">
						<div class=""><span class="dark">Profile Created By:</span></div>
					</div>
					<div class="col-md-4">
					<?php echo isset($UserDetails[0]['ProfileFor']) ? $UserDetails[0]['ProfileFor'] :"Not Specified"; ?>
					</div>
					
					
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop5"><span class="dark">Gender :</span></div>
					</div>
					
					<div class="col-md-4">
						<div class="margtop5"><?php if($UserDetails[0]['Gender'] == 2){ echo "Female";}else{echo "Male";}?></div>
					</div>
					
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<div class="col-md-4 textright">
						<div class="margtop20"><span class="dark">Date Of Birth :</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop20"><?php echo isset($UserDetails[0]['DOB']) ? date('d-m-Y',strtotime($UserDetails[0]['DOB'])) : "Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Marital status :</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['MaritalStatus']) ? $UserDetails[0]['MaritalStatus'] : "Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Height :</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['Height']) ? $UserDetails[0]['Height'] :"Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					<br/>
					
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Body Type :</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['BodyType']) ? $UserDetails[0]['BodyType'] : "Not Specified";?></div>
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Body Weight :</span></div>
					</div>
					<div class="col-md-4">
								<div class="margtop7"><?php echo isset($UserDetails[0]['Weight']) ? $UserDetails[0]['Weight']."Kg" :  "Not Specified";?></div>
							
						
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					
					<br/>
				
					
					<div class="col-md-4 textright"> 
						<div class="margtop7"><span class="dark">Skin Tone :</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php  echo isset($UserDetails[0]['SkinTone']) ? $UserDetails[0]['SkinTone'] : "Not Specified";?></div>
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					
					<div class="col-md-4 textright">
						<div class="margtop20"><span class="dark">Any Disability? :</span></div>
					</div>
					<div class="col-md-4">
							
							<div class="margtop20"><?php $val=""; if($UserDetails[0]['Disability'] == 1){$val ="Yes";}else{$val ="No";} 
							echo $val; ?></div>
								
						
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					
				
				
					</div>
					
					
					<br/>
					<br/>
					
					
					<span class="size16px bold lblue left">Religious Background</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Background"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>
					<div id="religious">
					
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Religion:</span></div>
					</div>
					<div class="col-md-4">
							<div class="margtop7"><?php echo isset($UserDetails[0]['Religion_Name']) ? $UserDetails[0]['Religion_Name']: "Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft margtop15">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Mother Tongue:</span></div>
					</div>
					<div class="col-md-4">						
							<div class="margtop7"><?php echo isset($UserDetails[0]['Language_Name']) ? $UserDetails[0]['Language_Name'] : "Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Community:</span></div>
					</div>
					<div class="col-md-4">						
							<div class="margtop7"><?php echo isset($UserDetails[0]['Community_Name']) ? $UserDetails[0]['Community_Name'] : "Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Sub-Community:</span></div>
					</div>
					<div class="col-md-4">	
						<div class="margtop7"><?php echo isset($UserDetails[0]['SubCommunity']) ? $UserDetails[0]['SubCommunity'] : "Not Specified"; ?></div>		
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
						
					<br/>
				
					
					</div>
					
					<br/>
					<br/>
					<span class="size16px bold lblue left">Family</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Family"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>	
					<div id="family">
					
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Father's Status:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['father_status']) ? $UserDetails[0]['father_status'] : "Not Specified";?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Mother's Status:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['mother_status']) ? $UserDetails[0]['mother_status'] : "Not Specified";?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">No. of Brothers:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?=$UserDetails[0]['num_bro'];?> Total No. of Brothers</div>
					</div>
					<div class="col-md-4 textleft">
						<div class="margtop7"><?=$UserDetails[0]['num_bro_married'];?> Of Which Married</div>
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">No. of Sisters:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?=$UserDetails[0]['num_sis'];?> Total No. of Sisters</div>
					</div>
					<div class="col-md-4 textleft">
						<div class="margtop7"><?=$UserDetails[0]['num_sis_married'];?> Of Which Married</div>
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Native Place:</span></div>
					</div>
					<div class="col-md-8">
						<div class="margtop7"><?php isset($UserDetails[0]['native']) ? $UserDetails[0]['native']: "Not Specified";?> (Parent's birth place or Origin Place)</div>
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Affluence Level:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php isset($UserDetails[0]['level']) ? $UserDetails[0]['level'] : "Not Specified";?></div>
					
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Family Values:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php isset($UserDetails[0]['family_value']) ? $UserDetails[0]['family_value']: "Not Specified";?></div>
					
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					</div>
				
					
					<br/>
					<br/>
					<span class="size16px bold lblue left">Education & Career</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Education"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>		
					
					
					<div id="education">
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Education:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['Degree_Name']) ? $UserDetails[0]['Degree_Name'] : "Not Specified";?></div>
						
					</div>
					<div class="col-md-4 textleft">
						
						<div class="margtop7"><?php echo isset($UserDetails[0]['SName']) ? $UserDetails[0]['SName'] : "Not Specified";?></div>
						
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Working With:</span></div>
					</div>
					<div class="col-md-4">
						<select class="form-control m10" name="WorkingWith" id="WorkingWith">
							<?php $val="";  
							switch($UserDetails[0]['WorkingWith']){
								case 1 :
									$val ="not working";
									break;
								case 2 :
									$val ="Government";
									break;
								case 3 :
									$val ="Private";
									break;
								default :
									$val = "Not Specified";
								break;
							}?>
							<?=$val;?>
					   </select>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Working As:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['Work_Name']) ? $UserDetails[0]['Work_Name'] : "Not Specified";?></div>
						
					</div>
				
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Annual Income:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['AnualIncome']) ? $UserDetails[0]['AnualIncome'] : "Not Specified";?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					</div>
					
					<br/>
					<br/>
					<span class="size16px bold lblue left">Lifestyle</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Lifestyle"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>		
					
					
					<div id="lifestyle">
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Diet:</span></div>
					</div>

					<div class="col-md-8">
						<div class="margtop7"><?php echo isset($UserDetails[0]['Diet']) ? $UserDetails[0]['Diet'] : "Not Specified";?></div>
								
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Drink:</span></div>
					</div>
					<div class="col-md-6">
					
						<div class="margtop7"><?php $val="Not Specified";if($UserDetails[0]['Drink'] == 1){$val="Yes";}else{$val="No";}
							echo $val; ?></div>
							
							
					</div>
					<div class="col-md-2 textleft">
					</div>
					<div class="clearfix"></div>
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Smoke:</span></div>
					</div>
					
					<div class="col-md-6">
							<div class="margtop7"><?php $val="Not Specified";if($UserDetails[0]['Smoke'] == 1){$val="Yes";}else{$val="No";}
							echo $val; ?></div>
					</div>
					<div class="col-md-2 textleft">
					</div>
					<div class="clearfix"></div>
					</div>
					
					<br/>
					<br/>
					<span class="size16px bold lblue left">Location</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Location"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>		
					
					<div id="location">
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Country Living In:</span></div>
					</div>
					
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['Country_Name']) ? $UserDetails[0]['Country_Name'] :"Not Specified" ; ?></div>
					
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">State Living In:</span></div>
					</div>
					<div class="col-md-4" id="">
						<div class="margtop7"><?php echo isset($UserDetails[0]['State_Name']) ? $UserDetails[0]['State_Name'] : "Not Specified"; ?></div>
					</div>
					
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					<br/>
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">City Living In:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['City_Name']) ? $UserDetails[0]['City_Name']: "Not Specified"; ?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					
					
					<br/>
					
					<div class="col-md-4 textright">
						<div class="margtop7"><span class="dark">Zip/Pin Code:</span></div>
					</div>
					<div class="col-md-4">
						<div class="margtop7"><?php echo isset($UserDetails[0]['PinNo']) ? $UserDetails[0]['PinNo'] : "Not Specified";?></div>
					</div>
					<div class="col-md-4 textleft">
					</div>
					<div class="clearfix"></div>
					</div>
					
					<br/>
					<br/>
					
					<span class="size16px bold lblue left">More About Yourself, Partner and Family</span>
					<div class="roundstep active  right"><a href="<?php echo WEB_URL;?>home/edit_profile#personal"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit About Yourself"></i></a></div>
					<div class="clearfix"></div>
					<div class="line4"></div>	
					<div id="collapse5">
						
						<div class="col-md-2 textright">
						</div>
						<div class="col-md-8">
							<?php echo isset($UserDetails[0]['About']) ? $UserDetails[0]['About'] : "Not Specified";?>
						</div>
					
					</div>
			
				</div>
		
			</div>
			</form>
		</div>
		<div id="partner" class="tab-pane fade">
			<form action="<?php echo WEB_URL;?>home/UpdateProfile" method="POST">
			<!-- LEFT CONTENT -->
			<div class="col-md-8 pagecontainer2 offset-0">
			<?php if(isset($PartnerDetails) && $PartnerDetails != ""){ ?>
				<div class="padding30 grey">
				
				
					<span class="size16px bold dark left"> Basic Info</span>
					<div class="roundstep active right"><a href="<?php echo WEB_URL;?>home/edit_profile#partner"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="left" title="Edit Basic Info"></i></a></div>
				
					<div class="clearfix"></div>
					<div class="line4"></div>
					
					 <div class="form-group">
						<label class="control-label col-sm-4" for="email"> Age From</label>
						<div class="col-sm-8">
						<?php echo isset($PartnerDetails[0]['AgeFrom']) ? $PartnerDetails[0]['AgeFrom'] : "Not Specified";?>
						</div>
					  </div>  	
					  <br/><br/>
					 <div class="form-group">
						<label class="control-label col-sm-4" for="email"> Age To</label>
						<div class="col-sm-8">
							<?php echo isset($PartnerDetails[0]['AgeTo']) ? $PartnerDetails[0]['AgeTo'] : "Not Specified";?>
						</div>
					  </div>  	  
					  <br/><br/>
					  <div class="form-group">
					  <?php $heightfrom = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$PartnerDetails[0]['HeightFrom']); 
					  ?>
						<label class="control-label col-sm-4" for="email"> Height From</label>
						<div class="col-sm-8">
							<?php echo isset($heightfrom[0]->Height) ? $heightfrom[0]->Height : "Not Specified";?>
						</div>
					  </div> 
					<br/><br/>					  
					 <div class="form-group">
					 <?php $heightto = $this->action_model->getpart_table_deatils("HeightList","Height_Id",$PartnerDetails[0]['HeightTo']); ?>
						<label class="control-label col-sm-4" for="email"> Height To</label>
						<div class="col-sm-8">
							<?php echo isset($heightto[0]->Height) ? $heightto[0]->Height : "Not Specified";?>
						</div>
					  </div>  
<br/><br/>					  
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email">Marital Status</label>
						<div class="col-sm-8">
							<?php echo isset($PartnerDetails[0]['MaritalStatus']) ? $PartnerDetails[0]['MaritalStatus'] : "Not Specified";?>
						</div>
					  </div> 	
<br/><br/>					  
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email">Partner Religion</label>
						
						<div class="col-sm-8">
							<?php  $religion = $this->action_model->get_table_details('religionlist');
								$Religion = explode(",",$PartnerDetails[0]['Religion']);
								if(count($Religion) > 0){
								foreach($religion as $value){
									if(in_array($value->Religion_Id,$Religion)){
										echo $value->Religion_Name.",";
									}
								}}else{echo "Not Specified";}?>
						</div>
					  </div>
<br/><br/>					  
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email">partner Community</label>
						<div class="col-sm-8">
								<?php   $Community = $this->action_model->get_table_details('communitylist');
										$Com = explode(",",$PartnerDetails[0]['Community']);
									if(count($Com) > 0){
									foreach($Community as $value){
									if(in_array($value->Community_Id,$Com)){
										echo $value->Community_Name.",";
									}
								}}else{echo "Not Specified";}?>
						</div>
					  </div> 		
<br/><br/>					  
					 <div class="form-group">
						<label class="control-label col-sm-4" for="email">Partner profile created by</label>
						<div class="col-sm-8">
						<?php echo isset($PartnerDetails[0]['ProfileCreatedby']) ? $PartnerDetails[0]['ProfileCreatedby'] : "Not Specified";?>
						
						</div>
					  </div>
					  <br/><br/>
					   <div class="form-group">
						<label class="control-label col-sm-4" for="email">Country Grew up</label>
						<?php   $country = $this->action_model->get_table_details('countrylist'); ?>
						<div class="col-sm-8">
							<?php if(isset($PartnerDetails[0]['CountryGrewUp'])){
								$CountryGrewUp = explode(",",$PartnerDetails[0]['CountryGrewUp']);
								$countrys = $this->action_model->get_table_details('countrylist');
								for($i=0;$i<count($CountryGrewUp);$i++){
								foreach($countrys as $country){
									if($country->Country_Id == $CountryGrewUp[$i]){echo $country->Country_Name."<br/>";}else{continue;}
								}}}else{ echo "Not Specified";}
							?>
						</div>
					  </div>
					  <br/><br/>
						<div class="form-group">
						<label class="control-label col-sm-4" for="email">Mother Tongue</label>
						<div class="col-sm-8">
							<?php if(isset($PartnerDetails[0]['MotherTongue'])){
								$MotherTongue = explode(",",$PartnerDetails[0]['MotherTongue']);
								$languagelists = $this->action_model->get_table_details('languagelist');
								for($i=0;$i<count($MotherTongue);$i++){
								foreach($languagelists as $languagelist){
									if($languagelist->Language_Id == $MotherTongue[$i]){echo $languagelist->Language_Name.",";}else{continue;}
								}}}else{ echo "Not Specified";}
							?>
						</div>
					  </div>
						<br/><br/>
						<div class="form-group">
						<label class="control-label col-sm-4" for="email">Living Country</label>
						
						<div class="col-sm-8">
							<?php if(isset($PartnerDetails[0]['CountryLivingIn'])){
								$CountryLivingIn = explode(",",$PartnerDetails[0]['CountryLivingIn']);
								$countrys = $this->action_model->get_table_details('countrylist');
								for($i=0;$i<count($CountryLivingIn);$i++){
								foreach($countrys as $country){
									if($country->Country_Id == $CountryLivingIn[$i]){echo $country->Country_Name."<br/>";}else{continue;}
								}}}else{ echo "Not Specified";}
							?>
						</div>
					  </div>		
					
					<div class="form-group" id="StateList" style="display:none;">
						<label class="control-label col-sm-4" for="email">Living State</label>
						<div class="col-sm-8">
							<?php if(isset($PartnerDetails[0]['LivingState'])){
								$LivingState = explode(",",$PartnerDetails[0]['LivingState']);
								$states = $this->action_model->get_table_details('statelist');
								for($i=0;$i<count($LivingState);$i++){
								foreach($states as $state){
									if($state->State_Id == $LivingState[$i]){echo $state->State_Name."<br/>";}else{continue;}
								}}}
							?>
						</div>
					</div>		
					
					<div class="form-group" id="CityList" style="display:none;">
						<label class="control-label col-sm-4" for="email">Living City</label>
						
						<div class="col-sm-8">
								<?php if(isset($PartnerDetails[0]['LivingCity'])){
									$LivingCity = explode(",",$PartnerDetails[0]['LivingCity']);
									$cityes = $this->action_model->get_table_details('citylist');
									for($i=0;$i<count($LivingCity);$i++){
									foreach($cityes as $city){
										if($city->City_Id == $LivingCity[$i]){echo $city->City_Name."<br/>";}else{continue;}
									}}}
								?>
						</div>
					  </div>	
					<br/><br/>
				
					<div class="form-group">
						<label class="control-label col-sm-4" for="residencyStatus">Education level</label>
						
						<div class="col-sm-8">
							<?php if(isset($PartnerDetails[0]['Degree_Name'])){
								$Degree_Name = explode(",",$PartnerDetails[0]['Degree_Name']);
								$degrees = $this->action_model->get_table_details('degree');
								for($i=0;$i<count($Degree_Name);$i++){
								foreach($degrees as $degree){
									if($degree->Degree_Name == $Degree_Name[$i]){echo $degree->Degree_Name.",";}else{continue;}
								}}}else{ echo "Not Specified";}
							?>
							</div>
					 </div>		
<br/><br/>					 
					<div class="form-group">
						<label class="control-label col-sm-4" for="residencyStatus">working with</label>
						
						<div class="col-sm-8">
							<?php $val="";  
							$WorkingWiths = explode(",",$PartnerDetails[0]['WorkingWith']);
							foreach($WorkingWiths as $WorkingWith){		
							switch($WorkingWith){
								case 1 :
									$val ="not working";
									break;
								case 2 :
									$val ="Government";
									break;
								case 3 :
									$val ="Private";
									break;
								default :
									$val ="Not Specified";
								break;
							}
							echo $val.",";
							}?>
							
						</div>
					 </div>		
<br/><br/>					 
					<div class="form-group">
						<label class="control-label col-sm-4" for="residencyStatus">Education Fields</label>
						
						<div class="col-sm-8">
							<?php echo isset($PartnerDetails[0]['SName']) ? $PartnerDetails[0]['SName'] : "Not Specified";?>
						</div>
					 </div>	         
<br/><br/>					 
					<div class="form-group">
						<label class="control-label col-sm-4" for="residencyStatus">Work Area</label>
					
						<div class="col-sm-8">
							<?php echo isset($PartnerDetails[0]['Work_Name']) ? $PartnerDetails[0]['Work_Name'] : "Not Specified";?>
						</div>
					 </div>	
<br/><br/>					 
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Annual Income</label>
						<div class="col-sm-8">
						   <?php echo isset($PartnerDetails[0]['AnualIncome']) ? $PartnerDetails[0]['AnualIncome'] : "Not Specified";?>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Disablities</label>
						<div class="col-sm-8"> 
							<?php $val="";  switch($PartnerDetails[0]['Disability']){
								case 1 :
									$val ="Yes";
									break;
								case 2 :
									$val ="No";
									break;
								default :
									$val = "Does not matter";
								break;
							}?>
							<?=$val;?>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Diet</label>
						<div class="col-sm-8"> 
							<?php echo isset($PartnerDetails[0]['Diet']) ? $PartnerDetails[0]['Diet'] : "Not Specified";?>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Body Type</label>
						<div class="col-sm-8"> 
							<?php echo isset($PartnerDetails[0]['BodyType']) ? $PartnerDetails[0]['BodyType'] : "Not Specified";?>
						</div>
					</div>		
					<br/><br/>
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Smoke</label>
						<div class="col-sm-8"> 
							<?php $val="";  switch($PartnerDetails[0]['Smoke']){
								case 1 :
									$val ="Yes";
									break;
								case 2 :
									$val ="No";
									break;
								default :
									$val = "Does not matter";
								break;
							}?>
							<?=$val;?>
						</div>
					</div>	
<br/><br/>					
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Drinks</label>
						<div class="col-sm-8"> 
							<?php $val="";  switch($PartnerDetails[0]['Drink']){
								case 1 :
									$val ="Yes";
									break;
								case 2 :
									$val ="No";
									break;
								default :
									$val = "Does not matter";
								break;
							}?>
							<?=$val;?>
						</div>
					</div>	
<br/><br/>					
					<div class="form-group">					
						<label class="control-label col-sm-4" for="residencyStatus">Dosham</label>
						<div class="col-sm-8"> 
							<?php $val="";  switch($PartnerDetails[0]['Dosham']){
								case 1 :
									$val ="Yes";
									break;
								case 2 :
									$val ="No";
									break;
								default :
									$val = "Does not matter";
								break;
							}?>
							<?=$val;?>
						</div>
					</div>	
	
				</div>
			<?php }else{ ?>
				<div class="padding30 grey">Partner Details not provided <a href="<?php echo WEB_URL;?>home/edit_profile#partner">Click here</a> to provide details</div>
			<?php } ?>
			</div>
			</form>
		</div>
	</div>
	</div>
			<!-- END OF LEFT CONTENT -->			
			<?php $this->load->view("include/sidebar"); ?>
			<!-- RIGHT CONTENT -->
			
			<!-- END OF RIGHT CONTENT -->
			
			
		</div>
		
		
	</div>
	<!-- END OF CONTENT -->
	<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
	
<?php $this->load->view("include/footer"); ?>
