<?php session_start();
$this->load->view("include/header");
$partner_id = $this->uri->segment(3);
$user_id = $this->session->userdata('User_Id');

$PartnerDetails = $this->action_model->full_profile($partner_id);
$SelfDetails = $this->action_model->getpart_table_deatils("userprofile", "User_Id", $user_id);
$PartnerFamily = $this->action_model->getpart_table_deatils("familyinfo", "User_id", $partner_id);
$PartnerSettings = $this->action_model->getpart_table_deatils("settings","User_Id",$partner_id);
switch($PartnerSettings[0]->NameSetting) {
	case 1 :
		$pname = $PartnerDetails[0]['Name'];
	break;
	
	case 2 :
		$pname = $PartnerDetails[0]['LastName'];
	break;
	
	case 3 :
		$pname = $PartnerDetails[0]['Name']." ".$PartnerDetails[0]['LastName'];
	break;
	
	case 4 :
		$pname = $PartnerDetails[0]['User_Id'];
	break;
	
	default:
		$pname = $PartnerDetails[0]['Name']." ".$PartnerDetails[0]['LastName'];
	break;
}
switch($PartnerSettings[0]->PhotoSetting) {
	case 10 :
		if($PartnerDetails[0]['ProfilePic'] == ""){
			$pimg1 ="no-profile.gif";
		}else{
			$pimg1 = $PartnerDetails[0]['ProfilePic'];
		}
		
		$pimg = WEB_DIR."images/profiles/".$pimg1;
			
	break;
	
	case 11 :
		$pimg = WEB_DIR . "images/profiles/no-profile.gif";
	break;
	
	default:
		$pimg = WEB_DIR . "images/profiles/no-profile.gif";
	break;
}
switch ($PartnerDetails[0]['ProfileFor']) {
    case 'Son':
        $created = "parents";
        break;
    case 'Daughter' :
        $created = "parents";
        break;
    Default :
        $created = $PartnerDetails[0]['ProfileFor'];
    break;
}?>

<div class="container">
 <div class="row">
        <div class="col-xs-6 col-md-4"></div>
        <div class="col-xs-6 col-md-4"></div>
        <div class="col-xs-6 col-md-4">
            <div class="col-xs-6 col-md-6"><h3 class="opensans" style="font-size:16px;">Need Assistance?</h3></div>
            <div class="col-xs-6 col-md-6"><p class="opensans size30 lblue xslim"
                                              style="position: relative; font-weight: 400; font-size: 15px;">
                    +91-7259872851 <br/>+91-9342627372</p></div>
        </div>
    </div>
<div>&nbsp;</div>
<!--  BreadCrumb for User Profile  -->
<!--User Profile Content-->
<div class="row">
<!--User Details-->
<div class="col-md-9">
<ol class="breadcrumb">
    <!--        <li><a href="#" style="color:#fff;">Home</a></li>-->
    <li><?php echo anchor('home/index', 'Home', ['style' => 'color:#fff;']); ?></li>
    <!--        <li><a href="#" style="color:#fff;">User Profile</a></li>-->
    <li><?php echo anchor('home/index', 'User Profile', ['style' => 'color:#fff;']); ?></li>
    <li class="active"><?php echo ucfirst($pname); ?></li>
</ol>
<h2><?php echo ucfirst($pname); ?>
    <small>(<?php echo $PartnerDetails[0]['User_Id']; ?>)</small>
</h2>
<p>
    <small><span class="glyphicon glyphicon-user">&nbsp;</span>Profile created by <?php echo $created; ?>
    </small>
</p>
<hr/>
<!--UserDetailsList-->
<div class="userDetailsList">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<ul class="nav profile_icon">
					<p>
					<li><i class="glyphicon glyphicon-user"></i> Age :<?php $age = $this->action_model->birthda($PartnerDetails[0]['DOB']); 
										echo isset($age) ? $age : "Not Specified"; ?> Years</li>
					</p>
					<p>
					<li><i class="glyphicon glyphicon-heart"></i> Marital Status :<?php if($PartnerDetails != ""){echo isset($PartnerDetails[0]['MaritalStatus']) ? $PartnerDetails[0]['MaritalStatus'] : "Not Specified";}?></li>
					</p>
					<p>
					<li><i class="glyphicon glyphicon-flag"></i> Religion : <?php if($PartnerDetails != ""){echo  isset($PartnerDetails[0]['Religion_Name']) ? $PartnerDetails[0]['Religion_Name'] : 'not Specified';}?></li>
					</p>
					<p>
					<li class="active"><i class="glyphicon glyphicon-exclamation-sign"></i> Community : <?php if($PartnerDetails != ""){echo  isset($PartnerDetails[0]['Community_Name']) ? $PartnerDetails[0]['Community_Name'] : 'not Specified';}?></li>
					</p>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="nav profile_icon">
					<p>
					<li><i class="glyphicon glyphicon-book"></i> Degree : <?php  if($PartnerDetails != ""){if(isset($PartnerDetails[0]['Degree_Name']) && $PartnerDetails[0]['Degree_Name'] != ""){
					echo $PartnerDetails[0]['Degree_Name'].isset($PartnerDetails[0]['Degree_Name']) ? " in".$PartnerDetails[0]['SName'] : '';}else{ echo 'not Specified'; } }?> </li>
					</p>
					<p>
					<li><i class="glyphicon glyphicon-heart"></i> Profession: <?php if($PartnerDetails != ""){ echo  isset($PartnerDetails[0]['Work_Name']) ? $PartnerDetails[0]['Work_Name'] : 'not Specified'; }?></li>
					</p>
					<p>
					<li><i class="glyphicon glyphicon-flag"></i> City : <?php if($PartnerDetails != ""){ echo  isset($PartnerDetails[0]['City_Name']) ? $PartnerDetails[0]['City_Name'] : ''; }?> <?php if($PartnerDetails != ""){ echo  isset($PartnerDetails[0]['Country_Name']) ? $PartnerDetails[0]['Country_Name'] : 'not Specified';}?></li>
					</p>
				</ul>
			</div>
		</div>
	</div>
</div>
<hr/>
<!--End: UserDetailsList-->
<!--WouldYouLikeToConnect-->
<div class="well">
<input type="hidden" name="partner_id" id="partner_id" value="<?=$partner_id;?>">
		<input type="hidden" name="user_id" id="user_id" value="<?=$user_id;?>">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <h3>Would You Like to Keep in Touch with <?php echo ucfirst($pname); ?>?</h3>
    <hr/>
	<?php $notifications = $this->action_model->GetNotificationDetails($user_id,$partner_id);
							
							if($notifications != "failure"){
							for($i=0;$i<count($notifications);$i++){
							if($notifications[$i]['NotificationTo'] == $partner_id){
								if($notifications[$i]['Notification_Id'] == 7){
									echo  "This profile has been added to maybe list";
								}else if($notifications[$i]['Notification_Id'] == 6){
									echo  "Invitation has been sent to this profile";
								}else if($notifications[$i]['Notification_Id'] == 8){
									echo "This profile has been ignored";
								}else{ ?>
							 <div class="row container" id="ConnectionType">
								<a href="#myModal"  data-toggle="modal" class="btn btn-success connect_user">Yes</a>
								<input type="button" class="btn btn-danger connect_user_no" onclick="Connection(8)" value="No">
							   
								   <input type="button" class="btn btn-info connect_user_maybe" onclick="Connection(7)" value="Maybe">				  
							</div>
						   <span class="mt10" id="ConnectionResult"></span>
							<?php	}
								}} }
							else{ ?>
							 <div class="row container" id="ConnectionType">
								<a href="#myModal"  data-toggle="modal" class="btn btn-success connect_user">Yes</a>
								<input type="button" class="btn btn-danger connect_user_no" onclick="Connection(8)" value="No">
							   
								   <input type="button" class="btn btn-info connect_user_maybe" onclick="Connection(7)" value="Maybe">				  
							</div>
						   <span class="mt10" id="ConnectionResult"></span>
							<?php } ?>
							
							
   
</div>
<!--End: WouldYouLikeToConnect-->
<div class="row">
<!-- Previous Code -->
<div class="col-md-12">
<div class="panel with-nav-tabs panel-primary">
<div class="panel-heading">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1primary" data-toggle="tab">Lifestyle &amp; Appearance</a></li>
        <li><a href="#tab2primary" data-toggle="tab">Partner Preference</a></li>
        <li><a href="#tab3primary" data-toggle="tab">Similar Profiles</a></li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown">Options <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#tab4primary" data-toggle="tab">Invite</a></li>
                <li><a href="#tab5primary" data-toggle="tab">Message</a></li>
            </ul>
        </li>
    </ul>
</div>
<div class="panel-body">
<div class="tab-content">
<div class="tab-pane fade in active" id="tab1primary">
    <h4 class="div_name">Lifestyle & Appearance </h4>
	
    <div class="each_Lifestyle">
        <?php switch ($PartnerDetails[0]['Diet']) {
            case 'veg':
                $style = "background-position: 25px -508px";
                break;
            case 'Non veg' :
                $style = "background-position: 24px -693px;";
                break;
            case 'Occasionally Non-Veg' :
                $style = "background-position: 25px -699px;";
                break;
            case 'Eggetarian' :
                $style = "background-position: 29px -601px";
                break;
            Default :
                $style = "background-position: 25px -510px;";
                break;
                break;
        }?>

       
        <span class="mt10 each_Lifestyle_description" style="color:#F68181;"><?php if($PartnerDetails != ""){ echo isset($PartnerDetails[0]['Diet']) ? $PartnerDetails[0]['Diet'] : "Not Specified";} ?></span>

    </div>

    <?php if($PartnerDetails != ""){
		if ($PartnerDetails[0]['Drink'] == 2) {
        $style = "background-position: 35px -869px;";
        $val = "Drink";
		} else {
			$style = "background-position: 22px -778px;";
			$val = "Doesn't Drink";
		}
	}else{
		$val = "Not Specified";
	}?>
    <div class="each_Lifestyle">
        
        <span class="mt10 each_Lifestyle_description" style="color:#F8A06A;"><?= $val; ?></span>

    </div>
    <?php if($PartnerDetails != ""){
		if ($PartnerDetails[0]['Smoke'] == 2) {
        $style = "background-position: 25px -953px;";
        $val = "Smoke";
    } else {
        $style = "background-position: 25px -1028px;";
        $val = "Doesn't Smoke";
    }
	}else{ $val = "Not Specified";}?>
    <div class="each_Lifestyle">
       
        <span class="mt10 each_Lifestyle_description" style="color:#D290DB;"><?= $val; ?></span>
    </div>

    <div class="each_Lifestyle">
        
        <span class="mt10 each_Lifestyle_description" style="color:#95B8EB;"><?= $PartnerDetails[0]['BodyType']; ?></span>
    </div>
	<br/><br/><br/>
	<div>
		<h4 class="div_name">About <?php if ($PartnerDetails[0]['Gender'] == 1) {
			echo "His";
		} else {
			echo "Her";
		} ?></h4>
		<p><?php if($PartnerDetails != ""){ echo isset($PartnerDetails[0]['About']) ? $PartnerDetails[0]['About'] : "Not Specified"; }?></p>
	</div>
</div>
<div class="tab-pane fade" id="tab2primary">
<?php if($PartnerDetails != ""){ if (isset($PartnerDetails[0]['level'])) { ?>
    <div style="padding:10px 0px 0px 30px;border-bottom:3px solid #E8E8E8;">

        <h3 class="div_name" style="font-size:20px;">About Family</h3>

        <p style="padding: 0px 10px; color: black; font: italic 15px/24px arial;">Ours is
            a <?php echo isset($PartnerDetails[0]['level']) ? $PartnerDetails[0]['level'] : ""; ?> family, originally
            from <?php echo isset($PartnerDetails[0]['native']) ? $PartnerDetails[0]['native'] : ""; ?>,
            with <?php echo isset($PartnerDetails[0]['family_value']) ? $PartnerDetails[0]['family_value'] : ""; ?>
            values. father
            is <?php echo isset($PartnerDetails[0]['father_status']) ? $PartnerDetails[0]['father_status'] : ""; ?> and
            her mother is
            a  <?php echo isset($PartnerDetails[0]['mother_status']) ? $PartnerDetails[0]['mother_status'] : ""; ?>. and
            have <?php echo isset($PartnerDetails[0]['num_bro']) ? $PartnerDetails[0]['num_bro'] : ""; ?> brother
            and <?php echo isset($PartnerDetails[0]['num_sis']) ? $PartnerDetails[0]['num_sis'] : ""; ?>
            (<?php echo isset($PartnerDetails[0]['num_sis_married']) ? $PartnerDetails[0]['num_sis_married'] : ""; ?>
            married) sister. </p>


    </div>
<?php }} ?>
<?php $HerPreference = $this->action_model->getpart_table_deatils("partnerprofile", "User_Id", $partner_id); 
$PartnerPreference = $this->action_model->getpart_table_deatils("partnerprofile", "User_Id", $user_id);
?>
<?php if ($HerPreference != "" && $PartnerPreference != "") { 
 $count = 0; 
if($PartnerPreference[0]->AgeFrom != "" && $HerPreference[0]->AgeFrom != ""){
if ($PartnerPreference[0]->AgeFrom >= $HerPreference[0]->AgeFrom || $PartnerPreference[0]->AgeTo <= $HerPreference[0]->AgeTo) {
	$count += 1;}}
if($PartnerPreference[0]->HeightFrom != "" && $HerPreference[0]->HeightTo != ""){
if ($PartnerPreference[0]->HeightFrom >= $HerPreference[0]->HeightFrom || $PartnerPreference[0]->HeightTo <= $HerPreference[0]->HeightTo) {
	$count += 1; }}
if ($PartnerPreference[0]->MaritalStatus != "" && $HerPreference[0]->MaritalStatus != "") {
if ($PartnerPreference[0]->MaritalStatus == $HerPreference[0]->MaritalStatus) {
	$count += 1; }}
if ($PartnerPreference[0]->MotherTongue != "" && $HerPreference[0]->MotherTongue != "") {
$mothertoungue = explode(",",$PartnerPreference[0]->MotherTongue);
if (in_array($HerPreference[0]->MotherTongue,$mothertoungue)) {
	$count += 1; }}
if ($PartnerPreference[0]->CountryLivingIn != "" && $HerPreference[0]->CountryLivingIn != "") {
		$CountryLivingIn= explode(",",$PartnerPreference[0]->CountryLivingIn);
		if (in_array($HerPreference[0]->CountryLivingIn,$CountryLivingIn)) {
	$count += 1;
	}}
if ($PartnerPreference[0]->Diet != "" && $HerPreference[0]->Diet != "") {
if ($PartnerPreference[0]->Diet == $HerPreference[0]->Diet) {
	$count += 1; }}
if ($PartnerPreference[0]->Smoke != "" && $HerPreference[0]->Smoke != "") {
if ($PartnerPreference[0]->Smoke == $HerPreference[0]->Smoke) {
	$count += 1; }} 
if ($PartnerPreference[0]->Drink != "" && $HerPreference[0]->Drink != ""){
if ($PartnerPreference[0]->Drink == $HerPreference[0]->Drink) {
	$count += 1; 
}}
 if ($PartnerPreference[0]->BodyType != "" && $HerPreference[0]->BodyType != ""){
	if ($PartnerPreference[0]->BodyType == $HerPreference[0]->BodyType) {
	$count += 1;  } }
$_SESSION['count'] = $count;
?>

    <div style="padding:10px 30px 0px 30px;border-bottom:3px solid #E8E8E8;">
        <h3 class="div_name" style="font-size:20px;">What <?php if ($PartnerDetails[0]['Gender'] == 1) {
                echo "he";
            } else {
                echo "she";
            } ?> Is Looking For</h3>
        <br/>

        <div>
            <div style="display:inline-block;width:130px;">
                <img src="<?php echo $pimg; ?>"
                     style="border-radius: 100%; width: 120px; box-shadow: 1px 0px 4px rgba(0, 0, 0, 0.58); border: 5px solid white; display: block; margin: 0px auto;">

                <div style="clear:both;"></div>
                <p style="text-align:center;margin-top:10px;color:black;"><?php if ($PartnerDetails[0]['Gender'] == 1) {
                        echo "His";
                    } else {
                        echo "Her";
                    } ?> Preferences</p>
            </div>

            <div style="display:inline-block;vertical-align:top;width:390px;">
                <p style="text-align: center; margin-top: 50px; color: rgb(29, 218, 18); font-weight: bold; font-size: 15px;">
                    You match <?php echo isset($_SESSION['count']) ? $_SESSION['count'] : $count; ?>/9 of <?php if ($PartnerDetails[0]['Gender'] == 1) {
                        echo "His";
                    } else {
                        echo "Her";
                    } ?> Preferences</p>
            </div>

            <div style="display:inline-block;vertical-align:top;width:130px;">
			 <?php
				$img = $this->session->userdata('ProfilePic');
				$name = $this->session->userdata('DisplayName');
				?>
                <img src="<?php echo $img; ?>"
                     style="border-radius: 100%; width: 120px; box-shadow: 1px 0px 4px rgba(0, 0, 0, 0.58); border: 5px solid white; display: block; margin: 0px auto;">

                <div style="clear:both;"></div>
                <p style="text-align:center;margin-top:10px;color:black;">You match</p>
            </div>
        </div>

        <div style="margin-left:25px;margin-right:25px;margin-top:5px;">
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Age</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php if($HerPreference != ""){ echo isset($HerPreference[0]->AgeFrom) ? $HerPreference[0]->AgeFrom : "Not Specified"; }else{ echo "Not Specified";} ?>
                        to <?php  if($HerPreference != ""){ echo isset($HerPreference[0]->AgeTo) ? $HerPreference[0]->AgeTo : "Not Specified"; }else{ echo "Not Specified";}?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if($PartnerPreference[0]->AgeFrom != "" && $HerPreference[0]->AgeFrom != ""){
					if ($PartnerPreference[0]->AgeFrom >= $HerPreference[0]->AgeFrom || $PartnerPreference[0]->AgeTo <= $HerPreference[0]->AgeTo) {
                        ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>


            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Height</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php if($HerPreference != ""){ echo isset($HerPreference[0]->HeightFrom) ? $HerPreference[0]->HeightFrom : "Not Specified";}else{echo "Not Specified";} ?>
                        to <?php if($HerPreference != ""){ echo isset($HerPreference[0]->HeightTo) ? $HerPreference[0]->HeightTo : "Not Specified"; } else{ echo "Not Specified";} ?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if($PartnerPreference[0]->HeightFrom != "" && $HerPreference[0]->HeightTo != ""){
					if ($PartnerPreference[0]->HeightFrom >= $HerPreference[0]->HeightFrom || $PartnerPreference[0]->HeightTo <= $HerPreference[0]->HeightTo) {
                       ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Marital Status</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php if($HerPreference != ""){  echo isset($HerPreference[0]->MaritalStatus) ? $HerPreference[0]->MaritalStatus : "Not Specified"; }else{ echo "Not Specified";}?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if ($PartnerPreference[0]->MaritalStatus != "" && $HerPreference[0]->MaritalStatus != "") {
					if ($PartnerPreference[0]->MaritalStatus == $HerPreference[0]->MaritalStatus) {
                        ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">MotherTongue</p>
                    <?php $MotherTongue = $this->action_model->getpart_table_deatils("languagelist", "Language_Id", $HerPreference[0]->MotherTongue);
                    $country = $this->action_model->getpart_table_deatils("countrylist", "Country_Id", $HerPreference[0]->CountryLivingIn);?>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php if($HerPreference != ""){ echo isset($MotherTongue[0]->Language_Name) ? $MotherTongue[0]->Language_Name : "Not Specified"; } else{ echo "Not Specified";}?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                  <?php  if ($PartnerPreference[0]->MotherTongue != "" && $HerPreference[0]->MotherTongue != "") {
						$mothertoungue = explode(",",$PartnerPreference[0]->MotherTongue);
						if (in_array($HerPreference[0]->MotherTongue,$mothertoungue)) { ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Country Living in</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php echo isset($country[0]->Country_Name) ? $country[0]->Country_Name : "Not Specified"; ?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if ($PartnerPreference[0]->CountryLivingIn != "" && $HerPreference[0]->CountryLivingIn != "") {
					$CountryLivingIn= explode(",",$PartnerPreference[0]->CountryLivingIn);
					if (in_array($HerPreference[0]->CountryLivingIn,$CountryLivingIn)) {
                         ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Diet</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php if($HerPreference != ""){  echo isset($HerPreference[0]->Diet) ? $HerPreference[0]->Diet : "Not Specified"; }else{echo "Not Specified";} ?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if ($PartnerPreference[0]->Diet != "" && $HerPreference[0]->Diet != "") {
					if ($PartnerPreference[0]->Diet == $HerPreference[0]->Diet) {
                        ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php }}else{ ?> 
					<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Smoke</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php echo ($HerPreference[0]->Smoke == 1) ? 'Smokes' : "Doesn't smoke"; ?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if ($PartnerPreference[0]->Smoke != "" && $HerPreference[0]->Smoke != "") {
					if ($PartnerPreference[0]->Smoke == $HerPreference[0]->Smoke) {
                        ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                     <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Drink</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php echo ($HerPreference[0]->Drink == 1) ? 'Drinks' : "Doesn't Drink"; ?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if ($PartnerPreference[0]->Drink != "" && $HerPreference[0]->Drink != ""){
					if ($PartnerPreference[0]->Drink == $HerPreference[0]->Drink) {
                         ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?> 
						<p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php } ?>
                </div>
            </div>
            <div style="border-bottom:2px solid #F3F2F1;margin-bottom:10px;">
                <div style="display:inline-block;width:540px;">
                    <p style="text-align:left;margin-bottom:0px;">Body type</p>

                    <p style="text-align:left;color:black;margin-bottom:2px;"><?php echo isset($HerPreference[0]->BodyType) ? $HerPreference[0]->BodyType : "Not Specified"; ?></p>
                </div>
                <div style="display:inline-block;vertical-align:top;">
                    <?php if ($PartnerPreference[0]->BodyType != "" && $HerPreference[0]->BodyType != ""){
					if ($PartnerPreference[0]->BodyType == $HerPreference[0]->BodyType) {
                       ?>
                        <p style="text-align:center;color:green;font-weight:bold;">
                            Yes
                        </p>
                    <?php } else { ?>
                        <p style="text-align:center;color:red;font-weight:bold;">
                            No
                        </p>
                    <?php } }else{ ?>
						 <p style="text-align:center;color:red;font-weight:bold;">Not Specified</p>
					<?php }
                     ?>
                </div>
            </div>

        </div>

        <br/>
        <br/>

    </div>
<?php } elseif($HerPreference == "") { ?>
    <div>
        Partner Preferences not Specified by this user
    </div>
<?php }elseif($PartnerPreference == ""){ ?>
 <div>
        You have not provided your partner preference. please fill your partner details <a href="<?php echo WEB_URL; ?>home/edit_profile">click here</a>
 </div>
 <?php }else{} ?>
</div>
<div class="tab-pane fade" id="tab3primary">Primary 3</div>
<div class="tab-pane fade" id="tab4primary">Primary 4</div>
<div class="tab-pane fade" id="tab5primary">Primary 5</div>
</div>
</div>
</div>
</div>
<!-- Previous Code -->


</div>
<!--row-->
</div>
<!--End: User Details-->
<!--  Sidebar  -->
<div class="col-md-3">
    <div class="thumbnail" style="text-align: center">
        <a href="#" rel="lightbox[murals]"><img src="<?php echo $pimg; ?>" width="200px" height="auto" class="center img-thumbnail"></a>

        <div class="caption">
            <h4><?php echo ucfirst($PartnerDetails[0]['Name']); ?></h4>

            <p>In order to request for a picture. Please click below:</p>
			<a href="<?php echo WEB_URL;?>home/album/<?php echo $PartnerDetails[0]['User_Id'];?>" style="margin-bottom:10px;" class="btn btn-success btn-sm">View Album</a>
			
            <div class="container" style="text-align: center">
                <a href="#" class="btn btn-primary btn-sm" role="button"><span
                        class="glyphicon glyphicon-eye-open"></span></a>
               <a href="#MailModel"  data-toggle="modal" class="btn btn-warning btn-sm" role="button"><span
                        class="glyphicon glyphicon-comment"></span></a>
                <a href="#" class="btn btn-info btn-sm" role="button"><span class="glyphicon glyphicon-envelope"></span></a>
                <a href="#" class="btn btn-danger btn-sm" role="button"><span
                        class="glyphicon glyphicon-star"></span></a>
            </div>
        </div>
    </div>
    <!-- Gallery Section-->
    <!-- Gallery Section-->
</div>
<!--End Sidebar-->
</div>
<!--End: User Profile Content-->
</div><!--  Container Section for User profile  -->
<!--Footer Section-->
<div>&nbsp;</div>
<!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Request to <?=$PartnerDetails[0]['Name'];?></h4>
                </div>
				<form action="<?php echo WEB_URL; ?>inbox/compose" name="SendEmial" id="SendEmial" method="post">
                <div class="modal-body">
                    <p>Add a message</p>
					
					<input type="hidden" name="MailType_Id" value="2">
					<input type="hidden" name="User_IdTo" value="<?=$PartnerDetails[0]['User_Id'];?>">
					<input type="hidden" name="User_IdFrom" value="<?=$SelfDetails[0]->User_Id;?>">
                    <textarea style="margin: 0px; width: 542px; height: 110px;" name="message">Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: <?=$SelfDetails[0]->MobileNo;?> or through email: <?=$SelfDetails[0]->Email;?>. Warm Regards, <?=$SelfDetails[0]->Name;?></textarea>
					
			  </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="submit">
                </div>
				</form>
            </div>
        </div>
    </div>
	
	<div id="MailModel" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send an Email to <?=$PartnerDetails[0]['Name'];?></h4>
                </div>
				<form action="<?php echo WEB_URL; ?>inbox/compose" name="SendEmial" id="SendEmial" method="post">
                <div class="modal-body">
                    <p>Add a message</p>
					
					<input type="hidden" name="MailType_Id" value="1">
					<input type="hidden" name="User_IdTo" value="<?=$PartnerDetails[0]['User_Id'];?>">
					<input type="hidden" name="User_IdFrom" value="<?=$SelfDetails[0]->User_Id;?>">
                    <textarea style="margin: 0px; width: 542px; height: 110px;" name="message">Hi, I have liked your profile and believe it to be good Match. If you like my profile too, kindly accept this Invitation. We can then pursue this further on ph: <?=$SelfDetails[0]->MobileNo;?> or through email: <?=$SelfDetails[0]->Email;?>. Warm Regards, <?=$SelfDetails[0]->Name;?></textarea>
					
			  </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="submit">
                </div>
				</form>
            </div>
        </div>
    </div>

<?php $this->load->view("include/footer"); ?>
<script>
function Connection(type){
	var partner_id = $("#partner_id").val();
	var user_id = $("#user_id").val();
	var notificationType = type;
	$.ajax({
		url: "<?php echo WEB_URL; ?>home/AddNotification",
		data: {partner_id : partner_id,user_id:user_id,notificationType:notificationType},
		type: "POST",
		success: function(data) {
			$("#ConnectionType").css("display","none");
			$("#ConnectionResult").html(data);
		}
	  });
}
</script>