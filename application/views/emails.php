<?php $this->load->view("include/header"); 
$id = $this->session->userdata('User_Id'); 
$UserDetails = $this->action_model->getpart_table_deatils("userprofile","User_Id",$id);
?>
<div class="container">
	<div class="container breadcrub">
	    
		<div class="brlines"></div>
	</div>	

	<!-- CONTENT -->
	<div class="container">
	<!--  Contact Us Field  -->
<!--  Contact Us Field  -->
		<div class="container pagecontainer offset-0">	

			<!-- SLIDER -->
			<div class="col-md-8 pagecontainer2 offset-0">
				<div class="cstyle10"></div>
		
				<ul class="nav nav-tabs" id="myTab">
					<li onclick="mySelectUpdate()" class="index match_padding active col-md-4"><a data-toggle="tab" href="#index"><span class="rates"></span><span class="hidetext">Inbox</span>&nbsp;</a></li>
					<li onclick="mySelectUpdate()" class="accepted match_padding col-md-4"><a data-toggle="tab" href="#accepted"><span class="preferences"></span><span class="hidetext">Accepted</span>&nbsp;</a></li>
					<li onclick="loadScript()" class="sent match_padding col-md-4"><a data-toggle="tab" href="#sent"><span class="maps"></span><span class="hidetext">Sent</span>&nbsp;</a></li>
				</ul>			
				<div class="tab-content4 inboxlist">
					<!-- TAB 1 -->	   	
					<div id="index" class="tab-pane fade active in">
						<?php $emails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'),1);
							  $Iemails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'),2);
							  $Remails = $this->email_model->GetInboxDetails($this->session->userdata('User_Id'),3);							  
						?>
						<ul class="col-md-3">
							<li class="active"><a data-toggle="tab" href="#emails_inbox">Emails <span class="badge indent0"><?php if($emails == 0){ echo 0;}else{ echo count($emails);}?></span></a></li>
							<li><a data-toggle="tab" href="#invitation_inbox">Invitations <span class="badge indent0"><?php if($Iemails == 0){ echo 0;}else{ echo count($Iemails);}?></span></a></li>
							<li><a data-toggle="tab" href="#requests_inbox">Requests </a><span class="badge indent0"><?php if($Remails == 0){ echo 0;}else{ echo count($Remails);}?></span></li>
						</ul>
						<div class="tab-content4 col-md-9" style="padding:20px 5px 0px 0px;">
							<div id="emails_inbox" class="tab-pane fade active in">
							<?php if($emails == 0){ ?>
								<h4 class="opensans dark bold margtop1 lh5 text-center">No Emails from your match</h4>
								<p class="padding20title">
								</p>
							  <?php }else{
								for($i=0;$i<count($emails);$i++){
								
								$FromUser =  $this->email_model->GetUserDetails($emails[$i]->User_IdFrom);
								
								  if($emails[$i]->MailRead == 0){$style='background: beige';}else{$style='';}
							  ?>
									<div>
										<input type="checkbox" id="selecctall" />
										<a class="btn-search5" href="" class="MailboxAction" id="delete_<?=$emails[$i]->Mail_Id;?>_<?=$this->session->userdata('User_Id');?>_1">Delete</a>&nbsp;&nbsp;
										<a class="btn-search5" href="" class="MailboxAction" id="read_<?=$emails[$i]->Mail_Id;?>_<?=$this->session->userdata('User_Id');?>_1">Mark read</a>&nbsp;&nbsp;
										<a class="btn-search5" href="" class="MailboxAction" id="unread_<?=$emails[$i]->Mail_Id;?>_<?=$this->session->userdata('User_Id');?>_1">Mark unread</a>
									</div>
								 <div class="line5"></div>
								 <div class="blog-post" style="<?=$style;?>">
									 <div class="fl thumb_60" id="BSH51593291_default_photo">
										<img src="" border="0" oncontextmenu="return false;" ondragstart="return false;" ondragenter="return false;" ondragover="return false;" ondrop="return false;" onmousedown="return false;">
									</div>

									<span class="blog-post-title"><?=$FromUser[0]->Name;?></span>
									<p class="blog-post-meta"><?=$emails[$i]->Date;?></p>
									<h5><?php echo $emails[$i]->Subject; ?></h5>
									<p class="message"><?=$emails[$i]->Message;?></p>
									<div>
										<input class="checkbox1" type="checkbox" name="check[]" value="item1" />
										<a href="" class="MailboxAction" id="delete_<?=$emails[$i]->Mail_Id;?>_<?=$this->session->userdata('User_Id');?>_1">Delete</a>&nbsp;&nbsp;
										<a href="" class="MailboxAction" id="read_<?=$emails[$i]->Mail_Id;?>_<?=$this->session->userdata('User_Id');?>_1">Mark read</a>&nbsp;&nbsp;
										<a href="" class="MailboxAction" id="unread_<?=$emails[$i]->Mail_Id;?>_<?=$this->session->userdata('User_Id');?>_1">Mark unread</a>
									</div>
								  </div>
								
								<div class="line3"></div>	
							  <?php }
							  } ?>
						
							</div>
							<div id="invitation_inbox" class="tab-pane fade">
							<?php if($Iemails == 0){ ?>
								<h4 class="opensans dark bold margtop1 lh5 text-center">No Invitation emails</h4>
								<p class="padding20title">
								</p>
							  <?php }else{
								for($i=0;$i<count($Iemails);$i++){
								
								$FromUser =  $this->email_model->GetUserDetails($Iemails[$i]->User_IdTo);
								  if($Iemails[$i]->MailRead == 0){$style='background: beige;';}else{$style='';}
							  ?>
								 
								 <div class="blog-post" style="<?=$style;?>">
									 <div class="fl thumb_60" id="BSH51593291_default_photo">
										<img src="" border="0" oncontextmenu="return false;" ondragstart="return false;" ondragenter="return false;" ondragover="return false;" ondrop="return false;" onmousedown="return false;">
									</div>

									<span class="blog-post-title col-md-4"><?=$FromUser[0]->Name;?></span>
									<p class="blog-post-meta"><?=$Iemails[$i]->Date;?></p>
									<h5><?php echo $Iemails[$i]->Subject; ?></h5>
									<p><?=$Iemails[$i]->Message;?></p>
								   
								  </div>
								
								<div class="line4"></div>	
							  <?php }
							  } ?>
							</div>
							<div id="requests_inbox" class="tab-pane fade">
							<?php if($Remails == 0){ ?>
								<h4 class="opensans dark bold margtop1 lh5 text-center">No request emails</h4>
								<p class="padding20title">
								</p>
							  <?php }else{
								for($i=0;$i<count($Remails);$i++){
								
								$FromUser =  $this->email_model->GetUserDetails($Remails[$i]->User_IdTo);
								  if($Remails[$i]->Read == 0){$style='background: beige';}else{$style='';}
							  ?>
								 
								 <div class="blog-post" style="<?=$style;?>">
									 <div class="fl thumb_60" id="BSH51593291_default_photo">
										<img src="" border="0" oncontextmenu="return false;" ondragstart="return false;" ondragenter="return false;" ondragover="return false;" ondrop="return false;" onmousedown="return false;">
									</div>

									<span class="blog-post-title"><?=$FromUser[0]->Name;?></span>
									<p class="blog-post-meta"><?=$Remails[$i]->Date;?></p>
									<h5><?php echo $Remails[$i]->Subject; ?></h5>
									<p><?=$Remails[$i]->Message;?></p>
								   
								  </div>
								
								<div class="line4"></div>	
							  <?php }
							  } ?>
							</div>
						</div>
					</div>
					<!-- TAB 2 -->
					
					
					<!-- TAB 3 -->					
					<div id="accepted" class="tab-pane fade">
						<ul class="col-md-3">
							<li class="active"><a href="">Members <span class="badge indent0">14</span></a></li>
							<li><a href="">Requests</a></li>
						</ul>
						<div class="tab-content4 col-md-9" style="padding:20px 5px 0px 0px;">
						<h4 class="opensans dark bold margtop1 lh5 text-center">Emails from your match</h4>
						
						<div class="line3"></div>	
					</div>						
					</div>
					
					<!-- TAB 4 -->										
					<div id="sent" class="tab-pane fade ">
					<?php $sent = $this->email_model->GetSentDetails($this->session->userdata('User_Id'),1);
						 $Isent = $this->email_model->GetSentDetails($this->session->userdata('User_Id'),2);
						 $Rsent = $this->email_model->GetSentDetails($this->session->userdata('User_Id'),3);
					?>	
						<ul class="col-md-3">
							<li class="active"><a data-toggle="tab" href="#emails_sent">Emails <span class="badge indent0"><?php if($sent == 0){ echo 0;}else{ echo count($sent);}?></span></a></li>
							<li><a data-toggle="tab" href="#invitation_sent">Invitations <span class="badge indent0"><?php if($Isent == 0){ echo 0;}else{ echo count($Isent);}?></span></a></li>
							<li><a data-toggle="tab" href="#requests_sent">Requests </a><span class="badge indent0"><?php if($Rsent == 0){ echo 0;}else{ echo count($Rsent);}?></span></li>
						</ul>
						<div class="tab-content4 col-md-9" style="padding:20px 5px 0px 0px;">
							<div id="emails_sent" class="tab-pane fade active in">
							<?php if($sent == 0){ ?>
								<h4 class="opensans dark bold margtop1 lh5 text-center">No sent emails</h4>
								<p class="padding20title">
								</p>
							  <?php }else{
								for($i=0;$i<count($sent);$i++){
								
								$FromUser =  $this->email_model->GetUserDetails($sent[$i]->User_IdTo);
								  if($sent[$i]->MailRead == 0){$style='background: beige';}else{$style='';}
							  ?>
								 
								 <div class="blog-post" style="<?=$style;?>">
									 <div class="fl thumb_60" id="BSH51593291_default_photo">
										<img src="" border="0" oncontextmenu="return false;" ondragstart="return false;" ondragenter="return false;" ondragover="return false;" ondrop="return false;" onmousedown="return false;">
									</div>

									<span class="blog-post-title"><?php echo isset($FromUser[0]->Name) ? $FromUser[0]->Name : "This user account is deleted";?></span>
									<p class="blog-post-meta"><?=$sent[$i]->Date;?></p>
									<h5><?php echo $sent[$i]->Subject; ?></h5>
									<p><?=$sent[$i]->Message;?></p>
								   
								  </div>
								
								<div class="line4"></div>	
							  <?php }
							  } ?>
							</div>
							<div id="invitation_sent" class="tab-pane fade">
							<?php if($Isent == 0){ ?>
								<h4 class="opensans dark bold margtop1 lh5 text-center">No Invitation sent emails</h4>
								<p class="padding20title">
								</p>
							  <?php }else{
								for($i=0;$i<count($Isent);$i++){
								
								$FromUser =  $this->email_model->GetUserDetails($Isent[$i]->User_IdTo);
								  if($Isent[$i]->MailRead == 0){$style='background: beige';}else{$style='';}
							  ?>
								 
								 <div class="blog-post" style="<?=$style;?>">
									 <div class="fl thumb_60" id="BSH51593291_default_photo">
										<img src="" border="0" oncontextmenu="return false;" ondragstart="return false;" ondragenter="return false;" ondragover="return false;" ondrop="return false;" onmousedown="return false;">
									</div>

									<span class="blog-post-title"><?php echo isset($FromUser[0]->Name) ? $FromUser[0]->Name : "This user account is deleted";?></span>
									<p class="blog-post-meta"><?=$Isent[$i]->Date;?></p>
									<h5><?php echo $Isent[$i]->Subject; ?></h5>
									<p><?=$Isent[$i]->Message;?></p>
								   
								  </div>
								
								<div class="line4"></div>	
							  <?php }
							  } ?>
							</div>
							<div id="requests_sent" class="tab-pane fade">
							<?php if($Rsent == 0){ ?>
								<h4 class="opensans dark bold margtop1 lh5 text-center">No sent requests</h4>
								<p class="padding20title">
								</p>
							  <?php }else{
								for($i=0;$i<count($Rsent);$i++){
								
								$FromUser =  $this->email_model->GetUserDetails($Rsent[$i]->User_IdTo);
								  if($Rsent[$i]->Read == 0){$style='background: beige';}else{$style='';}
							  ?>
								 
								 <div class="blog-post" style="<?=$style;?>">
									 <div class="fl thumb_60" id="BSH51593291_default_photo">
										<img src="" border="0" oncontextmenu="return false;" ondragstart="return false;" ondragenter="return false;" ondragover="return false;" ondrop="return false;" onmousedown="return false;">
									</div>

									<span class="blog-post-title"><?php echo isset($FromUser[0]->Name) ? $FromUser[0]->Name : "This user account is deleted";?></span>
									<p class="blog-post-meta"><?=$Rsent[$i]->Date;?></p>
									<h5><?php echo $Rsent[$i]->Subject; ?></h5>
									<p><?=$Rsent[$i]->Message;?></p>
								   
								  </div>
								
								<div class="line4"></div>	
							  <?php }
							  } ?>
							</div>
						</div>
					</div>		
					
					<!-- TAB 6 -->						
				</div>
			</div>
			<!-- END OF SLIDER -->			
			<!-- RIGHT INFO -->
			<?php $this->load->view("include/sidebar"); ?>
			<!-- END OF RIGHT INFO -->
			
		</div>
		<!-- END OF container-->
		
		
		
		
	</div>


<?php $this->load->view("include/footer"); ?>
<script>
$(document).ready(function(){
	$(".MailboxAction").click(function(){
		var id = $(this).attr("id").split("_");
		var action = id[0];
		var mailId = id[1];
		var userid = id[2];
		var mailType = id[3];
		$.ajax({
		  url: "<?php echo WEB_URL; ?>inbox/ActionMailBox",
			data:{action : action,mailId:mailId,userid:userid,mailType:mailType},
			type:"post",
			beforeSend:function(){alert("success");},
			success:function(data){
				console.log(data);
			}
		 });
		
	});
});
</script>
<script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>