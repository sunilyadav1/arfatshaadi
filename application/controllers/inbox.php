<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inbox extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('email_model');
		$this->load->model('action_model');
		$this->load->database();
		$this->load->model('message_model');
	}
	public function emails()
	{
		if ($this->session->userdata('user_logged_in')) {
			
			$data['mailtype'] = 1;
			$data['type'] = "";
			
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
				if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			
			$User_Id = $this->session->userdata('User_Id');
			$Gender = $this->session->userdata('Gender');
		
			$data['result']=$this->action_model->preferred_match($User_Id,$Gender,'preferred_match');
			
			$this->load->view('emails',$data);
		}else{
			$this->load->view('index');
		}
	}
	public function ActionMailBox(){
		if($_POST['action'] == "delete"){
			if($_POST['mailType'] == 1){ $Deleted_fromuser=1;}else{$Deleted_fromuser=0;}
			if($_POST['mailType'] == 2){ $Deleted_Touser=1;}else{$Deleted_Touser=0;}
			$sql = "UPDATE `mailbox` SET Deleted_fromuser =".$Deleted_fromuser.", Deleted_Touser=".$Deleted_Touser." where Mail_Id=".$_POST['mailId']."";
			$query = $this->db->query($sql);
			echo 1;
		}
		if($_POST['action'] == "read"){
			$sql = "UPDATE `mailbox` SET MailRead = 1 where Mail_Id=".$_POST['mailId']."";
			$query = $this->db->query($sql);
		}
		if($_POST['action'] == "unread"){
			$sql = "UPDATE `mailbox` SET MailRead = 0 where Mail_Id=".$_POST['mailId']."";
			$query = $this->db->query($sql);
		}
	
	}
	public function sent()
	{
		if ($this->session->userdata('user_logged_in')) {
			$data['mailtype'] = 1;
			$this->load->view('sent',$data);
		}	
	}
	public function sendmail(){
		if ($this->session->userdata('user_logged_in')) {
			$data['type'] = 'sendmail';
			$this->load->view('inbox',$data);
		}
	}
	
	public function compose(){
		if ($this->session->userdata('user_logged_in')) {
			$data =array(
				'NotificationTo' => $this->input->post('User_IdTo'),
				'NotificationFrom' =>$this->input->post('User_IdFrom'),
				'Notification_Id' => 4,
				'date'=>date('Y-m-d'),
				
			);
			$this->db->insert("notification",$data);
			
			$data =array(
				'User_IdTo'=>$this->input->post('User_IdTo'),
				'User_IdFrom' 	=> $this->input->post('User_IdFrom'),
				'MailType_Id' 	=> $this->input->post('MailType_Id'),
				'Message' =>$this->input->post('message'),
				'MailRead' => 0,
				'Date' => date("Y-m-d H:i:s")
			);
			
			$this->action_model->add_record("mailbox" ,$data);
			redirect('home/profile/'.$this->input->post('User_IdTo'),'refresh');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */