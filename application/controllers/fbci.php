<?php 
/* -----------------------------------------------------------------------------------------
   IdiotMinds - http://idiotminds.com
   -----------------------------------------------------------------------------------------
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include the facebook.php from libraries directory
require_once APPPATH.'libraries/facebook/facebook.php';

class Fbci extends CI_Controller {

   public function __construct(){
	    parent::__construct();
	    $this->load->library('session');  //Load the Session 
		$this->config->load('facebook'); //Load the facebook.php file which is located in config directory
		$this->load->model('action_model');
		$this->load->database(); 
    }
	public function index()
	{
	  $this->load->view('main'); //load the main.php file for view
	}
	function fblogin(){
		if(isset($_POST['email']) && $_POST['email'] !=""){
		
			$query = $this->db->query("select * from userprofile where Email='".$_POST['email']."' and fb_login=1");
			$result = $query->result();
			if(isset($_POST['id'])){$img = 'https://graph.facebook.com/'. $_POST['id'] .'/picture';}else{$img="";}
			if(isset($_POST['first_name'])){$fname = $_POST['first_name'];}else{$fname="";}
			if(isset($_POST['last_name'])){$lname = $_POST['last_name'];}else{$lname="";}
			if($_POST['gender'] = "female"){ $gender = 2; }else{ $gender =1 ;} 
			if(isset($_POST['hometown']['name'])){
				$place = explode(',',$_POST['hometown']['name']);
				$city = $place[0];
				$country = $place[1];
			}else{
				$city = ""; $country ="";
			}
			if(isset($_POST['birthday'])){
				$dob = explode('/', $_POST['birthday']); 
				$DOB = $dob[2].'-'.$dob[0].'-'.$dob[1];
			}else{
				$DOB = "";
			}
			if($query->num_rows() ==''){
				$data =array(
				'Name' 		=> $fname,
				'LastName' 	=> $lname,
				'Email' 	=> $_POST['email'],
				'DOB'		=> $DOB,
				'Gender' 	=> $gender,
				'LivingIn' 	=> $country,
				'LivingCity' => $city,
				'ProfilePic' => $img,
				'fb_login'	 => 1
				);
				$where = "";
				$id=0;
				$this->action_model->add_record('userprofile',$data,$id,$where);
				$id =  $this->db->insert_id();
			}else{
				$data =array(
				'Name' 		=> $fname,
				'LastName' 	=> $lname,
				'Email' 	=> $_POST['email'],
				'DOB'		=> $DOB,
				'Gender' 	=> $gender,
				'LivingIn' 	=> $country,
				'LivingCity' => $city,
				'ProfilePic' => $img
				);
				$id= $result[0]->User_Id;
				$where = "User_Id = $id";
				$this->action_model->add_record('userprofile',$data,$id,$where);
			}
			$sessionUserInfo = array( 
					'Gender'	=> $gender,
					'User_Id'	=> $id,
					'Email'		=> $_POST['email'],
					'Name'      => $fname,
					'LastName' => $lname,
					'user_logged_in' 	=> TRUE,
					'fb_login' => TRUE
			);
					
					
			$this->session->set_userdata($sessionUserInfo);
			$User_Id = $this->session->userdata('User_Id');
			$this->action_model->tbl_users_online($User_Id);
			echo 1;
			
		}
	}
	
}

/* End of file fbci.php */
/* Location: ./application/controllers/fbci.php */