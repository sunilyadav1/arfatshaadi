<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
	  	$this->load->helper(array('url', 'form', 'date'));
	  	$this->load->library('session');
	  	$this->load->library('form_validation');
		$this->load->model('action_model');
		$this->load->model('email_model');
		$this->load->model('message_model');
		//$this->load->model('inbox_model');
	  	$this->load->database(); 
        $this->load->library("pagination");
		
	}
	
	function index(){
		if ($this->session->userdata('user_logged_in')) {
			$User_Id = $this->session->userdata('User_Id');
			$Gender = $this->session->userdata('Gender');
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					
					//$is_online = $this->db->get_where('tbl_users_online',array('userid'=>$online_friend->User_Id))->num_rows();
					if($is_online == 1)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('search_match',$data);
	
		}else{
			$this->load->view('index');
		}
			
	}
	
	
	function login(){
		if ($this->session->userdata('user_logged_in')) {
			$User_Id = $this->session->userdata('User_Id');
			$Gender = $this->session->userdata('Gender');
		
			$data['result']=$this->action_model->preferred_match($User_Id,$Gender,'preferred_match');
			$data['result'] =$result;
			$this->load->view('search_match',$data);
		}else{
			$this->load->view('login');
		}
	}
	
	public function forgot(){
		$this->load->view('forgot');
	}
	
	function check_user(){
		$email=$this->input->post('email');
		$pass=$this->input->post('password');
		
		$res = $this->action_model->login($email, $pass);
		
		if($res=='failure'){
			echo "failure";
			redirect('home/login?status=failure', 'refresh'); 
			
		}
		else{
		
				$UserDetails = $this->action_model->full_profile($res->User_Id);
				$UserSettings = $this->action_model->getpart_table_deatils("settings","User_Id",$res->User_Id);
				switch($UserSettings[0]->NameSetting) {
					case 1 :
						$name = $UserDetails[0]['Name'];
					break;
					
					case 2 :
						$name = $UserDetails[0]['LastName'];
					break;
					
					case 3 :
						$name = $UserDetails[0]['Name']." ".$UserDetails[0]['LastName'];
					break;
					
					case 4 :
						$name = $UserDetails[0]['User_Id'];
					break;
				}
				switch($UserSettings[0]->PhotoSetting) {
					case 10 :
						if ($this->session->userdata('fb_login')) { 
							$img = $UserDetails[0]['ProfilePic'];
						}else{
						
							if($UserDetails[0]['ProfilePic'] == ""){
								$img ="no-profile.gif";
							}else{
								$img = $UserDetails[0]['ProfilePic'];
							}
							
							$img = WEB_DIR."images/profiles/".$img;
						}	
					break;
					
					case 11 :
						$img ="no-profile.gif";
					break;
					
					default:
					break;
				}
			
			$sessionUserInfo = array( 
				'Religion'	=>	$res->Religion,
				'Gender'	 		=> $res->Gender,
				'User_Id'	 		=> $res->User_Id,
				'Email'		=> $res->Email,
				'Name'       => $res->Name,
				'LastName' => $res->LastName,
				'DisplayName' => $name,
				'ProfilePic' => $img,
				'user_logged_in' 	=> TRUE
				);
				
				
	
				$this->session->set_userdata($sessionUserInfo);
				$User_Id = $this->session->userdata('User_Id');
				$this->action_model->tbl_users_online($User_Id);
				echo "success";
				redirect('/', 'refresh'); 
				
		}
		
	}
	
	function check_userAjax(){
		$email=$this->input->post('email');
		$pass=$this->input->post('password');
		
		$res = $this->action_model->login($email, $pass);
		
		if($res == 'failure'){
			echo 0;
		}
		else{
				$UserDetails = $this->action_model->full_profile($res->User_Id);
				$UserSettings = $this->action_model->getpart_table_deatils("settings","User_Id",$res->User_Id);
				//print_r($UserSettings );exit;
				switch($UserSettings[0]->NameSetting) {
					case 1 :
						$name = $UserDetails[0]['Name'];
					break;
					
					case 2 :
						$name = $UserDetails[0]['LastName'];
					break;
					
					case 3 :
						$name = $UserDetails[0]['Name']." ".$UserDetails[0]['LastName'];
					break;
					
					case 4 :
						$name = $UserDetails[0]['User_Id'];
					break;
				}
				switch($UserSettings[0]->PhotoSetting) {
					case 10 :
						if ($this->session->userdata('fb_login')) { 
							$img = $UserDetails[0]['ProfilePic'];
						}else{
						
							if($UserDetails[0]['ProfilePic'] == ""){
								$img ="no-profile.gif";
							}else{
								$img = $UserDetails[0]['ProfilePic'];
							}
							
							$img = WEB_DIR."images/profiles/".$img;
						}	
					break;
					
					case 11 :
						$img ="no-profile.gif";
					break;
					
					default:
					break;
				}
				$sessionUserInfo = array( 
				'Religion'	=>	$res->Religion,
				'Gender'	 		=> $res->Gender,
				'User_Id'	 		=> $res->User_Id,
				'Email'		=> $res->Email,
				'Name'       => $res->Name,
				'LastName' => $res->LastName,
				'DisplayName' => $name,
				'ProfilePic' => $img,
				'user_logged_in' 	=> TRUE
				);
					
				$this->session->set_userdata($sessionUserInfo);
				$User_Id = $this->session->userdata('User_Id');
				$time = time();
				$sql ="update `userprofile` set TimeStamp = ".$time." where User_Id ='".$User_Id."'";
				$query = $this->db->query($sql);
				$this->action_model->tbl_users_online($User_Id);
				echo 1;
		}
		
	}
	
	function UpdateTimeStamp(){
		$User_Id = $this->session->userdata('User_Id');
		$time = time();
		$sql ="update `userprofile` set TimeStamp = ".$time." where User_Id ='".$User_Id."'";
		$query = $this->db->query($sql);
	}
	
	function logout(){
			// echo "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>";
			// echo "<script src='". WEB_DIR . "assets/js/jquery.v2.0.3.js'></script>";
			// echo "<script src='". WEB_DIR . "assets/js/jquery-cookie.js'></script>";
			// echo "<script>$.cookie('rskesre', null);</script>";
			$User_Id = $this->session->userdata('User_Id');
			$this->action_model->tbl_users_offline($User_Id);
			$this->session->unset_userdata('sessionUserInfo');
			$this->session->sess_destroy();
			$this->session->unset_userdata('User_Id');
			redirect('/', 'refresh');
		
	}
	
	
	public function page2(){
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$id = $this->session->userdata('User_Id');
			$profile = $this->action_model->getpart_table_deatils('partnerprofile','User_Id',$id);
			if($profile != ''){
				redirect('home/ViewProfile', 'refresh'); 
			}else{
				$this->load->view('page2');
			}
		}
	}
	
	public function submit_form($id="0"){
			$cal 	= $this->input->post('DOB');
			$date=date('Y-m-d',strtotime($cal));
			
			$data =array(
				'MobileNo'=>$this->input->post('phone_no'),
				'Name' 	=> $this->input->post('first_name'),
				'LastName' 	=> $this->input->post('last_name'),
				'Email' 	=> $this->input->post('email'),
				'Password' =>$this->input->post('password'),
				//'Community' 	=> $this->input->post('community'),
				'DOB'=>$date,
				'Gender' 	=> $this->input->post('gender'),
				'ProfileFor' 	=> $this->input->post('profile_for'),
				'Country' 	=> $this->input->post('country'),
				'Religion' 	=> $this->input->post('religion'),
				'MotherTongue' 	=> $this->input->post('mother_tongue'),
				'LivingIn' 	=> $this->input->post('living_in'),
			    'WorkingAs' 	=> $this->input->post('working_as'),
				);
			if($id == 0){
				$where = "";
				
			}else{
				$where = "User_Id = $id";
			}
			$id = $this->action_model->add_record('userprofile',$data,$id,$where);
			if($id != ""){
				$data =array(
				'User_Id'=> $id,
				'NameSetting' 	=> 3,
				'PhoneSetting' 	=> 11,
				'PhotoSetting' 	=> 10,
				'HoroscopeSetting' =>12,
				
				);
				$this->action_model->add_record('settings',$data,0,$where);
			
				$UserDetails = $this->action_model->full_profile($id);
				$UserSettings = $this->action_model->getpart_table_deatils("settings","User_Id",$id);
				switch($UserSettings[0]->NameSetting) {
					case 1 :
						$name = $UserDetails[0]['Name'];
					break;
					
					case 2 :
						$name = $UserDetails[0]['LastName'];
					break;
					
					case 3 :
						$name = $UserDetails[0]['Name']." ".$UserDetails[0]['LastName'];
					break;
					
					case 4 :
						$name = $UserDetails[0]['User_Id'];
					break;
				}
				switch($UserSettings[0]->PhotoSetting) {
					case 10 :
						if ($this->session->userdata('fb_login')) { 
							$img = $UserDetails[0]['ProfilePic'];
						}else{
						
							if($UserDetails[0]['ProfilePic'] == ""){
								$img ="no-profile.gif";
							}else{
								$img = $UserDetails[0]['ProfilePic'];
							}
							
							$img = WEB_DIR."images/profiles/".$img;
						}	
					break;
					
					case 11 :
						$img ="no-profile.gif";
					break;
					
					default:
					break;
				}
		}
			$sessionUserInfo = array( 
					'User_Id'	=> $id,
					'Email'		=> $this->input->post('email'),
					'Name'       => $this->input->post('first_name'),
					'Gender'	=> $this->input->post('gender'),
					'LastName' => $this->input->post('last_name'),
					'DisplayName' => $name,
					'ProfilePic' => $img,
					'user_logged_in' 	=> TRUE
					);
					
			$this->session->set_userdata($sessionUserInfo);
			$data['success'] = "successfully registered please login to continue";
			
			redirect('home/page2', 'refresh');
		
		
	}
	
	public function EmailCheck(){
		$sql = "select * from userprofile where Email = '".$this->input->post('email')."' ";
		
		$query = $this->db->query($sql);
		echo $query->num_rows();
			
	}
	
	public function contactform(){
		$id = $this->session->userdata('User_Id');
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$data =array(
				'Country' =>$this->input->post('country'),
				'State'=>$this->input->post('state'),
				'City' 	=> $this->input->post('city'),
				'PinNo' 	=> $this->input->post('pincode'),
				'LivingIn' => $this->input->post('Livingcountry'),
				'LivingState' => $this->input->post('LivingState'),
				'LivingCity' => $this->input->post('LivingCity'),
			);
			$where = "User_Id = $id";
			$this->action_model->update_profile('userprofile',$data,$id,$where);
		}
	}
	public function familyform(){
		$id = $this->session->userdata('User_Id');
		
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$data =array(
				'User_id' => $id,
				'father_status' =>$this->input->post('father_status'),
				'mother_status'=>$this->input->post('mother_status'),
				'num_bro' 	=> $this->input->post('num_bro'),
				'num_sis' 	=> $this->input->post('num_sis'),
				'num_bro_married' => $this->input->post('num_bro_married'),
				'num_sis_married' => $this->input->post('num_sis_married'),
				'native' => $this->input->post('nativea'),
				'level' => $this->input->post('level'),
				'family_value' => $this->input->post('family_value'),
			);
			$profile = $this->action_model->getpart_table_deatils('familyinfo','User_id',$id);
			if($profile==''){
				$this->db->insert("familyinfo",$data);
			}
			else{
				$where="User_id = ".$profile[0]->User_id."";
				$this->db->update("familyinfo",$data,$where);
			}
			
			
		}
	}
	public function educationform(){
	$id = $this->session->userdata('User_Id');
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$data =array(
				'EducationLevel'=>$this->input->post('edu_level'),
				'EducationField' 	=> $this->input->post('edu_field'),
				'WorkingWith' 	=> $this->input->post('working_with'),
				'WorkingAs' => $this->input->post('working_as'),
				'AnualIncome' => $this->input->post('anual_income')
			);
			$where = "User_Id = $id";
			$this->action_model->update_profile('userprofile',$data,$id,$where);
		}
	}
	public function basicform(){
		$id = $this->session->userdata('User_Id');
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$data =array(
				'Community' 	=> $this->input->post('community'),
				'MaritalStatus'=>$this->input->post('marital_status'),
				'Height' 	=> $this->input->post('height'),
				'Weight' 	=> $this->input->post('weight'),
				'HIV' => $this->input->post('hiv'),
				'SkinTone' => $this->input->post('skin_tone'),
				'BodyType' => $this->input->post('body_type'),
				'Diet' => $this->input->post('diet'),
				'Smoke' => $this->input->post('smoke'),
				'Drink' => $this->input->post('drink'),
				'Disability'=> $this->input->post('Disability'),
				'About' => $this->input->post('about'),
			);
			$where = "User_Id = $id";
			$this->action_model->update_profile('userprofile',$data,$id,$where);
		}
	}
	
	// partner profile 
	public function partner_profile(){
		 if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$this->load->view('partner_profile');
			
		} 
		//$this->load->view('partner_profile');
	}
	public function partnerform(){
	
		$id = $this->session->userdata('User_Id');
		if (!$this->session->userdata('user_logged_in')) {
			redirect('', 'refresh');
		}else{
			$data =array(
				'User_Id'=>$id,
				'AgeFrom' 	=> $this->input->post('PageFrom'),
				'AgeTo' 	=> $this->input->post('PageTo'),
				'HeightFrom' 	=> $this->input->post('PheightFrom'),
				'HeightTo' 	=> $this->input->post('PheightTo'),
				'Disability' 	=> $this->input->post('Pdisability') ,
				'Smoke' 	=> $this->input->post('Psmoke'),	
				'Drink' 	=> $this->input->post('Pdrink'),
				'Dosham' 	=> $this->input->post('PDosham'),
					
				);
			if(isset($_POST['Pmarital_status']) && count($_POST['Pmarital_status']) >= 1){
				$data['MaritalStatus'] = implode(",", $this->input->post('Pmarital_status')); 
			}
			if( isset($_POST['Preligion']) && count($_POST['Preligion']) >= 1){
				$data['Religion'] = implode(",", $_POST['Preligion']); 
			}
			if(isset($_POST['Pmother_tongue']) && count($_POST['Pmother_tongue']) >= 1){
				$data['MotherTongue'] = implode(",", $_POST['Pmother_tongue']); 
			}
			if(isset($_POST['Pcommunity']) && count($_POST['Pcommunity']) >= 1){
				$data['Community'] = implode(",", $_POST['Pcommunity']); 
			}else{
				$data['community'] = "";
			}
			if(isset($_POST['Pprofile_created_by']) && count($_POST['Pprofile_created_by']) >= 1){
				$data['ProfileCreatedby'] = implode(",", $_POST['Pprofile_created_by']); 
			}
			if(isset($_POST['PcountryGrewUp']) && count($_POST['PcountryGrewUp']) >= 1){
				$data['CountryGrewUp'] = implode(",", $_POST['PcountryGrewUp']); 
			}
			if(isset($_POST['Pliving_in']) && count($_POST['Pliving_in']) >= 1){
				$data['CountryLivingIn'] = implode(",", $_POST['Pliving_in']); 
			}
			if(isset($_POST['PLivingState']) && count($_POST['PLivingState']) >= 1){
				$data['LivingState'] = implode(",", $_POST['PLivingState']); 
			}
			if(isset($_POST['PLivingCity']) && count($_POST['PLivingCity']) >= 1){
				$data['LivingCity'] = implode(",", $_POST['PLivingCity']); 
			}
			if(isset($_POST['Peducation']) && count($_POST['Peducation']) >= 1){
				$data['Education'] = implode(",", $_POST['Peducation']); 
			}
			if(isset($_POST['Pdiet']) && count($_POST['Pdiet']) >= 1){
				$data['Diet'] = implode(",",$_POST['Pdiet']); 
			}
			if(isset($_POST['Pbody_type']) && count($_POST['Pbody_type']) >= 1){
				$data['BodyType'] = implode(",", $_POST['Pbody_type']); 
			}
			if(isset($_POST['PannualIncome']) && count($_POST['PannualIncome']) >= 1){
				$data['AnualIncome'] = implode(",", $_POST['PannualIncome']); 
			}
			if(isset($_POST['PworkingAs']) && count($_POST['PworkingAs']) >= 1){
				$data['ProfessionArea'] = implode(",", $_POST['PworkingAs']); 
			}
			if(isset($_POST['Peducation_field']) && count($_POST['Peducation_field']) >= 1){
				$data['Education_Field'] = implode(",", $_POST['Peducation_field']); 
			}
			if(isset($_POST['PworkinWith']) && count($_POST['PworkinWith']) >= 1){
				$data['WorkingWith'] = implode(",", $_POST['PworkinWith']); 
			}
			$where = "";
			$this->action_model->add_record('partnerprofile',$data,0,$where);
			redirect('/', 'refresh');
		}
	}
	
	public function UpdatePartnerProfile(){

		$id= $this->session->userdata('User_Id');

		$data = $_POST;
		if(isset($_POST['MaritalStatus']) && count($_POST['MaritalStatus']) >= 1){
			$data['MaritalStatus'] = implode(",", $_POST['MaritalStatus']);
		}
		if(isset($_POST['Religion']) && count($_POST['Religion']) >= 1){
			$data['Religion'] = implode(",", $_POST['Religion']); 
		}
		if(isset($_POST['MotherTongue']) && count($_POST['MotherTongue']) >= 1){
			$data['MotherTongue'] = implode(",", $_POST['MotherTongue']); 
		}
		if(isset($_POST['Community']) && count($_POST['Community']) >= 1){
			$data['Community'] = implode(",", $_POST['Community']); 
		}
		if(isset($_POST['ProfileCreatedby']) && count($_POST['ProfileCreatedby']) >= 1){
			$data['ProfileCreatedby'] = implode(",", $_POST['ProfileCreatedby']); 
		}
		if(isset($_POST['CountryGrewUp']) && count($_POST['CountryGrewUp']) >= 1){
			$data['CountryGrewUp'] = implode(",", $_POST['CountryGrewUp']); 
		}
		if(isset($_POST['CountryLivingIn']) && count($_POST['CountryLivingIn']) >= 1){
			$data['CountryLivingIn'] = implode(",", $_POST['CountryLivingIn']); 
		}
		if(isset($_POST['LivingState']) && count($_POST['LivingState']) >= 1){
			$data['LivingState'] = implode(",", $_POST['LivingState']); 
		}
		if(isset($_POST['LivingCity']) && count($_POST['LivingCity']) >= 1){
			$data['LivingCity'] = implode(",", $_POST['LivingCity']); 
		}
		if(isset($_POST['Education']) && count($_POST['Education']) >= 1){
			$data['Education'] = implode(",", $_POST['Education']); 
		}
		if(isset($_POST['Diet']) && count($_POST['Diet']) >= 1){
			$data['Diet'] =implode(",",$_POST['Diet']); 
		}
		if(isset($_POST['BodyType']) && count($_POST['BodyType']) >= 1){
			$data['BodyType'] = implode(",", $_POST['BodyType']); 
		}
		if(isset($_POST['AnualIncome']) && count($_POST['AnualIncome']) >= 1){
			$data['AnualIncome'] = implode(",", $_POST['AnualIncome']); 
		}
		if(isset($_POST['ProfessionArea']) && count($_POST['ProfessionArea']) >= 1){
			$data['ProfessionArea'] = implode(",", $_POST['ProfessionArea']); 
		}
		if(isset($_POST['Education_Field']) && count($_POST['Education_Field']) >= 1){
			$data['Education_Field'] = implode(",", $_POST['Education_Field']); 
		}
		
		
		if($id != ""){

			$where = "User_Id = $id";

			$sql = "select * from partnerprofile where ".$where;

			$query = $this->db->query($sql);

			if($query->num_rows() == 0){

				$where = "";

				$data['User_Id'] = $id;

				$this->action_model->add_record('partnerprofile',$data,0,$where);

				redirect('home/ViewProfile', 'refresh'); 

			}else{

				$this->action_model->add_record('partnerprofile',$data,$id,$where);
				redirect('home/ViewProfile', 'refresh'); 

			}

		}else{

			$where = "";

			

			$this->action_model->add_record('partnerprofile',$data,0,$where);

			redirect('home/UserReport', 'refresh'); 

		}

	}


	public function UpdateProfile(){
	
		if (!$this->session->userdata('user_logged_in')) {
			redirect('', 'refresh');
		}else{
			$id = $this->session->userdata('User_Id');
			$data['basic'] =array(
				'ProfileFor' 	=> $this->input->post('profilefor'),
				'BodyType' 	=> $this->input->post('BodyType'),
				'Weight' => $this->input->post('Weight'),
				'SkinTone' => $this->input->post('SkinTone'),
				'Disability' 	=> $this->input->post('Disability'),
				'SubCommunity' => $this->input->post('SubCommunity'),
				'EducationLevel' => $this->input->post('EducationLevel'),
				'EducationField' 	=> $this->input->post('EducationField'),
				'WorkingWith' 	=> $this->input->post('WorkingWith'),
				'WorkingAs' 	=> $this->input->post('WorkingAs'),
				'AnualIncome' 	=> $this->input->post('AnualIncome'),
				'LivingIn' => $this->input->post('LivingIn'),
				'LivingState' => $this->input->post('LivingState'),
				'LivingCity' => $this->input->post('LivingCity'),
				'PinNo' =>$this->input->post('PinNo'),
				'Smoke' 	=> $this->input->post('Smoke'),	
				'Drink' 	=> $this->input->post('Drink'),
				'Diet' 	=> $this->input->post('Diet'),
				'About' => $this->input->post('About'),
				);
			$data['family'] = array(
				'father_status' =>  $this->input->post('father_status'),
				'mother_status' =>$this->input->post('mother_status'),
				'num_bro' =>$this->input->post('num_bro'),
				'num_sis' =>$this->input->post('num_sis'),
				'num_bro_married' =>$this->input->post('num_bro_married'),
				'num_sis_married' =>$this->input->post('num_sis_married'),
				'native' =>$this->input->post('nativea'),
				'level' =>$this->input->post('level'),
				'family_value' => $this->input->post('family_value'),
			);
			
			$where = "User_Id = $id";
			$basicupdate = $this->action_model->add_record('userprofile',$data['basic'],$id,$where);
			$sql = "select * from familyinfo where ".$where;

			$query = $this->db->query($sql);

			if($query->num_rows() == 0){
				$where = "";

				$data['family']['User_Id'] = $id;

				$this->action_model->add_record('familyinfo',$data['family'],0,$where);


			}else{

				$this->action_model->add_record('familyinfo',$data['family'],$id,$where);

			}
			
			$this->load->view("edit_profile");
		}
	}
	public function ViewProfile(){
		 if (!$this->session->userdata('user_logged_in')) {
				redirect('/', 'refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('view_profile',$data);
		}
	}
	public function UploadProfilePic(){
		$_FILES['file']['name'] = $_POST['img'];
		$id = $this->session->userdata('User_Id');
		$tmp_name = $_FILES['file']['name'];
		$name = $_FILES['file']['name'];
		$image_name  = $id.".png";
		$imagespath='images/profiles/'.$image_name;
		copy($tmp_name,$imagespath);
		
		$data =array('ProfilePic' => $image_name);
		$where = "User_Id = $id";
		$this->action_model->add_record('userprofile',$data,$id,$where);
		echo "Image Uploaded Successfully...!!";
			
	}
	// search  start 
	public function search(){
	 if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
		$this->load->view('search',$data);
		}
	}
	public function basic_search(){
					$this->load->view('basic_search');
	
	}
	public function about(){
		 if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('about');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('about',$data);
		}
	
	}
	public function contact(){
	
	$message = $this->input->post("message");
	if(isset($message) && $message != ""){
			$login_details="Dear Admin,<br/>

			

			Name:".$_POST['username']." <br/>

			Email:".$_POST['useremail']."<br/>

			
			Phone:".$_POST['userphone']."<br/>
			Message:".$_POST['message']."

			";

			

			$subject =  'Contact Info';

			$bound_text = "Contact Info";

			$bound = "--".$bound_text."\r\n";

			$bound_last = "--".$bound_text."--\r\n";

			$headers = "From: milanrishta.com <lvijetha90@gmail.com>\r\n";

			$headers .= "MIME-Version: 1.0\r\n"."Content-Type: multipart/mixed; boundary=\"$bound_text\"";

			$message .= "If you can see this MIME than your client doesn't accept MIME types!\r\n".$bound;

		   

			$message .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n" ."Content-Transfer-Encoding: 7bit\r\n\r\n"

			.$login_details."\r\n"

			.$bound;

			if(mail('lvijetha90@gmail.com', $subject, $message,$headers)){
				$data['status'] ="mail sent successfully";
			}else{
				$data['status'] =  "could not send your mail.";
			}
	
	}else{
		$data['status'] = "";
	}
	if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('contact',$data);
	}else{
		$id = $this->session->userdata;
		$data['user_data']=$id;
		$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
		$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
		if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
		$this->load->view('contact',$data);
	}
	
		
	
	}
	public function terms(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('terms');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('terms',$data);
		}
	
	}
	public function faq(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('faq');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('faq',$data);
		}
	
	}
	public function PaymentOptions(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('paymentOptions');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('PaymentOptions',$data);
		}
	
	}
	public function membershipsFAQ(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('membershipsFAQ');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('membershipsFAQ',$data);
		}
	
	}
	public function profileManagement(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('profileManagement');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('profileManagement',$data);
		}
	
	}
	public function milanrishtaAlerts(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view(milanrishtaAlerts);
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('milanrishtaAlerts',$data);
		}
	
	}
	public function photographs(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('photographs');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('photographs',$data);
		}
	
	}
	public function searchingProfiles(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('searchingProfiles');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('searchingProfiles',$data);
		}
	
	}
	public function contactingMembers(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('contactingMembers');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('contactingMembers',$data);
		}
	
	}
	public function blockMisuse(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('blockMisuse');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('blockMisuse',$data);
		}
	
	}
	public function technicalIssues(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('technicalIssues');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('technicalIssues',$data);
		}
	
	}
	public function privacySecurity(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('privacySecurity');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('privacySecurity',$data);
		}
	
	}
	public function milanrishtaChat(){
		if (!$this->session->userdata('user_logged_in')) {
			$this->load->view('milanrishtaChat');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view('milanrishtaChat',$data);
		}
	
	}
	
	
	public function search_match(){
			$User_Id = $this->session->userdata('User_Id');
			$partner_Id = $this->input->post('partner_Id');
			$ageT=$this->input->post('ageTo');
			$agef=$this->input->post('ageFrom');
			if($this->input->post('gender') != ""){ $gender= $this->input->post('gender'); }else{$gender =  $this->session->userdata("Gender");}
			
			$heightFrom=$this->input->post('heightFrom');
			$heightTo=$this->input->post('heightTo');
			
			if($this->input->post('CountryLivingIn') != ""){$living_in=implode(",",$this->input->post('CountryLivingIn'));}
			elseif($this->input->post('LivingCountry') != ""){$living_in=implode(",",$this->input->post('LivingCountry'));}
			else{$living_in="";}
			
			if($this->input->post('LivingState') != ""){$LivingState=implode(",",$this->input->post('LivingState'));}
			else{$LivingState="";}
			
			if($this->input->post('LivingCity') != ""){$LivingCity=implode(",",$this->input->post('LivingCity'));}
			else{$LivingCity="";}
			
			if($this->input->post('religion') != ""){$religion=implode(",",$this->input->post('religion'));}
			else{$religion="";}
			
			if($this->input->post('mother_tongue') != ""){ $mother_tongue=implode(",",$this->input->post('mother_tongue'));}
			else{$mother_tongue = "";}
			
			if($this->input->post('community') != ""){ $community= implode(",",$this->input->post('community')); }
			else{$community = "";} 
			
			if($this->input->post('marital_status') != ""){ $marital_status= "".implode('","', $this->input->post('marital_status')) . ""; }
			else{$marital_status = "";}
			
			if($this->input->post('working_as') != ""){ $working_as=implode(",",$this->input->post('working_as'));}
			else{$working_as="";}
			
			if($this->input->post('smoke') != ""){  $smoke 	= $this->input->post('smoke');	}
			else{$smoke="";}
			
			if($this->input->post('drink') != ""){ $drink 	= $this->input->post('drink');}
			else{$drink= "";}
			
			if($this->input->post('diet') != ""){ $diet 	= "".implode('","', $this->input->post('diet'))."";	}else{$diet ="";}
			if($this->input->post('body_type') != ""){$body_type 	= "".implode('","', $this->input->post('body_type'))."";}else{$body_type="";}
			
			if($this->input->post('skin_tone') != ""){$skin_tone 	= "".implode('","', $this->input->post('skin_tone')).""; }else{$skin_tone="";}
			
			$HIV 	= $this->input->post('HIV');	
			$Disability 	= $this->input->post('disability');	
			$Dosham 	= $this->input->post('Dosham');	
			

			$today=date("Y-m-d");
			$month=date("m");
			$day=date("d");
			$ageFrom=(($today-$agef)-1)."-$month-$day";
			$ageTo=(($today-$ageT)-1)."-$month-$day";

			
			
	
			$data['result'] = $this->action_model->search_prof($User_Id,$community,$gender,$religion,$mother_tongue,
			$ageFrom,$ageTo,$heightFrom,$heightTo,$living_in,$LivingState,$LivingCity,$marital_status,$working_as,$smoke,
			$drink,$diet,$body_type,$skin_tone,$HIV,$Disability,$partner_Id,$Dosham);
			$data['post'] = $_POST;
			$this->session->set_userdata($_POST);
			$this->load->view('SearchResults',$data);
				
	
	}
	//search end 
	
	public function match(){
		$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));


			//print_r(count($data['onlinefriends']));exit;
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					//print_r($online_friend->User_Id);exit;
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					//print_r($is_online);exit;
					if($is_online > 0)
					{
						//print_r($data['onlinefriends'][$key]->status_online);exit;
						$data['onlinefriends'][$key]->status_online = 'online'; 

					}
					else
					{

						$data['onlinefriends'][$key]->status_online = 'offline'; 

					}
				}
			}
		// print_r($data);exit;
		$this->load->view('matches',$data);
	}
	public function NewMatch(){
		
		$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			//print_r($data['onlinefriends']);exit;
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) >= 1){
				
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			
		$this->load->view('newmatch',$data);
	}
	public function BroaderMatch(){
		
		$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
		
		$this->load->view('broadermatch',$data);
	}
	// profile match starts
	

	public function Reverse_match(){
					
					$User_Id = $this->session->userdata('User_Id');
					$Gender = $this->session->userdata('Gender');
				
					$data['result']=$this->action_model->preferred_match($User_Id,$Gender,'Reverse_match');
					
					$this->load->view('search_match',$data);
	
	}
	
	public function maybe(){
				
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
		
		$this->load->view('maybe_match',$data);
	}
	
	
	public function save_maybe(){
	
			$data=array(

			'User_Id' => $this->session->userdata('User_Id'),
			'Profile_Id' => $this->input->post('User_Idd'),
			);
						
			$data=$this->action_model->insert_maybe('maybe_shortlisted',$data);
			
			if($data != ''){
			echo "success";}
			else{
			echo "empty";}
								
	}
	//view full profile
	
	public function full_profile($id){
		$User_Id= $this->session->userdata('User_Id');
		
		$data['result']=$this->action_model->full_profile($id);
		
		$this->load->view('full_profile',$data);
	
	}
	function profile(){
		$User_Id= $this->session->userdata('User_Id');
		$partner_id= $this->uri->segment(3);
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
		if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			
		  $recentvisitor = $this->action_model->getpart_table_deatils('recentvisitors','Visitor_Id',$User_Id);
		  if($recentvisitor == ""){
			$data1 =array(
				'User_id' => $partner_id,
				'Visitor_Id' =>$User_Id,
				'Visit_date'=>date('Y-m-d'),
				
			);
			$this->db->insert("recentvisitors",$data1);
		  }
		  $this->load->view('new_full_profile',$data);
		}
	}
	public function edit_profile(){
		if (!$this->session->userdata('user_logged_in')) {
			redirect('/', 'refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
		if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
				
			$this->load->view('edit_profile',$data);
		}
	
	}
	
	
	public function GetStates(){
		 $states = $this->action_model->getpart_table_deatils('statelist','Country_Id',$_POST['living_in']);
		
		if($states != ""){
			
			for($i=0;$i<count($states);$i++){
				$data = '<option value="'.$states[$i]->State_Id.'">'.$states[$i]->State_Name.'</option>';    
				 print_r( $data );
			 } 
			
		}
		
	}
	
	public function GetCities(){
		
		$cities = $this->action_model->getpart_table_deatils('citylist','State_Id',$_POST['living_in']);
		
		if($cities != ""){
			
			for($i=0;$i<count($cities);$i++){
				$data = '<option value="'.$cities[$i]->City_Id.'">'.$cities[$i]->City_Name.'</option>';    
				 print_r( $data );
			 } 
			
		}
	}
	
	public function GetCommunities(){
	
		$result = $this->action_model->getpart_table_deatils('communitylist','Religion_Id',$_POST['religion']);
		
		if($result != ""){
			
			for($i=0;$i<count($result);$i++){
				$data = '<option value="'.$result[$i]->Community_Id.'">'.$result[$i]->Community_Name.'</option>';    
				 print_r( $data );
			 } 
			
		}
	}
	public function AddNotification(){
		$data =array(
				'NotificationTo' => $this->input->post('partner_id'),
				'NotificationFrom' =>$this->input->post('user_id'),
				'Notification_Id' =>$this->input->post('notificationType'),
				'date'=>date('Y-m-d'),
				
			);
			$this->db->insert("notification",$data);
		switch($this->input->post('notificationType')){
			case '7':
			echo "This profile is added to maybe list";
			break;
			case '8' :
			echo "This profile is ignored";
			break;
			Default :
			echo "";
			break;
			
		}
	}
	
	public function GetList(){
		 $this->db->select('*');
		 $this->db->from($_POST['table']);
		 $this->db->where($_POST['column'],$_POST['value']);
		 $query = $this->db->get();
		 
		 $return = "";
		 if($query->num_rows() ==''){
				$return.= 'No data available';		
		}else{
			$query = $query->result_array();
			foreach($query as $value){
				$return.='<option value="'.$value[$_POST['display1']].'">'.$value[$_POST['display2']].'</option>';
			 } 
			
		}
		echo  $return;	
	}
	
	public function GetJsonList(){
		 $value= implode(",",json_decode($_POST['value']));
		 $sql = "select * from ".$_POST['table']." where ".$_POST['column']." in (".$value.")";
		
		 $query = $this->db->query($sql);
		 $return = "";
		 if($query->num_rows() ==''){
				$return.= 'No data available';		
		}else{
			$query = $query->result_array();
			foreach($query as $value){
				$return.='<option value="'.$value[$_POST['display1']].'">'.$value[$_POST['display2']].'</option>';
			 } 
			
		}
		echo  $return;	
	}
	
    /*Function for Privacy Setting Page*/
    public function privacySetting(){
        $data = array();
        if ($this->session->userdata('user_logged_in')) {
//            var_dump($this->session);
            $data['user_id'] = $this->session->userdata('User_Id');
            $data['full_name'] = $this->session->userdata('Name') . " " . $this->session->userdata('LastName');
            $data['gender'] = $this->session->userdata('Gender');
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
		if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
            $this->load->view('privacySetting',$data);
        }else{
            $this->load->view('index');
        }
    }/*end:privacySetting*/
	 public function AccountSettings(){
        $data = array();
        if ($this->session->userdata('user_logged_in')) {
            $data['user_id'] = $this->session->userdata('User_Id');
            $data['full_name'] = $this->session->userdata('Name') . " " . $this->session->userdata('LastName');
            $data['gender'] = $this->session->userdata('Gender');
            $this->load->view('AccountSettings',$data);
        }else{
            $this->load->view('index');
        }
    }
	public function EmailAlerts(){
        $data = array();
        if ($this->session->userdata('user_logged_in')) {
            $data['user_id'] = $this->session->userdata('User_Id');
            $data['full_name'] = $this->session->userdata('Name') . " " . $this->session->userdata('LastName');
            $data['gender'] = $this->session->userdata('Gender');
            $this->load->view('EmailAlerts',$data);
        }else{
            $this->load->view('index');
        }
    }
	
	public function ContactFilters(){
        $data = array();
        if ($this->session->userdata('user_logged_in')) {
            $data['user_id'] = $this->session->userdata('User_Id');
            $data['full_name'] = $this->session->userdata('Name') . " " . $this->session->userdata('LastName');
            $data['gender'] = $this->session->userdata('Gender');
            $this->load->view('ContactFilters',$data);
        }else{
            $this->load->view('index');
        }
    }
	
	public function ForgotPassword(){
	if(isset($_POST['forgot'])){
	
		$email=$_POST['email'];
	
		 $this->db->select('*');
		 $this->db->from('userprofile');
		 $this->db->where('Email',$email);
		 $query = $this->db->get();
		 if($query->num_rows() > 0){
		 $result = $query->result_array();
			$result = $result[0];
				$login_details="Dear Admin,<br/>
				Here is your Password:".$result['Password']."
				";
				$subject =  'Your Password';
				$bound_text = "Your Password";
				$bound = "--".$bound_text."\r\n";
				$bound_last = "--".$bound_text."--\r\n";
				$headers = "From: The Sender <info@bigperl.com>\r\n";
				$headers .= "MIME-Version: 1.0\r\n"
			  ."Content-Type: multipart/mixed; boundary=\"$bound_text\"";
				$message .= "If you can see this MIME than your client doesn't accept MIME types!\r\n"
				.$bound;
			   
				$message .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
				."Content-Transfer-Encoding: 7bit\r\n\r\n"
				.$login_details."\r\n"
				.$bound;
			
				mail($email, $subject, $message,$headers);
				$data['status'] = "Success";
				redirect('home/forgot', $data);
			
		}
		else{
			$data['status'] = "Failure";
			redirect('home/forgot', $data);
		}
	}
	}
	
	public function MatchFilter(){
		print_r($_POST);
	}
	
	public function AddSettings(){
	
		$id = $this->input->post('User_Id');
		$data =array('NameSetting' => $this->input->post('NameSetting'),
		'PhoneSetting' => $this->input->post('PhoneSetting'),
		'PhotoSetting'=>$this->input->post('PhotoSetting'),
		'HoroscopeSetting' =>$this->input->post('HoroscopeSetting'),
		'ProfilePrivacy' => $this->input->post('ProfilePrivacy'));
		$where = "User_Id = $id";
		$this->action_model->add_record('settings',$data,$id,$where);
	}
	public function AddContactFilters(){
		$id = $this->session->userdata('User_Id');
		$data =array(
		'AgeFrom' => $this->input->post('ageFrom'),
		'AgeTo' => $this->input->post('ageTo'),
		'HeightFrom'=>$this->input->post('heightFrom'),
		'HeightTo' =>$this->input->post('heightTo'),
		'MaritalStatus' => $this->input->post('marital_status'),
		'Religion' => $this->input->post('religion'),
		'Community' => $this->input->post('community'),
		'Smoke' => $this->input->post('smoke'),
		'Drink' => $this->input->post('drink'),
		'Disability' => $this->input->post('disability'),
		);
		
		$where = "User_Id = $id";
		$this->action_model->add_record('partnerprofile',$data,$id,$where);
	}
	
	public function AddAccountSettings(){
		$EmailId = $this->session->userdata('Email');
		$Id = $this->session->userdata('User_Id');
		$NewEmail = $this->input->post('Email');
		if(isset($NewEmail) && $NewEmail != ""){
			if($NewEmail != $EmailId){
				$sql = "select * from userprofile where Email = '".$NewEmail."' ";
				$query = $this->db->query($sql);
				if($query->num_rows() > 0){echo "This email already exists";}else{
					$sql = "UPDATE `userprofile` SET Email= '".$NewEmail."' where User_Id=".$Id;
					$query = $this->db->query($sql);
				}
			}else{
				$sql = "UPDATE `userprofile` SET Email= '".$NewEmail."',Password='".$this->input->post('Password')."',
				LivingIn ='".$this->input->post('Country')."',LivingState ='".$this->input->post('State')."',LivingCity ='".$this->input->post('City')."',
				PinNo ='".$this->input->post('Pincode')."'	where User_Id=".$Id;
				$query = $this->db->query($sql);
			}
		}
		
	}
	
	public function AddEmailAlerts(){
	
		$id= $_POST['id'];
		$data = $_POST;
		if($id != 0){
			$where = "User_Id = $id";
			$this->action_model->add_record('emailsalertssms',$data,$id,$where);
			redirect('home/EmailAlerts', 'refresh'); 
		}else{
			$where = "";
			
			$this->action_model->add_record('emailsalertssms',$data,$id,$where);
			redirect('home/EmailAlerts', 'refresh'); 
		}
	
	}
	public function PicUpload(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("PicUpload",$data);
		}
	}
	
	public function ImageUpload(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$p1 = $p2 = [];
			if (empty($_FILES['input-ru']['name'])) {
				echo '{}';
				return;
			}
			for ($i = 0; $i < count($_FILES['input-ru']['name']); $i++) {
			   $User_Id = $this->session->userdata('User_Id');

				$tmp_name = $_FILES['input-ru']['tmp_name'][$i];

				$name = $_FILES['input-ru']['name'][$i];

				$image_name  = rand().$_FILES['input-ru']['name'][$i];

				$imagespath='images/profiles/'.$image_name;

				copy($tmp_name,$imagespath);

				$data =array('User_Id'=> $User_Id,
				'ProfilePic' => $image_name);
				$where = "";
				$image = WEB_DIR.'images/profiles/'.$image_name;
				$p1[$i] = "<img src='".$image."' class='file-preview-image' width='260px'>";
				$p2[$i] = ['caption' => $image_name, 'width' => '120px'];
				$id = $this->action_model->add_record('profilepictures',$data,$id=0,$where);
				
			}
			echo json_encode([
				'initialPreview' => $p1, 
				'initialPreviewConfig' => $p2,   
				'append' => true // whether to append these configurations to initialPreview.
								 // if set to false it will overwrite initial preview
								 // if set to true it will append to initial preview
								 // if this propery not set or passed, it will default to true.
			 ]);
		}
	}
	public function ListProfilePic(){
		$User_Id = $this->session->userdata('User_Id');
		$p1 = $p2 = [];
		$sql = "select * from `profilepictures` where User_Id=".$User_Id." ";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$List=$query->result_array();
			
			for($i=0;$i<count($List);$i++){
				$image = WEB_DIR.'images/profiles/'.$List[$i]['ProfilePic'];
				$p1[$i] = "<img src='".$image."' class='file-preview-image' width='260px'>";
					
				$p2[$i] = ['caption' => $List[$i]['ProfilePic'], 'width' => '120px', 'url' => WEB_URL.'home/DeleteImage/'.$List[$i]['Id'], 'key' => $List[$i]['Id']];
							
			}
		
		}
	
		echo json_encode([
			'initialPreview' => $p1, 
			'initialPreviewConfig' => $p2,   
			'maxFileCount' => 4,
			'append' => true
		]); 
	}
	
	public function DeleteImage($id){
		$sql  = "select * from `profilepictures` where Id=".$id."";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$List=$query->result_array();
			$sql = "delete from `profilepictures` where Id=".$id."";
			$query = $this->db->query($sql);
			$filename =  WEB_DIR.'images/profiles/'.$List[0]['ProfilePic'];
		}
		if (file_exists($filename)) {
			unlink($filename);
		} 
		echo json_encode([
		
		]);
	}
	
	public function upgrade(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("upgrade",$data);
		}
	}
	
	public function PaymentOption(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("PaymentOption",$data);
		}
	}
	public function success(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$this->load->view("success");
		}
	}
	public function failure(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$this->load->view("failure");
		}
	}
	
	public function test(){
		$this->load->view("test");
	}
	
	public function album(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("album",$data);
		}
	}
	
	public function RecentVisitors(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("recentVisitors",$data);
		}
	}
	
	public function Ignored(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("Ignored",$data);
		}
	}
	
	public function ViewedProfiles(){
		if(!$this->session->userdata('user_logged_in')){
			redirect('/','refresh');
		}else{
			$id = $this->session->userdata;
			$data['user_data']=$id;
			$data['user_data']['User_Id']   =  $this->session->userdata('User_Id');
			$data['onlinefriends']=$this->message_model->getOnlineFriend($this->session->userdata('User_Id'));
			if(isset($data['onlinefriends']) && count($data['onlinefriends']) > 1){
				foreach($data['onlinefriends'] as $key =>$online_friend)
				{
					$is_online = $this->action_model->GetUserOnline($online_friend->User_Id);
					if($is_online > 0)
					{
						$data['onlinefriends'][$key]->status_online = 'online'; 
					}
					else
					{
						$data['onlinefriends'][$key]->status_online = 'offline'; 
					}
				}
			}
			$this->load->view("ViewedProfiles",$data);
		}
	}
	
	public function SearchFilter(){
		
			$post = $this->session->all_userdata('marital_status');
			
			

			$User_Id = $this->session->userdata('User_Id');
			$PartnerDetails =  $this->action_model->full_partnerprofile($User_Id); 
			
			if(isset($post['ageFrom']) && $post['ageFrom'] != ""){ $agef = $post['ageFrom']; }
			elseif(isset($PartnerDetails[0]['AgeTo']) && $PartnerDetails[0]['AgeTo'] != ""){ $agef = $PartnerDetails[0]['AgeTo'];}
			else{$agef = "";}
			
			if(isset($post['ageTo']) && $post['ageTo'] != ""){ $ageT = $post['ageTo']; }
			elseif(isset($PartnerDetails[0]['AgeFrom']) && $PartnerDetails[0]['AgeFrom'] != ""){ $ageT = $PartnerDetails[0]['AgeFrom'];}
			else{$ageT = "";}
			
			if($this->input->post('gender') != ""){ $gender= $this->input->post('gender'); }
			else{$gender =  $this->session->userdata("Gender");}
			
			if(isset($post['heightFrom']) && $post['heightFrom'] != ""){ $heightFrom = $post['heightFrom']; }
			elseif(isset($PartnerDetails[0]['HeightFrom']) && $PartnerDetails[0]['HeightFrom'] != ""){ $heightFrom = $PartnerDetails[0]['HeightFrom'];}
			else{$heightFrom = "";}
			
			if(isset($post['heightTo']) && $post['heightTo'] != ""){ $heightTo = $post['heightTo']; }
			elseif(isset($PartnerDetails[0]['HeightTo']) && $PartnerDetails[0]['HeightTo'] != ""){ $heightTo = $PartnerDetails[0]['HeightTo'];}
			else{$heightTo = "";}
			
			if(isset($_POST['CountryLivingIn']) && $_POST['CountryLivingIn'] != ""){ $living_in = implode(",",$_POST['CountryLivingIn']); }
			elseif(isset($post['CountryLiving']) && $post['CountryLiving'] != ""){ $living_in = implode(",",$post['CountryLiving']);}
			else{$living_in = "";}
			
			if(isset($_POST['LivingState']) && $_POST['LivingState'] != ""){ $LivingState = implode(",",$_POST['LivingState']); }
			elseif(isset($post['LivingState']) && $post['LivingState'] != ""){ $LivingState = implode(",",$post['LivingState']);}
			else{$LivingState = "";}
			
			if(isset($_POST['LivingCity']) && $_POST['LivingCity'] != ""){ $LivingCity = implode(",",$_POST['LivingState']); }
			elseif(isset($post['LivingCity']) && $post['LivingCity'] != ""){ $LivingCity = implode(",",$post['LivingCity']);}
			else{$LivingCity = "";}
			
			// if(isset($_POST['MaritalStatus']) && $_POST['MaritalStatus'] != ""){ $marital_status = "" . implode('","', $this->input->post('MaritalStatus')) . ""; }
			// elseif(isset($post['marital_status']) && $post['marital_status'] != ""){ $marital_status ="" . implode('","', $this->input->post('marital_status')) . "";}
			// else{$marital_status = "";}
			if(isset($_POST['MaritalStatus']) && $_POST['MaritalStatus'] != ""){ $marital_status = "" . implode('","', $this->input->post('MaritalStatus')) . ""; }
			elseif(isset($post['MaritalStatus']) && $post['MaritalStatus'] != ""){ $marital_status ="" . implode('","', $this->input->post('MaritalStatus')) . "";}
			else{$marital_status = "";}
			
			if(isset($_POST['Religion']) && $_POST['Religion'] != ""){ $religion = implode(",",$_POST['Religion']); }
			elseif(isset($post['religion']) && $post['religion'] != ""){ $religion = implode(",",$post['religion']);}
			else{$religion = "";}
			
			if(isset($_POST['Community']) && $_POST['Community'] != ""){ $community = implode(",",$_POST['Community']); }
			elseif(isset($post['community']) && $post['community'] != ""){ $community = implode(",",$post['community']);}
			else{$community = "";}
			
			if(isset($_POST['MotherTongue']) && $_POST['MotherTongue'] != ""){ $mother_tongue = implode(",",$_POST['MotherTongue']); }
			elseif(isset($post['mother_tongue']) && $post['mother_tongue'] != ""){ $mother_tongue = implode(",",$post['mother_tongue']);}
			else{$mother_tongue = "";}

			
			if(isset($_POST['ProfessionArea']) && $_POST['ProfessionArea'] != ""){ $working_as = implode(",",$_POST['ProfessionArea']); }
			elseif(isset($post['working_as']) && $post['working_as'] != ""){ $working_as = implode(",",$post['working_as']);}
			else{$working_as = "";}
			
			
			if($this->input->post('Diet') != ""){ $diet = "" . implode('","',$this->input->post('Diet'))."";	}
			elseif(isset($post['diet']) && $post['diet'] != ""){$diet = "" . implode('","',$post['diet'])."";}
			else{$diet ="";}
			
			if($this->input->post('CreatedBy') != ""){ $CreatedBy = "" . implode('","',$this->input->post('CreatedBy')) . "";}
			elseif(isset($post['CreatedBy']) && $post['CreatedBy'] != ""){$CreatedBy = "" . implode('","',$this->input->post('CreatedBy')) . "";}
			else{$CreatedBy ="";}
			
			if($this->input->post('Education') != ""){ $Education = implode(",",$this->input->post('Education'));	}
			elseif(isset($post['Education']) && $post['Education'] != ""){$Education = implode(",",$post['Education']);}
			else{$Education ="";}
			
			// if($this->input->post('AnualIncome') != ""){ $AnualIncome = "" . implode('","', $this->input->post('AnualIncome'))."";	}
			// elseif(isset($post['AnualIncome']) && $post['AnualIncome'] != ""){$AnualIncome = "".implode('","', $post['AnualIncome'])."";}
			// else{$AnualIncome ="";}
			if(isset($_POST['AnnualIncome']) && $_POST['AnnualIncome'] != ""){ $AnualIncome = "" . implode('","', $this->input->post('AnnualIncome')) . ""; }
			elseif(isset($post['AnnualIncome']) && $post['AnnualIncome'] != ""){ $AnualIncome ="" . implode('","', $this->input->post('AnnualIncome')) . "";}
			else{$AnualIncome = "";}

			
			
			if($this->input->post('Smoke') != ""){  $smoke 	= implode(",", $this->input->post('Smoke'));	}
			else{$smoke="";}

			
			
			if($this->input->post('Drink') != ""){ $drink = implode(",",$this->input->post('Drink'));}
			else{$drink= "";}
			
			if($this->input->post('body_type') != ""){$body_type 	= "".implode('","', $this->input->post('body_type'))."";}else{$body_type="";}
			
			if($this->input->post('skin_tone') != ""){$skin_tone 	= "".implode('","', $this->input->post('skin_tone')).""; }else{$skin_tone="";}
			
			
			$Disability 	= "";	
			$today=date("Y-m-d");
			$month=date("m");
			$day=date("d");
			$ageFrom=(($today-$agef)-1)."-$month-$day";
			$ageTo=(($today-$ageT)-1)."-$month-$day";
	
			$result = $this->action_model->FilterSearch($User_Id,$community,$gender,$religion,$mother_tongue,
			$ageFrom,$ageTo,$heightFrom,$heightTo,$living_in,$LivingState,$LivingCity,$marital_status,$working_as,$smoke,
			$drink,$diet,$body_type,$skin_tone,$Education,$Disability,$CreatedBy,$AnualIncome);

//print_r($result);exit;
			
			if($result == 'failure'){
				echo '<div class="alert alert-info">
							  No matches found !
							</div>';
			}else{
				//print_r($result);exit;
				foreach($result['result'] as $value){
					if ($this->session->userdata('fb_login')) { 
						$pimg = $value['ProfilePic'];
					}else{
						if($value['ProfilePic'] == ""){
							$ppic ="no-profile.gif";
						}else{
							$ppic = $value['ProfilePic'];
						}
						
						$pimg = WEB_DIR."images/profiles/".$ppic;
					}
					echo '<div class="padding20">
						<div class="col-md-2 text-center  offset-0">
							<a href="#"><img src="'.$pimg.'" alt="" class="fwimg img-thumbnail"></a>
						</div>
						<div class="col-md-10 offset-0">
							<div class="col-md-12">
								<div class="col-md-9">
									<h4 class="opensans dark bold margtop1 lh1">'.$value['Name'].'</h4>
									'.$value['Community_Name'].', '.$value['SubCommunity_Name'].', '.$value['Language_Name'].'
									<p class="hpadding20">'.$value['About'].'</p>
								</div>
								<div class="col-md-3">
									<p class="text-center"> Connect with her?</p>
									<a href="'.WEB_URL.'home/profile/'.$value['User_Id'].'" target="_blank" class=" mb-10 btn btn-block btn-success">Yes</a>
									<a href="'.WEB_URL.'home/profile/'.$value['User_Id'].'" target="_blank" class="btn btn-danger">No</a>
									<a href="'.WEB_URL.'home/profile/'.$value['User_Id'].'" target="_blank" class="btn btn-info"> Maybe</a>
									
								</div>									
							</div>									
						</div>
						<div class="clearfix"></div>
					</div>';
				}
			}						
	}
	
	public function hobbies(){
		$this->load->view("hobies");
	}
	
	public function GetImages(){
		$id = explode("_",$_REQUEST['id']);
		$sql = "select * from `profilepictures` where Id=".$id[1]." ";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$List=$query->result_array();
			
			$filename = WEB_DIR."/images/profiles/".$List[0]['ProfilePic'];
			$size = getimagesize($filename);
			if($size[0] > 590){
				
				echo "<img src='".$filename."' style='width:100%;'>";
			}else{
				echo "<img src='".$filename."'>";
			}
		}

	}
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */